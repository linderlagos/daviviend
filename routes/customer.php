<?php

/*
|--------------------------------------------------------------------------
| Card Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Card Process routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "web" and "auth" middleware group.
|
*/

Route::prefix('clientes')->group(function ()
{
	Route::get('/{id}', 'CustomerController@show')->name('show.customer')->middleware('block_users');
});