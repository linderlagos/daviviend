<?php

//    Auth::routes();
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('login', 'Auth\LoginController@login');
	Route::post('/2fa', function () {
//		return redirect(URL()->previous());
		return redirect()->route('home');
	})->name('2fa')->middleware('2fa');
	Route::post('logout', 'Auth\LoginController@logout')->name('logout');

	
	Route::get('/', function () {
		return view('welcome');
	});
	
	Auth::routes();
	
	
	Route::get('/home', 'HomeController@index')->name('home');
	
	Route::get('/autenticar', 'HomeController@autenticar')->name('autenticar');
	
	Route::post('/serial', 'HomeController@generarSerial')->name('serial');
	
	Route::post('/autenticarCodigo', 'HomeController@autenticarCodigo')->name('autenticarCodigo');

	Route::get('/atras', 'HomeController@atras')->name('atras');
