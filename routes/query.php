<?php

/*
|--------------------------------------------------------------------------
| Card Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Card Process routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "web" and "auth" middleware group.
|
*/

Route::prefix('buscar')->middleware(['role:' . env('ROL_GESTOR') . '|' . env('ROL_CIF') . '|' . env('ROL_FIRMAS')])->group(function ()
{
	Route::get('/todos-los-productos', 'QueryController@index')->name('query.index')->middleware('block_users');

	Route::get('/todos-los-productos/descargar-excel', 'QueryController@export')->name('query.export')->middleware('block_users');

//	Route::post('/todos-los-productos', 'QueryController@search')->name('search.query');
});