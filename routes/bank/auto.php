<?php

Route::namespace('Bank')->prefix('banco')->middleware('block_users')->group(function ()
{
    Route::namespace('Auto')->prefix('prestamo-de-auto')->group(function ()
    {
        Route::get('/', 'AutoController@index')->name('bank.auto.index');

        Route::post('/', 'AutoController@search')->name('bank.auto.search');

        Route::post('/iniciar/', 'AutoController@start')->name('bank.auto.start');

        Route::post('/nueva-cotizacion/', 'AutoController@new')->name('bank.auto.new');

	    Route::get('/solicitudes-en-tramite/', 'AutoController@query')->name('bank.auto.query');

        Route::post('/2', 'AutoController@second')->name('bank.auto.second');

        Route::post('/calculate', 'AutoController@calculate')->name('bank.auto.calculate');
        
        Route::post('/3', 'AutoController@third')->name('bank.auto.third');

        Route::post('/continuar/{id}/{correlative}/{response_motor}', 'AutoController@continue')->name('bank.auto.continue');

        Route::post('/4', 'AutoController@fourth')->name('bank.auto.fourth');

        Route::post('/5', 'AutoController@fifth')->name('bank.auto.fifth');

        Route::post('/recalculate', 'AutoController@recalculate')->name('bank.auto.recalculate');

        Route::post('/traditional', 'AutoController@traditional')->name('bank.auto.traditional');
    });
});