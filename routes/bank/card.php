<?php

/*
|--------------------------------------------------------------------------
| Card Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Card Process routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "web" and "auth" middleware group.
|
*/

Route::namespace('Bank')->prefix('banco')->middleware('block_users')->group(function ()
{
	Route::namespace('Card')->prefix('tarjeta-de-credito')->group(function ()
	{
		Route::get('/', 'CardController@index')->name('bank.card.index')->middleware(['role:' . env('ROL_GESTOR')]);

		Route::post('/2', 'CardController@second')->name('bank.card.second')->middleware(['role:' . env('ROL_GESTOR')]); 

		Route::post('/3', 'CardController@third')->name('bank.card.third')->middleware(['role:' . env('ROL_GESTOR')]);
	});
}); 
