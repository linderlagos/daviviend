<?php

/*
|--------------------------------------------------------------------------
| Card Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Card Process routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "web" and "auth" middleware group.
|
*/

Route::prefix('productos')->group(function ()
{
	Route::get('/enviar-por-correo/{product}', 'ProductController@send')->name('send.product.document')->middleware(['role:' . env('ROL_GESTOR')]);

	Route::get('/mostrar/{product}', 'ProductController@show')->name('show.product');

	Route::post('/subir-archivo/{product}', 'ProductController@upload')->name('upload.product.document')->middleware(['role:' . env('ROL_GESTOR') . '|' . env('ROL_CIF')]);

	Route::post('/actualizar-tipo-de-archivo/', 'ProductController@updateUploadType')->name('update.product.upload.type')->middleware(['role:' . env('ROL_CIF')]);

	Route::post('/actualizar-status-de-archivo/', 'ProductController@updateUploadAudit')->name('update.product.upload.audit')->middleware(['role:' . env('ROL_FIRMAS')]);

	Route::post('/actualizar-status/', 'ProductController@updateAudit')->name('update.product.audit')->middleware(['role:' . env('ROL_CIF')]);

	Route::post('/agregar-comentario/{product}/', 'ProductController@comment')->name('store.product.comment')->middleware(['role:' . env('ROL_CIF')]);

	Route::post('/resolver-comentario/{product}/{comment}', 'ProductController@solveComment')->name('solve.product.comment')->middleware(['role:' . env('ROL_GESTOR') . '|' . env('ROL_CIF')]);
});