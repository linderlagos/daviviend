<?php

/*
|--------------------------------------------------------------------------
| Test Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Test Process routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "web" and "auth" middleware group.
|
*/

Route::namespace('Test')->prefix('flujo-de-prueba')->group(function ()
{
	Route::get('/', 'TestController@index')->name('test.index')->middleware(['block_users', 'role:' . env('ROL_GESTOR')]);

	Route::post('/search', 'TestController@search')->name('test.search')->middleware(['role:' . env('ROL_GESTOR')]);

	Route::post('/2', 'TestController@second')->name('test.second')->middleware(['role:' . env('ROL_GESTOR')]);

	Route::post('/3', 'TestController@third')->name('test.third')->middleware(['role:' . env('ROL_GESTOR')]);

	Route::post('/4', 'TestController@fourth')->name('test.fourth')->middleware(['role:' . env('ROL_GESTOR')]);

	Route::post('/5', 'TestController@fifth')->name('test.fifth')->middleware(['role:' . env('ROL_GESTOR')]);

	Route::post('/6', 'TestController@sixth')->name('test.sixth')->middleware(['role:' . env('ROL_GESTOR')]);
});