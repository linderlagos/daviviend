<?php

namespace App\Jobs;

use App\Core\GetPreFilledData;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class CachePreFilledData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Artisaninweb\SoapWrapper\Exceptions\ServiceAlreadyExists
     */
    public function handle()
    {
        $callPreFilledData = new GetPreFilledData();

        $lists = $callPreFilledData->data();

        Storage::disk('local')->put('public/lists.txt', json_encode($lists));
    }
}
