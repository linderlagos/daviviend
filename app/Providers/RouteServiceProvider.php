<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        //$this->mapApiRoutes();

        $this->mapAuthRoutes();

        $this->mapWebRoutes();

        $this->mapCustomerRoutes();

	    $this->mapProductRoutes();

	    $this->mapQueryRoutes();

	    if(env('ENABLE_BANK_CARD'))
	    {
		    $this->mapCardRoutes();
	    }

        $this->mapAccountRoutes();

        $this->mapTestRoutes();

        $this->mapAutoRoutes();

        $this->mapSafeFamilyRoutes();

        $this->mapSecurityRoutes();

        //
    }

	/**
	 * Define the "web" routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @return void
	 */
	protected function mapAuthRoutes()
	{
		Route::middleware(['web', 'browser.block'])
			->namespace($this->namespace)
			->group(base_path('routes/auth.php'));
	}

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware(['web', 'auth', 'browser.block', '2fa'])
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

	/**
	 * Define the product routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @return void
	 */
	protected function mapProductRoutes()
	{
		Route::middleware(['web', 'auth', 'browser.block', '2fa'])
			->namespace($this->namespace)
			->group(base_path('routes/product.php'));
	}

	/**
	 * Define the query routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @return void
	 */
	protected function mapQueryRoutes()
	{
		Route::middleware(['web', 'auth', 'browser.block', '2fa'])
			->namespace($this->namespace)
			->group(base_path('routes/query.php'));
	}

	/**
	 * Define the card routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @return void
	 */
	protected function mapCustomerRoutes()
	{
		Route::middleware(['web', 'auth', 'browser.block', '2fa'])
			->namespace($this->namespace)
			->group(base_path('routes/customer.php'));
	}

    /**
     * Define the card routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapCardRoutes()
    {
        Route::middleware(['web', 'auth', 'browser.block', '2fa'])
            ->namespace($this->namespace)
            ->group(base_path('routes/bank/card.php'));
    }

    /**
     * Define the account routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAccountRoutes()
    {
        Route::middleware(['web', 'auth', 'browser.block', '2fa'])
            ->namespace($this->namespace)
            ->group(base_path('routes/bank/account.php'));
    }

	/**
	 * Define the account routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @return void
	 */
	protected function mapTestRoutes()
	{
		Route::middleware(['web', 'auth', 'browser.block', '2fa'])
			->namespace($this->namespace)
			->group(base_path('routes/test/test.php'));
	}

    /**
     * Define the account routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAutoRoutes()
    {
        Route::middleware(['web', 'auth', 'browser.block', '2fa'])
            ->namespace($this->namespace)
            ->group(base_path('routes/bank/auto.php'));
    }

	/**
	 * Define the safe family insurance routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @return void
	 */
	protected function mapSafeFamilyRoutes()
	{
		Route::middleware(['web', 'auth', 'browser.block', '2fa'])
			->namespace($this->namespace)
			->group(base_path('routes/insurance/safeFamily.php'));
	}

	/**
	 * Define the safe family insurance routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @return void
	 */
	protected function mapSecurityRoutes()
	{
		Route::middleware(['web', 'auth', 'browser.block', '2fa'])
			->namespace($this->namespace)
			->group(base_path('routes/security.php'));
	}

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
