<?php

namespace App\Core;


/**
 * Class GetResponseToken
 * @package App\Core
 */
class GetResponseToken
{
    /**
     * @var $response
     */
    protected $response;

    /**
     * @var $field
     */
    protected $field;

    /**
     * @var $listName
     */
    protected $listName;

    /**
     * @var $level
     */
    protected $level;

    /**
     * Fetch result from response based on field name
     *
     * @param $response
     * @param $field
     * @param $returnObject
     * @return string
     */
    public function result($response, $field = null, $returnObject = null)
    {
	    if (empty($response->getDoProcessResult()->Data->DataList->Data))
	    {
		    return null;
	    }

        $results = $response->getDoProcessResult()->Data->DataList->Data;

        foreach ($results as $result)
        {

            if ($result->Field === $field)
            {
                if ($result->HasData) {
                    if ($returnObject) {
                        return $result;
                    }

                    return SanitizeString::single($result->Value);
                }

                return null;
            }
        }

        return false;
    }

    /**
     * Fetch result from response based on field name
     *
     * @param $response
     * @param $firstList
     * @param $field
     * @param $returnObject
     * @return string
     */
    public function secondLevelResult($response, $firstList, $field, $returnObject = null)
    {
    	if (empty($response->getDoProcessResult()->Data->DataList->Data))
	    {
	    	return null;
	    }

        $firstResults = $response->getDoProcessResult()->Data->DataList->Data;

        foreach ($firstResults as $firstResult)
        {
            if ($firstResult->Field === $firstList)
            {
                if ($firstResult->HasData)
                {
                    $lastResults = $firstResult->DataList->Data;

                    foreach ($lastResults as $result)
                    {
                    	if (isset($result->Field))
	                    {
		                    if ($result->Field === $field)
		                    {
			                    if ($result->HasData)
			                    {
				                    if ($returnObject)
				                    {
					                    return $result;
				                    }

				                    return SanitizeString::single($result->Value);
			                    }

			                    return null;
		                    }
                        }
                    }
                }

                return null;
            }
        }

        return null;
    }

	/**
	 * @param $response
	 * @param $firstList
	 * @param $secondList
	 * @param $field
	 * @param null $returnObject
	 * @return mixed|null|string|string[]
	 */
	public function thirdLevelResult($response, $firstList, $secondList, $field, $returnObject = null)
    {
	    if (empty($response->getDoProcessResult()->Data->DataList->Data))
	    {
		    return null;
	    }

        $firstResults = $response->getDoProcessResult()->Data->DataList->Data;

        foreach ($firstResults as $firstResult)
        {
            if ($firstResult->Field === $firstList)
            {
                if ($firstResult->HasData)
                {
                    if (\is_array($firstResult->DataList->Data))
                    {
                        $secondResults = $firstResult->DataList->Data;

                        foreach ($secondResults as $secondResult)
                        {
                            $result = $this->secondResult($secondResult, $secondList, $field, $returnObject);

                            if ($result)
                            {
                                return $result;
                            }
                        }
                    } else {
                        $secondResult = $firstResult->DataList->Data;

                        return $this->secondResult($secondResult, $secondList, $field, $returnObject);
                    }
                }

                return null;
            }
        }

	    return null;
    }

	/**
	 * @param $response
	 * @param $firstList
	 * @param $secondList
	 * @param $thirdList
	 * @param $field
	 * @param null $returnObject
	 * @return mixed|null|string|string[]
	 */
	public function fourthLevelResult($response, $firstList, $secondList, $thirdList, $field, $returnObject = null)
	{
		if (empty($response->getDoProcessResult()->Data->DataList->Data))
		{
			return null;
		}

		$firstResults = $response->getDoProcessResult()->Data->DataList->Data;

		foreach ($firstResults as $firstResult)
		{
			if ($firstResult->Field === $firstList)
			{
				if ($firstResult->HasData)
				{
					if (\is_array($firstResult->DataList->Data))
					{
						$secondResults = $firstResult->DataList->Data;

						foreach ($secondResults as $secondResult)
						{
							if (isset($secondResult->Field))
							{
								if ($secondResult->Field === $secondList)
								{
									if ($secondResult->HasData)
									{
										if(\is_array($secondResult->DataList->Data))
										{
											$thirdResults = $secondResult->DataList->Data;

											foreach ($thirdResults as $thirdResult)
											{
												$result = $this->secondResult($thirdResult, $thirdList, $field, $returnObject);

												if ($result)
												{
													return $result;
												}
											}
										}
									}

									return null;
								}

							}
						}
					} else {
						$secondResult = $firstResult->DataList->Data;

						return $this->secondResult($secondResult, $secondList, $field, $returnObject);
					}
				}

				return null;
			}
		}

		return null;
	}


	/**
	 * @param $secondResult
	 * @param $secondList
	 * @param $field
	 * @param $returnObject
	 * @return mixed|null|string|string[]
	 */
	private function secondResult($secondResult, $secondList, $field, $returnObject)
    {
        if ($secondResult->Field === $secondList) {
            if ($secondResult->HasData) {
                $lastResults = $secondResult->DataList->Data;

                foreach ($lastResults as $result) {
                    if ($result->Field === $field) {
                        if ($result->HasData) {
                            if ($returnObject) {
                                return $result;
                            }

                            return SanitizeString::single($result->Value);
                        }

                        return null;
                    }
                }
            }

            return null;
        }

	    return null;
    }
}