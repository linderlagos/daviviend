<?php

namespace App\Core;

use App\Core\Traits\HasData;
use App\Core\Traits\GetLists;
use App\Core\Traits\Peoplesoft;
use App\UserRole;
use Jenssegers\Date\Date;
use App\Core\Traits\Bridge;
use App\Http\Response\CreditCardProcessResponse;

/**
 * Class EntailmentProcessParameters
 * @package App\Core
 */
class Parameters
{
	use HasData;
	use Peoplesoft;
	use GetLists;
    use Bridge;

	/**
	 * Construct a user object data
	 *
	 * @param $request
	 * @return array
	 */
	public function user($request): array
	{
		/*$roles = UserRole::all();

		$roleList = [];

		foreach ($roles as $role)
		{
			$roleList[] = new Data(true, 'Rol', null, $role->name);
		}*/

		$roleList = ['HN-Apps TDA Gestor UAT-S-G'];
		return [
			$this->hasData('PeopleSoft', $request->peopleSoft),
			$this->hasData('Password', $request->password),
			new Data(
				true,
				'ROLES_LIST',
				$roleList
			)
		];
	}

	/**
	 * Construct the dependants object with input data
	 *
	 * @param $resource
	 * @param $field
	 * @return Data
	 */
	protected function checkIfDependantsListHasValues($resource, $field): Data
	{
		if ($resource['dependants']['code'] === 'N')
		{
			return new Data(
				true,
				$field,
				[
					$this->dependant('NO TIENE', 'DEPENDIENTE_1', '1'),
				]
			);
		}

		$dependants = [];

		for ($i = 1; $i <= 6; $i++)
		{
			$dependants[] = $this->dependant($resource['dependants'][$i]['name'], 'DEPENDIENTE_' . $i, $i);
		}

		return new Data(
			true,
			$field,
			$dependants
		);
	}

	/**
	 * Construct a single dependant data object
	 *
	 * @param $resource
	 * @param $field
	 * @param $correlative
	 * @return Data
	 */
	protected function dependant($resource, $field, $correlative): Data
	{
		if ($resource) {
			return new Data(true, $field, [
				$this->hasData('DependienteNombre', $resource),
				$this->hasData('DependienteCorrelativo', $correlative),
			]);
		}

		return new Data(
			false,
			$field
		);
	}

	/**
	 * Construct the public job object with input data
	 *
	 * @param $flow
	 * @param $field
	 * @return Data
	 */
	protected function checkIfPublicJobHasValues($flow, $field): Data
	{
		if ($flow['public_job']['code'] === 'N') {
			return new Data(
				false,
				$field
			);
		}

		return new Data(
			true,
			$field,
			[
				new Data(
					true,
					'CARGO_PUBLICO_1',
					[
						$this->hasData('CargosPublicosGobiernoCorrelativo', '1'),
						$this->hasData('CargosPublicosGobiernoCargo', $flow['public_job']['job']),
						$this->hasData('CargosPublicosGobiernoInstitucion', $flow['public_job']['employer_name']),
						$this->hasData('CargosPublicosGobiernoEsActual', $flow['public_job']['active']),
						$this->hasData('CargosPublicosGobiernoFechaInicio', $this->getDate($flow['public_job']['from'])),
						$this->hasData('CargosPublicosGobiernoFechaFinal', $this->getDate($flow['public_job']['to'])),
					]
				)
			]
		);
	}

	protected function getDate($field, $format = 'Ymd')
	{
		if ($field && $field !== 'NULO')
		{
			$date = Date::createFromTimestamp($field)->format($format);
		} else {
			$date = null;
		}

		return $date;
	}

	/**
	 * Checks if card limits is different from approved
	 *
	 * @param $resource
	 * @param $field
	 * @return Data
	 */
	protected function checkIfCardLimitIsDifferent($resource, $field): Data
	{
		if ($resource['product']['customer_limit'] === $resource['product']['limit']) {
			return new Data(
				false,
				$field
			);
		}

		return new Data(
			true,
			$field,
			[
				$this->hasData('NumeroSolicitud', $resource['request']['number']),
				$this->hasData('TarjetaCreditoSeleccionadaDAVIPUNTOSoDADINEROPLUS', $resource['product']['code']),
				new Data(true, 'PromocionSeleccionada', null, '0'),
				new Data(true, 'PorcentajeSeleccionado', null, '0'),
				$this->hasData('LimiteAceptadoCliente', $resource['product']['customer_limit']),
				new Data(true, 'CambioLimiteEfectuado', null, '0'),
			]
		);
	}

	/**
	 * Construct the references object with input data
	 *
	 * @param $resource
	 * @param $field
	 * @return Data
	 */
	protected function getReferences($resource, $field) : Data
    {
	    $data = [];

	    for ($i = 1; $i <= 2; $i++)
	    {
	    	$name = get_json($resource, ['references', $i, 'name']);

	    	$type = 4;

	    	if ($i === 2)
		    {
		    	$type = 3;
		    }

	    	$relationship = get_json($resource, ['references', $i, 'relationship', 'code']);
	    	$mobile = get_json($resource, ['references', $i, 'mobile']);
	    	$phone = get_json($resource, ['references', $i, 'phone']);
	    	$workPhone = get_json($resource, ['references', $i, 'work_phone']);
	    	$address = get_json($resource, ['references', $i, 'address']);
	    	$addressReference = get_json($resource, ['references', $i, 'reference']);

		    if($name !== 'NULO')
		    {
			    $data[] = new Data(
				    true,
				    'REFERENCIA_' . $i,
				    [
					    $this->hasData('ReferenciasNumero', (string) $i ),
					    $this->hasData('ReferenciasNombre', $name),
					    $this->hasData('ReferenciasTipo', $type),
					    $this->hasData('ReferenciasParentesco', $relationship),
					    $this->hasData('ReferenciasCelular', $mobile),
					    $this->hasData('ReferenciasTelefonoDomicilio', $phone),
					    $this->hasData('ReferenciasTelefonoEmpleador', $workPhone),
					    $this->hasData('ReferenciasDireccion', $address),
					    $this->hasData('ReferenciasDireccionComplemento', $addressReference),
				    ]
			    );
		    }
	    }

	    if (empty($data))
	    {
		    return new Data(
			    false,
			    $field
		    );
	    }

	    return new Data(
		    true,
		    $field,
		    $data
	    );

//        return new Data(
//            true,
//            $field,
//            [
//                new Data(
//                    true,
//                    'REFERENCIA_1',
//                    [
//                        $this->hasData('Referencia_1_Nombre', get_json($resource, ['references', 1, 'name'])),
//                        $this->hasData('Referencia_1_Tipo', '4'),
//                        $this->hasData('Referencia_1_Parentesco', get_json($resource, ['references', 1, 'relationship', 'code'])),
//                        $this->hasData('Referencia_1_Celular', get_json($resource, ['references', 1, 'mobile'])),
//                        $this->hasData('Referencia_1_TelefonoDomicilio', get_json($resource, ['references', 1, 'phone'])),
//                        $this->hasData('Referencia_1_TelefonoEmpleadorLaboral', get_json($resource, ['references', 1, 'work_phone'])),
//                    ]
//                ),
//                new Data(
//                    true,
//                    'REFERENCIA_2',
//                    [
//                        $this->hasData('Referencia_2_Nombre', get_json($resource, ['references', 2, 'name'])),
//                        $this->hasData('Referencia_2_Tipo', '3'),
//                        $this->hasData('Referencia_2_Parentesco', get_json($resource, ['references', 2, 'relationship', 'code'])),
//                        $this->hasData('Referencia_2_Celular', get_json($resource, ['references', 2, 'mobile'])),
//                        $this->hasData('Referencia_2_TelefonoDomicilio', get_json($resource, ['references', 2, 'phone'])),
//                        $this->hasData('Referencia_2_TelefonoEmpleadorLaboral', get_json($resource, ['references', 2, 'work_phone'])),
//                    ]
//                )
//            ]
//        );
    }

	/**
	 * Get value from json field in json column and construct xml data
	 *
	 * @param $field
	 * @param $resource
	 * @param $jsonColumn
	 * @param $parameter
	 * @param null $acceptZeroValues
	 * @return Data
	 */
    protected function getJsonField($field, $resource, $jsonColumn, $parameter, $acceptZeroValues = null)
    {
    	// Check if column has data
	    if ($resource->$jsonColumn) {
	    	// Decode json column data
		    $results = $resource->$jsonColumn;
		    // Iterate array
		    foreach ($results as $result) {
		    	// Check if json field has value
		    	if (isset($result[$parameter]))
			    {
			    	// Check if field should accept '0' as value
			    	if ($acceptZeroValues)
				    {
				    	// Create data structure to pass value on xml
					    new Data(true, $field, null, $result[$parameter]);
				    } else {
					    // Check if parameter has value and create data structure to pass value on xml
					    return $this->hasData($field, $result[$parameter]);
				    }
			    }
		    }
	    }

	    return new Data(
		    false,
		    $field
	    );
    }

    /**
     * Divide the string up to 3 sections
     *
     * @param $column
     * @param $length
     * @param $section
     * @return bool|string
     */
    protected function divideString($column, $length, $section)
    {
        $columnLength = \strlen($column);

        if ($section === 1) {
            return trim(mb_substr($column, 0, $length), " ");
        } elseif ($columnLength <= $length && $section !== 1) {
            return '';
        } elseif ($columnLength > $length && $section === 2) {
            return trim(mb_substr($column, $length, $length + 1), " ");
        } elseif ($columnLength <= ($length * 2) && $section === 3) {
            return '';
        } elseif ($columnLength > ($length * 2) && $section === 3) {
            return trim(mb_substr($column, ($length * 2) + 1, $length), " ");
        }

        return '';
    }

    /**
     * Create the fullname with available fields
     *
     * @param $firstname
     * @param $middlename
     * @param $lastname
     * @param $secondlastname
     * @return string
     */
    protected function fullname($firstname, $middlename, $lastname, $secondlastname) : string
    {
        $name = $firstname;

        if ($middlename) {
            $name .= ' ' . $middlename;
        }

        $name .= ' ' . $lastname;

        if ($secondlastname) {
            $name .= ' ' . $secondlastname;
        }

        return $name;
    }

    /**
     * Return the rejection code
     *
     * @param $code
     * @return mixed
     */
    protected function rejectionType($code)
    {
        $codes = [
            'TU15' => 'T',
            'TA01' => 'R'
        ];

        return $codes[$code];
    }

	protected function getColony($code, $colony, $otherColony)
	{
		if ($code === '999')
		{
			return $otherColony;
		}

		return $colony;
	}



}
