<?php

namespace App\Core;

// Helpers
use App\Core\Insurance\SafeFamily\SafeFamilyParameters;
use App\Core\Traits\Bridge;
use Carbon\Carbon;
use App\Http\Response;
/**
 * Class GetPreFilledData
 * @package App\Core
 */
class GetPreFilledData
{
    use Bridge;

    /**
     * @var $response
     */
    protected $response;

    /**
     * Get pre-filled lists for Honduras
     *
     * @return array
     */
    public function data() : array
    {
        $callSoap = $this->callSoap('HN', 'GLOBAL_PREFILLED_DATA');

        return [
            'dayOptions' => $this->getDays(),
            'monthOptions' => $this->getMonths(),
            'yearOptions' => $this->getYears(),
            'selectedYear' => Carbon::now()->year - 30,
            'employerYearOptions' => $this->getEmployerYears(),
            'accountOptions' => $this->getAccountOptions(),
            'selectedEmployerYear' => Carbon::now()->year - 10,
            'idOptions' => $this->firstIndentation($callSoap, 'IDENTIFICATIONS_LIST'),
            'idValidation' => $this->idValidationList($callSoap, 'EXCEPTION_CODES_FOR_ID_LIST'),
            'jobTypeOptions' => $this->firstIndentation($callSoap, 'COMMERCIAL_ACTIVITIES_LIST'),
            'cities' => $this->getCities($callSoap, 'DEPARTMENTS_LIST'),
            'nations' => $this->firstIndentation($callSoap, 'COUNTRIES_LIST'),
            'employeeTypes' => $this->firstIndentation($callSoap, 'COMPANY_TYPES_LIST'),
            'incomeRangeOptions' => $this->firstIndentation($callSoap, 'INCOME_RANGE_LIST'),
            'assetRangeOptions' => $this->firstIndentation($callSoap, 'RANGE_ASSETS_LIST'),
            'expenseRangeOptions' => $this->firstIndentation($callSoap, 'RANGE_EXPENSES_LIST'),
            'liabilityRangeOptions' => $this->firstIndentation($callSoap, 'RANGE_LIABILITIES_LIST'),
            'professionOptions' => $this->firstIndentation($callSoap, 'PROFESSIONS_LIST'),
            'dependants' => $this->makeRange(6),
            'cardLimits' => $this->cardLimits($callSoap),
            'departaments' => $this->firstIndentation($callSoap, 'DEPARTMENTS_LIST'),
            'currency' => [
                '000' => 'Lempira',
                '001' => 'Dólar',
                '003' => 'Euro'
            ],
            'transfersCurrency' => [
                '001' => 'Lempira',
                '002' => 'Dólar',
                '003' => 'Euro'
            ],
            'referenceOptions' => $this->firstIndentation($callSoap, 'RELATIONSHIP_REFERENCES_LIST'),
            'beneficiaryOptions' => $this->firstIndentation($callSoap, 'RELATIONSHIP_BENEFICIARIES_LIST', true),
            'jobOptions' => [
                '1' => 'ASALARIADO',
                '2' => 'COMERCIANTE',
                '3' => 'PROFESIONAL INDEPENDIENTE',
            ],
            'jobContractOptions' => [
                'P' => 'PERMANENTE',
                'C' => 'POR CONTRATO'
            ],
            'maritalStatusOptions' => [
                'S' => 'SOLTERO',
                'C' => 'CASADO'
            ],
            'genderOptions' => [
                'M' => 'MASCULINO',
                'F' => 'FEMENINO'
            ],
            'confirmation' => [
                'N' => 'No',
                'S' => 'Si'
            ],
            'promotions' => [
                '120' => 'TC ASISTIDA OPEN MARKET',
                '121' => 'TC ASISTIDA BASE PRECALIFICADA',
                '124' => 'TC ASISTIDA BASE PREAPROBADA'
            ]
        ];
    }

    /**
     * Get pre-filled lists for Honduras
     *
     * @return array
     */
    public function insuranceSafeFamilyData() : array
    {
        $callSoap = $this->callSoap('HN-SEGUROS', 'DIGITAL_INSURACE_LIST');

        return [
            'dayOptions' => $this->getDays(),
            'monthOptions' => $this->getMonths(),
            'yearOptions' => $this->getYears(),
            'selectedYear' => Carbon::now()->year - 30,
            'confirmation' => [
                'N' => 'No',
                'S' => 'Si'
            ],
            'beneficiaryOptions' => $this->firstIndentation($callSoap, 'DATA_INSURANCE_LIST_PARENTESCO'),
            'paymentTypes' => [
                'M' => 'Mensual',
                'A' => 'Anual'
            ],
            'hospitals' => $this->firstIndentation($callSoap, 'DATA_INSURANCE_LIST_HOSPITALES', true) + ['Otro' => 'OTRO'],
            'diseases' => [
                'Aneurismas' => 'Aneurismas',
                'Artereoesclerosis' => 'Artereoesclerosis',
                'Ataques o Convulsiones' => 'Ataques o Convulsiones',
                'Cáncer y/o tumores benigno' => 'Cáncer y/o tumores benigno',
                'Enfermedades mentales o psiquiáticas' => 'Enfermedades mentales o psiquiáticas',
                'Derrame cerebral' => 'Derrame cerebral',
                'Diábetes Dolores de cabeza' => 'Diábetes Dolores de cabeza',
                'Epilepsia' => 'Epilepsia',
                'Gastrointestinales' => 'Gastrointestinales',
                'Gibosis (jorobas)' => 'Gibosis (jorobas)',
                'Gota' => 'Gota',
                'Parkinson Tuberculosis' => 'Parkinson Tuberculosis',
                'Vértigos o Desmayos' => 'Vértigos o Desmayos',
                'De la Tiriodes' => 'De la Tiriodes',
                'Del Cerebro y Nervios' => 'Del Cerebro y Nervios',
                'Del Corazón' => 'Del Corazón',
                'De la Columna' => 'De la Columna',
                'De la Vista' => 'De la Vista',
                'De la Sangre' => 'De la Sangre',
                'De las vías Urinarias o Riñones' => 'De las vías Urinarias o Riñones',
                'Del hígado' => 'Del hígado',
                'De los oídos o audición' => 'De los oídos o audición',
                'De los órganos Reproductivos' => 'De los órganos Reproductivos',
                'De las vías Respiratorias' => 'De las vías Respiratorias',
                'De la Piel' => 'De la Piel',
                'De los Pulmones' => 'De los Pulmones',
                'Hipertensión o Hipotensión Arterial' => 'Hipertensión o Hipotensión Arterial',
                'Angina de pecho' => 'Angina de pecho',
                'Palpitaciones Disnea (cansancio)' => 'Palpitaciones Disnea (cansancio)',
                'Neurastenia' => 'Neurastenia',
                'Parálisis' => 'Parálisis',
                'VIH' => 'VIH',
                'SIDA' => 'SIDA',
                'Enfermedades venéreas' => 'Enfermedades venéreas',
                'Otro' => 'Otro padecimiento no mencionado anteriormente',
            ],
            'accidents' => $this->firstIndentation($callSoap, 'DATA_INSURANCE_LIST_ACCIDENTES', true),
            'branches' => $this->firstIndentation($callSoap, 'DATA_INSURANCE_LIST_AGENCIAS')
        ];
    }

    /**
     * Get pre-filled lists auto module
     *
     * @return array
     */
    public function bankAutoData() : array
    {
        $callSoap = $this->callSoap('HN', 'GLOBAL_PREFILLED_DATA');

        return [
            'dayOptions' => $this->getDays(),
            'monthOptions' => $this->getMonths(),
            'yearOptions' => $this->getYears(),
            'selectedYear' => Carbon::now()->year - 30,
            'employerYearOptions' => $this->getEmployerYears(),
            'accountOptions' => $this->getAccountOptions(),
            'selectedEmployerYear' => Carbon::now()->year - 10,
            'idValidation' => $this->idValidationList($callSoap, 'EXCEPTION_CODES_FOR_ID_LIST'),
            'jobTypeOptions' => $this->firstIndentation($callSoap, 'COMMERCIAL_ACTIVITIES_LIST'),
            'jobTypeOptionsByJob' => $this->jobTypeOptionsByJob($callSoap),
            'cities' => $this->getCities($callSoap, 'DEPARTMENTS_LIST'),
            'nations' => $this->firstIndentation($callSoap, 'COUNTRIES_LIST'),
            'employeeTypes' => $this->firstIndentation($callSoap, 'COMPANY_TYPES_LIST'),
            'incomeRangeOptions' => $this->firstIndentation($callSoap, 'INCOME_RANGE_LIST'),
            'assetRangeOptions' => $this->firstIndentation($callSoap, 'RANGE_ASSETS_LIST'),
            'expenseRangeOptions' => $this->firstIndentation($callSoap, 'RANGE_EXPENSES_LIST'),
            'liabilityRangeOptions' => $this->firstIndentation($callSoap, 'RANGE_LIABILITIES_LIST'),
            'professionOptions' => $this->firstIndentation($callSoap, 'PROFESSIONS_LIST'),
            'dependants' => $this->makeRange(6),
            'referenceOptions' => $this->firstIndentation($callSoap, 'RELATIONSHIP_REFERENCES_LIST'),
            'beneficiaryOptions' => $this->firstIndentation($callSoap, 'RELATIONSHIP_BENEFICIARIES_LIST', true),
            'departaments' => $this->firstIndentation($callSoap, 'DEPARTMENTS_LIST'),
            'jobOptions' => [
                '1' => 'ASALARIADO',
                '2' => 'COMERCIANTE',
                '3' => 'PROFESIONAL INDEPENDIENTE',
            ],
            'jobContractOptions' => [
                'P' => 'PERMANENTE',
                'C' => 'POR CONTRATO'
            ],
            'maritalStatusOptions' => [
                'S' => 'SOLTERO',
                'C' => 'CASADO'
            ],
            'genderOptions' => [
                'M' => 'MASCULINO',
                'F' => 'FEMENINO'
            ],
            'confirmation' => [
                'N' => 'No',
                'S' => 'Si'
            ],
            'flowOptions' => [
                'C' => 'Concesionaria',
                'A' => 'Asesor',
            ],
            'concessionaireOptions' => $this->firstIndentation($callSoap, 'DEALER_LIST'),
            'sellerOptions' => $this->firstIndentation($callSoap, 'SELLER_IN_CONCESSIONARIE_LIST'),
            'typeAutoOptions' => $this->firstIndentation($callSoap, 'TYPE_CARS_LIST'),
            'statusAutoOptions' => [
                '016' => 'Nuevo',
                '017' => 'Usado'
            ],
            'yearAutoOptions' => $this->inBetweenYears(date('Y') + 1, date('Y') - 3),
            'usedYearAutoOptions' => $this->inBetweenYears(date('Y') + 1, date('Y') - 8),
            'financingTermOptionsFour' => [
                '12' => '12 meses',
                '24' => '24 meses',
                '36' => '36 meses',
                '48' => '48 meses'
            ],
            'financingTermOptionsFive' => [
                '12' => '12 meses',
                '24' => '24 meses',
                '36' => '36 meses',
                '48' => '48 meses',
                '60' => '60 meses'
            ],
            'financingTermOptions' => [
                '12' => '12 meses',
                '24' => '24 meses',
                '36' => '36 meses',
                '48' => '48 meses',
                '60' => '60 meses',
                '72' => '72 meses'
            ],
            'interestRateOptions' => [
                '12' => '12%',
                '13' => '13%',
                '14' => '14%',
                '15' => '15%',
                '12.5' => '12.5%',
                '13.5' => '13.5%',
                '14.5' => '14.5%'
            ],
            'businessZoneOptions' => [
                '1' => 'Centro Sur',
                '2' => 'Nor Occidente',
                '3' => 'Nor Oriente'
            ],
            'agentOptions' => $this->getAutoExecutives($callSoap),
//			'agentOptions' => [
//                '1' => 'Eduwin Avila',
//                '2' => 'Norman Diaz'
//            ],
            'insuranceTypeOptions' => [
                'd' => 'Davivienda',
                'o' => 'Otro banco'
            ],
            'autoBrandOptions' => $this->firstIndentation($callSoap, 'BRAND_CARS_LIST'),
//            'sellerConcessionarie' => $this->firstIndentation($callSoap, 'SELLER_IN_CONCESSIONARIE_LIST'),
            'sellerConcessionarie' => $this->getSellerOptions($callSoap),
            'recognizedCompaines' => $this->firstIndentation($callSoap, 'RECOGNIZED_COMPANIES_LIST'),
            'correspondenceOptions' => $this->firstIndentation($callSoap, 'DESTINATION_CORRESPONDENCE_LIST'),
            'idOptions' => $this->firstIndentation($callSoap, 'IDENTIFICATIONS_LIST'),
        ];
    }

    /**
     * Generate list of Job Type options by job
     *
     * @param $soap
     * @return array
     */
    private function jobTypeOptionsByJob($soap)
    {
        $options = [];

        $object = $this->findField($soap, 'COMMERCIAL_ACTIVITY_BY_TYPES_EMPLOYMENT_LIST');

        $jobTypes = $this->firstIndentation($soap, 'COMMERCIAL_ACTIVITIES_LIST');

        if (empty($object->DataList->Data))
        {
            return [
                '1' => 'No hay opciones'
            ];
        }

        foreach ($object->DataList->Data as $element)
        {
            if (!empty($jobTypes[$element->Id]))
            {
//				$value = '';
//			} else {
                $value = $jobTypes[$element->Id];
            }

            if(empty($options[$element->Value]))
            {
                $options[$element->Value] = [];
            }

            $options[$element->Value] += [
                $element->Id => $value
            ];
        }

        return $options;
    }

    /**
     * Get pre-filled lists for Honduras
     *
     * @param $policy
     * @param $account
     * @return array
     */
    public function insuranceSafeFamilyPlans($policy, $account = null) : array
    {
        $parameters = new SafeFamilyParameters();

        $callSoap = $this->callSoapSafeFamily(
            'HN-SEGUROS',
            'DIGITAL_INSURACE_LIST',
            $parameters->secondResponse($policy),
            'DATA_ACCOUNT_CARD_EMBOSSED_PARAMS'
        );

        return [
            'plans' => $this->getInsuranceSafeFamilyPlans($callSoap, 'DIGITAL_INSURACE_LIST_PLANS'),
            'products' => $this->getInsuranceSafeFamilyProducts($callSoap, 'DIGITAL_INSURACE_LIST_ACCOUNTS')
        ];

    }

    /**
     * @param $policy
     * @return array
     */
    public function insuranceSafeFamilyCoverage($policy)
    {
        $parameters = new SafeFamilyParameters();

        $callSoap = $this->callSoapSafeFamily(
            'HN-SEGUROS',
            'DIGITAL_INSURACE_LIST',
            $parameters->secondResponse($policy),
            'DATA_ACCOUNT_CARD_EMBOSSED_PARAMS'
        );

        return $this->getInsuranceSafeFamilyCoverage($callSoap, 'DIGITAL_INSURACE_LIST_COVERAGE');
    }

    /**
     * Call approved lists SafeFamily soap
     *
     * @param $bank
     * @param $method
     * @param $instruction
     * @param $parameters
     * @return object
     */
    private function callSoapSafeFamily($bank, $method, $instruction = 'GET_ALL', $parameters = null)
    {
        $productName = 'lists';

        if ($bank == 'HN-SEGUROS')
        {
            $productName = 'insurance_lists';
        }

        $lists = $this->soap($productName, $bank, $method, $instruction, $parameters);

        return $lists['response'];
    }


    /**
     * Call approved lists soap
     *
     * @param $bank
     * @param $method
     * @param $instruction
     * @param $parameters
     * @return object
     */
    private function callSoap($bank, $method, $instruction = 'GET_ALL', $parameters = null)
    {
        $productName = 'lists';

        if ($bank == 'HN-SEGUROS')
        {
            $productName = 'insurance_lists';
        }

        $lists = $this->soap(
            $productName,
            $bank,
            $method,
            $parameters,
            $instruction);

        return $lists['response'];
    }

    /**
     * Filter lists of the first indentation by field name
     * Get array of options of the identified field
     *
     * @param $response
     * @param $field
     * @param $keyValue
     * @param $array
     * @return array
     */
    private function firstIndentation($response, $field, $keyValue = null, $array = null) : array
    {
        $options = [];

        $objects = $this->findField($response, $field);

        if (empty($objects->DataList->Data))
        {
            return [
                '1' => 'No hay opciones'
            ];
        }

        foreach ($objects->DataList->Data as $object) {
            $disabled = '';

            if (isset($object->Field))
            {
                if($object->Field === 'N')
                {
                    $disabled = ' (DESHABILITADO)';
                }
            }

            if ($keyValue)
            {
                $options[$object->Value] = mb_strtoupper($object->Value) . $disabled;
            } else {
                $options[$object->Id] = mb_strtoupper($object->Value) . $disabled;
            }
        }

        return $options;
    }

    private function getAutoExecutives($response)
    {
        $options = [];

        $zones = [
            1 => ' (CENTRO SUR)',
            2 => ' (NOR OCCIDENTE)',
            3 => ' (NOR ORIENTE)',
            99 => ' (CENTRO SUR)'
        ];

        $objects = $this->findField($response, 'AUTO_EXECUTIVES_LIST');

        if (empty($objects->DataList->Data))
        {
            return [
                '1' => 'No hay opciones'
            ];
        }

        foreach ($objects->DataList->Data as $object)
        {
            $options[$object->Field][$object->Id] = mb_strtoupper($object->Value) . $zones[$object->Field];
        }

        return $options;
    }

    private function getSellerOptions($response)
    {
        $options = [];

        $objects = $this->findField($response, 'SELLER_IN_CONCESSIONARIE_LIST');

        if (empty($objects->DataList->Data))
        {
            return [
                '1' => 'No hay opciones'
            ];
        }

        foreach ($objects->DataList->Data as $object)
        {
            $categories = explode('-', $object->Field);

            $concessionaire = $categories[0];

            $zoneCode = $categories[1];

            $zones = [
                '' => '',
                1 => ' (CENTRO SUR)',
                2 => ' (NOR OCCIDENTE)',
                3 => ' (NOR ORIENTE)'
            ];

            try
            {
                $zoneName = $zones[$zoneCode];
            } catch (\Exception $e)
            {
                $this->logException($e);
            }

            $options[$concessionaire][$object->Id] = mb_strtoupper($object->Value) . $zones[$zoneCode];
        }

        return $options;
    }

    /**
     * @param $response
     * @param $field
     * @return array
     */
    private function getInsuranceSafeFamilyPlans($response, $field)
    {

        $options = [];

        $objects = $this->findField($response, $field);

        foreach ($objects->DataList->Data as $object)
        {
            foreach ($object->DataList as $plan)
            {
                foreach ($plan as $value)
                {
                    if ($value->Field === 'Codigo')
                    {
                        $code = mb_strtoupper(trim($value->Value));
                    }

                    if (in_array($value->Field, ['Descripcion', 'Anual', 'Mensual']))
                    {
                        $options[$code][$value->Field] = 'L ' . number_format(str_replace('L.', '', $value->Value), 2);
                    } else {
                        $options[$code][$value->Field] = mb_strtoupper(trim($value->Value));
                    }
                }
            }
        }

        return $options;
    }

    /**
     * @param $response
     * @param $field
     * @param null $account
     * @return array
     */
    private function getInsuranceSafeFamilyProducts($response, $field, $account = null)
    {
        $products = [];

        $objects = $this->findField($response, $field);

        if ($account)
        {
            $products[0] = $account;
        }

        if (!is_array($objects->DataList->Data)) {

            $product = $objects->DataList->Data;

            $number = mb_strtoupper(trim($product->Field));
            $type = mb_strtoupper(trim($product->Value));

            $products[1]['code'] = $type;
            $products[1]['value'] = $number;
        } else {
            foreach ($objects->DataList->Data as $key => $product)
            {
                $correlative = $key + 1;

                $number = mb_strtoupper(trim($product->Field));
                $type = mb_strtoupper(trim($product->Value));

                $products[$correlative]['code'] = $type;
                $products[$correlative]['value'] = $number;
            }
        }

        return $products;
    }

    /**
     * @param $response
     * @param $field
     * @return array
     */
    private function getInsuranceSafeFamilyCoverage($response, $field)
    {
        $array = [];

        $objects = $this->findField($response, $field);

        foreach ($objects->DataList->Data as $coverage)
        {
            $type = mb_strtoupper(trim($coverage->Field));
            $value = mb_strtoupper(trim($coverage->Value));
            $code = mb_strtoupper(trim($coverage->Id));

            $array[$code]['code'] = $code;
            $array[$code]['value'] = $value;
            $array[$code]['type'] = $type;
        }

        return $array;
    }

    /**
     * Filter lists of states and municipalities to create and array
     * of state keys and arrays of municipalities in states
     *
     * @param $response
     * @param $field
     * @return array
     */
    private function idValidationList($response, $field) : array
    {
        $options = [];

        $objects = $this->findField($response, $field);

        foreach ($objects->DataList->Data as $object) {
            $options[$object->Id][] = mb_strtoupper($object->Value);
        }

        return $options;
    }

    /**
     * Get Honduran Cities
     *
     * @param $response
     * @param $field
     * @return array
     */
    private function getCities($response, $field) : array
    {
        $options = [];

        $objects = $this->findField($response, $field);

        foreach ($objects->DataList->Data as $state) {
            $municipality = $state->DataList->Data->DataList->Data;

            $array = \is_array($municipality);

            if ($array) {
                foreach ($municipality as $object) {
                    $options = $this->getColonies($object, $state, $options);
                }
            } else {
                $options = $this->getColonies($municipality, $state, $options);
            }
        }

        return $options;
    }

    /**
     * Get days
     *
     * @return array
     */
    private function getDays(): array
    {
        $options = [];

        for ($i = 01; $i < 32; $i++) {
            if ($i < 10) {
                $i = 0 . $i;
            }

            $options += [
                $i => $i
            ];
        }

        return $options;
    }

    /**
     * @return array
     */
    private function getMonths(): array
    {
        return [
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        ];
    }

    /**
     * Get years
     *
     * @return array
     */
    private function getYears() : array
    {
        $date = Carbon::now();

        $yearOptions = [];

        for ($i = ($date->year - 100); $i < ($date->year - 17); $i++) {
            $yearOptions += [
                $i => $i
            ];
        }

        return $yearOptions;
    }

    /**
     * Get years
     *
     * @param $start
     * @param $end
     * @return array
     */
    private function inBetweenYears($start, $end) : array
    {
        $yearOptions = [];

        for ($i = $end; $i <= $start; $i++) {
            $yearOptions += [
                $i => $i
            ];
        }

        arsort($yearOptions);

        return $yearOptions;
    }

    /**
     * Get years
     *
     * @return array
     */
    private function getEmployerYears() : array
    {
        $date = Carbon::now();

        $yearOptions = [];

        for ($i = ($date->year - 53); $i < ($date->year + 1); $i++) {
            $yearOptions += [
                $i => $i
            ];
        }

        return $yearOptions;
    }

    /**
     * Find field by name
     *
     * @param $response
     * @param $field
     * @return mixed
     */
    public function findField($response, $field)
    {
        foreach ($response->getCreditCardProcessResult()->Data->DataList->Data as $lists) {

            if ($lists->Field === $field) {
                return $lists;
            }
        }
    }

    /**
     * @param $response
     * @return array
     */
    private function cardLimits($response)
    {
        $list = $this->findField($response, 'CHANGE_CARD_LIMIT');

        $limits = [];

        foreach ($list->DataList->Data as $range) {
            $values = explode(' - ', $range->Value);

            $limits[$range->Field] = [
                'begin' => $values[0],
                'end' => $values[1],
            ];
        }

        return $limits;
    }

    /**
     * Get array of colonies
     *
     * @param $object
     * @param $state
     * @param $options
     * @return array
     */
    private function getColonies($object, $state, $options): array
    {
        if ($object->HasData) {
            if ($object->DataList->Data->HasData) {
                $colonyObject = $object->DataList->Data->DataList->Data;

                $colonyArray = \is_array($colonyObject);

                if ($colonyArray) {
                    foreach ($colonyObject as $colony) {
                        $key = $state->Value . '-' . $state->Id . '>' . $object->Value . '-' . $object->Id . '>' . $colony->Value . '-' . $colony->Id;

                        $options[$state->Value][$key] = $colony->Value . ', ' . $object->Value . ', ' . $state->Value;
                    }
                } else {
                    $key = $state->Value . '-' . $state->Id . '>' . $object->Value . '-' . $object->Id . '>' . $colonyObject->Value . '-' . $colonyObject->Id;

                    $options[$state->Value][$key] = $colonyObject->Value . ', ' . $object->Value . ', ' . $state->Value;
                }
            }
        }

        return $options;
    }

    /**
     * @param $amount
     * @return array
     */
    public function makeRange($amount)
    {
        $range = [
            'N' => 0
        ];

        for ($i = 1; $i <= $amount; $i++) {
            $range += [
                $i => $i
            ];
        }

        return $range;
    }

    /**
     * @return array
     */
    private function getAccountOptions()
    {
        return [
            '090' => 'Cuenta de inversión',
            '120' => 'Cuenta Inversión Tradicional',
            '110' => 'Cuenta Planilla Sin Libreta',
        ];
    }


}
