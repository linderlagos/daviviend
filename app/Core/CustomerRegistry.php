<?php

namespace App\Core;

use App\Core\Traits\DivideString;
use App\Core\Traits\RecentFlow;
use App\Core\Traits\SearchValueFromLists;
use Jenssegers\Date\Date;

/**
 * Class HandleCustomerRegistry
 * @package App\Core
 */
class CustomerRegistry
{
	use RecentFlow, DivideString, SearchValueFromLists;

    /**
     * Get request parameters
     *
     * @var
     */
    protected $request;

    /**
     * New Response
     *
     * @var GetResponse
     */
    protected $getResponse;

    /**
     * HandleCustomerRegistry constructor.
     * @param GetResponse $getResponse
     */
    public function __construct(GetResponse $getResponse)
    {
	    $this->getResponse = $getResponse;
    }

	protected function constructJson($request, $function, $quantity, $productName = null)
	{
		$array = [];

		for ($i = 1; $i <= $quantity; $i++)
		{
			$array[$i] = $this->$function($request, $i, $productName);
		}

		return $array;
	}

	protected function constructRequestFullAddress($addressLineOne, $addressLineTwo, $reference)
	{
		$address = $addressLineOne;

		if ($addressLineTwo)
		{
			$address .= ' ' . $addressLineTwo;
		}

		if ($reference)
		{
			$address .= ' ' . $reference;
		}

		return $address;
	}

    /**
     * Construct beneficiary array if request has beneficiaries data
     *
     * @param $request
     * @param $correlative
     * @return array
     */
    protected function getBeneficiary($request, $correlative)
    {
    	if ($request->get('beneficiary_' . $correlative . '_name'))
	    {
	    	$idType = 999;
	    	$identity = '';

	    	if ($request->filled('beneficiary_' . $correlative . '_identity'))
		    {
		    	$idType = $request->get('beneficiary_' . $correlative . '_id_type');
		    	$identity = $request->get('beneficiary_' . $correlative . '_identity');
		    }

		    return array_map('mb_strtoupper', [
			    'beneficiary_' . $correlative . '_name' => $request->get('beneficiary_' . $correlative . '_name'),
			    'beneficiary_' . $correlative . '_relationship' => $request->get('beneficiary_' . $correlative . '_relationship'),
			    'beneficiary_' . $correlative . '_id_type' => $idType,
			    'beneficiary_' . $correlative . '_identity' => $identity,
		    ]);
	    }

	    return null;
    }

	/**
	 * Construct references array if request has references data
	 *
	 * @param $request
	 * @param $correlative
	 * @return array
	 */
	protected function getReference($request, $correlative)
	{
		if ($request->get('reference_' . $correlative . '_name'))
		{
			return array_map('mb_strtoupper', [
				'reference_' . $correlative . '_name' => $request->get('reference_' . $correlative . '_name'),
//				'reference_' . $correlative . '_relationship' => $request->get('reference_' . $correlative . '_relationship'),
				'reference_' . $correlative . '_mobile' => $request->get('reference_' . $correlative . '_mobile'),
				'reference_' . $correlative . '_phone' => $request->get('reference_' . $correlative . '_phone'),
//				'reference_' . $correlative . '_work_phone' => $request->get('reference_' . $correlative . '_work_phone'),
			]);
		}

		return null;
	}

	/**
	 * Construct professional dependants array if request has professional dependants data
	 *
	 * @param $request
	 * @param $correlative
	 * @return array
	 */
	protected function getProfessionalDependants($request, $correlative)
	{
		if ($request->get('professional_dependant_' . $correlative . '_name'))
		{
			return array_map('mb_strtoupper', [
				'professional_dependant_' . $correlative . '_name' => $request->get('professional_dependant_' . $correlative . '_name'),
				'professional_dependant_' . $correlative . '_relationship' => $request->get('professional_dependant_' . $correlative . '_relationship'),
				'professional_dependant_' . $correlative . '_id_type' => $request->get('professional_dependant_' . $correlative . '_id_type'),
				'professional_dependant_' . $correlative . '_identity' => $request->get('professional_dependant_' . $correlative . '_identity'),
				'professional_dependant_' . $correlative . '_job_type' => $request->get('professional_dependant_' . $correlative . '_job_type'),
			]);
		}

		return null;
	}

    /**
     * Construct national institutions array
     *
     * @param $request
     * @param $correlative
     * @return array
     */
    protected function getNationalInstitution($request, $correlative)
    {
    	if ($request->get('national_institution_' . $correlative))
	    {
		    return [
			    'national_institution_' . $correlative => $request->get('national_institution_' . $correlative)
		    ];
	    }

	    return null;
    }

	/**
	 * Construct national institutions array
	 *
	 * @param $request
	 * @param $correlative
	 * @return array
	 */
	protected function getForeignInstitution($request, $correlative)
	{
		if ($request->get('foreign_institution_' . $correlative))
		{
			return [
				'foreign_institution_' . $correlative => $request->get('foreign_institution_' . $correlative)
			];
		}

		return null;
	}

	protected function getInternationalOperationsInformation($request, $correlative)
	{
		return array_map('mb_strtoupper', [
			'foreign_currency' => $request->get('foreign_currency'),
			'wire_transfers' => $request->get('wire_transfers'),
			'currency_purchase' => $request->get('currency_purchase'),
			'checks_in_dollars' => $request->get('checks_in_dollars'),
			'remittances' => $request->get('remittances'),
			'typical_monthly_income' => $this->cleanAmount($request->get('typical_monthly_income')),
			'typical_monthly_currency' => $request->get('typical_monthly_currency'),
			'probable_monthly_income' => $this->cleanAmount($request->get('probable_monthly_income')),
			'probable_monthly_currency' => $request->get('probable_monthly_currency'),
			'received_transfer_country' => $request->get('received_transfer_country'),
			'received_transfer_sender' => $request->get('received_transfer_sender'),
			'received_transfer_reason' => $request->get('received_transfer_reason'),
			'sent_transfer_country' => $request->get('sent_transfer_country'),
			'sent_transfer_sender' => $request->get('sent_transfer_sender'),
			'sent_transfer_reason' => $request->get('sent_transfer_reason'),
			'remittance_country' => $request->get('remittance_country'),
			'remittance_sender' => $request->get('remittance_sender'),
			'remittance_sender_relationship' => $request->get('remittance_sender_relationship'),
			'remittance_reason' => $request->get('remittance_reason'),
		]);
	}

	protected function storedProfessionalDependants($response, $productName = null)
	{
		return [
				'name' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DependenciaProfesionalNombre'),
				'relationship' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DependenciaProfesionalParentesco'), 'beneficiaryOptions'),
				'id_type' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DependenciaProfesionalTipoIdentificacion'), 'idOptions'),
				'job_type' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DependenciaProfesionalActividadComercial'), 'jobTypeOptions'),
				'identity' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DependenciaProfesionalIdentificacion'),
				'income' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'IngresoMensual'),
			];
	}

	protected function storedInternationalOperationsInformation($response, $productName = null)
	{
		return [
			'foreign_currency' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'RealizaOperacionesMonedaExtranjera'), 'confirmation'),

			'wire_transfers' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'TransferenciasInternacionales'), 'confirmation'),

			'currency_purchase' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'CompraVentaDivisa'), 'confirmation'),

			'checks_in_dollars' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'EmisionGirosDolarChequesDolares'), 'confirmation'),

			'remittances' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'RecibiraRemesasFamiliares'), 'confirmation'),

			'typical_monthly_income' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOIMontoMensualTipicoEstimado'),

			'typical_monthly_currency' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOIMonedaMensualTipicoEstimado'), 'transfersCurrency'),

			'probable_monthly_income' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOIMontoMensualEsperadoEventual'),

			'probable_monthly_currency' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOIMonedaMensualEsperadoEventual'), 'transfersCurrency'),

			'received_transfer_country' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOITransferenciasRecibidasPaisOrigen'), 'nations'),

			'received_transfer_sender' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOITransferenciasRecibidasNombreRemitente'),
			'received_transfer_reason' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOITransferenciasRecibidasMotivoTransferencia'),

			'sent_transfer_country' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOITransferenciasEnviadasPaisDestino'), 'nations'),

			'sent_transfer_sender' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOIRemesasRecibidasNombreRemitente'),

			'sent_transfer_reason' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOITransferenciasEnviadasMotivoTransferencia'),

			'remittance_country' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOIRemesasRecibidasPaisOrigen'), 'nations'),

			'remittance_sender' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOIRemesasRecibidasRelacionRemitente'),

			'remittance_sender_relationship' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'RecibiraRemesasFamiliares'), 'referenceOptions'),

			'remittance_reason' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'IOIRemesasRecibidasMotivoRemesa'),
		];
	}

	protected function storedBeneficiaries($response, $correlative, $productName = null)
	{
		return [
			'name' => $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'BENEFICIARIOS_LIST', 'BENEFICIARIO_' . $correlative, 'BeneficiarioNombre'),

			'relationship' => $this->searchValueFromList($productName,  $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'BENEFICIARIOS_LIST', 'BENEFICIARIO_' . $correlative, 'BeneficiarioParentesco'), 'beneficiaryOptions'),

			'id_type' => $this->searchValueFromList($productName,  $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'BENEFICIARIOS_LIST', 'BENEFICIARIO_' . $correlative, 'BeneficiarioTipoIdentificacion'), 'idOptions'),

			'identity' => $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'BENEFICIARIOS_LIST', 'BENEFICIARIO_' . $correlative, 'BeneficiarioIdentidad'),
		];
	}

	protected function storedReferences($response, $correlative, $productName = null)
	{
		return [
			'name' => $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'REFERENCIAS_LIST', 'REFERENCIA_' . $correlative, 'ReferenciaNombre'),

			'relationship' => $this->searchValueFromList($productName, $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'REFERENCIAS_LIST', 'REFERENCIA_' . $correlative, 'ReferenciaParentesco'), 'referenceOptions'),

			'mobile' => $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'REFERENCIAS_LIST', 'REFERENCIA_' . $correlative, 'ReferenciaCelular'),
			'phone' => $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'REFERENCIAS_LIST', 'REFERENCIA_' . $correlative, 'ReferenciaTelefono'),
			'work_phone' => $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'REFERENCIAS_LIST', 'REFERENCIA_' . $correlative, 'ReferenciaTelefonoTrabajo'),
			'address' => $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'REFERENCIAS_LIST', 'REFERENCIA_' . $correlative, 'ReferenciaDireccionDomicilio'),
		];
	}

	protected function storedNationalInstitutions($response, $correlative, $productName = null)
	{
		return [
			'name' => $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'CUENTAS_INSTITUCIONES_NACIONALES_LIST', 'INSTITUCION_' . $correlative, 'InstitucionNombre'),
		];
	}

	protected function storedForeignInstitutions($response, $correlative, $productName = null)
	{
		return [
			'name' => $this->getFourthLevelValue($response, 'INFORMACION_CUENTA', 'CUENTAS_INSTITUCIONES_INTERNACIONALES_LIST', 'INSTITUCION_' . $correlative, 'InstitucionNombre'),
		];
	}

	protected function storedAccountInformation($response, $correlative = null, $productName = null)
	{
		return [
			'account_type' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'TipoCuenta'), 'accountOptions'),
			'account_number' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'NumeroCuenta'),
		];
	}

    /**
     * Get job status
     *
     * @param $request
     * @param $productName
     * @return array
     */
    protected function getJobStatus($request, $productName): array
    {
        if ($request->job === '1') {
            $jobStatus = $this->searchValueFromList($productName, $request->job_status, 'jobOptions');
        } else {
	            $jobStatus = [
	                'code' => 'P',
	                'value' => 'P',
	            ];
        }

        return $jobStatus;
    }

    /**
     * Return second level value
     *
     * @param $response
     * @param $listName
     * @param $field
     * @param null $default
     * @return null|string
     */
    protected function getSecondLevelValue($response, $listName, $field, $default = null)
    {
        if ($default && $this->getResponse->secondLevelResult($response, $listName, $field) == 0) {
            return null;
        } else {
            return $this->getResponse->secondLevelResult($response, $listName, $field);
        }
    }


    /**
     * Return third level value
     *
     * @param $response
     * @param $firstLevelField
     * @param $secondLevelField
     * @param $field
     * @return null|void
     */
    protected function getThirdLevelValue($response, $firstLevelField, $secondLevelField, $field)
    {
        return $this->getResponse->thirdLevelResult($response, $firstLevelField, $secondLevelField, $field);
    }

	/**
	 * Return fourth level value
	 *
	 * @param $response
	 * @param $firstLevelField
	 * @param $secondLevelField
	 * @param $thirdLevelField
	 * @param $field
	 * @return null|void
	 */
	protected function getFourthLevelValue($response, $firstLevelField, $secondLevelField, $thirdLevelField, $field)
	{
		return $this->getResponse->fourthLevelResult($response, $firstLevelField, $secondLevelField, $thirdLevelField, $field);
	}

    /**
     * Check if input defines if customer has dependants
     *
     * @param $response
     * @param $productName
     * @return array
     */
    protected function dependants($response, $productName)
    {
    	$firstDependant = $this->getThirdLevelValue($response, 'DEPENDIENTES_LIST', 'DEPENDIENTE_1', 'DependienteNombre');

    	$dependants = [];

	    $code = 'N';

    	if ($firstDependant !== 'NO TIENE' && isset($firstDependant))
	    {
	    	$code = 'S';
	    }

    	$dependants['code'] = $code;
    	$dependants['value'] = $this->searchValueFromList($productName, $code, 'confirmation', false);
    	$dependants[1]['name'] = $firstDependant;

    	for ($i = 2; $i <= 6; $i++)
	    {
		    $dependants[$i]['name'] = $this->getThirdLevelValue($response, 'DEPENDIENTES_LIST', 'DEPENDIENTE_' . $i, 'DependienteNombre');
	    }

        return $dependants;
    }

    /**
     * Construct a full address with recovered fields if they exists
     *
     * @param $response
     * @param $addressField
     * @param $referenceField
     * @param $complementField
     * @return null|string
     */
    protected function constructFullAddress($response, $addressField, $referenceField, $complementField)
    {
        $address = $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', $addressField);

        $reference = $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', $referenceField);

        $complement = $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', $complementField);

        $fullAddress = $address;

        if ($reference) {
            $fullAddress .= ' ' . $reference;
        }

        if ($complement) {
            $fullAddress .= ' ' . $complement;
        }

        return $fullAddress;
    }

	/**
	 * @param $amount
	 * @return bool|mixed|string
	 */
	protected function cleanAmount($amount)
	{
		$cleanedAmount = str_replace(',', '', $amount);

		return substr($cleanedAmount, 0, strpos($cleanedAmount, '.'));
	}

	/**
	 * Create date from format
	 *
	 * @param $format
	 * @param $parameter
	 * @return int|null
	 */
	protected function generateDate($format, $parameter)
	{
		if ($parameter)
		{
			return Date::createFromFormat($format, $parameter)->timestamp;
		}

		return null;
	}

	/**
	 * Create the fullname with available fields
	 *
	 * @param $firstname
	 * @param $middlename
	 * @param $lastname
	 * @param $secondlastname
	 * @return string
	 */
	protected function fullname($firstname, $middlename, $lastname, $secondlastname) : string
	{
		$name = $firstname;

		if ($middlename) {
			$name .= ' ' . $middlename;
		}

		$name .= ' ' . $lastname;

		if ($secondlastname) {
			$name .= ' ' . $secondlastname;
		}

		return $name;
	}

	protected function updateIfInRequest($field, $parameter)
	{
		if (isset($parameter) && $parameter !== 'NULO')
		{
			return $parameter;
		}

		return $field;
	}

	protected function value($data, $values, $field, $fieldValues = null)
	{
		$parameter = $field;

		if ($fieldValues)
		{
			$parameter = get_json($field, $fieldValues);
		}

		return $this->updateIfInRequest(
				get_json($data, $values),
				$parameter
			);
	}

	/**
	 * Search input from request in product list
	 * and return value or code|value array
	 *
	 * @param $productName
	 * @param $data
	 * @param $values
	 * @param $field
	 * @param $lists
	 * @param bool $array
	 * @return mixed
	 */
	protected function codeValue($productName, $data, $values, $field, $lists, $array = true)
	{
		return $this->searchValueFromList(
			$productName,
			$this->updateIfInRequest(
				get_json($data, $values),
				$field
			),
			$lists,
			$array
		);
	}
}
