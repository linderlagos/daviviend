<?php

namespace App\Core;

use App\Core\Traits\Bridge;
use App\Core\Traits\HasData;
use App\Core\Traits\LogException;
use App\Core\Traits\Message;
use App\Core\Traits\Peoplesoft;
use App\Core\Traits\ProductParameters;

class SearchCustomer
{
	use HasData,
		Peoplesoft,
		Bridge,
		ProductParameters;

	public function searchCustomer($identity, $bank, $origin, $productName)
	{
		$response = $this->soap(
			$productName,
			$bank,
			$origin,
			$this->parameters($identity, $productName),
			'SEARCH_APPROVAL_DATA_PARAMS'
		);

		return $response;
	}

	/**
	 * Construct de data object for Search Process
	 *
	 * @param $identity
	 * @param $productName
	 * @return array
	 */
	private function parameters($identity, $productName) : array
	{
		$identity = str_replace('-', '', $identity);

		$productParameters = $this->productParameters($productName);

		$code = $productParameters['product_code'];
		$branch = $productParameters['user']['branch'];
		$agent = $productParameters['user']['agent'];
		$channel = $productParameters['channel'];

		return [
			$this->hasData('Identificacion', $identity),

			/*
			 * Static parameters
			 */
			new Data(true, 'TipoIdentificacion', null, 0),
			$this->hasData('OrigenFlujoCredito', 'A'),
			$this->hasData('ProductoOrigenRegistro', $code),
			$this->hasData('CanalOrigenRegistro', $channel),
			$this->hasData('Agencia', $branch),
			$this->hasData('GestorOficial', $agent),

			/*
			 * User information
			 */
			$this->peopleSoft(),
			new Data(true, 'Ip', null, request()->ip()),
		];
	}
}