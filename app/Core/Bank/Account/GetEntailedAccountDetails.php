<?php

namespace App\Core\Bank\Account;

use App\Core\GetResponse;

/**
 * Class GetApprovedCardDetails
 * @package App\Core
 */
class GetEntailedAccountDetails
{
    public function details($response, $flow)
    {
        $getValueFromResponse = new GetResponse();

        if(!env('BANK_ACCOUNT_DEMO'))
        {
            // Customer information
            $customer = $flow->customer_information;

            $customer['cif']['value'] = $getValueFromResponse->result($response, 'CIF');

            $customer['economic_activity']['code'] = $getValueFromResponse->result($response, 'ActividadEconomicaCodigo');
            $customer['economic_activity']['value'] = $getValueFromResponse->result($response, 'ActividadEconomicaDescripcion');

            $customer['customer_risk'] = $getValueFromResponse->result($response, 'NivelRiesgoCliente');

            $customer['birth_country']['code'] = $getValueFromResponse->result($response, 'PaisNacimientoCodigo');
            $customer['birth_country']['value'] = $getValueFromResponse->result($response, 'PaisNacimientoDescripcion');

            $customer['birthplace'] = $getValueFromResponse->result($response, 'LugarNacimiento');

            $customer['income_range']['code'] = $getValueFromResponse->result($response, 'RangoIngresosCodigo');
            $customer['income_range']['initial'] = $getValueFromResponse->result($response, 'RangoIngresosLimiteInicial');
            $customer['income_range']['end'] = $getValueFromResponse->result($response, 'RangoIngresosLimiteFinal');

            $customer['income_source'] = $getValueFromResponse->result($response, 'CuentaFuenteIngresos');

            // Product information
            $product = $flow->product_information;

            $accountNumber = $getValueFromResponse->result($response, 'NumeroCuenta');

            $product['product']['value'] = $accountNumber;
            $product['debit_card']['code'] = $getValueFromResponse->result($response, 'NumeroTarjetaDebito');
            $product['debit_card']['value'] = $getValueFromResponse->result($response, 'NombreTarjetaDebito');
            $product['purpose'] = $getValueFromResponse->result($response, 'CuentaProposito');
            $product['expected_deposits'] = $getValueFromResponse->result($response, 'CuentaDepositosEsperados');
            $product['expected_withdrawals'] = $getValueFromResponse->result($response, 'CuentaRetirosEsperados');

            // User information
            $user = $flow->user_information;

            $user['branch']['code'] = $getValueFromResponse->result($response, 'ContratoAgenciaCodigo');
            $user['branch']['value'] = $getValueFromResponse->result($response, 'ContratoAgenciaDescripcion');
            $user['branch']['city'] = $getValueFromResponse->result($response, 'ContratoCiudad');

            $user['agent']['code'] = $getValueFromResponse->result($response, 'GestorCodigo');
            $user['agent']['value'] = $getValueFromResponse->result($response, 'GestorNombre');


         } else {

             // Customer information
             $customer = $flow->customer_information;

             $customer['cif']['value'] = ' 100'. rand(0000000, 9999999);

             $customer['economic_activity']['code'] = '1';
             $customer['economic_activity']['value'] = 'ASALARIADO';

             $customer_risk = rand(1,10);
             $customer['customer_risk'] = ($customer_risk == 1)? 'M' : 'B';

            $customer['birth_country']['code'] = '504';
            $customer['birth_country']['value'] = 'HONDURAS';

            $customer['birthplace'] = 'HONDURAS';

            $customer['income_range']['code'] = '1';
            $customer['income_range']['initial'] = '00.00 - 40000.00';
            $customer['income_range']['end'] = '00.00 - 45000.00';

            $customer['income_source'] = 'SALARIO';

            // Product information
            $product = $flow->product_information;

             $accountNumber = '100' . rand(1000000, 9999999);
	        $debitCardCode = rand(100000000, 999999999);

            $product['product']['value'] = $accountNumber;
            $product['debit_card']['code'] = $accountNumber;
            $product['debit_card']['value'] = $debitCardCode;
            $product['purpose'] = 'AHORRO';
            $product['expected_deposits'] = rand(100, 10000);
            $product['expected_withdrawals'] = rand(100, 5000);

            // User information
            $user = $flow->user_information;

            $agencyCode = rand(0, 2);
            $cityCode = rand(1, 150);

            $agency = [
                'SUC.TECNICA BANCA DE PERSONA ZONA CENTRO',
                'SUC.TECNICA BANCA DE PERSONA ZONA SUR',
                'SUC.TECNICA BANCA DE PERSONA ZONA NORTE'
            ];

            $user['branch']['code'] = '120';
            $user['branch']['value'] = 'AGENCIA CITY MALL TEGUCIGALPA';
            $user['branch']['city'] = 'TEGUCIGALPA';


            $agentCode = rand(1, 3);
            $agent = [
                'MARTA AZUCENA MARTINEZ COLINDRES',
                'CARLOS JOEL FIGUEROA RAPALO',
                'DILCIA MONSERRATH CANO MENDOZA'
            ];

            $user['agent']['code'] = 'IS3';
            $user['agent']['value'] = $agent[$agencyCode];

         }


        $flow->update([
        	'identifier' => $accountNumber,
            'customer_information' => $customer,
	        'product_information' => $product,
	        'user_information' => $user
        ]);
    }


}
