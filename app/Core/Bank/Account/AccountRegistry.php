<?php

namespace App\Core\Bank\Account;

// Helpers
use Jenssegers\Date\Date;
use App\Core\SanitizeString;
use App\Core\CustomerRegistry;

/**
 * Class HandleCustomerRegistry
 * @package App\Core
 */
class AccountRegistry extends CustomerRegistry
{
	/**
	 * Get request parameters
	 *
	 * @var
	 */
	protected $request;

	/**
	 * Get form first step and update flow
	 *
	 * @param $request
	 * @return mixed
	 */
	public function second($request)
	{
		$flow = $this->recentFlow($this->productName(), true);

		$flow->update([
				'customer_information' => $this->secondCustomerInformation($flow, $request),
				'product_information' => $this->secondProductInformation($flow, $request)
			]);

		return $flow;
	}

    /**
     * Get form second step and update flow
     *
     * @param $request
     * @return mixed
     */
    public function third($request)
    {
	    $flow = $this->recentFlow($this->productName(), true);

	    $flow->update([
		    'customer_information' => $this->thirdCustomerInformation($flow, $request)
	    ]);

	    return $flow;
    }

    /**
     * Get form third step and update flow
     *
     * @param $request
     * @return mixed
     */
    public function fourth($request)
    {
	    $flow = $this->recentFlow($this->productName(), true);

	    $flow->update([
		    'customer_information' => $this->fourthCustomerInformation($flow, $request)
	    ]);

	    return $flow;
    }

    /**
     * Get form fourth step and update flow
     *
     * @param $request
     * @return mixed
     */
    public function fifth($request)
    {
	    $flow = $this->recentFlow($this->productName(), true);

	    $flow->update([
		    'customer_information' => $this->fifthCustomerInformation($flow, $request)
	    ]);

	    return $flow;
    }

	/**
	 * Get form fifth step and update flow
	 *
	 * @param $request
	 * @return mixed
	 */
	public function sixth($request)
    {
	    $flow = $this->recentFlow($this->productName(), true);

	    $flow->update([
		    'customer_information' => $this->sixthCustomerInformation($flow, $request)
	    ]);

	    return $flow;
    }

	/**
	 * Recover customer information from form first step
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function secondCustomerInformation($flow, $request)
    {
    	$data = $flow->customer_information;

	    $r = SanitizeString::clean($request->except(['city', 'employer_city']));

    	$data['name']['first'] = $this->value($data, ['name', 'first'], $r, 'first_name');
    	$data['name']['middle'] = $this->value($data, ['name', 'middle'], $r, 'middle_name');
    	$data['name']['last'] = $this->value($data, ['name', 'last'], $r, 'last_name');
    	$data['name']['second_last'] = $this->value($data, ['name', 'second_last'], $r, 'second_last_name');

    	$data['name']['fullname'] = $this->fullname(
		    $this->value($data, ['name', 'first'], $r, 'first_name'),
		    $this->value($data, ['name', 'middle'], $r, 'middle_name'),
		    $this->value($data, ['name', 'last'], $r, 'last_name'),
		    $this->value($data, ['name', 'second_last'], $r, 'second_last_name')
	    );

    	$data['nationality'] = $this->codeValue($this->productName(), $data,  ['nationality', 'code'], $request->nationality, 'nations');

    	$data['employer']['job']['type'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'type', 'code'], $request->job_type, 'jobTypeOptions');

    	$fatca = $this->value($data, ['fatca', 'code'], $request->fatca);

    	$data['fatca'] = $this->codeValue($this->productName(), $data, ['fatca', 'code'], $fatca, 'confirmation');

	    return $data;
    }

	/**
	 * Recover product information from form first step
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function secondProductInformation($flow, $request)
	{
		$data = $flow->product_information;

		$data['product'] = $this->codeValue($this->productName(), $data, ['product', 'code'], $request->account_type, 'accountOptions');

		return $data;
    }

	/**
	 * Recover customer information from form second step
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function thirdCustomerInformation($flow, $request)
	{
		$data = $flow->customer_information;

		$r = SanitizeString::clean($request->except(['city', 'employer_city']));

		$receiveSms = 'N';

		if ($request->receive_sms) {
			$receiveSms = 'S';
		}

		$data['receive_sms'] = $this->codeValue($this->productName(), $data, ['receive_sms', 'code'], $receiveSms, 'confirmation');

		if (isset($r['year'], $r['month'], $r['day'])) {
			$data['birth'] = Date::createFromDate($r['year'], $r['month'], $r['day'])->timestamp;
		}

		if (isset($r['phone'])) {
			$data['phone'] = str_replace('-', '', $r['phone']);
		}

		if (isset($r['mobile'])) {
			$data['mobile'] = str_replace('-', '', $r['mobile']);
		}

		if($request->has_email === null)
		{
			$data['email']['has_email'] = 'S';
		} else {
			$data['email']['has_email'] = 'N';//$this->value($data, ['email', 'has_email'], $request->has_email);
		}

		$receiveEmail = 'N';

		if ($request->has_email === 'N') {
			$data['email']['account'] = 'NOTIENE@DAVIVIENDA.COM.HN';
		} else {
			$data['email']['account'] = $this->value($data, ['email', 'account'], $r, 'email');

			if ($request->receive_emails) {
				$receiveEmail = 'S';
			}
		}

		$data['receive_emails'] = $this->codeValue($this->productName(), $data, ['receive_emails', 'code'], $receiveEmail, 'confirmation');

		$data['address']['other_city'] = $this->value($data, ['address', 'other_city'], $r, 'other_city');

		$cityCode = $this->value($data, ['address', 'code'], $request->city);

		$addressLineOne = $this->value($data, ['address', 'first'], $r, 'address_1');
		$addressLineTwo = $this->value($data, ['address', 'second'], $r, 'address_2');
		$addressReference = $this->value($data, ['address', 'third'], $r, 'address_3');

		$address = $this->constructRequestFullAddress($addressLineOne, $addressLineTwo, $addressReference);

		$data['address']['code'] = $cityCode;
		$data['address']['full'] = $address;

		$data['address']['first'] = $addressLineOne;
		$data['address']['second'] = $addressLineTwo;
		$data['address']['third'] = $addressReference;

		$city = explode('>', $cityCode);
		$state = explode('-', $city[0]);
		$municipality = explode('-', $city[1]);
		$colony = explode('-', $city[2]);

		$data['address']['state'] = $state[0];
		$data['address']['state_code'] = $state[1];
		$data['address']['municipality'] = $municipality[0];
		$data['address']['municipality_code'] = $municipality[1];
		$data['address']['colony'] = $colony[0];
		$data['address']['colony_code'] = $colony[1];

		$data['gender'] = $this->codeValue($this->productName(), $data, ['gender', 'code'], $request->gender, 'genderOptions');

		$data['marital_status'] = $this->codeValue($this->productName(), $data, ['marital_status', 'code'], $request->marital_status, 'maritalStatusOptions');

		$data['spouse'] = $this->value($data, 'spouse', $r, 'spouse');

		if ($data['marital_status']['code'] === 'S')
		{
			$data['spouse'] = null;
		}

		return $data;
	}

	/**
	 * Recover customer information from form third step
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function fourthCustomerInformation($flow, $request)
	{
		$data = $flow->customer_information;

		// Clean request to prepare personalized inputs
		$r = SanitizeString::clean($request->all());

		if (isset($request->income)) {
			$data['income'] = $this->cleanAmount($r['income']);
		}

		$publicJob = $this->value($data, ['public_job', 'code'],$request->public_job);

		$data['public_job']['code'] = $publicJob;
		$data['public_job']['value'] = $this->searchValueFromList($this->productName(), $publicJob, 'confirmation', false);

		if ($publicJob === 'N')
		{
			$data['public_job']['employer_name'] = null;
			$data['public_job']['job'] = null;
			$data['public_job']['active'] = null;
			$data['public_job']['from'] = null;
			$data['public_job']['to'] = null;
		} else {
			$data['public_job']['employer_name'] = $this->value($data, ['public_job', 'employer_name'], $r, 'public_job_employer_name');

			$data['public_job']['job'] = $this->value($data, ['public_job', 'employer_name'], $r, 'public_job_name');

			$publicJobActive = $this->value($data, ['public_job', 'active'], $r, 'public_job_active');

			$data['public_job']['active'] = $publicJobActive;
			$data['public_job']['active_value'] = $this->searchValueFromList($this->productName(), $publicJobActive, 'confirmation', false);

			$data['public_job']['from'] = Date::createFromFormat('dmY', $r['public_job_from'])->timestamp;

			$data['public_job']['to'] = null;

			if ($publicJobActive === 'N')
			{
				$data['public_job']['to'] = Date::createFromFormat('dmY', $r['public_job_to'])->timestamp;
			}
		}

		if (isset($request->employer_year, $request->employer_month, $request->employer_day)) {
			$data['employer']['started_at'] = Date::createFromDate($r['employer_year'], $r['employer_month'], $r['employer_day'])->timestamp;
		}

		$data['profession'] = $this->codeValue($this->productName(), $data, ['profession', 'code'], $request->profession, 'professionOptions');

		$data['employer']['name'] = $this->value($data, ['employer', 'name'], $r, 'employer_name');

		if (isset($r['employer_phone']))
		{
			$data['employer']['phone'] = str_replace('-', '', $r['employer_phone']);
		}

		$data['employer']['type'] = $this->codeValue($this->productName(), $data, ['employer', 'type', 'code'], $request->employer_type, 'employeeTypes');

		$data['employer']['job']['name'] = $this->value($data, ['employer', 'job', 'name'], $r, 'job_name');

		if($request->has_job_email === null )
		{
			$data['job_email']['has_job_email'] = 'S';
		} else {
			$data['job_email']['has_job_email'] = 'N';
		}

		$receiveEmailJob = 'N';

		if ($request->has_job_email !== null) {
			$data['job_email']['account'] = 'NOTIENE@DAVIVIENDA.COM.HN';
		} else {
			$data['job_email']['account'] = $this->value($data, ['job_email', 'account'], $r, 'job_email');

			if ($request->receive_emails_job) {
				$receiveEmailJob = 'S';
			}
		}

		$data['receive_emails_job'] = $this->codeValue($this->productName(), $data, ['receive_emails_job', 'code'], $receiveEmailJob, 'confirmation');

		$codeTypeEmployer = get_json($flow->customer_information, ['employer', 'job', 'type', 'code']);

		if(!in_array($codeTypeEmployer,['10', '11', '44'])) {

            $data['employer']['job']['status'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'status', 'code'], $request->job, 'jobOptions');

            $data['employer']['address']['other_city'] = $this->value($data, ['employer', 'address', 'other_city'], $r, 'other_employer_city');

            $employerCityCode = $this->value($data, ['employer', 'address', 'code'], $request->employer_city);

            $employerAddressLineOne = $this->value($data, ['employer', 'address', 'first'], $r, 'employer_address_1');
            $employerAddressLineTwo = $this->value($data, ['employer', 'address', 'second'], $r, 'employer_address_2');
            $employerAddressReference = $this->value($data, ['employer', 'address', 'third'], $r, 'employer_address_3');

            $employerAddress = $this->constructRequestFullAddress($employerAddressLineOne, $employerAddressLineTwo, $employerAddressReference);

            $data['employer']['address']['code'] = $employerCityCode;

            $data['employer']['address']['full'] = $employerAddress;

            $data['employer']['address']['first'] = $employerAddressLineOne;
            $data['employer']['address']['second'] = $employerAddressLineTwo;
            $data['employer']['address']['third'] = $employerAddressReference;

            $employerCity = explode('>', $employerCityCode);
            $employerState = explode('-', $employerCity[0]);
            $employerMunicipality = explode('-', $employerCity[1]);
            $employerColony = explode('-', $employerCity[2]);

            $data['employer']['address']['state'] = $employerState[0];
            $data['employer']['address']['state_code'] = $employerState[1];
            $data['employer']['address']['municipality'] = $employerMunicipality[0];
            $data['employer']['address']['municipality_code'] = $employerMunicipality[1];
            $data['employer']['address']['colony'] = $employerColony[0];
            $data['employer']['address']['colony_code'] = $employerColony[1];
        }

		$data['assets'] = $this->codeValue($this->productName(), $data, ['assets', 'code'],  $request->assets, 'assetRangeOptions');

		$data['passive'] = $this->codeValue($this->productName(), $data, ['passive', 'code'], $request->passive, 'liabilityRangeOptions');

		$data['expenses'] = $this->codeValue($this->productName(), $data, ['expenses', 'code'], $request->expenses, 'expenseRangeOptions');

		$data['other_income'] = $this->codeValue($this->productName(), $data, ['other_income', 'code'], $request->other_income, 'confirmation');

		$jobType = $data['employer']['job']['type']['code'];

		if (in_array($jobType, ['10', '11', '44']))
		{
			$data['professional_dependant']['name'] = $this->value($data, ['professional_dependant', 'name'], $r, 'professional_dependant_name');

			$data['professional_dependant']['relationship'] = $this->codeValue($this->productName(), $data, ['professional_dependant', 'relationship', 'code'], $request->professional_dependant_relationship, 'beneficiaryOptions');

			$data['professional_dependant']['job_type'] = $this->codeValue($this->productName(), $data, ['professional_dependant', 'job_type', 'code'], $request->professional_dependant_job_type, 'jobTypeOptions');

			$data['professional_dependant']['id_type'] = $this->codeValue($this->productName(), $data, ['professional_dependant', 'id_type', 'code'], $request->professional_dependant_id_type, 'idOptions');

			$data['professional_dependant']['identity'] = $this->value($data, ['professional_dependant', 'identity'], $r, 'professional_dependant_identity');

			$data['profesional_dependant']['income'] = $this->cleanAmount($r['income']);

		} else {
			$data['professional_dependant']['name'] = null;
			$data['professional_dependant']['relationship'] = $this->searchValueFromList($this->productName(), null, 'beneficiaryOptions');
			$data['professional_dependant']['job_type'] = $this->searchValueFromList($this->productName(), null, 'jobTypeOptions');

			$data['professional_dependant']['id_type'] = $this->searchValueFromList($this->productName(), null, 'idOptions');
			$data['professional_dependant']['identity'] = null;
		}

		return $data;
	}

	/**
	 * Recover customer information from form fourth step
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function fifthCustomerInformation($flow, $request)
	{
		$data = $flow->customer_information;

		// Clean request to prepare personalized inputs
		$r = SanitizeString::clean($request->all());

		for ($i = 1; $i <= 10; $i++)
		{
			if ($request->get('beneficiary_' . $i . '_name'))
			{
				$idType = 999;
				$identity = null;

				if ($request->filled('beneficiary_' . $i . '_identity'))
				{
					$idType = $request->get('beneficiary_' . $i . '_id_type');
					$identity = $request->get('beneficiary_' . $i . '_identity');
				}

				$data['beneficiaries'][$i]['name'] = $this->value($data, ['beneficiaries', $i, 'name'], $r, 'beneficiary_' . $i . '_name');

				$relationship = $request->get('beneficiary_' . $i . '_relationship');

				$data['beneficiaries'][$i]['relationship'] = $this->codeValue($this->productName(), $data, ['beneficiaries', $i, 'relationship', 'code'], $relationship, 'beneficiaryOptions');

				$data['beneficiaries'][$i]['id_type'] = $this->codeValue($this->productName(), $data, ['beneficiaries', $i, 'id_type', 'code'], $idType, 'idOptions');

				$data['beneficiaries'][$i]['identity'] = $identity;
			}
		}

		for ($i = 1; $i <= 2; $i++)
		{
			$data['references'][$i]['name'] = $this->value($data, ['references', $i, 'name'], $r, 'reference_' . $i . '_name');

			if ($request->get('reference_' . $i . '_mobile'))
			{
				$data['references'][$i]['mobile'] = str_replace('-', '', $r['reference_' . $i . '_mobile']);
			}

			if ($request->get('reference_' . $i . '_phone'))
			{
				$data['references'][$i]['phone'] = str_replace('-', '', $r['reference_' . $i . '_phone']);
			}
		}

		return $data;
	}

	/**
	 * Recover customer information from form fifth step
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function sixthCustomerInformation($flow, $request)
	{
		$data = $flow->customer_information;

		// Clean request to prepare personalized inputs
		$r = SanitizeString::clean($request->all());

		$foreignCurrency = $this->value($data, ['international_operations_information', 'foreign_currency', 'code'], $request->foreign_currency);

		$data['international_operations_information']['foreign_currency'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'foreign_currency', 'code'], $foreignCurrency, 'confirmation');

		if ($foreignCurrency === 'N')
		{
			$data['international_operations_information']['wire_transfers']['code'] = null;
			$data['international_operations_information']['wire_transfers']['value'] = null;
			$data['international_operations_information']['currency_purchase']['code'] = null;
			$data['international_operations_information']['currency_purchase']['value'] = null;
			$data['international_operations_information']['checks_in_dollars']['code'] = null;
			$data['international_operations_information']['checks_in_dollars']['value'] = null;
			$data['international_operations_information']['remittances']['code'] = null;
			$data['international_operations_information']['remittances']['value'] = null;
			$data['international_operations_information']['typical_monthly_income'] = null;
			$data['international_operations_information']['typical_monthly_currency']['code'] = null;
			$data['international_operations_information']['typical_monthly_currency']['value'] = null;
			$data['international_operations_information']['probable_monthly_income'] = null;
			$data['international_operations_information']['probable_monthly_currency'] = null;
			$data['international_operations_information']['received_transfer_country'] = null;
			$data['international_operations_information']['received_transfer_sender'] = null;
			$data['international_operations_information']['received_transfer_reason'] = null;
			$data['international_operations_information']['sent_transfer_country']['code'] = null;
			$data['international_operations_information']['sent_transfer_country']['value'] = null;
			$data['international_operations_information']['sent_transfer_sender'] = null;
			$data['international_operations_information']['sent_transfer_reason'] = null;
			$data['international_operations_information']['remittance_country']['code'] = null;
			$data['international_operations_information']['remittance_country']['value'] = null;
			$data['international_operations_information']['remittance_sender'] = null;
			$data['international_operations_information']['remittance_sender_relationship']['code'] = null;
			$data['international_operations_information']['remittance_sender_relationship']['value'] = null;
			$data['international_operations_information']['remittance_reason'] = null;
		} else {
			$wireTransfers = 'N';

			if ($request->wire_transfers)
			{
				$wireTransfers = $this->value($data, ['international_operations_information', 'wire_transfers', 'code'], $request->wire_transfers);
			}

			$data['international_operations_information']['wire_transfers'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'wire_transfers', 'code'],  $wireTransfers, 'confirmation');

			if ($wireTransfers === 'N')
			{
				$data['international_operations_information']['received_transfer_country'] = null;
				$data['international_operations_information']['received_transfer_sender'] = null;
				$data['international_operations_information']['received_transfer_reason'] =  null;
				$data['international_operations_information']['sent_transfer_country']['code'] = null;
				$data['international_operations_information']['sent_transfer_country']['value'] = null;
				$data['international_operations_information']['sent_transfer_sender'] = null;
				$data['international_operations_information']['sent_transfer_reason'] = null;
			} else {

				$data['international_operations_information']['received_transfer_country'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'received_transfer_country', 'code'], $request->received_transfer_country, 'nations');

				$data['international_operations_information']['received_transfer_sender'] = $this->value($data, ['international_operations_information', 'received_transfer_sender'], $r, 'received_transfer_sender');

				$data['international_operations_information']['received_transfer_reason'] =  $this->value($data, ['international_operations_information', 'received_transfer_reason'], $r, 'received_transfer_reason');

				$data['international_operations_information']['sent_transfer_country'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'sent_transfer_country', 'code'], $request->sent_transfer_country, 'nations');

				$data['international_operations_information']['sent_transfer_sender'] = $this->value($data, ['international_operations_information', 'sent_transfer_sender'], $r, 'sent_transfer_sender');

				$data['international_operations_information']['sent_transfer_reason'] = $this->value($data, ['international_operations_information', 'sent_transfer_reason'], $r, 'sent_transfer_reason');
			}

			$currencyPurchase = 'N';

			if($request->currency_purchase)
			{
				$currencyPurchase = $this->value($data, ['international_operations_information', 'currency_purchase', 'code'], $request->currency_purchase);
			}


			$data['international_operations_information']['currency_purchase'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'currency_purchase', 'code'], $currencyPurchase, 'confirmation');

			$checksInDollars = 'N';

			if($request->checks_in_dollars)
			{
				$checksInDollars = $this->value($data, ['international_operations_information', 'checks_in_dollars', 'code'], $request->checks_in_dollars);
			}

			$data['international_operations_information']['checks_in_dollars'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'checks_in_dollars', 'code'], $checksInDollars, 'confirmation');

			$data['international_operations_information']['currency_purchase'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'currency_purchase', 'code'], $currencyPurchase, 'confirmation');

			$checksInDollars = 'N';


			if($request->checks_in_dollars)
			{
				$checksInDollars = $this->value($data, ['international_operations_information', 'checks_in_dollars', 'code'], $request->checks_in_dollars);
			}

			$data['international_operations_information']['checks_in_dollars'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'checks_in_dollars', 'code'], $checksInDollars, 'confirmation');

			if (isset($request->typical_monthly_income))
			{
				$data['international_operations_information']['typical_monthly_income'] = $this->cleanAmount($request->get('typical_monthly_income'));
			}

			$data['international_operations_information']['typical_monthly_currency'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'typical_monthly_currency', 'code'], $request->typical_monthly_currency, 'transfersCurrency');

			if (isset($request->probable_monthly_income)) {
				$data['international_operations_information']['probable_monthly_income'] = $this->cleanAmount($request->get('probable_monthly_income'));
			}

			$data['international_operations_information']['probable_monthly_currency'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'probable_monthly_currency', 'code'], $request->probable_monthly_currency, 'transfersCurrency');

			$remittances = 'N';

			if ($request->remittances)
			{
				$remittances = $this->value($data, ['international_operations_information', 'remittances', 'code'], $request->remittances);
			}

			$data['international_operations_information']['remittances'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'remittances', 'code'], $remittances, 'confirmation');

			if ($remittances === 'N')
			{
				$data['international_operations_information']['remittance_country']['code'] = null;
				$data['international_operations_information']['remittance_country']['value'] = null;
				$data['international_operations_information']['remittance_sender'] = null;
				$data['international_operations_information']['remittance_sender_relationship']['code'] = null;
				$data['international_operations_information']['remittance_sender_relationship']['value'] = null;
				$data['international_operations_information']['remittance_reason'] = null;
			} else {
				$data['international_operations_information']['remittance_country'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'remittance_country', 'code'], $request->remittance_country, 'nations');

				$data['international_operations_information']['remittance_sender'] = $this->value($data, ['international_operations_information', 'remittance_sender'], $r, 'remittance_sender');

				$data['international_operations_information']['remittance_sender_relationship'] = $this->codeValue($this->productName(), $data, ['international_operations_information', 'remittance_sender_relationship', 'code'], $request->remittance_sender_relationship, 'referenceOptions');

				$data['international_operations_information']['remittance_reason'] = $this->value($data, ['international_operations_information', 'remittance_reason'], $r, 'remittance_reason');
			}
		}

		return $data;
	}

	/**
	 * Get product name
	 *
	 * @return string
	 */
	private function productName()
	{
		return 'bank_account';
	}
}
