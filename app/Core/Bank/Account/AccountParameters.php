<?php

namespace App\Core\Bank\Account;

// Helpers
use App\Core\Data;
use App\Core\Parameters;
use Jenssegers\Date\Date;

/**
 * Class EntailmentProcessParameters
 * @package App\Core
 */
class AccountParameters extends Parameters
{
	/**
	 * Construct de data object for Review Process
	 *
	 * @param $flow
	 * @return array
	 */
	public function second($flow) : array
	{
		return [
			$this->hasData('Identificacion', $flow->customer->identifier),
			new Data(true, 'TipoIdentificacion', null, $flow->customer->identifier_type),
			$this->hasData('Nombre', $flow->customer_information['name']['fullname']),
			$this->hasData('Nacionalidad', $flow->customer_information['nationality']['code']),
			$this->hasData('ActividadComercial', $flow->customer_information['employer']['job']['type']['code']),
			$this->hasData('EsFatca', $flow->customer_information['fatca']['code']),

			/*
			 * Static parameters
			 */
			$this->hasData('OrigenFlujoCredito', 'A'),
			$this->hasData('ProductoOrigenRegistro', 'CTA'),
			$this->hasData('CanalOrigenRegistro', 'LBY'),
			$this->hasData('Agencia', '120'),
			$this->hasData('GestorOficial', 'T14'),

			/*
			 * User information
			 */
			$this->peopleSoft(),
			new Data(true, 'Ip', null, request()->ip()),
		];
	}

    /**
     * Construct de data object for Entailment Process
     *
     * @param $flow
     * @return array
     */
    public function sixth($flow) : array
    {
        $customer = $flow->customer_information;
        $product = $flow->product_information;

        return [
            new Data(
                true,
                'DATA_ENTAILMENT_ACCOUNT_PARAMS',
                [

                    /*
                     * Dynamic parameters
                     */
                    new Data(true, 'TipoIdentificacion', null, $flow->customer->identifier_type),
                    $this->hasData('Identificacion', $flow->customer->identifier),
                    $this->hasData('Nombre', $customer['name']['fullname']),
                    $this->hasData('ActividadComercial', $customer['employer']['job']['type']['code']),
                    $this->hasData('Nacionalidad', $customer['nationality']['code']),
                    $this->hasData('IngresoMensual', $customer['income']),
                    $this->hasData('Sexo', $customer['gender']['code']),
                    $this->hasData('EstadoCivil', $customer['marital_status']['code']),
                    $this->hasData('DireccionDomicilioCompletaColoniaCodigo',$customer['address']['colony_code']),
                    $this->hasData('DireccionDomicilioCompletaBarrioColonia', $this->getColony($customer['address']['colony_code'], $customer['address']['colony'], $customer['address']['other_city'])),
                    $this->hasData('DireccionDomicilioCompletaAvenidaBloqueZonaCalleCasa', $customer['address']['first']),
                    $this->hasData('DireccionDomicilioCompletaPuntoReferencia', $customer['address']['second']),
                    $this->hasData('DireccionDomicilioCompletaPuntoReferenciaComplemento', $customer['address']['third']),
                    $this->hasData('DireccionDomicilioCompletaDepartamentoCodigo', $customer['address']['state_code']),
                    $this->hasData('DireccionDomicilioCompletaMunicipioCodigo', $customer['address']['municipality_code']),
                    $this->hasData('DireccionDomicilioCompletaPaisCodigo', '1'),
                    $this->hasData('TelefonoDomicilio', $customer['phone']),
                    $this->hasData('Celular', $customer['mobile']),
                    $this->hasData('AutorizaEnvioMensajesCelular', $customer['receive_sms']['code']),
                    $this->hasData('CorreoPersonal', $customer['email']['account']),
	                $this->hasData('CorreoLaboral', $customer['job_email']['account']),
	                $this->hasData('AutorizaEnvioEstadosCuentaCorreoPersonal', $customer['receive_emails']['code']),
	                $this->hasData('AutorizaEnvioEstadosCuentaCorreoLaboral', $customer['receive_emails_job']['code']),
                    $this->hasData('FechaNacimientoAAAAMMDD', $this->getDate($customer['birth'])),
                    $this->hasData('PrimerNombre', $customer['name']['first']),
                    $this->hasData('SegundoNombre', $customer['name']['middle']),
                    $this->hasData('PrimerApellido', $customer['name']['last']),
                    $this->hasData('SegundoApellido', $customer['name']['second_last']),
                    $this->hasData('FechaIngresoLaboralAAAAMMDD', $this->getDate($customer['employer']['started_at'])),
                    $this->hasData('NombreEmpleadorLugarTrabajo', $customer['employer']['name']),
                    $this->hasData('TipoEmpresa', $customer['employer']['type']['code']),
                    $this->hasData('TelefonoEmpleadorLaboral', $customer['employer']['phone']),
                    $this->hasData('TipoEmpleo', $customer['employer']['job']['status']['code']),
                    $this->hasData('DireccionEmpleadorCompletaColoniaCodigo',$customer['employer']['address']['colony_code']),
                    $this->hasData('DireccionEmpleadorCompletaBarrioColonia', $this->getColony($customer['employer']['address']['colony_code'], $customer['employer']['address']['colony'], $customer['employer']['address']['other_city'])),
                    $this->hasData('DireccionEmpleadorCompletaAvenidaBloqueZonaCalleCasa', $customer['employer']['address']['first']),
                    $this->hasData('DireccionEmpleadorCompletaPuntoReferencia', $customer['employer']['address']['second']),
                    $this->hasData('DireccionEmpleadorCompletaPuntoReferenciaComplemento', $customer['employer']['address']['third']),
                    $this->hasData('DireccionEmpleadorCompletaDepartamentoCodigo', $customer['employer']['address']['state_code']),
                    $this->hasData('DireccionEmpleadorCompletaMunicipioCodigo', $customer['employer']['address']['municipality_code']),
                    $this->hasData('DireccionEmpleadorCompletaPaisCodigo', '1'),
                    $this->hasData('Profesion', $customer['profession']['code']),
                    $this->hasData('CodigoRangoTotalEgresos', $customer['expenses']['code']),
                    $this->hasData('CodigoRangoTotalActivos', $customer['assets']['code']),
                    $this->hasData('CodigoRangoTotalPasivos', $customer['passive']['code']),
                    $this->hasData('OtrosIngresosOtros', $customer['other_income']['code']),
                    $this->hasData('CargoLaboralPuestoTrabajo', $customer['employer']['job']['name']),
                    $this->hasData('ConyugeNombre', $customer['spouse']),
                    $this->hasData('CargosPublicosUltimos4Anhos', $customer['public_job']['code']),
                    $this->hasData('EsFatca', $customer['fatca']['code']),

                    // Professional dependants
                    $this->hasData('DependenciaProfesionalNombre', $customer['professional_dependant']['name']),
                    $this->hasData('DependenciaProfesionalParentesco', $customer['professional_dependant']['relationship']['code']),
                    $this->hasData('DependenciaProfesionalTipoIdentificacion', $customer['professional_dependant']['id_type']['code']),
                    $this->hasData('DependenciaProfesionalIdentificacion', $customer['professional_dependant']['identity']),
                    $this->hasData('DependenciaProfesionalActividadComercial', $customer['professional_dependant']['job_type']['code']),

                    // Foreign transfers
                    $this->hasData('RealizaOperacionesMonedaExtranjera', $customer['international_operations_information']['foreign_currency']['code']),
                    $this->hasData('TransferenciasInternacionales', $customer['international_operations_information']['wire_transfers']['code']),
                    $this->hasData('CompraVentaDivisa', $customer['international_operations_information']['currency_purchase']['code']),
                    $this->hasData('EmisionGirosDolarChequesDolares', $customer['international_operations_information']['checks_in_dollars']['code']),
                    $this->hasData('RecibiraRemesasFamiliares', $customer['international_operations_information']['remittances']['code']),
                    $this->hasData('IOIMontoMensualTipicoEstimado', $customer['international_operations_information']['typical_monthly_income']),
                    $this->hasData('IOIMonedaMensualTipicoEstimado', $customer['international_operations_information']['typical_monthly_currency']['code']),
                    $this->hasData('IOIMontoMensualEsperadoEventual', $customer['international_operations_information']['probable_monthly_income']),
                    $this->hasData('IOIMonedaMensualEsperadoEventual', $customer['international_operations_information']['probable_monthly_currency']['code']),
                    $this->hasData('IOITransferenciasRecibidasPaisOrigen', $customer['international_operations_information']['received_transfer_country']['code']),
                    $this->hasData('IOITransferenciasRecibidasNombreRemitente', $customer['international_operations_information']['received_transfer_sender']),
                    $this->hasData('IOITransferenciasRecibidasMotivoTransferencia', $customer['international_operations_information']['received_transfer_reason']),
                    $this->hasData('IOITransferenciasEnviadasPaisDestino', $customer['international_operations_information']['sent_transfer_country']['code']),
                    $this->hasData('IOITransferenciasEnviadasNombreBeneficiario', $customer['international_operations_information']['sent_transfer_sender']),
                    $this->hasData('IOITransferenciasEnviadasMotivoTransferencia', $customer['international_operations_information']['sent_transfer_reason']),
                    $this->hasData('IOIRemesasRecibidasPaisOrigen', $customer['international_operations_information']['remittance_country']['code']),
                    $this->hasData('IOIRemesasRecibidasRelacionRemitente', $customer['international_operations_information']['remittance_sender_relationship']['value']),
                    $this->hasData('IOIRemesasRecibidasNombreRemitente', $customer['international_operations_information']['remittance_sender']),
                    $this->hasData('IOIRemesasRecibidasMotivoRemesa', $customer['international_operations_information']['remittance_reason']),


                    // Account information
                    $this->hasData('NumeroCuenta', $product['product']['value']),
                    $this->hasData('ProductoCuenta', $product['product']['code']),

                    /*
					 * User information
					 */
                    $this->peopleSoft(),
                    new Data(true, 'Ip', null, request()->ip()),

                    /*
					 * Static fields
					 */
                    $this->hasData('NumeroEmpleadoApertura'),
                    $this->hasData('NumeroEmpleadoReferencia'),
//                    $this->hasData('NumeroCuenta'),
                    $this->hasData('LugarNacimiento'),
                    $this->hasData('PaisNacimiento'),
//                    $this->hasData('DireccionEmpleadorCompletaPais'),
                    $this->hasData('CodigoRangoTotalIngresos'),
                    $this->hasData('RTN'),
                    $this->hasData('OrigenFlujoCredito', 'A'),
                    $this->hasData('TipoCuenta', '1'),
                    $this->hasData('EstatusCuenta', '1'),
                    new Data(true, 'NumeroEmpleado', null, 0),
                    $this->hasData('ProductoOrigenRegistro', 'CTA'),
                    $this->hasData('CanalOrigenRegistro', 'LBY'),
                    $this->hasData('DatosSensitivosConfirmacionAutomatica', 'S'),
                    $this->hasData('DatosSensitivosPrioridad', '01'),
                    $this->hasData('DatosSensitivosCanalOrigen', ''),
                    $this->hasData('DatosSensitivosProgramaOrigen', 'CTA'),
                    $this->hasData('TipoCif', 'P'),
                    $this->hasData('TelefonoDomicilioCodigoArea', '504'),
                    $this->hasData('TelefonoDomicilioCodigoPais', '1'),
                    $this->hasData('TelefonoEmpleadorCodigoArea', '504'),
                    $this->hasData('TelefonoEmpleadorCodigoPais', '1'),


                    $this->hasData('Agencia', '120'),
                    $this->hasData('GestorOficial', 'T14'),
                ]
            ),

            $this->checkIfPublicJobHasValues($customer, 'CARGOS_PUBLICOS_LIST'),
//			$this->checkIfDependantsListHasValues($customer, 'DEPENDIENTES_LIST'),
            $this->getJsonReferences($customer['references'], 'REFERENCES_LIST'),
            $this->getBeneficiaries($customer['beneficiaries'], 'BENEFICIARIOS_LIST'),
            $this->getNationalInstitutions($customer['national_institutions'], 'CUENTAS_NACIONALES_LIST'),
            $this->getForeignInstitutions($customer['foreign_institutions'], 'CUENTAS_EXTRANJERAS_LIST'),
        ];
    }

	/**
	 * Construct the beneficiaries object with resource data
	 *
	 * @param $resource
	 * @param $field
	 * @return Data
	 */
	private function getBeneficiaries($resource, $field) : Data
	{
		$data = [];

		for ($i = 1; $i <= 10; $i++)
		{
			if($resource[$i]['name'] && $resource[$i]['name'] !== 'NO TIENE')
			{
				$data[] = new Data(
					true,
					'BENEFICIARIO_' . $i,
					[
						$this->hasData('BeneficiarioCuentaCorrelativo', $i),
						$this->hasData('BeneficiarioCuentaNombre', $resource[$i]['name']),
						$this->hasData('BeneficiarioCuentaParentesco', $resource[$i]['relationship']['code']),
						$this->hasData('BeneficiarioCuentaTipoIdentificacion', $resource[$i]['id_type']['code']),
						$this->hasData('BeneficiarioCuentaIdentificacion', $resource[$i]['identity']),
					]
				);
			}
		}

		if (empty($data))
		{
			return new Data(
				false,
				$field
			);
		}

		return new Data(
			true,
			$field,
			$data
		);
	}

	/**
	 * Construct the national institutions object with resource data
	 *
	 * @param $resource
	 * @param $field
	 * @return Data
	 */
	private function getNationalInstitutions($resource, $field) : Data
	{
		$data = [];

		for ($i = 1; $i <= 3; $i++)
		{
			$name = $resource[$i]['name'];

			if ($i === 1 && !$name)
			{
				$name = 'NO TIENE';
			}

			if($name)
			{
				$data[] = new Data(
					true,
					'CUENTA_NACIONAL_' . $i,
					[
						$this->hasData('CuentaOtrasInstitucionesNacionalesCorrelativo', $i),
						$this->hasData('CuentaOtrasInstitucionesNacionalesNombre', $name)
					]
				);
			}
		}

		return new Data(
			true,
			$field,
			$data
		);
	}

	/**
	 * Construct the national institutions object with resource data
	 *
	 * @param $resource
	 * @param $field
	 * @return Data
	 */
	private function getForeignInstitutions($resource, $field) : Data
	{
		$data = [];

		for ($i = 1; $i <= 3; $i++)
		{
			$name = $resource[$i]['name'];

			if ($i === 1 && !$name)
			{
				$name = 'NO TIENE';
			}

			if($name)
			{
				$data[] = new Data(
					true,
					'CUENTA_EXTRANJERA_' . $i,
					[
						$this->hasData('CuentaOtrasInstitucionesExtranjerasCorrelativo', $i),
						$this->hasData('CuentaOtrasInstitucionesExtranjerasNombre', $name)
					]
				);
			}
		}

		return new Data(
			true,
			$field,
			$data
		);
	}

	/**
	 * Construct the beneficiaries object with resource data
	 *
	 * @param $resource
	 * @param $field
	 * @return Data
	 */
	private function getJsonReferences($resource, $field) : Data
	{
		$data = [];

		for ($i = 1; $i <= 2; $i++)
		{
			if($resource[$i]['name'])
			{
				$data[] = new Data(
					true,
					'REFERENCIA_' . $i,
					[
						$this->hasData('ReferenciaCuentaCorrelativo', $i),
						$this->hasData('ReferenciaCuentaNombre', $resource[$i]['name']),
						$this->hasData('ReferenciaCuentaCelular', $resource[$i]['mobile']),
						$this->hasData('ReferenciaCuentaTelefono', $resource[$i]['phone']),
					]
				);
			}
		}

		if (empty($data))
		{
			return new Data(
				false,
				$field
			);
		}

		return new Data(
			true,
			$field,
			$data
		);
	}
}
