<?php

namespace App\Core\Bank\Account;

use App\Core\Data;
use App\Core\Parameters;

/**
 * Class EntailmentProcessParameters
 * @package App\Core
 */
class GeneratorParameters extends Parameters
{
	/**
	 * Construct de data object for Review Process
	 *
	 * @param identidad
	 * @return array
	 */
	public function generatorSerial($identidad) : array
	{
		return [
			$this->hasData('valNumeroIdentificacion', $identidad),
		];
	}

	/**
	 * Construct de data object for Review Process
	 *
	 * @param identidad
	 * @param codigoAutenticacion
	 * @return array
	 */
	public function autenticaconCodigo($data) : array
	{
		return [
			$this->hasData('valNumeroIdentificacion', $data['identificacion']),
			$this->hasData('codigoAutenticacion', $data['codigo'])
		];
	}
	
}