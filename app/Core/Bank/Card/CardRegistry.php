<?php

namespace App\Core\Bank\Card;

use App\Core\CustomerRegistry;
use App\Core\SanitizeString;
use Jenssegers\Date\Date;

/**
 * Class HandleCustomerRegistry
 * @package App\Core
 */
class CardRegistry extends CustomerRegistry
{
    /**
     * Get request parameters
     *
     * @var
     */
    protected $request;

    /**
     *
     *
     * @param $request
     * @return mixed
     */
    public function second($request)
    {
	    $flow = $this->recentFlow($this->productName(), true);

	    $flow->update([
            'customer_information' => $this->secondCustomerInformation($flow, $request),
            'product_information' => $this->secondProductInformation($flow, $request),
        ]);

	    return $flow;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function third($request)
    {
	    $flow = $this->recentFlow($this->productName(), true);

	    $flow->update([
		    'customer_information' => $this->thirdCustomerInformation($flow, $request),
		    'product_information' => $this->thirdProductInformation($flow, $request)
	    ]);

	    return $flow;
    }


    private function secondCustomerInformation($flow, $request)
    {
        $data = $flow->customer_information;

        $r = SanitizeString::clean($request->all());

        if (isset($r['year'], $r['month'], $r['day'])) {
            $data['birth'] = Date::createFromDate($r['year'], $r['month'], $r['day'])->timestamp;
        }

        $data['name']['first'] = $this->value($data, ['name', 'first'], $r, 'first_name');
        $data['name']['middle'] = $this->value($data, ['name', 'middle'], $r, 'middle_name');
        $data['name']['last'] = $this->value($data, ['name', 'last'], $r, 'last_name');
        $data['name']['second_last'] = $this->value($data, ['name', 'second_last'], $r, 'second_last_name');

        $data['name']['fullname'] = $this->fullname(
            $this->value($data, ['name', 'first'], $r, 'first_name'),
            $this->value($data, ['name', 'middle'], $r, 'middle_name'),
            $this->value($data, ['name', 'last'], $r, 'last_name'),
            $this->value($data, ['name', 'second_last'], $r, 'second_last_name')
        );

        $data['nationality'] = $this->codeValue($this->productName(), $data, ['nationality', 'code'], $request->nationality, 'nations');

        if (isset($request->employer_year, $request->employer_month, $request->employer_day)) {
            $data['employer']['started_at'] = Date::createFromDate($r['employer_year'], $r['employer_month'], $r['employer_day'])->timestamp;
        }

        if (isset($request->income)) {
            $data['income'] = $this->cleanAmount($r['income']);
        }

        if (isset($r['phone'])) {
            $data['phone'] = str_replace('-', '', $r['phone']);
        }

        if (isset($r['mobile'])) {
            $data['mobile'] = str_replace('-', '', $r['mobile']);
        }

        $data['address']['other_city'] = $this->value($data, ['address', 'other_city'], $r, 'other_city');

        $cityCode = $this->value($data, ['address', 'code'], $request->city);

        $addressLineOne = $this->value($data, ['address', 'first'], $r, 'address_1');
        $addressLineTwo = $this->value($data, ['address', 'second'], $r, 'address_2');
        $addressReference = $this->value($data, ['address', 'third'], $r, 'address_3');

        $address = $this->constructRequestFullAddress($addressLineOne, $addressLineTwo, $addressReference);

        $data['address']['code'] = $cityCode;
        $data['address']['full'] = $address;

        $data['address']['first'] = $addressLineOne;
        $data['address']['second'] = $addressLineTwo;
        $data['address']['third'] = $addressReference;

        $city = explode('>', $cityCode);
        $state = explode('-', $city[0]);
        $municipality = explode('-', $city[1]);
        $colony = explode('-', $city[2]);

        $data['address']['state'] = $state[0];
        $data['address']['state_code'] = $state[1];
        $data['address']['municipality'] = $municipality[0];
        $data['address']['municipality_code'] = $municipality[1];
        $data['address']['colony'] = $colony[0];
        $data['address']['colony_code'] = $colony[1];


        $data['employer']['job']['type'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'type', 'code'], $request->job_type, 'jobTypeOptions');

        $data['employer']['name'] = $this->value($data, ['employer', 'name'], $r, 'employer_name');

        $data['employer']['type'] = $this->codeValue($this->productName(), $data, ['employer', 'type', 'code'], $request->employer_type, 'employeeTypes');

        if (isset($r['employer_phone'])) {
            $data['employer']['phone'] = str_replace('-', '', $r['employer_phone']);
        }


        $data['employer']['address']['other_city'] = $this->value($data, ['employer', 'address', 'other_city'], $r, 'other_employer_city');

        $employerCityCode = $this->value($data, ['employer', 'address', 'code'], $request->employer_city);

        $employerAddressLineOne = $this->value($data, ['employer', 'address', 'first'], $r, 'employer_address_1');
        $employerAddressLineTwo = $this->value($data, ['employer', 'address', 'second'], $r, 'employer_address_2');
        $employerAddressReference = $this->value($data, ['employer', 'address', 'third'], $r, 'employer_address_3');

        $employerAddress = $this->constructRequestFullAddress($employerAddressLineOne, $employerAddressLineTwo, $employerAddressReference);

        $data['employer']['address']['code'] = $employerCityCode;

        $data['employer']['address']['full'] = $employerAddress;

        $data['employer']['address']['first'] = $employerAddressLineOne;
        $data['employer']['address']['second'] = $employerAddressLineTwo;
        $data['employer']['address']['third'] = $employerAddressReference;

        $employerCity = explode('>', $employerCityCode);
        $employerState = explode('-', $employerCity[0]);
        $employerMunicipality = explode('-', $employerCity[1]);
        $employerColony = explode('-', $employerCity[2]);

        $data['employer']['address']['state'] = $employerState[0];
        $data['employer']['address']['state_code'] = $employerState[1];
        $data['employer']['address']['municipality'] = $employerMunicipality[0];
        $data['employer']['address']['municipality_code'] = $employerMunicipality[1];
        $data['employer']['address']['colony'] = $employerColony[0];
        $data['employer']['address']['colony_code'] = $employerColony[1];

        $data['employer']['job']['status'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'status', 'code'], $request->job, 'jobOptions');

        $data['employer']['job']['contract'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'contract', 'code'], $request->job_contract, 'jobContractOptions');

//	    $data['employer']['job']['status'] = $this->getJobStatus($request, $this->productName());

        $publicJob = $this->value($data, ['public_job', 'code'], $request->public_job);

        $data['public_job']['code'] = $publicJob;
        $data['public_job']['value'] = $this->searchValueFromList($this->productName(), $publicJob, 'confirmation', false);

        $data['bureau'] = $this->codeValue($this->productName(), $data, ['bureau', 'code'], 'S', 'confirmation');

	    return $data;
    }


    private function secondProductInformation($flow, $request)
	{
		$data = $flow->product_information;

		$data['promotion'] = $this->codeValue($this->productName(), $data, ['promotion', 'code'], $request->promotion, 'promotions');

		return $data;
	}

	private function thirdCustomerInformation($flow, $request)
	{
		$data = $flow->customer_information;

		$r = SanitizeString::clean($request->all());

		$receiveEmail = 'N';

		if($request->has_email === null)
		{
			$data['email']['has_email'] = 'S';
		} else {
			$data['email']['has_email'] = 'N';//$this->value($data, ['email', 'has_email'], $request->has_email);
		}

		if ($request->has_email != null) {
			$data['email']['account'] = 'NOTIENE@DAVIVIENDA.COM.HN';
		} else {
			$data['email']['account'] = $this->value($data, ['email', 'account'], $r, 'email');

			if ($request->receive_emails) {
				$receiveEmail = 'S';
			}
		}

		$data['receive_emails'] = $this->codeValue($this->productName(), $data, ['receive_emails', 'code'], $receiveEmail, 'confirmation');

		$receiveSms = 'N';

		if ($request->receive_sms) {
			$receiveSms = 'S';
		}

		$data['receive_sms'] = $this->codeValue($this->productName(), $data, ['receive_sms', 'code'], $receiveSms, 'confirmation');

		$receiveEmailJob = 'N';

		if($request->has_job_email === null)
		{
			$data['job_email']['has_job_email'] = 'S';
		} else {
			$data['job_email']['has_job_email'] = 'N';
		}

		if ($request->has_job_email != null) {
			$data['job_email']['account'] = 'NOTIENE@DAVIVIENDA.COM.HN';
		} else {
			$data['job_email']['account'] = $this->value($data, ['job_email', 'account'], $r, 'job_email');

			if ($request->receive_emails_job) {
				$receiveEmailJob = 'S';
			}
		}

        $data['receive_emails_job'] = $this->codeValue($this->productName(), $data, ['receive_emails_job', 'code'], $receiveEmailJob, 'confirmation');

		$data['gender'] = $this->codeValue($this->productName(), $data, ['gender', 'code'], $request->gender, 'genderOptions');

		$data['marital_status'] = $this->codeValue($this->productName(), $data, ['marital_status', 'code'], $request->marital_status, 'maritalStatusOptions');

		$data['spouse'] = $this->value($data, 'spouse', $r, 'spouse');

		$data['profession'] = $this->codeValue($this->productName(), $data, ['profession', 'code'], $request->profession, 'professionOptions');

		$data['employer']['job']['name'] = $this->value($data, ['employer', 'job', 'name'], $r, 'job_name');

		if ($data['public_job']['code'] === 'N')
		{
			$data['public_job']['employer_name'] = null;
			$data['public_job']['job'] = null;
			$data['public_job']['active'] = null;
			$data['public_job']['from'] = null;
			$data['public_job']['to'] = null;
		} else {
			$data['public_job']['employer_name'] = $this->value($data, ['public_job', 'employer_name'], $r, 'public_job_employer_name');

			$data['public_job']['job'] = $this->value($data, ['public_job', 'job'], $r, 'public_job_name');

			$publicJobActive = $this->value($data, ['public_job', 'active'], $r, 'public_job_active');

			$data['public_job']['active'] = $publicJobActive;
			$data['public_job']['active_value'] = $this->searchValueFromList($this->productName(), $publicJobActive, 'confirmation', false);

			if (isset($r['public_job_from']))
			{
				$data['public_job']['from'] = Date::createFromFormat('dmY', $r['public_job_from'])->timestamp;
			}

			if ($publicJobActive === 'N')
			{
				if (isset($r['public_job_to']))
				{
					$data['public_job']['to'] = Date::createFromFormat('dmY', $r['public_job_to'])->timestamp;
				}
			} else {
				$data['public_job']['to'] = null;
			}
		}

		$data['assets'] = $this->codeValue($this->productName(), $data, ['assets', 'code'],  $request->assets, 'assetRangeOptions');

		$data['passive'] = $this->codeValue($this->productName(), $data, ['passive', 'code'], $request->passive, 'liabilityRangeOptions');

		$data['expenses'] = $this->codeValue($this->productName(), $data, ['expenses', 'code'], $request->expenses, 'expenseRangeOptions');

		$data['other_income'] = $this->codeValue($this->productName(), $data, ['other_income', 'code'], $request->other_income, 'confirmation');

		for ($i = 1; $i <= 2; $i++)
		{
			$data['references'][$i]['name'] = $this->value($data, ['references', $i, 'name'], $r, 'reference_' . $i . '_name');

			if ($request->get('reference_' . $i . '_relationship'))
			{
				$data['references'][$i]['relationship'] = $this->codeValue($this->productName(), $data, ['references', $i, 'relationship', 'code'], $request->get('reference_' . $i . '_relationship'), 'referenceOptions');
			}

			if ($request->get('reference_' . $i . '_mobile'))
			{
				$data['references'][$i]['mobile'] = str_replace('-', '', $r['reference_' . $i . '_mobile']);
			}

			if ($request->get('reference_' . $i . '_phone'))
			{
				$data['references'][$i]['phone'] = str_replace('-', '', $r['reference_' . $i . '_phone']);
			}

			if ($request->get('reference_' . $i . '_work_phone'))
			{
				$data['references'][$i]['work_phone'] = str_replace('-', '', $r['reference_' . $i . '_work_phone']);
			}
		}

		return $data;
	}

	private function thirdProductInformation($flow, $request)
	{
		$data = $flow->product_information;

		$r = SanitizeString::clean($request->all());

		if (isset($request->customer_limit)) {
			$limit = str_replace(',', '', $request->customer_limit);

			$limit = number_format(floor($limit/100) * 100, 2, '.', '');

			$data['product']['customer_limit'] = $limit;
		}

		$notes = $this->value($data, ['notes', 'full'], $request->notes);

		$data['notes']['full'] = $notes;

		$data['notes']['first'] = $this->divideString($notes, 40, 1);
		$data['notes']['second'] = $this->divideString($notes, 40, 2);
		$data['notes']['third'] = $this->divideString($notes, 40, 3);
		$data['notes']['fourth'] = $this->divideString($notes, 40, 4);
		$data['notes']['fifth'] = $this->divideString($notes, 40, 5);
		$data['notes']['sixth'] = $this->divideString($notes, 40, 6);

		return $data;
	}

	private function productName()
	{
		return 'bank_card';
	}
}
