<?php

namespace App\Core\Bank\Card;

use App\Core\GetResponse;

/**
 * Class GetApprovedCardDetails
 * @package App\Core
 */
class GetApprovedCardDetails
{
    public function details($response, $flow)
    {
	    $product = $flow->product_information;

	    if (!env('BANK_CARD_DEMO'))
	    {
		    $getValueFromResponse = new GetResponse();

		    $cardSegment = $product['product']['code'];

		    $product['request']['number'] = $getValueFromResponse->result($response, 'NumeroSolicitudCredito');

	        if ($cardSegment === 'DDP') {
	            $limit = $getValueFromResponse->result($response, 'TarjetaCreditoLimiteDADINEROPLUS');
	            $type = $getValueFromResponse->result($response, 'TarjetaCreditoDescripcionTarjetaDADINEROPLUS');
	        } else {
	            $limit = $getValueFromResponse->result($response, 'TarjetaCreditoLimiteDAVIPUNTOS');
	            $type = $getValueFromResponse->result($response, 'TarjetaCreditoDescripcionTarjetaDAVIPUNTOS');
	        }

	        $product['product']['limit'] = $limit;
	        $product['product']['type'] = ucfirst(strtolower($type));
	    } else {
		    $product['request']['number'] = rand(100000, 999999);
		    $product['product']['limit'] = rand(10000, 300000);
		    $product['product']['type'] = 'BLACK DIAMOND';
	    }

	    $flow->update([
        	'product_information' => $product
        ]);

	    return $flow;
    }

}
