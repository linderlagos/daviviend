<?php

namespace App\Core\Bank\Card;

use App\Core\Data;
use App\Core\Parameters;
use Illuminate\Support\Facades\Auth;

/**
 * Class EntailmentProcessParameters
 * @package App\Core
 */
class CardParameters extends Parameters
{
    /**
     * Construct de data object for Approval Process
     *
     * @param $flow
     * @return array
     */
    public function second($flow) : array
    {
	    $customer = $flow->customer_information;
	    $product = $flow->product_information;
        //dd($product['product']);
        return [
            new Data(
                true,
                'DATA_APPROVAL_PARAMS',
                [
                    /*
					 * Dynamic fields
					 */
                    
	                new Data(true, 'TipoIdentificacion', null, $flow->customer->identifier_type),
	                $this->hasData('Identificacion', $flow->customer->identifier),
	                $this->hasData('FechaNacimientoAAAAMMDD', $this->getDate($customer['birth'])),
	                $this->hasData('Nombre', $customer['name']['fullname']),
	                $this->hasData('ActividadComercial', $customer['employer']['job']['type']['code']),
	                $this->hasData('IngresoMensual', $customer['income']),
	                $this->hasData('TelefonoDomicilio', $customer['phone']),
	                $this->hasData('Celular', $customer['mobile']),
	                $this->hasData('DireccionDomicilioCompletaBarrioColonia', $this->getColony($customer['address']['colony_code'], $customer['address']['colony'], $customer['address']['other_city'])),
	                $this->hasData('DireccionDomicilioCompletaAvenidaBloqueZonaCalleCasa', $customer['address']['first']),
	                $this->hasData('DireccionDomicilioCompletaPuntoReferencia', $customer['address']['second']),
	                $this->hasData('DireccionDomicilioCompletaPuntoReferenciaComplemento', $customer['address']['third']),
	                $this->hasData('DireccionDomicilioCompletaDepartamentoCodigo', $customer['address']['state_code']),
	                $this->hasData('DireccionDomicilioCompletaMunicipioCodigo', $customer['address']['municipality_code']),
	                $this->hasData('DireccionDomicilioCompletaColonia', $customer['address']['colony_code']), // Comprobar dónde enviar código de la colonia y dónde el nombre


	                $this->hasData('Nacionalidad', $customer['nationality']['code']),
                    $this->hasData('Promocion', $product['promotion']['code']),
                    $this->hasData('NombreEmpleador', $customer['employer']['name']),
                    $this->hasData('TipoEmpresa', $customer['employer']['type']['code']),
	                $this->hasData('FechaIngresoLaboralAAAAMMDD', $this->getDate($customer['employer']['started_at'])),
                    $this->hasData('TelefonoEmpleador', $customer['employer']['phone']),
                    $this->hasData('TipoEmpleo', $customer['employer']['job']['status']['code']),
                    $this->hasData('SituacionLaboral', $customer['employer']['job']['contract']['code']),
                    $this->hasData('AutorizacionClienteConsultaBuros', $customer['bureau']['code']),
                    
                    $this->hasData('TarjetaCreditoSeleccionadaDAVIPUNTOSDADINEROPLUS', $product['product']['code']),
	                $this->hasData('DireccionEmpleadorCompletaBarrioColonia', $this->getColony($customer['employer']['address']['colony_code'], $customer['employer']['address']['colony'], $customer['employer']['address']['other_city'])),
	                $this->hasData('DireccionEmpleadorCompletaAvenidaBloqueZonaCalleCasa', $customer['employer']['address']['first']),
	                $this->hasData('DireccionEmpleadorCompletaPuntoReferencia', $customer['employer']['address']['second']),
	                $this->hasData('DireccionEmpleadorCompletaPuntoReferenciaComplemento', $customer['employer']['address']['third']),
	                $this->hasData('DireccionEmpleadorCompletaDepartamentoCodigo', $customer['employer']['address']['state_code']),
	                $this->hasData('DireccionEmpleadorCompletaMunicipioCodigo', $customer['employer']['address']['municipality_code']),
	                $this->hasData('DireccionEmpleadorCompletaColoniaCodigo', $customer['employer']['address']['colony_code']),
                    $this->hasData('CargosPublicosUltimos4Anhos', $customer['public_job']['code']),
                    /*
					 * User information
					 */
                    $this->peopleSoft(),
                    new Data(true, 'Ip', null, request()->ip()),

                    /*
					 * Static fields
					 */

                    new Data(true, 'CodigoEmpresaReconocida', null, '0'),
                    $this->hasData('OrigenFlujoCredito', 'A'),
                    $this->hasData('TipoFlujoCredito', 'S'),
                    $this->hasData('ProductoOrigenRegistro', 'TCA'),
                    $this->hasData('CanalOrigenRegistro', 'LBY'),
                    new Data(true, 'PromocionSeleccionada', null, '0'),
                    new Data(true, 'PorcentajeSeleccionado', null, '0'),
                    new Data(true, 'Profesion', null, '0'),
                    new Data(true, 'CondicionVivienda', null, '0'),
                    $this->hasData('CentroCosto', '169'),
                    $this->hasData('Agencia', '169'),
                    $this->hasData('EsFatca', 'N'),
                    $this->hasData('GestorOficial', 'TDS'),
                    new Data(true, 'NumeroEmpleado', null, '0'),
                    new Data(true, 'AlianzaComercial', null, '0'),
                ]
            )
        ];
    }

    /**
     * Construct de data object for Entailment Process
     *
     * @param $flow
     * @return array
     */
    public function third($flow) : array
    {
	    $customer = $flow->customer_information;
	    $product = $flow->product_information;

        return [
            new Data(
                true,
                'DATA_ENTAILMENT_PARAMS',
                [

                    $this->hasData('NumeroSolicitudCredito', $product['request']['number']),
	                new Data(true, 'TipoIdentificacion', null, $flow->customer->identifier_type),
	                $this->hasData('Identificacion', $flow->customer->identifier),
	                $this->hasData('Nombre', $customer['name']['fullname']),
	                $this->hasData('ActividadComercial', $customer['employer']['job']['type']['code']),
	                $this->hasData('IngresoMensual', $customer['income']),
                    $this->hasData('Sexo', $customer['gender']['code']),
                    $this->hasData('EstadoCivil', $customer['marital_status']['code']),
	                $this->hasData('Nacionalidad', $customer['nationality']['code']),
	                $this->hasData('DireccionDomicilioCompletaBarrioColonia', $this->getColony($customer['address']['colony_code'], $customer['address']['colony'], $customer['address']['other_city'])),
	                $this->hasData('DireccionDomicilioCompletaAvenidaBloqueZonaCalleCasa', $customer['address']['first']),
	                $this->hasData('DireccionDomicilioCompletaPuntoReferencia', $customer['address']['second']),
	                $this->hasData('DireccionDomicilioCompletaPuntoReferenciaComplemento', $customer['address']['third']),
	                $this->hasData('DireccionDomicilioCompletaDepartamentoCodigo', $customer['address']['state_code']),
	                $this->hasData('DireccionDomicilioCompletaMunicipioCodigo', $customer['address']['municipality_code']),
	                $this->hasData('DireccionDomicilioCompletaColoniaCodigo', $customer['address']['colony_code']), // Comprobar dónde enviar código de la colonia y dónde el nombre
	                $this->hasData('TelefonoDomicilio', $customer['phone']),
	                $this->hasData('Celular', $customer['mobile']),
                    $this->hasData('AutorizaEnvioMensajesCelular', $customer['receive_sms']['code']),
                    $this->hasData('CorreoPersonal', $customer['email']['account']),
	                $this->hasData('CorreoLaboral', $customer['job_email']['account']),
                    $this->hasData('AutorizaEnvioEstadosCuentaCorreoPersonal', $customer['receive_emails']['code']),
	                $this->hasData('AutorizaEnvioEstadosCuentaCorreoLaboral', $customer['receive_emails_job']['code']),
	                $this->hasData('FechaNacimientoAAAAMMDD', $this->getDate($customer['birth'])),
	                $this->hasData('PrimerNombre', $customer['name']['first']),
	                $this->hasData('SegundoNombre', $customer['name']['middle']),
	                $this->hasData('PrimerApellido', $customer['name']['last']),
	                $this->hasData('SegundoApellido', $customer['name']['second_last']),
	                $this->hasData('FechaIngresoLaboralAAAAMMDD', $this->getDate($customer['employer']['started_at'])),
	                $this->hasData('NombreEmpleadorLugarTrabajo', $customer['employer']['name']),
	                $this->hasData('TelefonoEmpleadorLaboral', $customer['employer']['phone']),
                    $this->hasData('TipoEmpresa', $customer['employer']['type']['code']),
	                $this->hasData('TipoEmpleo', $customer['employer']['job']['type']['code']),
	                $this->hasData('DireccionEmpleadorCompletaBarrioColonia', $this->getColony($customer['employer']['address']['colony_code'], $customer['employer']['address']['colony'], $customer['employer']['address']['other_city'])),
	                $this->hasData('DireccionEmpleadorCompletaAvenidaBloqueZonaCalleCasa', $customer['employer']['address']['first']),
	                $this->hasData('DireccionEmpleadorCompletaPuntoReferencia', $customer['employer']['address']['second']),
	                $this->hasData('DireccionEmpleadorCompletaPuntoReferenciaComplemento', $customer['employer']['address']['third']),
	                $this->hasData('DireccionEmpleadorCompletaDepartamentoCodigo', $customer['employer']['address']['state_code']),
	                $this->hasData('DireccionEmpleadorCompletaMunicipioCodigo', $customer['employer']['address']['municipality_code']),
	                $this->hasData('DireccionEmpleadorCompletaColoniaCodigo', $customer['employer']['address']['colony_code']),
	                $this->hasData('Profesion', $customer['profession']['code']),
	                $this->hasData('CodigoRangoTotalEgresos', $customer['expenses']['code']),
	                $this->hasData('CodigoRangoTotalActivos', $customer['assets']['code']),
	                $this->hasData('CodigoRangoTotalPasivos', $customer['passive']['code']),
	                $this->hasData('OtrosIngresosOtros', $customer['other_income']['code']),
	                $this->hasData('CargoLaboralPuestoTrabajo', $customer['employer']['job']['name']),
	                $this->hasData('ConyugeNombre', $customer['spouse']),
	                $this->hasData('CargosPublicosUltimos4Anhos', $customer['public_job']['code']),
	                $this->hasData('Observacion_1', $product['notes']['first']),
	                $this->hasData('Observacion_2', $product['notes']['second']),
	                $this->hasData('Observacion_3', $product['notes']['third']),
	                $this->hasData('Observacion_4', $product['notes']['fourth']),
	                $this->hasData('Observacion_5', $product['notes']['fifth']),
	                $this->hasData('Observacion_6', $product['notes']['sixth']),

                    /*
					 * User information
					 */
                    $this->peopleSoft(),
                    new Data(true, 'Ip', null, request()->ip()),
                    /*
					 * Static fields
					 */
                    $this->hasData('OrigenFlujoCredito', 'A'),
                    $this->hasData('LugarNacimiento'),
                    $this->hasData('PaisNacimiento'),
                    $this->hasData('DireccionEmpleadorCompletaPaisCodigo'),
                    $this->hasData('CodigoRangoTotalIngresos'),
                    $this->hasData('RealizaOperacionesMonedaExtranjera'),
                    $this->hasData('TransferenciasInternacionales'),
                    $this->hasData('CompraVentaDivisa'),
	                $this->hasData('EsFatca', 'N'),
                    $this->hasData('EmisionGirosDolarChequesDolares'),
                    $this->hasData('RecibiraRemesasFamiliares'),
                    $this->hasData('ProductoOrigenRegistro', 'TCA'),
	                $this->hasData('CanalOrigenRegistro', 'LBY'),
                    $this->hasData('DatosSensitivosConfirmacionAutomatica', 'N'),
                    $this->hasData('DatosSensitivosPrioridad', '01'),
                    $this->hasData('DatosSensitivosCanalOrigen', ''),
                    $this->hasData('DatosSensitivosProgramaOrigen', 'TCA'),
                    $this->hasData('TipoCif', 'P'),
                    $this->hasData('TelefonoDomicilioCodigoArea', '504'),
                    $this->hasData('TelefonoDomicilioCodigoPais', '1'),
                    $this->hasData('RTN'),
                    $this->hasData('TelefonoEmpleadorCodigoArea', '504'),
                    $this->hasData('TelefonoEmpleadorCodigoPais', '1'),
                    $this->hasData('DireccionDomicilioCompletaPaisCodigo', '1'),
                    $this->hasData('DireccionEmpleadorCompletaPaisCodigo', '1'),
                    $this->hasData('Agencia', '169'),
                    $this->hasData('GestorOficial', 'TDS'),
                ]
            ),

            $this->checkIfCardLimitIsDifferent($product, 'CHANGE_CARD_LIMIT'),
            $this->checkIfPublicJobHasValues($customer, 'CARGOS_PUBLICOS_LIST'),
            $this->checkIfDependantsListHasValues($customer, 'DEPENDIENTES_LIST'),
	        $this->getReferences($customer, 'REFERENCES_LIST'),

        ];
    }

    /**
     * Construct de data object for Rejection Process
     *
     * @param $customer
     * @param $request
     * @return array
     */
    public function rejectionProcessParameters($customer, $request) : array
    {
        return [
            $this->hasData('NumeroSolicitudCredito', $customer->card_request),

            /*
			 * User information
			 */
            $this->peopleSoft(),
            new Data(true, 'Ip', null, request()->ip()),
            /*
			 * Static fields
			 */
            $this->hasData('TipoRechazo', $this->rejectionType($request->reject)),
	        $this->hasData('ProductoOrigenRegistro', 'TCA'),
	        $this->hasData('CanalOrigenRegistro', 'LBY'),
            $this->hasData('CodigoRechazo', $request->reject),
        ];
    }

	/**
	 * Construct de data object for Exit Process
	 *
	 * @param $customer
	 * @param $request
	 * @return array
	 */
	public function exitProcessParameters($customer, $request) : array
	{
		return [
			new Data(true, 'TipoIdentificacion', null, $customer->id_type),
			$this->hasData('Identificacion', $customer->identity),

			/*
			 * User information
			 */
			$this->peopleSoft(),

			/*
			 * Static fields
			 */
			$this->hasData('ProductoOrigenRegistro', 'TCA'),
			$this->hasData('CanalOrigenRegistro', 'LBY'),
//			$this->hasData('TipoRechazo', $this->rejectionType($request->reject)),
			$this->hasData('CodigoRechazo', $request->reject),
		];
	}


}
