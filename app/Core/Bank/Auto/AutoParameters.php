<?php

namespace App\Core\Bank\Auto;

// Helpers
use App\Core\Data;
use App\Core\Parameters;
use App\Core\Bank\Auto\AutoResponse;
use App\Core\GetPreFilledData;
use Jenssegers\Date\Date;
use App\Core\Traits\Bridge;
use App\Http\Response\CreditCardProcessResponse;

/**
 * Class EntailmentProcessParameters
 * @package App\Core
 */
class AutoParameters extends Parameters
{


    public function second($flow) : array
    {
        $customer = $flow->customer_information;

        return [
            new Data(true, 'TipoIdentificacion', null, $flow->customer->identifier_type),
            $this->hasData('Identificacion', $flow->customer->identifier),
            //$this->hasData('Nombre', $customer['name']['fullname']),
            //$this->hasData('Celular', $customer['mobile']),
            //$this->hasData('AutorizaEnvioMensajesCelular', $customer['receive_sms']['code']),
            $this->hasData('AutorizacionClienteParaConsultarBuros', $customer['bureau']['code']),

            /*
             * Static fields
             */
            $this->hasData('TipoFlujoCredito', 'S'),
            $this->hasData('CentroCosto', '178'),
            $this->hasData('Agencia', '153'),
            $this->hasData('GestorOficial', 'P20'),
            $this->hasData('CanalOrigenRegistro', 'LBY'),
            //new Data(true, 'CanalOrigenRegistro', null, 'LBY'),
            $this->hasData('ProductoOrigenRegistro', 'AUA'),


            /*
             * User information
             */
            $this->peopleSoft(),
            new Data(true, 'Ip', null, request()->ip()),

        ];
    }

	public function calculate($flow) : array
	{
	    $customer = $flow->customer_information;
	    $product = $flow->product_information;
	    $user = $flow->user_information;

		return [
			
                    $this->hasData('NumeroSolicitudCredito', $flow->identifier),
                    new Data(true, 'TipoIdentificacion', null, $flow->customer->identifier_type),
                    $this->hasData('Identificacion', $flow->customer->identifier),
                    $this->hasData('Nombre', $customer['name']['fullname']),
                    $this->hasData('PrimerNombre', $customer['name']['first']),
                    $this->hasData('SegundoNombre', $customer['name']['middle']),
                    $this->hasData('PrimerApellido', $customer['name']['last']),
                    $this->hasData('SegundoApellido', $customer['name']['second_last']),
                    $this->hasData('Celular', $customer['mobile']),
                    $this->hasData('AutorizaEnvioMensajesCelular', $customer['receive_sms']['code']),
                    $this->hasData('EsConcesionariaQuienEstaConectado', $user['dealership_is_connected']['code']),
                    $this->hasData('CodigoConcesionaria', $user['concessionaire']['code']),
                    $this->hasData('CodigoVendedor', $user['seller']['code']),
                    $this->hasData('TipoAuto', $product['auto']['type']['code']),
                    $this->hasData('MarcaAuto', $product['auto']['brand']['code']),
                    $this->hasData('ModeloAuto', $product['auto']['model']),
                    $this->hasData('EstadoAuto', $product['auto']['status']['code']),
                    $this->hasData('AnhoAuto', $product['auto']['year']['code']),
                    $this->hasData('PrecioVenta', $product['sale_price']),
                    $this->hasData('PrecioAvaluo', $product['appraisal']),
                    $this->hasData('FechaNacimientoAAAAMMDD', $this->getDate($customer['birth'])),
                    $this->hasData('TipoEmpleo', $customer['employer']['job']['status']['code']),
                    $this->hasData('ActividadComercial', $customer['employer']['job']['type']['code']),
                    $this->hasData('SalarioMensualBruto', $customer['income']),
                    $this->hasData('DeduccionISRImpuestoSobreLaRenta', $customer['isr_deduction']),
                    $this->hasData('DeduccionRAPRegimenAportacionPrivada', $customer['rap_deduction']),
                    $this->hasData('DeduccionIHSSInstitutoHondurenhoSeguridadSocial', $customer['ihss_deduction']),

                    $this->hasData('DeduccionOtros', '00'),

                    $this->hasData('Prima', $product['premium']),
                    $this->hasData('FinanciarGastosCierre', $product['closing_costs']['code']),
                    $this->hasData('PlazoDeclarado', $product['financing_term']['code']),
                    $this->hasData('AplicarPeriodoGracia', $product['grace_period']['code']),

                    /*
                    * Static fields
                    */

                    $this->hasData('CodigoSeguroDeuda', '43'),
                    $this->hasData('Promocion', '132'),
                    $this->hasData('MonedaCreadito', 'LPS'),
                    $this->hasData('TipoRelacion', '00'),
                    $this->hasData('CentroCosto', '178'),
                    $this->hasData('Agencia', '153'),
                    $this->hasData('GestorOficial', 'P20'),
                    $this->hasData('CanalOrigenRegistro', 'LBY'),
                    //new Data(true, 'CanalOrigenRegistro', null, 'LBY'),
                    $this->hasData('ProductoOrigenRegistro', 'AUA'),


                    /*
                     * User information
                     */
                    $this->peopleSoft(),
                    new Data(true, 'Ip', null, request()->ip()),
              
		];
	}


    public function fourth($flow) : array
    {
        $customer = $flow->customer_information;
        $product = $flow->product_information;
        $user = $flow->user_information;

        return [
                    $this->hasData('Identificacion', $flow->customer->identifier),
                    $this->hasData('NumeroSolicitudCredito', $flow->identifier),

                    $this->hasData('CodigoConcesionaria', $user['concessionaire']['code']),
                    $this->hasData('CorrelativoEscenario', $product['correlative_stage']), //
                    $this->hasData('Nacionalidad', $customer['nationality']['code']),
                    $this->hasData('EsFATCA', $customer['fatca']['code']),
                    $this->hasData('TelefonoDomicilio', $customer['phone']),
                    $this->hasData('CorreoPersonal', $customer['email']['account']),
                    $this->hasData('AutorizaEnvioEstadosCuentaCorreoPersonal', $customer['receive_emails']['code']),
                    $this->hasData('NombreEmpleadorLugarTrabajo', $customer['employer']['name']),
                    $this->hasData('SituacionLaboral', $customer['employer']['job']['contract']['code']),
                    $this->hasData('TelefonoEmpleadorLaboral', $customer['employer']['phone']),
                    $this->hasData('FechaIngresoLaboralAAAAMMDD', $this->getDate($customer['employer']['started_at'])),
                    $this->hasData('CorreoLaboral', $customer['job_email']['account']),
                    $this->hasData('AutorizaEnvioEstadosCuentaCorreoLaboral', $customer['receive_emails_job']['code']),
                    $this->hasData('TipoEmpresa', $customer['employer']['type']['code']),
                    $this->hasData('EstadoCivil', $customer['marital_status']['code']),
                    $this->hasData('ConyugeTipoIdentificacion', $customer['spouse_information']['id_type']['code']),
                    $this->hasData('ConyugeIdentificacion', $customer['spouse_information']['identity']),
                    $this->hasData('ConyugeNombre', $customer['spouse']),
                    $this->hasData('Profesion', $customer['profession']['code']),
                    $this->hasData('CargoLaboralPuestoTrabajo', $customer['employer']['job']['name']),
                    $this->hasData('SexoGenero', $customer['gender']['code']),

                    /*
                     * Values static
                     */
                    $this->hasData('ConyugeNacionalidad', ''),
                    $this->hasData('ConyugeActividadComercial', ''),
                    $this->hasData('RTN', ''),

                    $this->hasData('Referencia_1_Nombre', $customer['references']['1']['name']),
                    $this->hasData('Referencia_1_Celular', $customer['references']['1']['mobile']),
                    $this->hasData('Referencia_1_TelefonoDomicilio', $customer['references']['1']['phone']),
                    $this->hasData('Referencia_1_TelefonoEmpleadorLaboral', $customer['references']['1']['work_phone']),
                    $this->hasData('Referencia_1_Parentesco', $customer['references']['1']['relationship']['code']),
                    $this->hasData('Referencia_1_Tipo', '3'),


                    $this->hasData('Referencia_2_Nombre', $customer['references']['2']['name']),
                    $this->hasData('Referencia_2_Celular', $customer['references']['2']['mobile']),
                    $this->hasData('Referencia_2_TelefonoDomicilio',  $customer['references']['2']['phone']),
                    $this->hasData('Referencia_2_TelefonoEmpleadorLaboral', $customer['references']['2']['work_phone']),
                    $this->hasData('Referencia_2_Parentesco', '00'),
                    $this->hasData('Referencia_2_Tipo', '4'),

                    //$this->hasData('EjecutivoAuto', $this->issetValue($user, ['auto_executive'], $this->staticAutoExecutive('99'), 'code')),
                    $this->hasData('EjecutivoAuto', $this->getAutoExecutive($user, '99')),

                    $this->hasData('DireccionDomicilioCompletaPaisCodigo', '1'),
                    $this->hasData('DireccionDomicilioCompletaDepartamentoCodigo', $customer['address']['state_code']),
                    $this->hasData('DireccionDomicilioCompletaMunicipioCodigo', $customer['address']['municipality_code']),
                    $this->hasData('DireccionDomicilioCompletaColoniaCodigo', $customer['address']['colony_code']),
                    $this->hasData('DireccionDomicilioCompletaBarrioColonia', $this->getColony($customer['address']['colony_code'], $customer['address']['colony'], $customer['address']['other_city'])),
                    $this->hasData('DireccionDomicilioCompletaAvenidaBloqueZonaCalleCasa', $customer['address']['first']),
                    $this->hasData('DireccionDomicilioCompletaPuntoReferencia', $customer['address']['second']),
                    $this->hasData('DireccionDomicilioCompletaPuntoReferenciaComplemento', $customer['address']['third']),

                    $this->hasData('DireccionEmpleadorCompletaPaisCodigo', '1'),
                    $this->hasData('DireccionEmpleadorCompletaDepartamentoCodigo', $customer['employer']['address']['state_code']),
                    $this->hasData('DireccionEmpleadorCompletaMunicipioCodigo', $customer['employer']['address']['municipality_code']),
                    $this->hasData('DireccionEmpleadorCompletaColoniaCodigo', $customer['employer']['address']['colony_code']),
                    $this->hasData('DireccionEmpleadorCompletaBarrioColonia', $this->getColony($customer['employer']['address']['colony_code'], $customer['employer']['address']['colony'], $customer['employer']['address']['other_city'])),
                    $this->hasData('DireccionEmpleadorCompletaAvenidaBloqueZonaCalleCasa', $customer['employer']['address']['first']),
                    $this->hasData('DireccionEmpleadorCompletaPuntoReferencia', $customer['employer']['address']['second']),
                    $this->hasData('DireccionEmpleadorCompletaPuntoReferenciaComplemento', $customer['employer']['address']['third']),

                    /*
                     * Static fields
                     */
                    $this->hasData('CanalOrigenRegistro', 'LBY'),
                    $this->hasData('ProductoOrigenRegistro', 'AUA'),

                    /*
					 * User information
					 */
                    $this->peopleSoft(),
                    new Data(true, 'Ip', null, request()->ip()),
        ];
    }


	/**
	 * Get auto executive peoplesoft from database or static value
	 *
	 * @param $data
	 * @param $zone
	 * @return mixed|string
	 */
	private function getAutoExecutive($data, $zone)
    {

        $autoExecutive = get_json($data, ['auto_executive', 'code']);

        if($autoExecutive !== 'NULO')

        {
            return $autoExecutive;
        }

        $lists = $this->getLists('bank_auto');

        if(isset($lists['agentOptions'][$zone]))
        {
	        $array = $lists['agentOptions'][$zone];

	        reset($array);

	        return key($array);
        }

        // Returns null if no executive code was found
        return null;

    }


    public function fifth($flow) : array
    {

        $customer = $flow->customer_information;
        $product = $flow->product_information;
        $user = $flow->user_information;

        return [
            $this->hasData('Identificacion', $flow->customer->identifier),
            $this->hasData('NumeroSolicitudCredito', $flow->identifier),
            $this->hasData('CodigoConcesionaria', $user['concessionaire']['code']),
            $this->hasData('CorrelativoEscenario', $product['correlative_stage']),
            $this->hasData('ColorAuto', $product['auto']['color']),
            $this->hasData('CilindrajeAuto', $product['auto']['displacement']),
            $this->hasData('NumeroSerieChasisAuto', $product['auto']['chassis']),
            $this->hasData('NumeroSerieMotorAuto', $product['auto']['motor']),
            $this->hasData('NumeroSerieVINAuto', $product['auto']['vin']),
//	        $this->hasData('Observacion_1', $product['notes']['first']),
//	        $this->hasData('Observacion_2', $product['notes']['second']),
//	        $this->hasData('Observacion_3', $product['notes']['third']),
//	        $this->hasData('Observacion_4', $product['notes']['fourth']),
//	        $this->hasData('Observacion_5', $product['notes']['fifth']),
//	        $this->hasData('Observacion_6', $product['notes']['sixth']),

            /*
             * Static fields
             */
            $this->hasData('CanalOrigenRegistro', 'LBY'),
            $this->hasData('ProductoOrigenRegistro', 'AUA'),

            /*
             * User information
             */
            $this->peopleSoft(),
            new Data(true, 'Ip', null, request()->ip()),
        ];

    }


    public function recalculate($flow) : array
    {
        $customer = $flow->customer_information;
        $product = $flow->product_information;
        $user = $flow->user_information;

        return [
            $this->hasData('Identificacion', $flow->customer->identifier),
            $this->hasData('NumeroSolicitudCredito', $flow->identifier),
            $this->hasData('CodigoConcesionaria', $user['concessionaire']['code']),
            $this->hasData('CorrelativoEscenario', $product['correlative_stage']),

            /*
             * Static fields
             */
            $this->hasData('CanalOrigenRegistro', 'LBY'),
            $this->hasData('ProductoOrigenRegistro', 'AUA'),

            /*
             * User information
             */
            $this->peopleSoft(),
            new Data(true, 'Ip', null, request()->ip()),
        ];

    }


    public function evaluationListCar($flow) : array
    {
////        $customer = $flow->customer_information;
//        $product = $flow->product_information;
//        $user = $flow->user_information;

        return [
            $this->hasData('Identificacion', $flow->customer->identifier),
            $this->hasData('NumeroSolicitudCredito', $flow->identifier),

            /*
             * Static fields
             */
            $this->hasData('CanalOrigenRegistro', 'LBY'),
            $this->hasData('ProductoOrigenRegistro', 'AUA'),

            /*
             * User information
             */
            $this->peopleSoft(),
            new Data(true, 'Ip', null, request()->ip()),
        ];

    }


    public function integrationTraditional($flow) : array
    {
        $customer = $flow->customer_information;
        $product = $flow->product_information;
        $user = $flow->user_information;

        return [
            $this->hasData('Identificacion', $flow->customer->identifier),
            $this->hasData('NumeroSolicitudCredito', $flow->identifier),
            $this->hasData('CodigoConcesionaria', $user['concessionaire']['code']),
            $this->hasData('CorrelativoEscenario', $product['correlative_stage']),
            $this->hasData('Observacion_1', $product['notes']['first']),
            $this->hasData('Observacion_2', $product['notes']['second']),
            $this->hasData('Observacion_3', $product['notes']['third']),
            $this->hasData('Observacion_4', $product['notes']['fourth']),
            $this->hasData('Observacion_5', $product['notes']['fifth']),
            $this->hasData('Observacion_6', $product['notes']['sixth']),


            /*
             * Static fields
             */
            $this->hasData('CanalOrigenRegistro', 'LBY'),
            $this->hasData('ProductoOrigenRegistro', 'AUA'),

            /*
             * User information
             */
            $this->peopleSoft(),
            new Data(true, 'Ip', null, request()->ip()),
        ];

    }

    public function initialFilterData($flow) : array
    {
       
        return [
            new Data(true, 'TipoIdentificacion', null, $flow->customer->identifier_type),
            $this->hasData('Identificacion', $flow->customer->identifier),

            /*
             * Static fields
             */
            $this->hasData('CentroCosto', '178'),
            $this->hasData('Agencia', '153'),
            $this->hasData('GestorOficial', 'P20'),
            $this->hasData('CanalOrigenRegistro', 'LBY'),
            //new Data(true, 'CanalOrigenRegistro', null, 'LBY'),
            $this->hasData('ProductoOrigenRegistro', 'AUA'),


            /*
             * User information
             */
            $this->peopleSoft(),
            new Data(true, 'Ip', null, request()->ip()),
        ];

    }


    public function sendAlertMessage($flow) : array
    {
        $customer = $flow->customer_information;
        $product = $flow->product_information;
        $user = $flow->user_information;

        return [
            $this->hasData('Identificacion', $flow->customer->identifier),
            $this->hasData('NumeroSolicitudCredito', $flow->identifier),
            $this->hasData('CodigoConcesionaria', $user['concessionaire']['code']),
            $this->hasData('CorrelativoEscenario', $product['correlative_stage']),

            /*
             * Static fields
             */
            $this->hasData('CanalOrigenRegistro', 'LBY'),
            $this->hasData('ProductoOrigenRegistro', 'AUA'),

            /*
             * User information
             */
            $this->peopleSoft(),
            new Data(true, 'Ip', null, request()->ip()),
        ];

    }




}
