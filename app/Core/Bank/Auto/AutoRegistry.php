<?php 
 
namespace App\Core\Bank\Auto;

use App\Models\Customer;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;
use App\Core\SanitizeString;
use App\Core\CustomerRegistry;
use App\Core\GetResponse;
use App\User;

class AutoRegistry extends CustomerRegistry
{
    protected $request;

    public function search($customer)
    {
        $flow = $customer->flows()->create([
            'customer_information' => $this->searchCustomerInformation(),
            'user_information' => $this->searchUserInformation(),
            'type' => $this->productName(),
            'user_id' => Auth::user()->id,
            'step' => '1',
            'customer_status' => 1,
        ]);

        return $flow;
    }

    public function new($currentFlow)
    {
        $flow = $currentFlow->customer->flows()->create([
            'customer_information' => $currentFlow->customer_information,
            //'product_information' => $currentFlow->product_information,
            'user_information' => $currentFlow->user_information,
            'type' => $this->productName(),
            'user_id' => Auth::user()->id,
            'step' => '1',
            'customer_status' => 1,
        ]);

        return $flow;
    }

    public function continue($newFlow, $oldFlow, $step)
    {
        $oldFlow->update([
            'customer_information' => $newFlow->customer_information,
            'product_information' => $newFlow->product_information,
            //'user_information' => $newFlow->user_information
        ]);

        $newFlow->update([
            'step' => $step
        ]);

        return $oldFlow;
    }


    private function searchCustomerInformation()
    {
    	$data = [];

	    $bureau = 'S';

    	$data['bureau'] = $this->codeValue($this->productName(), $data, ['bureau', 'code'], $bureau, 'confirmation');

    	//$data['identity'] = $this->value($data, 'identity', $r, 'identity');

	    return $data;
    }

    private function searchUserInformation()
    {
	    $data = [];

        $data['peoplesoft'] = Auth::user()->peopleSoft;

	    return $data;
    }


    /**
     * @param $request
     * @return mixed
     */
    public function second($request)
    {
        $flow = $this->recentFlow($this->productName(), true);

        $flow->update([
            'customer_information' => $this->secondCustomerInformation($flow, $request),
        ]);

        return $flow;
    }

    public function calculate($request)
    {
	    $flow = $this->recentFlow($this->productName(), true);

	    $flow->update([
		    //'customer_information' => $this->calculateCustomerInformation($flow, $request),
            'user_information' => $this->calculateUserInformation($flow, $request),
            'product_information' => $this->calculateProductInformation($flow, $request)

	    ]);

	    return $flow;
    }

    public function responseScenario($flow, $response)
    {
        $flow->update([
            'product_information' => $this->resultScenario($flow, $response),
        ]);

        return $flow;
    }

    public function resultScenario($flow, $response)
    {
        $data = $flow->product_information;

	    if(!env('BANK_AUTO_DEMO'))
	    {
		    $results = new GetResponse();

		    $data['response_engine_evaluation']['answer_motor_decisions_credit'] = $results->result($response, 'RespuestaMotorDecisionesCredito');

		    $data['response_engine_evaluation']['destination_motor_decisions_credit'] = $results->result($response, 'DestinoMotorDecisionesCredito');
	    } else {
		    $data['response_engine_evaluation']['answer_motor_decisions_credit'] = 'T';

		    $data['response_engine_evaluation']['destination_motor_decisions_credit'] = 'G';
	    }

        return $data;
    }

    public function quotation($flow, $response)
    {
        $flow->update([
            'product_information' => $this->quotedResult($flow, $response),
        ]);

	    $flow->quotations()->create([
		    'identifier' => $flow->identifier,
		    'correlative' => get_json($flow->product_information, ['correlative_stage']),
		    'customer_information' => $flow->customer_information,
		    'product_information' => $flow->product_information,
		    'user_information' => $flow->user_information,
	    ]);

        return $flow;
    }

	public function suggestions($flow, $response)
	{
		$product = $flow->product_information;

		if(!env('BANK_AUTO_DEMO'))
		{
			$getValueFromResponse = new GetResponse();

			$suggestions = $getValueFromResponse->result($response, 'SUGERENCIAS_LIST', true);

			//convertir el objeto suggestions a arreglo
			foreach ($suggestions as $object) {
				$array[] = (array)$object;
			}

			$descriptions = [];

			if (is_array($array))
			{

				// Contar los elementos dentro de la listas "SUGERENCIAS_LIST
				$elements = count($array) - 1;

				for ($i = 1; $i <= $elements; $i++) {

				    $suggestion = $getValueFromResponse->thirdLevelResult($response, 'SUGERENCIAS_LIST', 'SUGERENCIA_' . $i, 'SugerenciaDescripcion');

                    if($suggestion != null)
                    {
                        $descriptions[] = $suggestion;
                    }

				}
			}
//			else {
//				$descriptions[] = $getValueFromResponse->thirdLevelResult($response, 'SUGERENCIAS_LIST', 'SUGERENCIA_1', 'SugerenciaDescripcion');
//			}
		} else {
			$descriptions = [
               'La prima minima debe ser de 40000 LPS',
               'El plazo debe ser menor al seleccionado'
			];
		}

		$product['suggestions'] = $descriptions;

		$flow->update([
			'product_information' => $product,
		]);

		return $flow;
	}

    public function fourth($request)
    {
        $flow = $this->recentFlow($this->productName(), true);

        $flow->update([
            'customer_information' => $this->fourthCustomerInformation($flow, $request),
            //'product_information' => $this->fourthProductInformation($flow, $request),
            'user_information' => $this->fourthUserInformation($flow, $request)
        ]);

        return $flow;
    }

    public function fifth($request)
    {
        $flow = $this->recentFlow($this->productName(), true);

        $flow->update([
            //'customer_information' => $this->fifthCustomerInformation($flow, $request),
            'product_information' => $this->fifthProductInformation($flow, $request),
            'user_information' => $this->fifthUserInformation($flow, $request)
        ]);

        return $flow;
    }


    private function secondCustomerInformation($flow, $request)
    {
        
        $data = $flow->customer_information;

        $r = SanitizeString::clean($request->all());

        $data['name']['first'] = $this->value($data, ['name', 'first'], $r, 'first_name');
        $data['name']['middle'] = $this->value($data, ['name', 'middle'], $r, 'middle_name');
        $data['name']['last'] = $this->value($data, ['name', 'last'], $r, 'last_name');
        $data['name']['second_last'] = $this->value($data, ['name', 'second_last'], $r, 'second_last_name');

        $data['name']['fullname'] = $this->fullname(
            $this->value($data, ['name', 'first'], $r, 'first_name'),
            $this->value($data, ['name', 'middle'], $r, 'middle_name'),
            $this->value($data, ['name', 'last'], $r, 'last_name'),
            $this->value($data, ['name', 'second_last'], $r, 'second_last_name')
        );

        if (isset($r['phone'])) {
            $data['phone'] = str_replace('-', '', $r['phone']);
        }

        if (isset($r['mobile'])) {
            $data['mobile'] = str_replace('-', '', $r['mobile']);
        }

        $data['employer']['job']['status'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'status','code'],  $request->job, 'jobOptions');

        if (isset($request->income)) {
            $data['income'] = $this->cleanAmount($r['income']);
        }

        $data['employer']['job']['type'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'type', 'code'], $request->job_type, 'jobTypeOptions');

        if ($request->job == '1') {
            $data['isr_deduction'] = $this->cleanAmount($r['isr_deduction']);
        }
        else {
            $data['isr_deduction'] = '00';
        }

        if ($request->job == '1') {
            $data['rap_deduction'] = $this->cleanAmount($r['rap_deduction']);
        } else {
            $data['rap_deduction'] = '00';
        }

        if ($request->job == '1') {
            $data['ihss_deduction'] = $this->cleanAmount($r['ihss_deduction']);
        } else {
            $data['ihss_deduction'] = '00';
        }

        if (isset($request->other_deduction)) {
            $data['other_deduction'] = $this->cleanAmount($r['other_deduction']);
        }

        if (isset($r['year'], $r['month'], $r['day'])) {
            $data['birth'] = Date::createFromDate($r['year'], $r['month'], $r['day'])->timestamp;
        }

        $receiveSms = 'N';

        if ($request->receive_sms) {
            $receiveSms = 'S';
        }

        $data['receive_sms'] = $this->codeValue($this->productName(), $data, ['receive_sms', 'code'], $receiveSms, 'confirmation');


        return $data;
    }

	private function calculateCustomerInformation($flow, $request)
	{
		$data = $flow->customer_information;

		$r = SanitizeString::clean($request->all());

		$data['income'] = $this->value($data, 'income', $r, 'income');

		$data['deductions'] = $this->value($data, 'deductions', $request->deductions);

		if (isset($r['year'], $r['month'], $r['day'])) {
			$data['birth'] = Date::createFromDate($r['year'], $r['month'], $r['day'])->timestamp;
		}

        $data['employer']['job']['status'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'status','code'],  $request->job, 'jobOptions');

		return $data;
	}

    private function calculateProductInformation($flow, $request)
    {
        $data = $flow->product_information;

        $r = SanitizeString::clean($request->all());

        $data['auto']['type'] = $this->codeValue($this->productName(), $data, ['auto', 'type', 'code'], $request->type_auto, 'typeAutoOptions');

        $data['auto']['year'] = $this->codeValue($this->productName(), $data, ['auto', 'year', 'code'], $request->year_auto, 'yearAutoOptions');

        $data['auto']['status'] = $this->codeValue($this->productName(), $data, ['auto', 'status', 'code'], $request->status_auto, 'statusAutoOptions');

        $data['auto']['brand'] = $this->codeValue($this->productName(), $data, ['auto', 'brand', 'code'], $request->auto_brand, 'autoBrandOptions');

        $data['auto']['model'] = $this->value($data, ['auto', 'model'], $r, 'auto_model');

        if (isset($request->sale_price)) {
            $data['sale_price'] = $this->cleanAmount($r['sale_price']);
        }

        if (isset($request->premium)) {
            $data['premium'] = $this->cleanAmount($r['premium']);
        }

        if (isset($request->appraisal)) {
            if ($this->cleanAmount($r['appraisal']) == 0)
            {
                $data['appraisal'] = '00';
            } else {
                $data['appraisal'] = $this->cleanAmount($r['appraisal']);
            }
        } else {
            $data['appraisal'] = '00';
        }

        $data['financing_term'] = $this->codeValue($this->productName(), $data, ['financing_term', 'code'],  $request->financing_term, 'financingTermOptions');

	    $data['closing_costs'] = $this->codeValue($this->productName(), $data, ['closing_costs', 'code'],  $request->closing_costs, 'confirmation');

        $data['grace_period'] = $this->codeValue($this->productName(), $data, ['grace_period', 'code'],  $request->grace_period, 'confirmation');

        return $data;

    }

	private function calculateUserInformation($flow, $request)
	{
        $data = $flow->user_information;

        $data['concessionaire'] = $this->codeValue($this->productName(), $data, ['concessionaire', 'code'],  $request->concessionaire, 'concessionaireOptions');

        $data['seller'] = $this->codeValue($this->productName(), $data, ['seller', 'code'], $request->seller, 'sellerOptions');

        return $data;
	}



	private function fourthCustomerInformation($flow, $request)
	{
        $data = $flow->customer_information;

        $r = SanitizeString::clean($request->all());

		$receiveEmail = 'N';

        if($request->has_email === null)
        {
            $data['email']['has_email'] = 'S';
        } else {
            $data['email']['has_email'] = 'N';//$this->value($data, ['email', 'has_email'], $request->has_email);
        }

        if ($request->has_email != null) {
            $data['email']['account'] = 'NOTIENE@DAVIVIENDA.COM.HN';
        } else {
            $data['email']['account'] = $this->value($data, ['email', 'account'], $r, 'email');

	        if ($request->receive_emails) {
		        $receiveEmail = 'S';
	        }
        }

		$data['address']['other_city'] = $this->value($data, ['address', 'other_city'], $r, 'other_city');

		$cityCode = $this->value($data, ['address', 'code'], $request->city);

		$addressLineOne = $this->value($data, ['address', 'first'], $r, 'address_1');
		$addressLineTwo = $this->value($data, ['address', 'second'], $r, 'address_2');
		$addressReference = $this->value($data, ['address', 'third'], $r, 'address_3');

		$address = $this->constructRequestFullAddress($addressLineOne, $addressLineTwo, $addressReference);

		$data['address']['code'] = $cityCode;
		$data['address']['full'] = $address;

		$data['address']['first'] = $addressLineOne;
		$data['address']['second'] = $addressLineTwo;
		$data['address']['third'] = $addressReference;

		$city = explode('>', $cityCode);
		$state = explode('-', $city[0]);
		$municipality = explode('-', $city[1]);
		$colony = explode('-', $city[2]);

		$data['address']['state'] = $state[0];
		$data['address']['state_code'] = $state[1];
		$data['address']['municipality'] = $municipality[0];
		$data['address']['municipality_code'] = $municipality[1];
		$data['address']['colony'] = $colony[0];
		$data['address']['colony_code'] = $colony[1];

        $data['employer']['name'] = $this->value($data, ['employer', 'name'], $r, 'employer_name');

        $data['profession'] = $this->codeValue($this->productName(), $data, ['profession', 'code'], $request->profession, 'professionOptions');

        $data['employer']['job']['name'] = $this->value($data, ['employer', 'job', 'name'], $r, 'job_name');

        $data['employer']['job']['status'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'status', 'code'], $request->job, 'jobOptions');

        if (isset($r['employer_phone']))
        {
            $data['employer']['phone'] = str_replace('-', '', $r['employer_phone']);
        } else {
            $data['employer']['phone'] = '';
        }

		$receiveEmailJob = 'N';

        if($request->has_job_email === null)
        {
            $data['job_email']['has_job_email'] = 'S';
        } else {
            $data['job_email']['has_job_email'] = 'N';
        }

        if ($request->has_job_email != null) {
            $data['job_email']['account'] = 'NOTIENE@DAVIVIENDA.COM.HN';
        } else {
            $data['job_email']['account'] = $this->value($data, ['job_email', 'account'], $r, 'job_email');

	        if ($request->receive_emails_job) {
		        $receiveEmailJob = 'S';
	        }
        }


        $data['receive_emails'] = $this->codeValue($this->productName(), $data, ['receive_emails', 'code'], $receiveEmail, 'confirmation');

		$data['receive_emails_job'] = $this->codeValue($this->productName(), $data, ['receive_emails_job', 'code'], $receiveEmailJob, 'confirmation');

        $data['employer']['type'] = $this->codeValue($this->productName(), $data, ['employer', 'type', 'code'], $request->employer_type, 'employeeTypes');

        if(isset($request->job_contract))
        {
            $data['employer']['job']['contract'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'contract', 'code'], $request->job_contract, 'jobContractOptions');
        }else {
            $data['employer']['job']['contract'] = $this->codeValue($this->productName(), $data, ['employer', 'job', 'contract', 'code'], 'P', 'jobContractOptions');
        }

		$data['employer']['address']['other_city'] = $this->value($data, ['employer', 'address', 'other_city'], $r, 'other_employer_city');

		$employerCityCode = $this->value($data, ['employer', 'address', 'code'], $request->employer_city);

		$employerAddressLineOne = $this->value($data, ['employer', 'address', 'first'], $r, 'employer_address_1');
		$employerAddressLineTwo = $this->value($data, ['employer', 'address', 'second'], $r, 'employer_address_2');
		$employerAddressReference = $this->value($data, ['employer', 'address', 'third'], $r, 'employer_address_3');

		$employerAddress = $this->constructRequestFullAddress($employerAddressLineOne, $employerAddressLineTwo, $employerAddressReference);

		$data['employer']['address']['code'] = $employerCityCode;

		$data['employer']['address']['full'] = $employerAddress;

		$data['employer']['address']['first'] = $employerAddressLineOne;
		$data['employer']['address']['second'] = $employerAddressLineTwo;
		$data['employer']['address']['third'] = $employerAddressReference;

		$employerCity = explode('>', $employerCityCode);
		$employerState = explode('-', $employerCity[0]);
		$employerMunicipality = explode('-', $employerCity[1]);
		$employerColony = explode('-', $employerCity[2]);

		$data['employer']['address']['state'] = $employerState[0];
		$data['employer']['address']['state_code'] = $employerState[1];
		$data['employer']['address']['municipality'] = $employerMunicipality[0];
		$data['employer']['address']['municipality_code'] = $employerMunicipality[1];
		$data['employer']['address']['colony'] = $employerColony[0];
		$data['employer']['address']['colony_code'] = $employerColony[1];

        if (isset($request->employer_year, $request->employer_month, $request->employer_day)) {
            $data['employer']['started_at'] = Date::createFromDate($r['employer_year'], $r['employer_month'], $r['employer_day'])->timestamp;
        }

        $maritalStatus = $this->value($data, ['marital_status', 'code'], $request->marital_status);

        $data['marital_status'] = $this->codeValue($this->productName(), $data, ['marital_status', 'code'], $maritalStatus, 'maritalStatusOptions');

        if ($request->marital_status == 'C') {
            $data['spouse_information']['id_type'] = $this->codeValue($this->productName(), $data, ['spouse_information', 'id_type', 'code'], $request->spouse_id_type, 'idOptions');
            $data['spouse_information']['identity'] = $this->value($data, ['spouse_information', 'identity'], $r, 'spouse_identity');

            $data['spouse'] = $this->value($data, 'spouse', $r, 'spouse');
        } else {
            $data['spouse_information']['id_type'] = $this->codeValue($this->productName(), $data, ['spouse_information', 'id_type', 'code'], '', 'idOptions');
            $data['spouse_information']['identity'] = '';
            $data['spouse'] = '';
        }

        $data['gender'] = $this->codeValue($this->productName(), $data, ['gender', 'code'], $request->gender, 'genderOptions');

        $data['nationality'] = $this->codeValue($this->productName(), $data, ['nationality', 'code'], $request->nationality, 'nations');

        $fatca = $this->value($data, ['fatca', 'code'], $request->fatca);
        $data['fatca'] = $this->codeValue($this->productName(), $data, ['fatca', 'code'], $fatca, 'confirmation');

        for ($i = 1; $i <= 2; $i++)
        {
            $data['references'][$i]['name'] = $this->value($data, ['references', $i, 'name'], $r, 'reference_' . $i . '_name');

            if ($request->get('reference_' . $i . '_relationship'))
            {
                $data['references'][$i]['relationship'] = $this->codeValue($this->productName(), $data, ['references', $i, 'relationship', 'code'], $request->get('reference_' . $i . '_relationship'), 'referenceOptions');
            }

            if ($request->get('reference_' . $i . '_mobile'))
            {
                $data['references'][$i]['mobile'] = str_replace('-', '', $r['reference_' . $i . '_mobile']);
            } else {
                $data['references'][$i]['mobile'] = '';
            }

            if ($request->get('reference_' . $i . '_phone'))
            {
                $data['references'][$i]['phone'] = str_replace('-', '', $r['reference_' . $i . '_phone']);
            } else {
                $data['references'][$i]['phone'] = '';
            }

            if ($request->get('reference_' . $i . '_work_phone'))
            {
                $data['references'][$i]['work_phone'] = str_replace('-', '', $r['reference_' . $i . '_work_phone']);
            } else {
                $data['references'][$i]['work_phone'] = '';
            }
        }

        return $data;
	}

    private function fourthProductInformation($flow, $request)
    {
        $data = $flow->product_information;

        $data['insurance_type'] = $this->codeValue($this->productName(), $data, ['insurance_type', 'code'], $request->insurance_type, 'insuranceTypeOptions');

        return $data;
    }

    private function fourthUserInformation($flow, $request)
    {
        $data = $flow->user_information;

        $data['concessionaire'] = $this->codeValue($this->productName(), $data, ['concessionaire', 'code'],  $request->concessionaire, 'concessionaireOptions');

        $data['seller'] = $this->codeValue($this->productName(), $data, ['seller', 'code'], $request->seller, 'sellerOptions');

        if(isset($request->auto_executive))
        {
            $data['auto_executive'] = $this->codeValue($this->productName(), $data, ['auto_executive', 'code'], $request->auto_executive, 'agentOptions');
        }

        return $data;
    }



    private function fifthCustomerInformation($flow, $request)
    {
        $data = $flow->customer_information;

        $r = SanitizeString::clean($request->all());

	    for ($i = 1; $i <= 2; $i++)
	    {
		    $data['references'][$i]['name'] = $this->value($data, ['references', $i, 'name'], $r, 'reference_' . $i . '_name');

		    if ($request->get('reference_' . $i . '_mobile'))
		    {
			    $data['references'][$i]['mobile'] = str_replace('-', '', $r['reference_' . $i . '_mobile']);
		    }

		    if ($request->get('reference_' . $i . '_phone'))
		    {
			    $data['references'][$i]['phone'] = str_replace('-', '', $r['reference_' . $i . '_phone']);
		    }
	    }

	    $receiveSms = 'N';

	    $receiveEmail = 'N';

	    if ($request->receive_sms) {
		    $receiveSms = 'S';
	    }

	    if ($request->receive_emails) {
		    $receiveEmail = 'S';
	    }

	    $data['receive_sms'] = $this->codeValue($this->productName(), $data, ['receive_sms', 'code'], $receiveSms, 'confirmation');

	    $data['receive_emails'] = $this->codeValue($this->productName(), $data, ['receive_emails', 'code'], $receiveEmail, 'confirmation');

        return $data;
    }

    private function fifthProductInformation($flow, $request)
    {
    	$data = $flow->product_information;

	    $r = SanitizeString::clean($request->all());

//	    $data['grace_period'] = $this->value($data, 'grace_period', $r, 'grace_period');

	    $data['auto']['brand'] = $this->codeValue($this->productName(), $data, ['auto', 'brand', 'code'], $request->auto_brand, 'autoBrandOptions');

	    $data['auto']['model'] = $this->value($data, ['auto', 'model'], $r, 'auto_model');

	    $data['auto']['type'] = $this->codeValue($this->productName(), $data, ['auto', 'type', 'code'], $request->auto_type, 'typeAutoOptions');

	    $data['auto']['color'] = $this->value($data, ['auto', 'color'], $r, 'auto_color');

	    $data['auto']['year'] = $this->codeValue($this->productName(), $data, ['auto', 'year', 'code'], $request->auto_year, 'yearAutoOptions');

	    $data['auto']['displacement'] = $this->value($data, ['auto', 'displacement'], $r, 'auto_displacement');

	    $data['auto']['motor'] = $this->value($data, ['auto', 'motor'], $r, 'motor_numbers');

	    $data['auto']['vin'] = $this->value($data, ['auto', 'vin'], $r, 'vin_numbers');

	    $data['auto']['chassis'] = $this->value($data, ['auto', 'chassis'], $r, 'chassis_numbers');

	    $notes = $this->value($data, ['notes', 'full'], $request->notes);

	    $data['notes']['full'] = $notes;

	    $data['notes']['first'] = $this->divideString($notes, 40, 1);
	    $data['notes']['second'] = $this->divideString($notes, 40, 2);
	    $data['notes']['third'] = $this->divideString($notes, 40, 3);
	    $data['notes']['fourth'] = $this->divideString($notes, 40, 4);
	    $data['notes']['fifth'] = $this->divideString($notes, 40, 5);
	    $data['notes']['sixth'] = $this->divideString($notes, 40, 6);

    	return $data;
    }

	private function fifthUserInformation($flow, $request)
	{
        $data = $flow->user_information;

        $data['concessionaire'] = $this->codeValue($this->productName(), $data, ['concessionaire', 'code'],  $request->concessionaire, 'concessionaireOptions');

        $data['seller'] = $this->codeValue($this->productName(), $data, ['seller', 'code'], $request->seller, 'sellerOptions');

        return $data;
	}

	private function quotedResult($flow, $response)
    {
        $data = $flow->product_information;

	    if(!env('BANK_AUTO_DEMO'))
	    {
		    $results = new GetResponse();

		    $data['response_scenario']['interest_rate'] = $results->result($response, 'TasaInteres');

		    $data['response_scenario']['safe_insurance'] = $results->result($response, 'CuotaSeguroDanho');

		    $data['response_scenario']['debt_insurance'] = $results->result($response, 'CuotaSeguroDeuda');

		    $data['response_scenario']['value_finance'] = $results->result($response, 'ValorFinanciar');

		    $data['response_scenario']['level_fee'] = $results->result($response, 'CuotaNivelada');

		    $data['response_scenario']['total_quota'] = $results->result($response, 'CuotaTotal');

		    $data['response_scenario']['original_debt_insurance'] = $results->result($response, 'CuotaOriginalSeguroDeuda');

		    $data['response_scenario']['miscellaneous_expenses'] = $results->result($response, 'GastosVarios');

		    $data['response_scenario']['legal_expenses'] = $results->result($response, 'GastosLegales');

		    $data['response_scenario']['answer_scenario'] = $results->result($response, 'RespuestaEscenario');
	    } else {
		    $data['response_scenario']['interest_rate'] = rand(11.75, 12.70);

		    $data['response_scenario']['safe_insurance'] = rand(300, 600);

		    $data['response_scenario']['debt_insurance'] = rand(400, 700);

		    $data['response_scenario']['value_finance'] = rand(165000, 200000);

		    $data['response_scenario']['level_fee'] = rand(5000, 7000);

		    $data['response_scenario']['total_quota'] = rand(6000, 11500);

		    $data['response_scenario']['original_debt_insurance'] = rand(150, 500);

		    $data['response_scenario']['miscellaneous_expenses'] = rand(200, 400);

		    $data['response_scenario']['legal_expenses'] = 1500;

		    $data['response_scenario']['answer_scenario'] = get_json($data, ['result_stage']);
	    }

        return $data;
    }


    public function traditional($request)
    {
        $flow = $this->recentFlow($this->productName(), true);

        $flow->update([
            'product_information' => $this->traditionalNotes($flow, $request),
        ]);

        return $flow;
    }



    private function traditionalNotes($flow, $request)
    {
        $data = $flow->product_information;

        $r = SanitizeString::clean($request->all());

        $notes = $this->value($data, ['notes', 'full'], $r, 'notes');

        $data['notes']['full'] = $notes;

        $data['notes']['first'] = $this->divideString($notes, 40, 1);
        $data['notes']['second'] = $this->divideString($notes, 40, 2);
        $data['notes']['third'] = $this->divideString($notes, 40, 3);
        $data['notes']['fourth'] = $this->divideString($notes, 40, 4);
        $data['notes']['fifth'] = $this->divideString($notes, 40, 5);
        $data['notes']['sixth'] = $this->divideString($notes, 40, 6);

        return $data;
    }


    private function productName()
	{
	 return 'bank_auto';
	}
 }