<?php

/**
 * Format phone with dash in middle
 *
 * @param $phone
 * @return mixed
 */
function formatPhone($phone)
{
	return substr_replace($phone, '-', 4, 0);
}

/**
 * Converts a string with a range to numbered formatted range string
 *
 * @param $range
 * @return string
 */
function convertRangeToNumberFormat($range)
{
	$numberFormat = function ($number)
	{
		return number_format($number, 2);
	};

	return implode(' - ', array_map($numberFormat, explode(' - ', $range)));
}

/**
 * Recover field value from json column and set default if empty
 *
 * @param $resource
 * @param $fields
 * @param $empty null
 * @return mixed
 */
function get_json($resource, $fields, $empty = null)
{
	$default = 'NULO';

	if ($empty)
	{
		$default = '';
	}

	if (is_array($fields))
	{
		$length = count($fields);

		if ($length === 5) {
			$result = isset($resource[$fields[0]][$fields[1]][$fields[2]][$fields[3]][$fields[4]]) ? $resource[$fields[0]][$fields[1]][$fields[2]][$fields[3]][$fields[4]] : $default;
		} elseif ($length === 4) {
			$result = isset($resource[$fields[0]][$fields[1]][$fields[2]][$fields[3]]) ? $resource[$fields[0]][$fields[1]][$fields[2]][$fields[3]] : $default;
		} elseif ($length === 3) {
			$result = isset($resource[$fields[0]][$fields[1]][$fields[2]]) ? $resource[$fields[0]][$fields[1]][$fields[2]] : $default;
		} elseif ($length === 2) {
			$result = isset($resource[$fields[0]][$fields[1]]) ? $resource[$fields[0]][$fields[1]] : $default;
		} elseif ($length === 1) {
			$result = isset($resource[$fields[0]]) ? $resource[$fields[0]] : $default;
		}
	} else {
		$result = isset($resource[$fields]) ? $resource[$fields] : $default;
	}

	return $result;
}

function cifUsers($peoplesoft)
{
	$users = [
		504900025,
		50400183,
		50400397, // Lorena Gomez
		50401129,
		43600965,
		50401048, // Heidy
		50400856,
		50400757,
		50400627,
	    43746151, // Arely
		12083404,
		50400312,
		12081789,
		12082162,
		12083002,
		43621328,
		43475612,
		12082772,
		50401107,
		50400810,
		43515275, // Fredy Antonio Matute Perfil autorizado
		12082719,
		504000304,
		12081842, // Iván Ordóñez
		43741325, // Alejandro Lara agregar perfil
		50401175, // Carla Marina Villanueva Reyes

	];

	if (in_array($peoplesoft, $users))
	{
		return true;
	}

	return false;
}

function identifyProductType($code)
{
	$types = [
		1 => 'AHORRO',
		2 => 'CHEQUE',
		3 => 'TARJETA DE CRÉDITO'
	];

	if (array_key_exists($code, $types))
	{
		return $types[$code];
	}

	return 'NO EXISTE EL PRODUCTO';
}

function idTypeValue($idType)
{
	$types = [
		'0' => 'IDENTIDAD',
		'1' => 'CARNET DE RESIDENCIA'
	];

	if (array_key_exists($idType, $types))
	{
		return $types[$idType];
	}

	return 'NO EXISTE EL TIPO DE IDENTIDAD';
}

function checkStep($resource, $steps)
{
	if ($resource)
	{
		if (is_array($steps))
		{
			if (in_array($resource->step, $steps, true))
			{
				return true;
			}

			return false;
		} else {
			if ($resource->step == $steps)
			{
				return true;
			}

			return false;
		}
	}

	return false;
}

function get_json_date($parameter, $format)
{
	if ($parameter === 'NULO')
	{
		return null;
	}

	return date($format, $parameter);
}

function product_name($type)
{
	$products = [
		'bank_auto' => 'Préstamo de auto',
		'bank_card' => 'Tarjeta de crédito',
		'bank_account' => 'Cuenta de ahorro',
		'insurance_safe_family' => 'Familia segura',
	];

	return $products[$type];
}

function get_default_or_value($resource, $comparison, $default)
{
	if ($resource === $comparison)
	{
		return $default;
	}

	return $resource;
}

function address($resource, $type = null)
{
	if ($type === 'employer')
	{
		$colonyArray = ['employer', 'address', 'colony'];
		$colonyCodeArray = ['employer', 'address', 'colony_code'];
		$addressArray = ['employer', 'address', 'full'];
		$municipalityArray = ['employer', 'address', 'municipality'];
		$stateArray = ['employer', 'address', 'state'];
		$otherColony = ['employer', 'address', 'other_city'];
	} else {
		$colonyArray = ['address', 'colony'];
		$colonyCodeArray = ['address', 'colony_code'];
		$addressArray = ['address', 'full'];
		$municipalityArray = ['address', 'municipality'];
		$stateArray = ['address', 'state'];
		$otherColony = ['address', 'other_city'];
	}

	$colonyCode = get_json($resource->customer_information, $colonyCodeArray);

	$colony = get_json($resource->customer_information, $colonyArray);

	if ($colonyCode == '999')
	{
		$colony = get_json($resource->customer_information, $otherColony);
	}

	$address = get_json($resource->customer_information, $addressArray);
	$municipality = get_json($resource->customer_information, $municipalityArray);
	$state = get_json($resource->customer_information, $stateArray);

	return $colony . ' ' . $address . ', ' . $municipality . ', ' . $state;
}

function filter_by_id($filters)
{
	$ids = [];

	$i = 0;

	foreach ($filters as $filter)
	{
		if ($filter !== null)
		{
			if (count($filter) === 0)
			{
//				$ids = [];

				$i++;

				break;
			}

			$ids = array_merge($ids, $filter);

			$i++;
		}
	}

	if ($i === 0)
	{
		return [];
	}

	// Count occurrences of assets ids
	$ids = array_count_values($ids);

	// Sort in reverse from the max occurrences to the lowest
	arsort($ids);

	// Get the max occurrences count
	$maxOccurrence = reset($ids);

	if ($maxOccurrence !== $i)
	{
		return [];
	}

	// Get array of asset ids with the same number of occurrences
	$resources = array_keys($ids, $maxOccurrence);

	return $resources;
}


function flowAccordingToMotor($responseMotor)
{
    if( $responseMotor == 'T')
    {
        return true;
    }

    return false;
}

function grid($flow)
{
    $flow->update([
        'step' => 4
    ]);

    return true;
}