<?php

namespace App\Core;

use App\Core\Traits\Bridge;
use App\Core\Traits\ForgetId;
use App\Core\Traits\HasData;
use App\Core\Traits\LogException;
use App\Core\Traits\Message;
use App\Core\Traits\Peoplesoft;
use App\Core\Traits\ProductParameters;

/**
 * Exit product flow
 *
 * Class ExitFlow
 * @package App\Core
 */
class ExitFlow
{
	use Bridge,
		HasData,
		Peoplesoft,
		ForgetId,
		ProductParameters;

	/**
	 * Call process to exit product flow
	 *
	 * @param $flow
	 * @param $productName
	 * @param $request
	 * @param $process
	 * @param $processParams
	 * @return bool|\Illuminate\Http\RedirectResponse
	 */
	public function exit($flow, $productName, $request, $process, $processParams)
	{
		$productParameters = $this->productParameters($productName);


		$response = $this->soap(
			$productName,
			$productParameters['company'],
			$productParameters[$process],
			$this->parameters($flow, $productName, $request),
			$productParameters[$processParams],
			$flow
        );

		$exitProcess = $productParameters['exit_process'];

		if ($productName === 'bank_card' && $flow->step === '1')
		{
			$exitProcess = $productParameters['search_exit_process'];
		}

		try {
			$response = $this->soap(
				$productParameters['company'],
				$exitProcess,
				$this->parameters($flow, $productName, $request),
				'REJECT_REQUEST_PARAMS'
			);
		} catch (\Exception $e)
		{
			$this->logException($e);

			$this->errorMessage($e->getMessage());

			return false;
		}

		$error = $response->checkIfError($productName);

		if ($error)
		{
			$this->errorMessage(
				$error['message'],
				$flow,
				$error['code'],
				$productParameters['exit_process']
			);

			return false;
		}

		$this->forgetId($productName);

		$this->genericMessage(
			'¡Listo!',
			'<p>La solicitud ha sido rechazada con éxito</p>',
			'success'
		);

		return $response;
	}

	/**
	 * Construct de data object for exit process
	 *
	 * @param $flow
	 * @param $productName
	 * @param $request
	 * @return array
	 */
	public function parameters($flow, $productName, $request) : array
	{
		$productParameters = $this->productParameters($productName);

		if (in_array($productName, ['bank_account']))
		{
			$rejectionType = null;
			$requestNumber = null;

		} elseif (in_array($productName, ['bank_auto'])) {
			$rejectionType = '';
			$requestNumber = $flow->identifier;
		} else {
			$rejectionType = $this->rejectionType($request->reject);
			$requestNumber = get_json($flow->product_information, ['request', 'number']);
		}

		$code = $productParameters['product_code'];

//		$channel = $productParameters['user']['agent'];
		$channel = $productParameters['channel'];

		return [
			new Data(true, 'TipoIdentificacion', null, $flow->customer->identifier_type),
			$this->hasData('Identificacion', $flow->customer->identifier),

			/*
			 * User information
			 */
			$this->peopleSoft(),
			new Data(true, 'Ip', null, request()->ip()),

			/*
			 * Static fields
			 */
			$this->hasData('ProductoOrigenRegistro', $code),
			$this->hasData('CanalOrigenRegistro', $channel),
			$this->hasData('NumeroSolicitudCredito', $requestNumber),
            $this->hasData('TipoRechazo', $rejectionType),
			$this->hasData('CodigoError', $request->reject),
		];
	}


	/**
	 * Return the rejection code
	 *
	 * @param $code
	 * @return mixed
	 */
	private function rejectionType($code)
	{
		$codes = [
			'TU15' => 'T',
			'TA01' => 'R'
		];

		return $codes[$code];
	}

}