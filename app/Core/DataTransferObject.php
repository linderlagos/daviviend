<?php

namespace App\Core;

class DataTransferObject
{
    /**
     * @var string
     */
    protected $Bank;

    /**
     * @var string
     */
    protected $ExecuteMethod;
    
    /**
     * @var string
     */
    protected $Key;
    
 /**
     * @var string
     */
    protected $Error;

    /**
     * @var string
     */
    protected $ExternalError;

    /**
     * @var string
     */
    protected $InternalError;

    /**
     * @var object
     */
    protected $Data;

    /**
     * Constructor
     *
     * @param string $Bank
     * @param string $ExecuteMethod
     * @param string $Key
     * @param string $Error
     * @param string $ExternalError
     * @param string $InternalError
     * @param object $Data
     */
    public function __construct($Bank, $ExecuteMethod = null, $Key =null, $Error = null, $ExternalError = null, $InternalError = null, $Data = null)
    {
        $this->Bank = $Bank;
        $this->ExecuteMethod = $ExecuteMethod;
        $this->Key = 'TERxS3F1Qkx6UTREZlQraldtM1lSQT09LEdIeWpDdUxURWtoaXZEaGtIT0g5b04vbllwUnc2NnphTFIyeUVxTVl1VDg9';
        $this->Error = $Error;
        $this->ExternalError = $ExternalError;
        $this->InternalError = $InternalError;
        $this->Data = $Data;
    }

    /**
     * @return string
     */
    public function getBank() : string
    {
        return $this->Bank;
    }

    /**
     * @return string
     */
    public function getExecuteMethod() : string
    {
        return $this->ExecuteMethod;
    }

    /**
     * @return string
     */
    public function getKey() : string
    {
        return $this->Key;
    }

    /**
     * @return string
     */
    public function getError() : string
    {
        return $this->Error;
    }

    /**
     * @return string
     */
    public function getExternalError() : string
    {
        return $this->ExternalError;
    }

    /**
     * @return string
     */
    public function getInternalError() : string
    {
        return $this->InternalError;
    }

    /**
     * @return object
     */
    public function getData()
    {
        return $this->Data;
    }
}
