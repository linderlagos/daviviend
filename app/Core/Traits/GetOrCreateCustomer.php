<?php

namespace App\Core\Traits;

use App\Models\Customer;

/**
 * Get or creates customer
 *
 * Trait UpdateStep
 * @package App\Core\Traits
 */
trait GetOrCreateCustomer
{
	/**
	 * Get or create customer by identifier
	 *
	 * @param $identifier
	 * @param null $type
	 * @return mixed
	 */
	protected function getOrCreateCustomer($identifier, $type = null)
	{
		$customer = Customer::byIdentifier($identifier)->first();

		if (!$type)
		{
			$type = '0';
		}

		if (!$customer)
		{
			$customer = Customer::create([
				'identifier' => $identifier,
				'identifier_type' => $type
			]);
		}

		return $customer;
	}

}