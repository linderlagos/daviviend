<?php

namespace App\Core\Traits;

// Helpers
use Illuminate\Support\Facades\Storage;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Core\GetPreFilledData;
use App\Http\Response\CreditCardProcessResponse;
use Sabberworm\CSS\Value\Value;

/**
 * Trait to recover lists from file in storage
 *
 * Trait GetLists
 * @package App\Core\Traits
 */
trait StaticAutoExecutive
{
    /**
     * Get lists from file
     *
     * @param $flow
     * @param $decode
     * @return mixed
     */
    public function staticAutoExecutive($staticData)
    {
        $data = [];

        $lists = $this->getLists('bank_auto');

        foreach ($lists['agentOptions'] as $key => $list)
        {
            if($key === (int)$staticData)
            {
                $data = array_key_first($list);

                return $data;
            }
        }

        return $staticData;
    }

}