<?php

namespace App\Core\Traits;

use App\Models\Customer;
use App\Models\Product;
use App\Models\Upload;
use Jenssegers\Date\Date;

/**
 * Trait to put flow id on session
 *
 * Trait PersistId
 * @package App\Core\Traits
 */
trait FilterProducts
{

	/**
	 * Filter products by request
	 *
	 * @param $request
	 * @return \Illuminate\Support\Collection
	 */
	protected function filterProducts($request)
	{
		$ids = filter_by_id([
			$this->dateFilter($request),
			$this->customerFilter($request),
			$this->productFilter($request),
			$this->auditFilter($request),
			$this->signatureAuditFilter($request)
		]);

		$collection = collect($ids);

		$products = collect();

		foreach ($collection->chunk(2000) as $chunk)
		{
			$filter = Product::filterByRole()->filterByIds($chunk);

			if ($request->type)
			{
				$filter->where('type', $request->type);
			} else {
				$filter->where('type', '!=' , 'bank_card');
			}

			$products = $products->merge($filter->get());
		}

		return $products;
	}

	/**
	 * @param $request
	 * @return |null
	 */
	private function dateFilter($request)
	{
		if ($request->from)
		{
			$from = Date::createFromFormat('d/m/Y', $request->from);

			if ($request->to)
			{
				$to = Date::createFromFormat('d/m/Y', $request->to);

				$products = Product::dateRange($from, $to)->pluck('id')->toArray();
			} else {

				$today = Date::now();

				$products = Product::dateRange($from, $today)->pluck('id')->toArray();
			}

			return $products;
		}

		return null;
	}

	/**
	 * @param $request
	 * @return array|null
	 */
	private function customerFilter($request)
	{
		if ($request->customer)
		{
			$customer = Customer::where('identifier', $request->customer)->first();

			if ($customer)
			{
				$products = $customer->products()->pluck('id')->toArray();

				return $products;
			}

			return [];
		}

		return null;
	}

	/**
	 * @param $request
	 * @return |null
	 */
	private function productFilter($request)
	{
		if ($request->product)
		{
			$products = Product::where('identifier', $request->product)->pluck('id')->toArray();

			return $products;
		}

		return null;
	}

	/**
	 * @param $request
	 * @return |null
	 */
	private function auditFilter($request)
	{
		if ($request->audit)
		{
			$name = $request->audit;

			$products = Product::whereHas('audits', function ($query) use ($name) {
				$query->where('name', $name);
			})->pluck('id')->toArray();

			return $products;
		}

		return null;
	}

	/**
	 * @param $request
	 * @return |null
	 */
	private function signatureAuditFilter($request)
	{
		if ($request->upload_audit)
		{
			$name = $request->upload_audit;

			$products = Product::whereHas('uploads', function ($query) use ($name) {
				$query->where('type', 'firma')->whereHas('audits', function ($query) use ($name) {
					$query->where('name', $name);
				});
			})->pluck('id')->toArray();

			return $products;
		}

		return null;
	}
}