<?php

namespace App\Core\Traits;

/**
 * Trait to put flow id on session
 *
 * Trait PersistId
 * @package App\Core\Traits
 */
trait PersistId
{
	/**
	 * Persist product flow id
	 *
	 * @param $flow
	 * @param $id
	 * @return bool
	 */
	protected function persistId($flow, $id)
	{
		session()->put([
			$flow . '_id' => $id
		]);

		return true;
	}
}