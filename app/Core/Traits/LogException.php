<?php

namespace App\Core\Traits;

// Helpers
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Auth;

/**
 * Trait to log exception on Bugsnag
 *
 * Trait LogException
 * @package App\Core\Traits
 */
trait LogException
{
	/**
	 * Log exception on Bugsnag
	 *
	 * @param $e
	 * @param $flow
	 * @return bool
	 */
	protected function logException($e, $flow = null)
	{
		/*if ($flow)
		{
			$customer = 'No se ha asociado el flujo a un cliente';

			if ($flow->customer)
			{
				$customer = [
					'name' => $flow->customer->name,
					'identifier' => $flow->customer->identifier,
					'cif' => $flow->customer->cif,
				];
			}

			Bugsnag::registerCallback(function ($report) use ($flow, $customer) {
				$report->setMetaData([
					'customer' => $customer,
					'flow' => [
						'identifier' => $flow->identifier,
						'type' => $flow->type,
						'company' => $flow->company,
						'step' => $flow->step,
						'error' => $flow->error,
					],
					'user' => [
						'name' => Auth::user()->name,
					]
				]);
			});
		}

		Bugsnag::notifyException($e);*/

		return true;
	}
}