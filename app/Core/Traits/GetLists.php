<?php

namespace App\Core\Traits;

// Helpers
use Illuminate\Support\Facades\Storage;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

/**
 * Trait to recover lists from file in storage
 *
 * Trait GetLists
 * @package App\Core\Traits
 */
trait GetLists
{
	/**
	 * Get lists from file
	 *
	 * @param $flow
	 * @param $decode
	 * @return mixed
	 */
	public function getLists($flow, $decode = true)
	{
		$getLists = '';
		try {
			$getLists = Storage::disk('local')->get('public/' . $flow . '/' . env(strtoupper($flow) . '_PRINTABLES_VERSION') . '/lists.txt');
		} catch (\Exception $e)
		{
			Bugsnag::notifyException($e);

			alert()
				->html(
					'<p>No se pudieron recuperar las listas prellenadas</p>',
					'error',
					'¡Oh no!'
				)
				->showConfirmButton('Continuar');
		}

		if ($decode)
		{
			return json_decode($getLists, true);
		} else {
			return $getLists;
		}
	}

}