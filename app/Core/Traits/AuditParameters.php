<?php

namespace App\Core\Traits;

/**
 * Trait to return basic product parameters
 *
 * Trait UpdateStep
 * @package App\Core\Traits
 */
trait AuditParameters
{
	/**
	 * Return parameters based on product name
	 *
	 * @return array
	 */
	protected function auditParameters()
	{
		$parameters = [
			'cif_types' => [
				'creado' => 'Sin asignar',
				'incompleto' => 'Incompleto',
				'completo' => 'Completo',
				'cerrado' => 'Cancelada',
			],
			'formalization_upload_types' => [
				'creado' => 'Sin revisar',
				'no-corresponde' => 'No corresponde a firma',
				'revisado' => 'Revisado',
				'subido' => 'Subido al sistema'
			]
		];

		return $parameters;
	}
}