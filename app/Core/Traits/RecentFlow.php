<?php

namespace App\Core\Traits;


use App\Models\Flow;

/**
 * Retrieve recent flow with stored session
 *
 * Trait RecentFlow
 * @package App\Core\Traits
 */
trait RecentFlow
{
	/**
	 * Get recent flow based on session
	 *
	 * @param $productName
	 * @param $fail
	 * @return mixed
	 */
	public function recentFlow($productName, $fail = false)
	{
		if ($fail)
		{

			return Flow::recent($productName)->firstOrFail();
		}

		return Flow::recent($productName)->first();
	}
}