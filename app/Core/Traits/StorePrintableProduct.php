<?php

namespace App\Core\Traits;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Auth;

/**
 * Trait to return basic product parameters
 *
 * Trait UpdateStep
 * @package App\Core\Traits
 */
trait StorePrintableProduct
{
	use RandomString;

	/**
	 * Store flow in final product table
	 *
	 * @param $flow
	 * @param $version
	 * @param $company
	 * @return object
	 */
	protected function storePrintableProduct($flow, $version, $company)
	{
//		try {
			$product = $flow->customer->products()->create([
				'identifier' => $flow->identifier,
				'random' => time() . $this->generateRandomString(30),
				'customer_information' => $flow->customer_information,
				'product_information' => $flow->product_information,
				'user_information' => $flow->user_information,
				'version' => $version,
				'type' => $flow->type,
				'parent_id' => $flow->product_id,
				'user_id' => $flow->user_id,
				'company' => $company,
				'customer_status' => $flow->customer_status,
			]);

			$product->customer->update([
				'cif' => $this->updateCustomerIfValueIsDifferent($product, ['cif', 'value'], 'cif'),
				'cif_type' => $this->updateCustomerIfValueIsDifferent($product, ['cif', 'code'], 'cif_type'),
				'name' => $this->updateCustomerIfValueIsDifferent($product, ['name', 'fullname'], 'name'),
				'birth' => $this->updateCustomerIfValueIsDifferent($product, 'birth', 'birth'),
			]);

			$user = Auth::user();

			$product->audits()->create([
				'name' => 'creado',
				'type' => 'cif',
				'user_id' => $user->id
			]);

			activity('product')
				->causedBy($user)
				->performedOn($product)
				->log('Se creó el producto');
//		} catch (\Exception $e)
//		{
//			Bugsnag::notifyException($e);
//
//			return abort(500);
//		}

		return $product;
	}

	/**
	 * Checks if value exists in product, is not nullable and is different than customer's column value
	 *
	 * @param $product
	 * @param $field
	 * @param $column
	 * @return mixed
	 */
	private function updateCustomerIfValueIsDifferent($product, $field, $column)
	{
		$getProductField = get_json($product->customer_information, $field);

		$value = $product->customer->$column;

		if ($getProductField !== 'NULO' && $getProductField !== $value)
		{
			$value = $getProductField;
		}

		return $value;
	}
}