<?php

namespace App\Core\Traits;

/**
 * Trait to generate flash message
 *
 * Trait Message
 * @package App\Core\Traits
 */
trait Message
{
	/**
	 * Flash generic message
	 *
	 * @param $title
	 * @param $code
	 * @param $type
	 * @param $button
	 * @return bool
	 */
	public function genericMessage($title, $code, $type, $button = 'Continuar')
	{
		alert()
			->html(
				$title,
				$code,
				$type
			)
			->showConfirmButton($button);

		return true;
	}

	/**
	 * Flash error message
	 *
	 * @param $error
	 * @param null $flow
	 * @param null $code
	 * @param null $process
	 * @return bool
	 */
	public function errorMessage($error, $flow = null, $code = null, $process = null): bool
	{
		if ($flow) {
			$this->updateFlow($flow, $code, $process, $error);
		}

		$this->genericMessage(
			'¡Oh no!',
			'<p>Se ha presentado el siguiente error:</p>
						<p>' . $error . '</p>
					',
			'error',
			'Volver a comenzar'
		);

		return true;
	}

	/**
	 * Updated product flow with error info
	 *
	 * @param $flow
	 * @param $code
	 * @param $process
	 * @param $error
	 * @return bool
	 */
	private function updateFlow($flow, $code, $process, $error)
	{
		/*$flow->update([
			'error' => [
				'code' => $code,
				'process' => $process,
				'message' => $error
			]
		]);*/

		return true;
	}
}