<?php

namespace App\Core\Traits;

use App\Rules\hn\RealId;
use App\Rules\NumDash;
use FontLib\Table\Type\name;

/**
 * Trait to return basic product parameters
 *
 * Trait UpdateStep
 * @package App\Core\Traits
 */
trait ProductParameters
{
	/**
	 * Return parameters based on product name
	 *
	 * @param $productName
	 * @return bool
	 */
	protected function productParameters($productName)
	{
		$parameters = [
			'bank_account' => [
				'home_route' => route('bank.account.index'),
				'search_rules' => $this->bankAccountSearchRules(),
				'exit_rules' => $this->bankAccountExitRules(),
				'company' => 'HN',
				'search_process' => 'DIGITAL_ACCOUNT_SEARCH_ENTAILMENT_DATA',
				'exit_process' => 'DIGITAL_ACCOUNT_REJECT_REQUEST',
				'exit_process_params' => 'REJECT_REQUEST_PARAMS',
				'product_code' => 'CTA',
				'active_flows' => $this->bankAccountActiveFlows(),
				'channel' => 'LBY',
				'user' => [
					'branch' => '120',
					'agent' => 'T14'
				],
				'cross_sell' => [
					'insurance_safe_family'
				],
				'upload_types' => [
					'identidad' => 'Identidad',
					'firma' => 'Firma',
					'escritura-de-comerciante' => 'Escritura de Comerciante'
				]
			],
			'bank_card' => [
				'home_route' => route('bank.card.index'),
				'search_rules' => $this->bankCardSearchRules(),
				'exit_rules' => $this->bankCardExitRules(),
				'company' => 'HN',
				'search_process' => 'DIGITAL_CARD_SEARCH_APPROVED_DATA_PROCESS',
				'exit_process' => 'DIGITAL_CARD_REJECT_SEARCH',
				'entailment_exit_process' => 'DIGITAL_CARD_REJECT_REQUEST',
				'exit_process_params' => 'REJECT_REQUEST_PARAMS',
				'exit_process' => 'DIGITAL_CARD_REJECT_REQUEST',
				'search_exit_process' => 'DIGITAL_CARD_REJECT_REQUEST',
				'product_code' => 'TCA',
				'active_flows' => $this->bankCardActiveFlows(),
				'channel' => 'LBY',
				'user' => [
					'branch' => '169',
					'agent' => 'TDS'
				],
				'cards' => [
					'DPT' => [
						'code' => 'DPT',
						'name' => 'Tarjeta DaviPuntos'
					],
					'DDP' => [
						'code' => 'DDP',
						'name' => 'Tarjeta Dadinero Plus'
					]
				],
				'upload_types' => []
			],
			'insurance_safe_family' => [
				'home_route' => route('insurance.safe.family.index'),
				'upload_types' => []
			],
            'bank_auto' => [
                'home_route' => route('bank.auto.index'),
	            'search_rules' => $this->bankAutoSearchRules(),
	            'exit_rules' => $this->bankAutoExitRules(),
	            'company' => 'HN',
	            'search_process' => 'DIGITAL_CARD_SEARCH_APPROVED_DATA_PROCESS',
	            'exit_process' => 'DIGITAL_CARD_REJECT_SEARCH', // Confirmar si están correctos
                //'exit_process_params' => 'REJECT_CAR_REQUEST_PARAMS', // Confirmar si están correctos
	            'exit_process_params' => 'REJECT_REQUEST_PARAMS',
	            'product_code' => 'AUA', // Confirmar si están correctos
	            'active_flows' => $this->bankAutoActiveFlows(),
	            'channel' => 'LBY', // Confirmar si están correctos
            ]
		];

		if (isset($parameters[$productName]))
		{
			return $parameters[$productName];
		}

		return abort(404);
	}


	private function bankAccountSearchRules()
	{
		return [
			'identity' => [
				'required',
				new NumDash(),
				new RealId()
			]
		];
	}

	private function bankAccountExitRules()
	{
		return [
			'reject' => 'required',
		];
	}

	private function bankAccountActiveFlows()
	{
		return [
			'bank_card',
		];
	}

	private function bankAutoSearchRules()
	{
		return [
			'identity' => [
				'required',
				new NumDash()
			]
		];
	}

	private function bankCardSearchRules()
	{
		return [
			'card' => [
				'required'
			],
			'identity' => [
				'required',
				new NumDash(),
				new RealId()
			]
		];
	}

	private function bankCardExitRules()
	{
		return [
			'reject' => 'required',
		];
	}

	private function bankCardActiveFlows()
	{
		return [
			'bank_account'
		];
	}

	private function bankAutoActiveFlows()
	{
		return [

		];
	}


	private function bankAutoExitRules()
	{
		return [
//			'reject' => 'required',
		];
	}
}