<?php

namespace App\Core\Traits;

/**
 * Retrieve recent flow with stored session
 *
 * Trait RecentFlow
 * @package App\Core\Traits
 */
trait RedirectIfResponseHasError
{
	use ProductParameters;

	/**
	 * Check if response has error and redirect to accorded route
	 *
	 * @param $productName
	 * @param $code
	 * @return mixed
	 */
	public function redirectIfResponseHasError($code, $productName)
	{
		if (env('ENABLE_VUE_RESPONSES'))
		{
			return response()->json([], $code);
		}

		$productParameters = $this->productParameters($productName);

		if($code === 403)
		{
			return redirect()->to($productParameters['home_route']);
		}

		return redirect()->back();
	}
}