<?php

namespace App\Core\Traits;

// Helpers
use App\Core\SanitizeString;
use Illuminate\Support\Facades\Storage;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

/**
 * Trait to recover lists from file in storage
 *
 * Trait GetLists
 * @package App\Core\Traits
 */
trait GetValueFromResponse
{
	public function getValueFromResponse($response, $lists, $returnObject = null)
	{
		if (empty($response))
		{
			return null;
		}

		$indentations = 1;

		if (is_array($lists))
		{
			$indentations = count($lists);
		}

		$object = $response;

		for ($i = 1; $i <= $indentations; $i++)
		{
			$results = [];

			if (!is_array($object))
			{
				$results[] = $object;
			} else {
				$results = $object;
			}

			foreach ($results as $result)
			{
				if ($result->Field == $lists[$i - 1])
				{
					if ($result->HasData)
					{
						if ($i === $indentations)
						{
							if ($returnObject)
							{
								return $result;
							}

							return SanitizeString::single($result->Value);
						}

						$object = $result->DataList->Data;
					}
				}
			}
		}

		return null;
	}
}