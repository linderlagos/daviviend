<?php

namespace App\Core\Traits;

// Core
use App\Core\Data;
use App\Core\CallSoap;
use App\Core\DataTransferObject;

// Requests
use App\Http\Requests\DoProcess;
use App\Http\Response\DoProcessResponse;
use App\Http\Requests\CreditCardProcess;
use App\Http\Response\CreditCardProcessResponse;


// Helpers
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

/**
 * Trait to create a bridge for the webservice
 *
 * Class Bridge
 * @package App\Core
 */
trait Bridge
{
	use Message,
		LogException;

    /**
     * Call webservice
     *
     * @param $productName
     * @param $bank
	 * @param $key
     * @param $parameters
     * @param $executeMethod
     * @param $field
     * @param $flow
     * @return mixed
     */
    public function soap($productName, $bank, $executeMethod, $parameters = null, $field = null, $flow = null)
    {
		//dd($productName, $bank, $executeMethod,  $parameters, $field, $flow);
        $soap = new CallSoap(
            'ServibotDavivienda',
            [
                DoProcess::class,
                DoProcessResponse::class
            ],
            'ServibotDavivienda.DoProcess',
            new DoProcess(
                new DataTransferObject(
                    $bank,
                    $executeMethod,
                    null,
                    null,
                    null,
                    null,
                    new Data(
                        true,
                        $field,
                        $parameters
                    )
                )
            )
        );
//dd($soap);
	    try {
			$response = $soap->soap();
			
        } catch (\Exception $e) {
		    $this->logException($e, $flow);

		    $this->errorMessage(
			    $e->getMessage(),
			    $flow,
			    $e->getCode(),
			    $executeMethod
		    );

		    return [
		    	'code' => 500,
			    'response_error_code' => null,
			    'message' => $e->getMessage(),
			    'response' => null
		    ];
	    }

	    // Checks if soap responses returns error
	    $error = $response->checkIfError($productName);

	    if ($error )
	    {
		    $this->errorMessage(
			    $error['message'],
			    $flow,
			    $error['code'],
			    $executeMethod
		    );

		    return [
			    'code' => 403,
			    'response_error_code' => $error['code'],
			    'message' => $error['message'],
			    'response' => $response
		    ];
	    }

	    return [
	    	'code' => 200,
		    'response_error_code' => null,
		    'message' => 'Posteo satisfactorio',
		    'response' => $response
	    ];
    }
}
