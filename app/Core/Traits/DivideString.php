<?php

namespace App\Core\Traits;

/**
 * Trait to update flow step
 *
 * Trait UpdateStep
 * @package App\Core\Traits
 */
trait DivideString
{
	/**
	 * Divide the string up to 3 sections
	 *
	 * @param $string
	 * @param $length
	 * @param $section
	 * @return bool|string
	 */
	protected function divideString($string, $length, $section)
	{
		if (empty($string))
		{
			return null;
		}

		$dividedString =  str_split($string, $length);

		$key = $section - 1;

		if (isset($dividedString[$key]))
		{
			return $dividedString[$key];
		}

		return null;
	}
}