<?php

namespace App\Core;

use Artisaninweb\SoapWrapper\SoapWrapper;

/**
 * Class GetApprovedLists
 * @package App\Core
 */
class CallSoap
{
    /**
     * @var $name
     */
    protected $name;

    /**
     * @var $classmap
     */
    protected $classmap;

    /**
     * @var $call
     */
    protected $call;

    /**
     * @var $object
     */
    protected $object;

    /**
     * SoapController constructor.
     *
     * @param $name
     * @param $classmap
     * @param $call
     * @param $object
     */
    public function __construct($name, $classmap, $call, $object)
    {
        $this->name = $name;
        $this->classmap = $classmap;
        $this->call = $call;
        $this->object = $object;
    }

    /**
     * Call Soap
     *
     * @return mixed
     * @throws \Artisaninweb\SoapWrapper\Exceptions\ServiceAlreadyExists
     */
    public function soap()
    {
       
        $soap = new SoapWrapper();
        
        $soap->add("ServibotDavivienda", function ($service) {
            $service
                ->wsdl(env('WSDL_URL'))
                ->trace(true)
                ->classmap($this->classmap);
        });
        
        return $soap->call($this->call, [
            $this->object
        ]);
    }


     /**
     * Call Soap
     *
     * @return mixed
     * @throws \Artisaninweb\SoapWrapper\Exceptions\ServiceAlreadyExists
     */
    public function soapActiveDirectory()
    {
       
        $soap = new SoapWrapper();
        
        $soap->add($this->name, function ($service) {
            $service
                ->wsdl(env('WSDL_URL_ACTIVE_DIRECTORY'))
                ->trace(true)
                ->classmap($this->classmap);
        });
        
        return $soap->call($this->call, [
            $this->object
        ]);
    }

}
