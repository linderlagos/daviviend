<?php

namespace App\Core\Insurance\SafeFamily;

// Helpers
use App\Core\Data;
use App\Core\Parameters;
use Jenssegers\Date\Date;

/**
 * Class EntailmentProcessParameters
 * @package App\Core
 */
class SafeFamilyParameters extends Parameters
{
	/**
	 * Construct de data object for Search Process
	 *
	 * @param $parent
	 * @return array
	 */
	public function first($parent): array
	{
		return [
			$this->hasData('Identificacion', $parent->customer->identifier),
			$this->hasData('ProductoOrigenRegistro', 'CTA'),
			new Data(true, 'Ip', null, request()->ip()),
			$this->peopleSoft(),
		];
	}

	public function second($product)
	{
		$firstMd = $this->getFirstMdAnswer($product);

		return [
			$this->hasData('Poliza', get_json($product->product_information, ['policy', 'working'])),
			$this->hasData('Cif', get_json($product->customer_information, ['cif', 'value'])),
			$this->hasData('Identificacion', $product->customer->identifier),
			$this->hasData('TipoCuenta', get_json($product->product_information, ['account', 'code'])),
			$this->hasData('Cuenta', get_json($product->product_information, ['account', 'value'])),
			$this->hasData('Agencia', get_json($product->user_information, ['branch', 'code'])),
			$this->hasData('Peso', get_json($product->customer_information, 'height')),
			$this->hasData('Altura', get_json($product->customer_information, 'weight')),
			$this->hasData('Imc', get_json($product->customer_information, 'imc')),
			$this->hasData('Cuestionario1', $firstMd),
			$this->hasData('ProductoOrigenRegistro', 'CTA'),
			new Data(true, 'Ip', null, request()->ip()),
			$this->peopleSoft(),
		];
	}

	public function secondResponse($policy)
	{
		return [
			$this->hasData('Poliza', $policy),
			$this->hasData('ProductoOrigenRegistro', 'CTA'),
			new Data(true, 'Ip', null, request()->ip()),
			$this->peopleSoft(),
		];
	}

	public function fourth($product)
	{
		return [
			new Data(
				true,
				'DIGITAL_INSURACE_FORM_ONE_ANSWER_PARAMS',
				[
					$this->hasData('FormaPago', get_json($product->product_information, ['payment_type', 'code'])),
					$this->hasData('Plan', get_json($product->product_information, ['amount', 'code'])),
					$this->hasData('CuentaSeleccionada', get_json($product->product_information, ['debit', 'value'])),
					$this->hasData('TipoCuentaSeleccionada', get_json($product->product_information, ['debit', 'code'])),


					$this->hasData('Poliza', $product->identifier),
					$this->hasData('ProductoOrigenRegistro', 'CTA'),
					new Data(true, 'Ip', null, request()->ip()),
					$this->peopleSoft()
				]
			),

			$this->constructSecondMedicalQuestionary($product, 'DIGITAL_INSURACE_FORM_ONE_ANSWER_LIST')
		];
	}

	public function fifth($flow)
	{
		return [
			new Data(
				true,
				'DIGITAL_INSURACE_INSERT_BENEFICIARY_PARAMS',
				[
					$this->hasData('Poliza', $flow->identifier),
					$this->hasData('ProductoOrigenRegistro', 'CTA'),
					new Data(true, 'Ip', null, request()->ip()),
					$this->peopleSoft()
				]
			),

			$this->constructBeneficiaries($flow, 'DIGITAL_INSURACE_INSERT_BENEFICIARY_LIST')
		];
	}

	public function sixth($product)
	{
		return [
			$this->hasData('Poliza', $product->identifier),
			$this->hasData('Confirmacion', get_json($product->customer_information, 'confirmation')),
			new Data(true, 'Ip', null, request()->ip()),
			$this->peopleSoft(),
		];
	}

	private function getFirstMdAnswer($customer)
	{
		$confirm = 0;

		for ($i = 1; $i <= 5; $i++)
		{
			if (get_json($customer->product_information, ['first_medical_questionary', $i, 'question', 'code']) === 'S') {
				$confirm++;
			}
		}

		$answer = 'N';

		if ($confirm > 0)
		{
			$answer = 'S';
		}

		return $answer;
	}


	private function constructSecondMedicalQuestionary($product, $field)
	{
		$array = $product->product_information['second_medical_questionary'];

		$questions = [];

		foreach ($array as $key => $question)
		{
			$hospital = $question['hospital'];
			$accident = $question['accident'];
			$date = $question['date'];

			if ($hospital === 'Otro')
			{
				$hospital = $question['other_hospital'];
			}

			if ($accident === 'OTROS')
			{
				$accident = $question['other_accident'];
			}

			if ($date !== 'NULO')
			{
				$date = Date::createFromTimestamp($question['date'])->format('d/m/Y');
			}

			$questions[] = new Data(
				true,
				'PREGUNTA_' . $key,
				[
					$this->hasData('Pregunta', $key),
					$this->hasData('Respuesta', $question['question']),
					$this->hasData('Diagnostico', $question['diagnostic']),
					$this->hasData('Fecha', $date),
					$this->hasData('Medico', $question['doctor']),
					$this->hasData('Hospital', $hospital),
					$this->hasData('TipoAccidente', $accident),
					$this->hasData('Secuelas', $question['sequel']),
				]
			);
		}

		return new Data(
			true,
			$field,
			$questions
		);
	}

	private function constructBeneficiaries($product, $field)
	{
		$array = $product->customer_information['beneficiaries'];

		$beneficiaries = [];

		foreach ($array as $key => $beneficiary)
		{
			$date = get_json_date(get_json($beneficiary, 'birth'), 'd/m/Y');

			$beneficiaries[] = new Data(
				true,
				'BENEFICIARIO_' . $key,
				[
					$this->hasData('NumeroBeneficiario', $key),
					$this->hasData('NombreBeneficiario', get_json($beneficiary, 'name')),
					$this->hasData('Parentesco', get_json($beneficiary, 'relationship')),
					$this->hasData('Participacion', get_json($beneficiary, 'participation')),
					$this->hasData('FechaBeneficiario', $date)
				]
			);
		}

		return new Data(
			true,
			$field,
			$beneficiaries
		);
	}
}
