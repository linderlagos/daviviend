<?php

namespace App\Core\Insurance\SafeFamily;

// Core
use App\Core\GetResponse;
use App\Core\CustomerRegistry;
use App\Core\GetPreFilledData;

// Helpers
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;

/**
 * Class HandleCustomerRegistry
 * @package App\Core
 */
class SafeFamilyRegistry extends CustomerRegistry
{
	/**
	 * Get request parameters
	 *
	 * @var
	 */
	protected $request;

	/**
	 * Store flow with error description if customer already has policy
	 *
	 * @param $parent
	 * @param $error
	 * @param $productFlow
	 * @return mixed
	 */
	public function customerAlreadyHasPolicy($parent, $error, $productFlow)
	{
		$flow = $parent->customer->flows()->create([
			'error' => $error + [
					'process' => 'DIGITAL_INSURACE_VALIDATE_IDENTITY'
				],
			'type' => $this->productName(),
			'product_id' => $parent->id,
			'company' => 'HN-SEGUROS'
		]);

		return $flow;
	}

	/**
	 * Store flow with parent data
	 *
	 * @param $parent
	 * @param $response
	 * @return mixed
	 */
	public function first($parent, $response)
	{
		$getValueFromResponse = new GetResponse();

		if(!env('INSURANCE_SAFE_FAMILY_DEMO'))
		{
            $policy = $getValueFromResponse->secondLevelResult($response, 'DIGITAL_INSURACE_VALIDATE_IDENTITY_PARAMS', 'Poliza');

		} else {

		    $policy = rand(1000000, 9999999);
        }

		$flow = $parent->customer->flows()->create([
			'identifier' => $policy,
			'customer_information' => $this->firstCustomerInformation($parent),
			'product_information' => $this->firstProductInformation($parent, $policy),
			'user_information' => $this->firstUserInformation($parent),
			'product_id' => $parent->id,
			'company' => 'HN-SEGUROS',
			'type' => $this->productName(),
			'user_id' => Auth::user()->id,
		]);

		return $flow;
	}

	/**
	 * Store data from first step
	 *
	 * @param $request
	 * @return mixed
	 */
	public function second($request)
	{
		$flow = $this->recentFlow($this->productName());

		$flow->update([
			'customer_information' => $this->secondCustomerInformation($flow, $request),
			'product_information' => $this->secondProductInformation($flow, $request),
		]);

		return $flow;
	}

	/**
	 * Store data from first step response
	 *
	 * @param $flow
	 * @return bool
	 */
	public function secondResponse($flow)
	{
		$flow->update([
			'product_information' => $this->secondResponseProductInformation($flow)
		]);

		return true;
	}

	/**
	 * Store data from second step
	 *
	 * @param $request
	 * @return mixed
	 */
	public function third($request)
	{
		$flow = $this->recentFlow($this->productName());

		$flow->update([
			'product_information' => $this->thirdProductInformation($flow, $request),
		]);

		return $flow;
	}

	/**
	 * Store data from third step
	 *
	 * @param $request
	 * @return mixed
	 */
	public function fourth($request)
	{
		$flow = $this->recentFlow($this->productName());

		$flow->update([
			'product_information' => $this->fourthProductInformation($flow, $request),
		]);

		return $flow;
	}

	/**
	 * Store data from fourth step
	 *
	 * @param $request
	 * @return mixed
	 */
	public function fifth($request)
	{
		$flow = $this->recentFlow($this->productName());

		$flow->update([
			'customer_information' => $this->fifthCustomerInformation($flow, $request),
		]);

		return $flow;
	}

	/**
	 * Store data from fourth step response
	 *
	 * @param $flow
	 * @param $response
	 * @return bool
	 */
	public function fifthResponse($flow, $response)
	{
		$flow->update([
			'product_information' => $this->fifthResponseProductInformation($flow, $response),
		]);

		return true;
	}

	/**
	 * Store data from fifth step
	 *
	 * @param $request
	 * @return mixed
	 */
	public function sixth($request)
	{
		$flow = $this->recentFlow($this->productName());

		$flow->update([
			'customer_information' => $this->sixthCustomerInformation($flow, $request),
			'user_information' => $this->sixthUserInformation($flow, $request)
		]);

		return $flow;
	}

	/**
	 * Store data from sixth step response
	 *
	 * @param $flow
	 * @param $response
	 * @return mixed
	 */
	public function sixthResponse($flow, $response)
	{
		$flow->update([
			'customer_information' => $this->sixthResponseCustomerInformation($flow, $response),
			'product_information' => $this->sixthResponseProductInformation($flow, $response),
			'user_information' => $this->sixthResponseUserInformation($flow, $response)
		]);

		return $flow;
	}

	/**
	 * Construct customer information
	 *
	 * @param $parent
	 * @return mixed
	 */
	private function firstCustomerInformation($parent)
	{
		$data = $parent->customer_information;

		return $data;
	}

	/**
	 * Construct product information
	 *
	 * @param $parent
	 * @param $policy
	 * @return array
	 */
	private function firstProductInformation($parent, $policy)
	{
		$data = [];

		$data['account']['code'] = 1;
		$data['account']['value'] = $parent->identifier;
		$data['account']['type'] = identifyProductType(1);
		$data['policy']['working'] = $policy;

		return $data;
	}

	/**
	 * Construct user information
	 *
	 * @param $parent
	 * @return mixed
	 */
	private function firstUserInformation($parent)
	{
		$data = $parent->user_information;

		$data['agent']['ip'] = request()->ip();

		return $data;
	}

	/**
	 * Construct customer information
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function secondCustomerInformation($flow, $request)
	{
		$data = $flow->customer_information;

		$imc = round(($request->weight / 2.205) / (pow($request->height, 2)), 2);

		$data['height'] = $request->height;
		$data['weight'] = $request->weight;
		$data['imc'] = $imc;

		return $data;
	}

	/**
	 * Construct product information
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function secondProductInformation($flow, $request)
	{
		$data = $flow->product_information;

		for ($i = 1; $i <= 5; $i++)
		{
			$data['first_medical_questionary'][$i]['question'] = $this->codeValue($this->productName(), $data, ['first_medical_questionary', $i, 'question', 'code'], $request->get('first_md_' . $i), 'confirmation');
		}

		return $data;
	}

	/**
	 * Construct response product information
	 *
	 * @param $flow
	 * @return mixed
	 */
	private function secondResponseProductInformation($flow)
	{
		$data = $flow->product_information;

		$lists = new GetPreFilledData();


		if(!env('INSURANCE_SAFE_FAMILY_DEMO'))
        {
            $parameters = $lists->insuranceSafeFamilyPlans(
                $flow->identifier
            );

        } else {

            $parameters = $this->plan_account();
        }

		$plans = $parameters['plans'];

		$recommendedPlanCode = 'NULO';
		$recommendedPlanValue = 'NULO';

		foreach ($plans as $plan)
		{
			if ($plan['Marca'] === 'S')
			{
				$recommendedPlanCode = $plan['Codigo'];
				$recommendedPlanValue = $plan['Descripcion'];
			}
		}

		$data['recommended_plan']['code'] = $recommendedPlanCode;
		$data['recommended_plan']['value'] = $recommendedPlanValue;
		$data['plans'] = $parameters['plans'];
		$data['products'] = $parameters['products'];

		return $data;
	}

	/**
	 * Construct product information
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function thirdProductInformation($flow, $request)
	{
		$data = $flow->product_information;

		for ($i = 1; $i <= 5; $i++)
		{
			$date = null;

			if ($request->get('second_md_' . $i . '_date'))
			{
				$date = Date::createFromFormat('d/m/Y', $request->get('second_md_' . $i . '_date'));

				$date = $date->timestamp;
			}

			$otherDisease = $request->get('second_md_' . $i . '_diagnostic');

			if ($i === 1)
			{
				$otherDisease = $request->get('second_md_' . $i . '_disease');

				if ($request->get('second_md_' . $i . '_disease') === 'Otro')
				{
					$otherDisease = $request->get('second_md_' . $i . '_diagnostic');
				}
			}

			$question = $request->get('second_md_' . $i);
			$disease = $request->get('second_md_' . $i . '_disease');
			$diagnostic = $otherDisease;
			$doctor = $request->get('second_md_' . $i . '_doctor');
			$hospital = $request->get('second_md_' . $i . '_hospital');
			$otherHospital = $request->get('second_md_' . $i . '_other_hospital');
			$accident = $request->get('second_md_' . $i . '_accident');
			$otherAccident = $request->get('second_md_' . $i . '_other_accident');
			$sequel = $request->get('second_md_' . $i . '_sequel');

			if ($request->get('second_md_' . $i) === 'N')
			{
				$diagnostic = 'NULO';
				$disease = 'NULO';
				$date = 'NULO';
				$doctor = 'NULO';
				$hospital = 'NULO';
				$otherHospital = 'NULO';
				$accident = 'NULO';
				$sequel = 'NULO';
			}

			$data['second_medical_questionary'][$i]['question'] = $question;
			$data['second_medical_questionary'][$i]['diagnostic'] = $diagnostic;
			$data['second_medical_questionary'][$i]['disease'] = $disease;
			$data['second_medical_questionary'][$i]['date'] = $date;
			$data['second_medical_questionary'][$i]['doctor'] = $doctor;
			$data['second_medical_questionary'][$i]['hospital'] = $hospital;
			$data['second_medical_questionary'][$i]['other_hospital'] = $otherHospital;
			$data['second_medical_questionary'][$i]['accident'] = $accident;
			$data['second_medical_questionary'][$i]['other_accident'] = $otherAccident;
			$data['second_medical_questionary'][$i]['sequel'] = $sequel;
		}

		return $data;
	}

	/**
	 * Construct product information
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function fourthProductInformation($flow, $request)
	{
		$data = $flow->product_information;

		$selectProduct = explode('-', $request->debit_option);

		$plans = $flow->product_information['plans'];

		$amountValue = 'NULO';

		foreach ($plans as $plan)
		{
			if ($plan['Codigo'] === $request->amount)
			{
				$amountValue = $plan['Descripcion'];
			}
		}

		$paymentValue = 'Mensual';

		if ($request->payment_type === 'A')
		{
			$paymentValue = 'Anual';
		}

		$data['amount']['code'] = $request->amount;
		$data['amount']['value'] = $amountValue;
		$data['payment_type']['code'] = $request->payment_type;
		$data['payment_type']['value'] = $paymentValue;
		$data['debit']['code'] = $selectProduct[0];
		$data['debit']['value'] = $selectProduct[1];
		$data['debit']['type'] = identifyProductType($selectProduct[0]);
		$data['debit']['default'] = $request->debit_option;

		return $data;
	}

	/**
	 * Construct customer information
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function fifthCustomerInformation($flow, $request)
	{
		$data = $flow->customer_information;

		$lists = $this->getLists($this->productName());

		for ($i = 1; $i <= 10; $i++)
		{
			$date = null;

			if ($request->get('beneficiary_' . $i . '_birth'))
			{
				$date = Date::createFromFormat('d/m/Y', $request->get('beneficiary_' . $i . '_birth'));

				$date = $date->timestamp;
			}

			$relationship = $request->get('beneficiary_' . $i . '_relationship');

			$relationshipName = $relationship;

			if (isset($relationship))
			{
				$relationshipName = $lists['beneficiaryOptions'][$relationship];
			}

			$data['beneficiaries'][$i]['name'] = $request->get('beneficiary_' . $i . '_name');
			$data['beneficiaries'][$i]['relationship'] = $relationship;
			$data['beneficiaries'][$i]['relationship_name'] = $relationshipName;
			$data['beneficiaries'][$i]['birth'] = $date;
			$data['beneficiaries'][$i]['participation'] = $request->get('beneficiary_' . $i . '_participation');
		}

		return $data;
	}

	/**
	 * Construct response product information
	 *
	 * @param $flow
	 * @param $response
	 * @return mixed
	 */
	private function fifthResponseProductInformation($flow, $response)
	{
		$data = $flow->product_information;

		$lists = new GetPreFilledData();

		if(!env('INSURANCE_SAFE_FAMILY_DEMO')) {

            $parameters = $lists->insuranceSafeFamilyCoverage(
                $flow->identifier
            );

        } else {
            $parameters = $this->safeFamilyCoverage();
        }

		$data['coverage'] = $parameters;

		return $data;
	}

	/**
	 * Construct customer information
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function sixthCustomerInformation($flow, $request)
	{
		$data = $flow->customer_information;

		$data['confirmation'] = $request->get('confirmation');

		return $data;
	}

	/**
	 * Construct user information
	 *
	 * @param $flow
	 * @param $request
	 * @return mixed
	 */
	private function sixthUserInformation($flow, $request)
	{
		$data = $flow->user_information;

		$data['new_branch']['code'] = $request->get('new_branch');

		return $data;
	}

	/**
	 * Construct response customer information
	 *
	 * @param $flow
	 * @param $response
	 * @return mixed
	 */
	private function sixthResponseCustomerInformation($flow, $response)
	{
		$data = $flow->customer_information;

		$getValueFromResponse = new GetResponse();

		if(!env('INSURANCE_SAFE_FAMILY_DEMO')) {

            $data['job'] = $getValueFromResponse->result($response, 'Ocupacion');
            $data['address'] = $getValueFromResponse->result($response, 'Direccion');
            $data['phone'] = $getValueFromResponse->result($response, 'Telefono');
            $data['fax'] = $getValueFromResponse->result($response, 'Fax');
            $data['city'] = $getValueFromResponse->result($response, 'Ciudad');
            $data['marital_status'] = $getValueFromResponse->result($response, 'EstadoCivil');
            $data['gender'] = $getValueFromResponse->result($response, 'Sexo');

		} else {

            $data['job'] = get_json($data, ['employer', 'job', 'name']);
            $data['address'] = get_json($data, ['address', 'full']);
            $data['phone'] = get_json($data, ['phone']);
            $data['fax'] = '';
            $data['city'] = get_json($data, ['address', 'municipality']);
            $data['marital_status'] = get_json($data, ['marital_status', 'value']);
            $data['gender'] = get_json($data, ['gender', 'value']);

        }

		return $data;
	}

	/**
	 * Construct response product information
	 *
	 * @param $flow
	 * @param $response
	 * @return mixed
	 */
	private function sixthResponseProductInformation($flow, $response)
	{
		$data = $flow->product_information;

		$getValueFromResponse = new GetResponse();

		$issue = Date::createFromFormat('dmy', $getValueFromResponse->result($response, 'FechaEmision'));

		$since = Date::createFromFormat('dmy', $getValueFromResponse->result($response, 'VigenciaDesde'));

		$until = Date::createFromFormat('dmy', $getValueFromResponse->result($response, 'VigenciaHasta'));

		$final = $getValueFromResponse->result($response, 'PolizaFinal');

		$data['policy']['final'] = $final;
		$data['policy']['issue'] = $issue->timestamp;
		$data['policy']['since'] = $since->timestamp;
		$data['policy']['until'] = $until->timestamp;
		$data['category'] = $getValueFromResponse->result($response, 'Categoria');
		$data['product_branch'] = $getValueFromResponse->result($response, 'Ramo');

		$flow->update([
			'identifier' => $final
		]);

		return $data;
	}

	/**
	 * Construct response user information
	 *
	 * @param $flow
	 * @param $response
	 * @return mixed
	 */
	private function sixthResponseUserInformation($flow, $response)
	{
		$data = $flow->user_information;

		$getValueFromResponse = new GetResponse();

		$manager = explode('-', $getValueFromResponse->result($response, 'Gerente'));

		$agent = explode('-', $getValueFromResponse->result($response, 'Empleado'));

		$data['sales_channel'] = $getValueFromResponse->result($response, 'CanalVenta');
		$data['agent']['number'] = $agent[0];
		$data['manager']['code'] = $manager[0];
		$data['manager']['value'] = $manager[1];

		return $data;
	}


	private function plan_account()
    {

        $code = ['090', '110', '120'];

        return [

            'plans' =>[
                "A" => [
                    "Codigo" =>"A",
                    "Descripcion" => "L. ".rand(100000, 199999),
                    "Anual" => "L. ".rand(600, 699),
                    "Mensual" => "L. ".rand(50, 59),
                    "Marca" => "N"
                ] ,
                "B" => [
                    "Codigo" => "B",
                    "Descripcion" => "L. ".rand(200000, 249000),
                    "Anual" => "L. ".rand(1200, 1299),
                    "Mensual" => "L. ".rand(100, 199),
                    "Marca" => "N"
                ],
                "C" => [
                    "Codigo" => "C",
                    "Descripcion" => "L. ".rand(250000, 259000),
                    "Anual" => "L. ".rand(1600, 1699),
                    "Mensual" => "L. ".rand(130, 140),
                    "Marca" => "N"
                ],
                "D" => [
                    "Codigo" => "D",
                    "Descripcion" => "L. ".rand(500000, 599999),
                    "Anual" => "L. ".rand(3200, 3259),
                    "Mensual" => "L. ".rand(260, 269),
                    "Marca" => "N"
                ],
                "N" => [
                    "Codigo" => "N",
                    "Descripcion" => "L. ".rand(1000000, 1100000),
                    "Anual" => "L. ".rand(6400, 6999),
                    "Mensual" => "L. ".rand(530, 540),
                    "Marca" => "S"
                ],
                "O" => [
                    "Codigo" => "O",
                    "Descripcion" => "L. ".rand(50000, 59000),
                    "Anual" => "L. ".rand(320, 340),
                    "Mensual" => "L. ".rand(20, 30),
                    "Marca" => "N"
                ],
                "P" => [
                    "Codigo" => "P",
                    "Descripcion" => "L. ".rand(750000, 759000),
                    "Anual" => "L. ".rand(4300, 4500),
                    "Mensual" => "L. ".rand(30, 40),
                    "Marca" => "N"
                ],
                "Q" => [
                    "Codigo" => "Q",
                    "Descripcion" => "L. ".rand(1500000, 1600000),
                    "Anual" => "L. ".rand(9000, 9500),
                    "Mensual" => "L. ".rand(600, 900),
                    "Marca" => "N"
                ],
                "R" => [
                    "Codigo" => "R",
                    "Descripcion" => "L. ".rand(25000, 26000),
                    "Anual" => "L. ".rand(160, 170),
                    "Mensual" => "L. ".rand(10, 15),
                    "Marca" => "N"
                ]
            ],
            'products' => [
               '1' => [
                   "code" => 1,
                   "value" => '1201399732'
               ],
                '2' => [
                    "code" => 1,
                    "value" => '1201399759'
                ],
                '3' => [
                    "code" => 1,
                    "value" => '1201399767'
                ]
            ]

        ];
    }

    private function safeFamilyCoverage()
    {
        return [
            '1' => [
                'code' => '1',
                'value' => '500000.00',
                'type' => 'VIDA'
            ],
            '2' => [
                'code' => '2',
                'value' => '500000.00',
                'type' => 'INCAPACIDAD TOTAL Y PERMANENTE'
            ],
            '3' => [
                'code' => '3',
                'value' => '250000.00',
                'type' => 'ENFERMEDADES GRAVES'
            ],
            '4' => [
                'code' => '4',
                'value' => '50000.00',
                'type' => 'GASTOS FUNEBRES'
            ],
            '5' => [
                'code' => '5',
                'value' => '2500.00',
                'type' => 'RENTA DIARIA POR HOSPITALIZACION'
            ]
        ];
    }


	/**
	 * Get product name
	 *
	 * @return string
	 */
	private function productName()
	{
		return 'insurance_safe_family';
	}
}