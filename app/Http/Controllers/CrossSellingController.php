<?php

namespace App\Http\Controllers;

use App\Models\Flow;
use App\Models\Product;
use Illuminate\Http\Request;

class CrossSellingController extends Controller
{
	public function index($productId)
	{

		$product = Product::where('version', '>', '1')->findOrFail($productId);

		$productParameters = $this->productParameters($product->type);

		return view('crossSell.index', compact('product', 'productParameters'));
	}
}
