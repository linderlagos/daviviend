<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function index()
    {
    	return view('test.index');
    }

    public function search()
    {
    	return response()->json([
			'success' => true,
			'status' => 200,
			'msg' => 'Se terminó con éxito'
	    ]);
    }

	public function second()
	{
		return response()->json([
			'success' => true,
			'status' => 200,
			'msg' => 'Se terminó con éxito'
		]);
	}

	public function third()
	{
		return response()->json([
			'success' => true,
			'status' => 500,
			'msg' => 'Error 500'
		]);
	}

	public function fourth()
	{
		return response()->json([
			'success' => true,
			'status' => 403,
			'msg' => 'Error 403'
		]);
	}
}
