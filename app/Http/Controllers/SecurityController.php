<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use PragmaRX\Google2FAQRCode\Google2FA as QRGenerator;
use Google2FA;

class SecurityController extends Controller
{
    public function index()
    {
    	$users = User::all();

    	return view('security.index', compact('users'));
    }

    public function user($id)
    {
    	$user = User::findOrFail($id);

    	$token = $user->google2fa_secret;

    	if (!$token)
	    {
	    	$token =  Google2FA::generateSecretKey();
	    }

	    $qrGenerator = new QRGenerator();

	    $qrUrl = $qrGenerator->getQRCodeInline(
		    'Así de Fácil | Banco Davivienda',
		    'honduras_contacto@davivienda.com.hn',
		    $token
	    );

    	return view('security.user', compact('user', 'qrUrl'));
    }

    public function generateNewSecretKey($id)
    {
	    $user = User::findOrFail($id);

	    $token =  Google2FA::generateSecretKey();

	    $user->update([
	    	'google2fa_secret' => $token
	    ]);

	    $this->genericMessage(
	    	'¡Excelente!',
		    '<p>La nueva llave secreta ha sido generada de manera exitosa</p>',
		    'success'
	    );

    	return redirect()->back();
    }
}
