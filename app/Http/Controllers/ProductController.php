<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\ResolveComment;
use App\Http\Requests\Product\StoreComment;
use App\Http\Requests\Product\UpdateAudit;
use App\Http\Requests\Product\UpdateUploadAudit;
use App\Http\Requests\Product\UpdateUploadTypeRequest;
use App\Http\Requests\Product\UploadRequest;
use App\Mail\SendPDFLink;
use App\Models\Comment;
use App\Models\Product;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Jenssegers\Date\Date;
Use JavaScript;
use Spatie\Activitylog\Models\Activity;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
	/**
	 * Show view of account products
	 *
	 * @param $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show($id)
    {
    	$product = Product::filterByRole()->findOrFail($id);

    	$user = Auth::user();

    	$productParameters = $this->productParameters($product->type);

	    $uploadTypes = $productParameters['upload_types'];

	    if ($user->hasRole(env('ROL_CIF')))
	    {
	    	$uploadTypes['soporte'] = 'Documento soporte';
	    }

	    $cifAudit = $product->audits()->where('type', 'cif')->first();

	    $cifAuditTypes = $this->auditParameters()['cif_types'];

	    $uploadAuditTypes = $this->auditParameters()['formalization_upload_types'];

	    $comments = $product->comments()->unsolved()->get();

	    $solvedComments = $product->comments()->solved()->get();

	    $printDebitCardURL = '';

	    if ($product->type === 'bank_account')
	    {
		    $printDebitCardURL = $this->renderDebitCardPrintURL($product,  $product->user_information);
	    }

	    $activities = Activity::forSubject($product)->get();

	    JavaScript::put([
		    'upload_types' => $uploadTypes
	    ]);

    	//return view('product.show', compact('product', 'printDebitCardURL', 'uploadTypes', 'activities', 'cifAudit', 'cifAuditTypes', 'uploadAuditTypes', 'comments', 'solvedComments'));
    	return view('product.show', compact('product', 'printDebitCardURL', 'uploadTypes', 'activities', 'cifAudit', 'cifAuditTypes', 'uploadAuditTypes', 'comments', 'solvedComments', 'productParameters'));

    }

    public function upload(UploadRequest $request, $id)
    {
	    $product = Product::filterByRole()->findOrFail($id);

	    $type = $request->type;

	    $user = Auth::user();

	    if ($user->hasRole(env('ROL_GESTOR')) && $product->uploads()->where('type', $type)->get()->count() > 1) {
		    return response()->json([
			    'success' => false,
			    'message' => 'No se pueden subir más archivos de este tipo'
		    ], 403);
	    }

	    if ($user->hasRole(env('ROL_GESTOR')) && $product->uploads()->count() > 3) {
		    return response()->json([
			    'success' => false,
			    'message' => 'No se pueden subir más archivos'
		    ], 403);
	    }

	    $signatureUploads = $product->uploads()->where('type', 'firma')->get();

	    if ($user->hasRole(env('ROL_GESTOR')) && $signatureUploads->count() > 0) {
		    foreach ($signatureUploads as $upload) {
			    if ($upload->audits()->first()->name === 'subido') {
				    return response()->json([
					    'success' => false,
					    'message' => 'No se puede subir este archivo debido a que ya existe un documento de firma subido al sistema.'
				    ], 403);
			    }
		    }
	    }

	    $file = $request->file('file');

	    $productParameters = $this->productParameters($product->type);

	    $path = '/uploads/' . $product->customer->identifier . '/' . $product->type . '/' . $product->identifier;

	    $name = $type . '-' . $product->identifier . '-' . $product->random . '-' . Date::now()->timestamp . '.' . $file->getClientOriginalExtension();

	    // Try to store file in public folder
	    try {
		    $file->storePubliclyAs($path, $name, ['disk', 'public']);

	    } catch(\Exception $e) {
	    	$this->logException($e);

		    return response()->json([
		    	'success' => false,
			    'message' => 'El usuario no tiene permisos en la carpeta para subir archivos. Favor contactar a soporte.'
                ]);

	    } catch (\Exception $e) {
		    return response()->json([
			    'success' => false

		    ], 500);
	    }

	    $upload = $product->uploads()->create([
		    'url' => $path . '/' . $name,
		    'type' => $type,
		    'user_id' => $user->id
	    ]);

	    $upload->audits()->create([
		    'name' => 'creado',
		    'type' => 'formalizacion',
		    'user_id' => $user->id
	    ]);


	    if ($upload->type === 'soporte')
	    {
	    	$uploadTypeName = 'Documento Soporte';
	    }

	    if ($upload->type != 'soporte')
	    {

		    $uploadTypeName = $productParameters['upload_types'][$upload->type];
	    }

	    activity('upload')
		    ->causedBy($user)
		    ->performedOn($product)
		    ->log('Se subió un documento de tipo ' . $uploadTypeName);

	    return response()->json([
		    'success' => true
	    ]);
    }

    public function updateUploadType(UpdateUploadTypeRequest $request)
    {
	    $product = Product::findOrFail($request->product);

	    $productParameters = $this->productParameters($product->type);

	    $upload = $product->uploads()->findOrFail($request->upload);

	    $oldType = $upload->type;

	    $upload->update([
	    	'type' => $request->type
	    ]);

	    $user = Auth::user();

	    $description = 'Se actualizó el tipo de documento a Soporte';

	    if ($upload->type !== 'soporte')
	    {
		    $description = 'Se actualizó el tipo de documento a ' . $productParameters['upload_types'][$upload->type];
	    }

	    activity('upload')
		    ->causedBy($user)
		    ->performedOn($product)
		    ->withProperties([
			    'attributes' => [
				    'type' => $upload->type,
			    ],
			    'old' => [
				    'type' => $oldType,
			    ],
		    ])
		    ->log($description);

	    return response()->json([
		    'success' => true
	    ]);
    }

	public function updateUploadAudit(UpdateUploadAudit $request)
	{
		$upload = Upload::findOrFail($request->upload);

		$product = $upload->product;

		$audit = $upload->audits()->findOrFail($request->audit);

		$oldName = $audit->name;

		$audit->update([
			'name' => $request->name
		]);

		$user = Auth::user();

		$auditParameters = $this->auditParameters()['formalization_upload_types'][$audit->name];

		activity('upload')
			->causedBy($user)
			->performedOn($product)
			->withProperties([
				'attributes' => [
					'name' => $audit->type,
				],
				'old' => [
					'name' => $oldName,
				],
			])
			->log('Se actualizó el status del documento a ' . $auditParameters);

		return response()->json([
			'success' => true
		]);
	}

    public function updateAudit(UpdateAudit $request)
    {
	    $product = Product::findOrFail($request->product);

	    $audit = $product->audits()->findOrFail($request->audit);

	    $oldName = $audit->name;

	    $audit->update([
	    	'name' => $request->name
	    ]);

	    $user = Auth::user();

	    $auditParameters = $this->auditParameters()['cif_types'][$audit->name];

	    activity('audit')
		    ->causedBy($user)
		    ->performedOn($product)
		    ->withProperties([
			    'attributes' => [
				    'name' => $audit->type,
			    ],
			    'old' => [
				    'name' => $oldName,
			    ],
		    ])
		    ->log('Se actualizó el status del producto a ' . $auditParameters);

	    return response()->json([
	    	'success' => true
	    ]);
    }

    public function comment(StoreComment $request, $productId)
    {
	    $product = Product::findOrFail($productId);

	    $user = Auth::user();

	    $comment = $product->comments()->create([
	    	'description' => $request->description,
	    	'created_by' => $user->id,
	    ]);

	    activity('comment')
		    ->causedBy($user)
		    ->performedOn($product)
		    ->log('Se creó un nuevo comentario para el producto con la descripción "' . $comment->description . '"');

	    $this->genericMessage(
		    '¡Muy bien!',
		    '<p>Se creó existosamente su comentario</p>',
		    'success'
	    );

	    return redirect()->back();
    }

	public function solveComment(ResolveComment $request, $productId, $commentId)
	{
		$product = Product::findOrFail($productId);

		$comment = $product->comments()->findOrFail($commentId);

		$user = Auth::user();

		$comment->update([
			'solved' => 1,
			'solved_by' => $user->id
		]);

		activity('comment')
			->causedBy($user)
			->performedOn($product)
			->log('Se resolvió el comentario "' . $comment->description . '"');

		$this->genericMessage(
			'¡Muy bien!',
			'<p>Resolvió el comentario exitosamente</p>',
			'success'
		);

		return redirect()->back();
	}

	/**
	 * Send link generator by email
	 *
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function send($id)
	{
		$print = Product::findOrFail($id);

		$email = get_json($print->customer_information, ['email', 'account']);

		$name = get_json($print->customer_information, ['name', 'first']) . ' ' . get_json($print->customer_information, ['name', 'last']);

		// If customer doesn't have email redirect back with error message
		if ($email == 'notiene@davivienda.com.hn')
		{
			$this->errorMessage(
				'<p>No se puede enviar correo al cliente ya que no tiene correo electrónico registrado</p>'
			);

			return redirect()->route('show.product', $print->id);
		}

		try {
			Mail::to($email)->send(new SendPDFLink(
				$name,
				$this->emailSubject($print->type),
				$this->emailContent($print->type),
				$this->pdfUrl($print->type, $print)
			));
		} catch (\Exception $e) {
			$this->errorMessage(
				'<p>No se pudo enviar el correo electrónico por un problema interno.</p>'
			);

			$this->logException($e);

			return redirect()->route('show.product', $print->id);
		}

		$this->genericMessage(
			'¡Excelente!',
			'<p>Se ha enviado correctamente el correo con el link del contrato</p>',
			'success'
		);

		return redirect()->route('show.product', $print->id);
	}

	/**
	 * Generate email subject based on product type
	 *
	 * @param $type
	 * @return string
	 */
	private function emailSubject($type)
	{
		$subject = 'Generar el documento de su nuevo producto Davivienda';

		if ($type === 'bank_account')
		{
			$subject = 'Contrato de cuenta de ahorro';
		}

		return $subject;
	}

	/**
	 * Generate email content based on product type
	 *
	 * @param $type
	 * @return string
	 */
	private function emailContent($type)
	{
		$content = '<p>Para revisar el documento de su nuevo producto Davivienda de click al siguiente botón:</p>';

		if ($type === 'bank_account')
		{
			$content = '<p>Para revisar su contrato dele click al siguiente botón:</p>';
		}

		return $content;
	}

	/**
	 * Generate PDF URL based on product type
	 *
	 * @param $type
	 * @param $print
	 * @return string
	 */
	private function pdfUrl($type, $print)
	{
		$url = env('PDF_GENERATOR_URL') . 'generar-pdf/' . $print->identifier . '/' . $print->random;

		return $url;
	}

	/**
	 * Generate Debit Card print URL
	 *
	 * @param $product
	 * @param $data
	 * @return string
	 */
	private function renderDebitCardPrintURL($product, $data)
	{
		$ip = request()->ip();

		return env('PRINT_DEBIT_CARD_URL') . $product->customer->identifier . '/1/' . $product->identifier . '/CTA/' . $data['peoplesoft'] . '/' . $ip;
	}
}