<?php

namespace App\Http\Controllers;

use App\Core\Bank\Account\GeneratorParameters;
use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionesToken;
use Illuminate\Http\Request;
use App\Core\CallSoap;
use App\Http\Response;
use App\Core\GetResponseToken;

class HomeController extends Controller
{

    /**
     * @var GeneratorParameters
     */
    protected $parameters;

    /**
	 * AccountController constructor.
	 * @param GeneratorParameters $parameters
	 */
    public function __construct(GeneratorParameters $parameters)
    {
        $this->parameters = $parameters;
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $codMessage = "";
        $message = "";
        $serial = "";

        return view('home', compact("serial", "message", "codMessage"));
    }

    //ir a pantalla de autenticacion de codigo generado
    public function autenticar(Request $request)
    {
        $item = $request->all();
        $identidad = $item["identidad"];
    
        $codMessage = "";
        $message = "";
        $serial = "";
        
        return view('autenticar', compact("serial", "message", "codMessage", "identidad"));
    }


    //gearcion de serial para autenticador de google
    public function generarSerial(Request $request) 
    { 
        $getValueFromResponse = new GetResponseToken();
        
        $item = $request->all();
        $identidad = $item["identidad"];
        
        
        $response = $this->soap(
            $this->productName(),
            'HN',
            'LAY_3_MS_24_M_1_GENERACION_SERIAL',
            $this->parameters->generatorSerial($identidad),
            'REQUEST_DETAIL'
        );
        
        if(in_array($response['code'], [403, 500]))
        {
            return $this->redirectIfResponseHasError($response['code'], 'Generar Serial');
        }

        $data = $response['response'];
        $codMessage = $getValueFromResponse->result($data, 'codMsjRespuesta');
        $message = (string) $getValueFromResponse->result($data, 'valMsjRespuesta');
        $serial = (string) $getValueFromResponse->result($data, 'serial');
        
       /* $codMessage = 1;
        $message = "USUARIO NO EXISTE";
        $serial = "";*/
        
        return view('home', compact("serial", "message", "codMessage", "identidad"));
    }


    //autenticacion de codigo de autenticator de google
    public function autenticarCodigo(Request $request)
    {
        $getValueFromResponse = new GetResponseToken();

        $item = $request->all();
        $identidad = $item["identidad"];

        $autenticar = [
            'identificacion' => $identidad,
            'codigo' => $item["codigo"]
        ];

        $response = $this->soap(
            $this->productName(),
            'HN',
            'LAY_3_MS_24_M_2_AUTENTICACION_TOKEN',
            $this->parameters->autenticaconCodigo($autenticar),
            'REQUEST_DETAIL'
        );

        if(in_array($response['code'], [403, 500]))
        {
            return $this->redirectIfResponseHasError($response['code'], 'Autenticar Codigo');
        }
   
        $data = $response['response'];
        $codMessage = $getValueFromResponse->result($data, 'codMsjRespuesta');
        $message = (string) $getValueFromResponse->result($data, 'valMsjRespuesta');
        $serial = (string) $getValueFromResponse->result($data, 'serial');
        /*$codMessage = "1";
        $message = "Exitoso";
        $serial = "";*/

        return view('autenticar', compact("serial", "message", "codMessage", "identidad"));
    }


    //nombre del producto
    private function productName()
	{
		return '';
	}



}
