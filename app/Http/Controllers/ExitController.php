<?php

namespace App\Http\Controllers;

use App\Core\ExitFlow;
use Illuminate\Http\Request;

/**
 * Class CoreController
 * @package App\Http\Controllers
 */
class ExitController extends Controller
{
	/**
	 * Exit process and delete request on core
	 *
	 * @param Request $request
	 * @param $productName
	 * @param $process
	 * @param $processParams
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function exit(Request $request, $productName, $process = 'exit_process', $processParams = 'exit_process_params')
	{

		$productParameters = $this->productParameters($productName);

		//$validatedData = $request->validate($productParameters['exit_rules']);

		$flow = $this->recentFlow($productName, true);

		$exit = new ExitFlow();

		if(env('EXIT_FLOW_DEMO'))
        {
            $this->forgetId($productName);

			$route = $productParameters['home_route'];

            $this->genericMessage(
                '¡Excelente!',
                '<p>Ha salido exitosamente</p>',
                'success'
            );

			return response()->json([
				'route' => $route,
				'data' => $flow
			]);

            //return redirect()->to($productParameters['home_route']);
        }

		$exitProcess = $exit->exit($flow, $productName, $request, $process, $processParams);

		if (in_array($exitProcess['code'], [403, 500]))
		{
			return $this->redirectIfResponseHasError($exitProcess['code'], $productName);
		}
		
		$this->forgetId($productName);

		if($productParameters[$process] === 'DIGITAL_CARD_REJECT_SEARCH')
        {
            // Return success flash message
            $this->genericMessage(
                '¡Excelente!',
                '<p>Ha salido exitosamente</p>',
                'success'
            );

        } else {
            // Return success flash message
            $this->genericMessage(
                '¡Excelente!',
                '<p>Se rechazó la solicitud de manera exitosa</p>',
                'success'
            );
        }


		return redirect()->to($productParameters['home_route']);
	}
}