<?php

namespace App\Http\Controllers;

use App\Models\Flow;

/**
 * Class CoreController
 * @package App\Http\Controllers
 */
class ReturnController extends Controller
{
	public function return($flowId)
	{
		$flow = Flow::findOrFail($flowId);

		if($flow->type === 'bank_auto' && $flow->step > 3)
       {
		   $flow->update(['step' => 'aprobado']);
		   
			if(!env('BANK_AUTO_DEMO'))
			{
				return response()->json([
					'route' => '/solicitudes-en-tramite/',
					'data' => $flow
				]);
			}

           return redirect()->route('bank.auto.query');
       }

		$productParamenters = $this->productParameters($flow->type);

		$this->updateStep($flow, 'previous');

		if(env('EXIT_FLOW_DEMO'))
        {
			$route = $productParamenters['home_route'];

			return response()->json([
				'route' => $route,
				'data' => $flow
			]);
		}


		return redirect()->to($productParamenters['home_route']);
	}
}