<?php

namespace App\Http\Controllers;

// Traits
use App\Core\Traits\Message;
use App\Core\Traits\PersistId;
use App\Core\Traits\FlowIsActive;
use App\Core\Traits\ProductParameters;
use App\Core\Traits\GetOrCreateCustomer;

// Core
use App\Core\GetResponse;
use App\Core\SearchCustomer;
use App\Core\CustomerRegistry;
use App\Core\Traits\GetValueFromResponse;
use App\Http\Response\CreditCardProcessResponse;

// Helpers
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class CoreController
 * @package App\Http\Controllers
 */
class SearchController extends CustomerRegistry
{
	use ProductParameters,
		GetOrCreateCustomer,
		PersistId,
		Message,
		FlowIsActive,
        GetValueFromResponse;

	public function search(Request $request, $productName)
	{

		$productParameters = $this->productParameters($productName);

		$request->validate($productParameters['search_rules']);
		
		$activeFlow = $this->flowIsActive($productParameters['active_flows']);

		if ($activeFlow)
		{
			$activeFlowParameters = $this->productParameters($activeFlow);

			//return redirect()->to($activeFlowParameters['home_route']);
			return response()->json([
				'status' => 300,
				'activeFlow' => $activeFlow,
				'message' => '1'
			], 200); 
		}

		$searchCustomer = ['response' => null];

		if (!env($productName . '_DEMO'))
		{
			$search = new SearchCustomer();

			$searchCustomer = $search->searchCustomer(
				$request->identity,
				$productParameters['company'],
				$productParameters['search_process'],
				$productName
			);


			// If response could not be made redirect back
			if (in_array($searchCustomer['code'], [403, 500]))
			{
				return redirect()->back();
			}
		}

		// Create product flow with retrieved info
		$flow = $this->store($request, $searchCustomer['response'], $productName);

		// Persist flow id in session
		$this->persistId($productName, $flow->id);

		//return redirect()->to($productParameters['home_route']);
		return response()->json([
			'status' => 200,
			'flow' => $flow,
			'searchCustomer' => $searchCustomer,
			'message' => '3'
		], 200);
	}

	/**
	 * Store new resource in customer table
	 *
	 * @param $request
	 * @param $response
	 * @param $productName
	 * @return mixed
	 */
	public function store($request, $response, $productName)
	{

		$customer = $this->getOrCreateCustomer($request->identity);

		$productInformation = [];

		if ($productName === 'bank_account')
		{
			$productInformation = $this->firstBankAccountProductInformation($response, $productName);
		}

        if($productName === 'bank_card') {
            $productInformation = $this->firstBankCardProductInformation($request);
        }

        $customerInformation = $this->firstCustomerInformation($response, $productName);

		$customerStatus = 0;

		if (!env($productName . '_DEMO'))
		{
			if ($customerInformation['cif']['value'])
			{
				$customerStatus = 1;
			}
		}

		return $customer->flows()->create([
			'customer_information' => $customerInformation,
			'product_information' => $productInformation,
			'user_information' => $this->firstUserInformation(),
			'user_id' => Auth::user()->id,
			'step' => '1',
			'type' => $productName,
			'customer_status' => $customerStatus
		], 200);
	}



    private function firstCustomerInformation($response, $productName)
	{
		
		if (env($productName . '_DEMO'))
		{
			return [];
		}

		$stateCodeHome = (string) $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionDomicilioCompletaDepartamentoCodigo');
		$municipalityCodeHome = (string) $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionDomicilioCompletaMunicipioCodigo');
		$colonyCodeHome =  (string) $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionDomicilioCompletaColoniaCodigo');
		$addressHome = $this->recoverAddressKey($productName, $stateCodeHome, $municipalityCodeHome, $colonyCodeHome);

		$stateCodeEmployer = (string) $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionEmpleadorCompletaDepartamento');
        $municipalityCodeEmployer = (string) $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionEmpleadorCompletaMunicipio');
        $colonyCodeEmployer =  (string) $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionEmpleadorCompletaColonia');
        $addressEmployer = $this->recoverAddressKey($productName, $stateCodeEmployer, $municipalityCodeEmployer, $colonyCodeEmployer);

		return [
			'nationality' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'Nacionalidad'), 'nations'),


			'cif' => [
				'code' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'TipoCif'),
				'value' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CIF'),
			],

			//Address
			'address' => [
				'code' => $addressHome,
				'other_city' => null,
				'state' => null,
				'state_code' => $stateCodeHome,
				'municipality' => $this->cityAddress($productName, $stateCodeHome, $municipalityCodeHome, $colonyCodeHome),
				'municipality_code' => $municipalityCodeHome,
				'colony' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionDomicilioCompletaBarrioColonia'),
				'colony_code' => $colonyCodeHome,
				'full' => $this->constructFullAddress($response, 'DireccionDomicilioCompletaAvenidaBloqueZonaCalleCasa', 'DireccionDomicilioCompletaPuntoReferencia', 'DireccionDomicilioCompletaPuntoReferenciaComplemento'),
				'first' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionDomicilioCompletaAvenidaBloqueZonaCalleCasa'),
				'second' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionDomicilioCompletaPuntoReferencia'),
				'third' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionDomicilioCompletaPuntoReferenciaComplemento'),
			],

			'phone' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'TelefonoDomicilio', true),
			'mobile' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'Celular'),
			'email' => $this->checkIfCustomerHasEmail($response),

			'receive_sms' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'AutorizaEnvioMensajesCelular'), 'confirmation'),

			'receive_emails' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'AutorizaEnvioEstadosCuentaCorreoPersonal'), 'confirmation'),

			'marital_status' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'EstadoCivil'), 'maritalStatusOptions'),

			'spouse' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'ConyugeNombre'),

			'birth' => $this->generateDate('Ymd', $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'FechaNacimientoAAAAMMDD')),

			'gender' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'Sexo'), 'genderOptions'),

			'profession' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'Profesion'), 'professionOptions'),

			// Name
			'name' => [
				'first' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'PrimerNombre'),
				'middle' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'SegundoNombre'),
				'last' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'PrimerApellido'),
				'second_last' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'SegundoApellido'),
				'fullname' => $this->fullname(
					$this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'PrimerNombre'),
					$this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'SegundoNombre'),
					$this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'PrimerApellido'),
					$this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'SegundoApellido')
				)
			],

			// Employer
			'employer' => [
				'started_at' => $this->generateDate('Ymd', $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'FechaIngresoLaboralAAAAMMDD')),
				'name' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'NombreEmpleadorLugarTrabajo'),
				'phone' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'TelefonoEmpleadorLaboral'),
                'type' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'TipoEmpresa'), 'employeeTypes'),

//				'type' => [
//					'code' => null,
//					'value' => null
//				],
				'address' => [
					'code' => $addressEmployer,
					'other_city' => null,
					'state' => null,
					'state_code' => $stateCodeEmployer,
					'municipality' => null,
					'municipality_code' => $municipalityCodeEmployer,
					'colony' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionEmpleadorCompletaBarrioColonia'),
					'colony_code' => $colonyCodeEmployer,
					'full' => $this->constructFullAddress($response, 'DireccionEmpleadorCompletaAvenidaBloqueZonaCalleCasa', 'DireccionEmpleadorCompletaPuntoReferencia', 'DireccionEmpleadorCompletaPuntoReferenciaComplemento'),
					'first' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionEmpleadorCompletaAvenidaBloqueZonaCalleCasa'),
					'second' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionEmpleadorCompletaPuntoReferencia'),
					'third' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'DireccionEmpleadorCompletaPuntoReferenciaComplemento'),
				],
				'job' => [
					'name' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CargoLaboralPuestoTrabajo'),

					'type' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'ActividadComercial'), 'jobTypeOptions'),

					'status' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'TipoEmpleo'), 'jobOptions'),
				],
			],

			'income' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'IngresoMensual'),
			'other_income' => null,

			'expenses' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CodigoRangoTotalEgresos'), 'expenseRangeOptions'),

			'assets' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CodigoRangoTotalActivos'), 'assetRangeOptions'),

			'passive' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CodigoRangoTotalPasivos'), 'liabilityRangeOptions'),

			'fatca' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'EsFatca'), 'confirmation'),

			// Public job
			'public_job' => [
				'code' => $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CargosPublicosUltimos4Anhos'),

				'value' => $this->searchValueFromList($productName, $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CargosPublicosUltimos4Anhos'), 'confirmation', false),

				'employer_name' => $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoInstitucion'),

				'job' => $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoCargo'),

				'active' => $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoEsActual'),
				'active_value' => $this->searchValueFromList($productName, $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoEsActual'), 'confirmation', false),

				'from' => $this->generateDate('Ymd', $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoFechaInicio')),
				'to' => $this->generateDate('Ymd', $this->getThirdLevelValue($response, 'CARGOS_PUBLICOS_LIST', 'CARGO_PUBLICO_1', 'CargosPublicosGobiernoFechaFinal')),
		],


			'dependants' => $this->dependants($response, $productName),
			'professional_dependant' => $this->storedProfessionalDependants($response),
			'international_operations_information' => $this->storedInternationalOperationsInformation($response, $productName),
			'beneficiaries' => $this->constructJson($response, 'storedBeneficiaries', 10, $productName),
			'references' => $this->constructJson($response, 'storedReferences', 2, $productName),
			'national_institutions' => $this->constructJson($response, 'storedNationalInstitutions', 3, $productName),
			'foreign_institutions' => $this->constructJson($response, 'storedForeignInstitutions', 3, $productName),
		]; 
		
	}




	private function firstBankAccountProductInformation($response, $productName)
	{
		if (env($productName . '_DEMO'))
		{
			return [];
		}

        $errorCode = (int) $response->getCreditCardProcessResult()->Data->Error;

		if($errorCode !== 172 && $errorCode !== 64) {

            return [
                'product' => [
                    'code' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'TipoCuenta'),
                    'value' => $this->getSecondLevelValue($response, 'INFORMACION_CUENTA', 'NumeroCuenta'),
                ]
            ];
        } else {
            return [];
        }

	}

	private function firstBankCardProductInformation($request)
	{
		$productParameters = $this->productParameters('bank_card');
		
		return response()->json([
			'product' => [
				'code' => $productParameters['cards'][$request->card]['code'],
				'name' => $productParameters['cards'][$request->card]['name'],
				'limit' => null,
				'type' => null,
			],
			'request' => [
				'number' => null
			]
		], 200); 
		
	}

	private function firstUserInformation()
	{
		return response()->json([
			'peoplesoft' => Auth::user()->peopleSoft,
		], 200); 
		
	}

	private function checkIfCustomerHasEmail($response)
	{
		$email = $this->getSecondLevelValue($response, 'APPROVED_DATA_LIST', 'CorreoPersonal');
		//$hasEmail = 'N';
        $hasEmail = '';

		if ($email) {

            if($email === 'notiene@davivienda.com.hn' | $email === 'NOTIENE@DAVIVIENDA.COM.HN')
            {
                $hasEmail = 'N';
            } else {
                $hasEmail = 'S';
            }
		}

		return response()->json([
			'account' => $email,
			'has_email' => $hasEmail
		], 200); 
	}

	/**
	 * Get address key with state, municipality and colony code
	 *
	 * @param $productName
	 * @param $stateCode
	 * @param $municipalityCode
	 * @param $colonyCode
	 * @return int|string
	 */
	private function recoverAddressKey($productName, $stateCode, $municipalityCode, $colonyCode)
	{
		$lists = $this->getLists($productName);

		$cities = $lists['cities'];

		foreach ($cities as $state)
		{
			foreach ($state as $key => $city)
			{
				$city = explode('>', $key);
				$state = explode('-', $city[0]);
				$municipality = explode('-', $city[1]);
				$colony = explode('-', $city[2]);

				if ($state[1] === $stateCode && $municipality[1] === $municipalityCode && $colony[1] === $colonyCode)
				{
					return $key;
				}
			}
		}

		return 404;
	}


    private function cityAddress($productName, $stateCode, $municipalityCode, $colonyCode)
    {
        $lists = $this->getLists($productName);

        $cities = $lists['cities'];

        foreach ($cities as $state)
        {
            foreach ($state as $key => $city)
            {
                $city = explode('>', $key);
                $state = explode('-', $city[0]);
                $municipality = explode('-', $city[1]);
                $colony = explode('-', $city[2]);

                if ($state[1] === $stateCode && $municipality[1] === $municipalityCode && $colony[1] === $colonyCode)
                {
                    return $municipality[0];
                }
            }
        }

        return 404;
    }
}
