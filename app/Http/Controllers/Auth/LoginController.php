<?php

namespace App\Http\Controllers\Auth;

use App\Core\GetResponse;
use App\Core\Traits\Bridge;
use App\Core\Parameters;
use App\Http\Controllers\Controller;
use App\User;
use Artisaninweb\SoapWrapper\Exceptions\ServiceAlreadyExists;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Google2FA;

class LoginController extends Controller
{

	use Bridge;
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
     * @var GeneratorParameters
     */
    protected $parameters;

	/**
	 * Create a new controller instance.
	 *
	 * @param $parameters
	 * @return void
	 */
	public function __construct(Parameters $parameters)
	{
		$this->middleware('guest')->except('logout');
		$this->parameters = $parameters;
	}

	/**
	 * Attempt to log the user into the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return bool
	 */
	protected function attemptLogin(Request $request)
	{
		dd($this->parameters->user($request));
		$login = $this->soapActiveDirectory(
			'login',
			'HN',
			'GLOBAL_VALIDATE_USER_PROCESS',
			$this->parameters->user($request),
			'VALIDATE_USER_PARAMS'
		);

		if (in_array($login['code'], [403, 500]))
		{
			$this->errorMessage(
				$login['message']
			);

			return false;
		}

		$response = $login['response'];

		/*$name = $response->getCreditCardProcessResult()->Data->Value;

		$getRoles = $response->getCreditCardProcessResult()->Data->DataList->Data;

		$roles = [];

		//elimanar estas dos lineas
		$name = 'Mauricios Rosales';
		$getRoles = 'HN-Apps TDA Gestor UAT-S-G';

		if (is_array($getRoles))
		{
			foreach ($getRoles as $role)
			{
				$roles[] = $role->Value;
			}
		} else {
			//descomentar la linea 101 y eliminar la linea 100
			$roles[] = $getRoles;
			//$roles[] = $getRoles->Value;
		}

		$user = User::where('peopleSoft', $request->peopleSoft)->first();

		$secretKey = Google2FA::generateSecretKey();

		if (!$user) {
			$user = User::create(
				[
					'peopleSoft' => $request->peopleSoft,
					'email' => $request->peopleSoft,
					'name' => $name,
					'password' => Hash::make($request->password),
					'google2fa_secret' => $secretKey
				]
			);

			$user->assignRole($roles);
		} else {
			if ($user->google2fa_secret)
			{
				$user->update(
					[
						'name' => $name,
						'password' => Hash::make($request->password)
					]
				);
			} else {
				$user->update(
					[
						'name' => $name,
						'password' => Hash::make($request->password),
						'google2fa_secret' => $secretKey
					]
				);
			}

			$user->syncRoles($roles);
		}
*/

		return true;
		return $this->guard()->attempt(
			$this->credentials($request),
			$request->filled('remember')
		);

		
	}

		/**
	 * The user has been authenticated.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  mixed  $user
	 * @return mixed
	 */
	protected function authenticated(Request $request)
	{
		$serial = "";
		$message = "";
		$codMessage = "";
		
		return view('home', compact("serial", "message", "codMessage"));
	}

	/**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return true;
        //return $request->only($this->username(), 'password');
    }

	/**
	 * Get the login username to be used by the controller.
	 *
	 * @return string
	 */
	public function username()
	{
		return 'peopleSoft';
	}

	/**
	 * If error is thrown by WSDL get the error and show it to the customer
	 *
	 * @param $response
	 * @return bool|\Illuminate\Http\RedirectResponse
	 */
	private function error($response)
	{
		$error = (int) $response->getCreditCardProcessResult()->Data->Error;

		if ($error > 0) {
			return true;
		}

		return false;
	}
}