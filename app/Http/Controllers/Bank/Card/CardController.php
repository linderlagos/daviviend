<?php

namespace App\Http\Controllers\Bank\Card;

// Core
use App\Core\Bank\Card\CardRegistry;
use App\Core\Bank\Card\CardParameters;
use App\Core\Bank\Card\GetApprovedCardDetails;
use App\Http\Controllers\Controller;

// Requests
use App\Http\Requests\Bank\Card\SecondRequest;
use App\Http\Requests\Bank\Card\ThirdRequest;

/**
 * Class CardController
 * @package App\Http\Controllers\Card
 */
class CardController extends Controller
{
	/**
	 * @var CardRegistry
	 */
	protected $store;

    /**
     * @var CardParameters
     */
    protected $parameters;

    /**
     * @var GetApprovedCardDetails
     */
    protected $cardDetails;

    /**
     * CardController constructor.
     * @param CardRegistry $store
     * @param CardParameters $parameters
     * @param GetApprovedCardDetails $cardDetails
     */
    public function __construct( CardRegistry $store, CardParameters $parameters, GetApprovedCardDetails $cardDetails)
    {
	    $this->store = $store;
	    $this->parameters = $parameters;
        $this->cardDetails = $cardDetails;
    }

	/**
	 * Show index view with prefilled data
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function index()
    {
		 
//    	session()->forget('bank_account_id');

//    	session()->put([
//    		'bank_card_id' => 31
//	    ]);

	    $productParameters = $this->productParameters($this->productName());

	    // Checks to see if there is an open session for card process and sends the user back to that view
	    $activeFlow = $this->flowIsActive($productParameters['active_flows']);

	    if ($activeFlow)
	    {
		    $activeFlowParameters = $this->productParameters($activeFlow);

			return redirect()->to($activeFlowParameters['home_route']);
			/* return response()->json([
				'status' => 500,
				'message' => 'Regresaría al home'
			]); */
	    }

	    // Gets prefilled lists
	    $lists = $this->getLists($this->productName());

	    $flow = $this->recentFlow($this->productName());

	    if ($flow)
	    {
	    	if ($flow->step === '2')
		    {
			    // Store in session the lowest available limit for credit cards
			    session()->put('lowest_limit', $lists['cardLimits']['CLASICA']['begin']);
		    }
	    }

		return view('bank.card.index', compact('lists', 'flow'));
		/* return response()->json([
			'status' => 200,
			'flow' => $flow,
			'message' => 'Regresaría al home',
			'lists' => $lists
		]); */
    }

    /**
     * Send form info for card approval
     *
     * @param SecondRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function second(SecondRequest $request)
    {
	    // Updates customer with form data

        $flow = $this->store->second($request);

	    $approvalProcess = ['response' => null];

	    if (!env('BANK_CARD_DEMO'))
	    {
		    // Call search customer process
		    $approvalProcess = $this->soap(
		        $this->productName(),
			    'HN',
			    'DIGITAL_CARD_APPROVAL_PROCESS',
			    $this->parameters->second($flow),
			    null,
			    $flow
		    );

		    if(in_array($approvalProcess['code'], [403, 500]))
		    {
		        return $this->redirectIfResponseHasError($approvalProcess['code'], $this->productName());
		    }
	    }

	    // Get approved card details and update flow
        $this->cardDetails->details($approvalProcess['response'], $flow);

	    $this->updateStep($flow, 'next');

	    if (env('ENABLE_VUE_RESPONSES'))
	    {
		    return response()->json([
		    	'flow' => $flow
		    ]);
	    }

        return redirect()->route('bank.card.index');

    }

    /**
     * Send form info for entailment
     *
     * @param ThirdRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function third(ThirdRequest $request)
    {
	    // Updates customer with form data
        $flow = $this->store->third($request);

	    if (!env('BANK_CARD_DEMO'))
	    {
	        $entailmentProcess = $this->soap(
	            $this->productName(),
		        'HN',
	            'DIGITAL_CARD_ENTAILMENT_PROCESS',
		        $this->parameters->third($flow),
		        null,
		        $flow
	        );

	        if (in_array($entailmentProcess['code'], [403, 500]))
	        {
	            return $this->redirectIfResponseHasError($entailmentProcess['code'], $this->productName());
	        }
	    }

	    $this->updateStep($flow, 'next');

	    $flow->update([
	    	'identifier' => 0
	    ]);

	    // Stores customer information in printable table
	    $product = $this->storePrintableProduct($flow, env('BANK_CARD_PRINTABLES_VERSION'), 'HN');

	    // Return success flash message
	    $this->genericMessage(
		    '¡Excelente!',
		    '<p>Ya ha terminado el proceso de solicitud de tarjeta de crédito</p>',
		    'success'
	    );

	    // Forget all session keys
	    $this->forgetId($this->productName());

	    if (env('ENABLE_VUE_RESPONSES'))
	    {
		    return response()->json([
			    'flow' => $flow
		    ]);
	    }


        return redirect()->route('bank.card.index');
    }

	private function productName()
	{
		/* return response()->json([
			'bank_card'
		], 200);  */
		return 'bank_card';
	}
}