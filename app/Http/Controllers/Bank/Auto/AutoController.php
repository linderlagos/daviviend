<?php

namespace App\Http\Controllers\Bank\Auto;

// Core
use App\Http\Controllers\Controller;
use App\Core\Bank\Auto\AutoRegistry;
use App\Core\Bank\Auto\AutoParameters;
use App\Core\Bank\Auto\AutoResponse;

// Requests
use App\Http\Requests\Bank\Auto\FifthRequest;
use App\Http\Requests\Bank\Auto\ThirdRequest;
use App\Http\Requests\Bank\Auto\FourthRequest;
use App\Http\Requests\Bank\Auto\SecondRequest;
use App\Http\Requests\Bank\Auto\SearchRequest;
use App\Http\Requests\Bank\Auto\CalculateRequest;

// Models
use App\Http\Requests\Bank\Auto\traditionalRequest;
use App\Models\Quotations;
use App\Models\Flow;
use App\Models\Customer;
use App\Core\GetPreFilledData;


/**
 * Class AutoController
 * @package App\Http\Controllers\Bank\Auto
 */
class AutoController extends Controller
{

	/**
	 * @var AutoRegistry
	 */
	protected $store;

	/**
	 * @var AutoParameters
	 */
	protected $parameters;

    /**
     * @var AutoResponse
     */
    protected $response;

	/**
	 * AutoController constructor.
	 * @param AutoRegistry $store
	 * @param AutoParameters $parameters
	 * @param AutoResponse $response
	 */
	public function __construct(AutoRegistry $store, AutoParameters $parameters, AutoResponse $response)
    {
        $this->store = $store;
        $this->parameters = $parameters;
        $this->response = $response;
    }

	/**
	 * Show auto module index view
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
    {
//    	session()->put([
//    		'bank_auto_id' => 319
//	    ]);

//	    session()->forget('bank_auto_id');

		// Get product parameters
		$productParameters = $this->productParameters($this->productName());

		// Check if there is another flow active
		$activeFlow = $this->flowIsActive($productParameters['active_flows']);

		// If another flow is active, redirect to that flow to complete process
	    if ($activeFlow)
	    {
		    $activeFlowParameters = $this->productParameters($activeFlow);

		    return redirect()->to($activeFlowParameters['home_route']);
	    }

        // Gets prefilled lists
		$lists = $this->getLists($this->productName());

		// Retrieve recent flow
		$flow = $this->recentFlow($this->productName());

		/* if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'lists' => $lists,
                'status' => 200,
				'message' => 'success',
				'route' => '/banco/prestamo-de-auto/',
            ]);
        } */
		
        return view('bank.auto.index', compact('lists', 'flow'));

    }


	/**
	 * Search if customer exists and create new flow
	 *
	 * @param SearchRequest $request
	 * @return \Illuminate\View\View
	 */
	public function search(SearchRequest $request)
    {
        $customer = $this->getOrCreateCustomer($request->get('identity'));

        $flow = $this->store->search($customer);

	    $initialCustomerData = ['response' => null];

	    if(!env('BANK_AUTO_DEMO'))
	    {
	        $initialFilterData = $this->initialFilterData($flow);

            //redireccionamiento segun el codigo de respuesta
		    if (in_array($initialFilterData['code'], [403, 500]))
		    {
			    return $this->redirectIfResponseHasError($initialFilterData['code'], $this->productName());
		    }

            $initialCustomerData = $this->initialCustomerData($flow);

            //redireccionamiento segun el codigo de respuesta
		    if (in_array($initialCustomerData['code'], [403, 500]))
		    {
			    return $this->redirectIfResponseHasError($initialCustomerData['code'], $this->productName());
		    }
	    }

	    $this->response->search($initialCustomerData['response'], $flow);

	    $this->persistId($this->productName(), $flow->id);
		//dd("enviaría esto como lista: 1" );
        if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'route' => '/solicitudes-en-tramite/',
                'status' => 200,
                'message' => 'success'
            ]);
        }

        return redirect()->route('bank.auto.query');
    }

	/**
	 * Start flow from first form
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function start()
    {
    	$flow = $this->recentFlow($this->productName());
		//dd("enviaría esto como lista: 2" );
        if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

	    return redirect()->route('bank.auto.index');
    }

	/**
	 * Generate list of evaluations
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function query()
    {
    	$flow = $this->recentFlow($this->productName());

	    $evaluations = ['response' => null];

	    if(!env('BANK_AUTO_DEMO'))
	    {
		    $evaluations = $this->evaluations($flow);

		    //redireccionamiento segun el codigo de respuesta
		    if (in_array($evaluations['code'], [403, 500]))
		    {
			    return $this->redirectIfResponseHasError($evaluations['code'], $this->productName());
		    }
	    }

	    $lists = $this->response->evaluations($evaluations['response'], $flow);
		
	    if (count($lists) > 0)
	    {
			
            if(env('ENABLE_VUE_RESPONSES')) {
				//dd("enviaría esto como lista: 3" . $lists);
                return response()->json([
                    'data' => $flow,
                    'lists' => $lists,
                    'status' => 200,
                    'message' => 'success'
                ]);
            }

		    return view('bank.auto.query', compact('flow', 'lists'));
	    }

		//dd("enviaría esto como lista: 4" );
        if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

	    return redirect()->route('bank.auto.index');
    }

	/**
	 * Search if customer exists and create new flow
	 *
	 * @return \Illuminate\View\View
	 */
	public function new()
	{
		$currentFlow = $this->recentFlow($this->productName());

		//$customer = $currentFlow->customer;

		$flow = $this->store->new($currentFlow);

		$initialCustomerData = ['response' => null];

		if(!env('BANK_AUTO_DEMO'))
		{
			$initialCustomerData = $this->initialCustomerData($flow);

            //redireccionamiento segun el codigo de respuesta
			if (in_array($initialCustomerData['code'], [403, 500]))
			{
				return $this->redirectIfResponseHasError($initialCustomerData['code'], $this->productName());
			}
		}

		$this->response->search($initialCustomerData['response'], $flow);

		$this->persistId($this->productName(), $flow->id);
		//dd("enviaría esto como lista: 5" );
        if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

		return redirect()->route('bank.auto.index');
	}

    /**
     * @param SecondRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function second(SecondRequest $request)
    {
        $flow = $this->store->second($request);

        $flow->update(['step' => '2']);
		//dd("enviaría esto como lista: 6" );
        if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

        return redirect()->route('bank.auto.index');
    }


	/**
	 * @param CalculateRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Throwable
	 */
	public function calculate(CalculateRequest $request)
    {
		$flow = $this->store->calculate($request);

	    $productParameters = $this->productParameters($this->productName());

	    $quotationScenarios = ['response' => null];

	    if(!env('BANK_AUTO_DEMO'))
		{
		   //Metodo(44CL, 45CL, 46CL) para calcular los escenarios del cliente.
		   $quotationScenarios = $this->soap(
		       $this->productName(),
		       'HN',
		       'DIGITAL_CAR_QUOTATION_SCENARIOS',
		       $this->parameters->calculate($flow),
		       'SCENARIO_DATA_PARAMS'
		   );

		   if (in_array($quotationScenarios['code'], [403, 500]))
		   {
		       return response()->json([
			       'success' => false,
			       'error' => $quotationScenarios['message'],
			       'route' => $productParameters['home_route']
		       ], $quotationScenarios['code']);
		   }
		}

		$this->response->calculate($quotationScenarios['response'], $flow);

		$stage = get_json($flow->product_information, ['result_stage']);

		$this->store->quotation($flow, $quotationScenarios['response']);

		if ($stage == 'A')
		{
		   $message = '¡Excelente! esta es la proyección del préstamo. Solo debe continuar con el proceso para enviar a aprobación';

		} elseif ($stage == 'D') {

		   $this->store->suggestions($flow, $quotationScenarios['response']);

		   $message = 'Los datos ingresados no son suficientes para aceptar la solicitud, esta es una lista de sugerencias para tomar en cuenta';
		} else {

		   $this->genericMessage(
		       '¡Muy Bien!',
		       '<p>Felicidades su solicitud esta por terminar. Debe abocarse a una sucursal de davivienda para terminar el proceso</p>',
		       'success'
		   );

		   $this->forgetId($this->productName());

		   return response()->json([
		       'success' => false,
			   'route' => $productParameters['home_route'],
			   'data' => $flow
		   ], 403);
		}
		$data = view('bank.auto.modules._results', compact('message', 'flow'))->render();

		return response()->json([
		   'success' => true,
		   'view' => $data,
		   'data' => $flow,
		   'message' => $message
		]);
    }

	/**
	 * @param ThirdRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function third(ThirdRequest $request)
    {
	    $flow = $this->recentFlow($this->productName(), true);

	    $flow->update(['step' => '3']);
		//dd("enviaría esto como lista: 7" );
        if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

        return redirect()->route('bank.auto.index');
    }


	/**
     * Metodo(52CL) para rechazar una cotizacion por parte del cliente.
	 * @return bool|\Illuminate\Http\JsonResponse
	 */
	public function recalculate()
    {
        $flow = $this->recentFlow($this->productName(), true);

        // Metodo(52CL) para rechazar una cotizacion por parte del cliente.
        if(!env('BANK_AUTO_DEMO'))
        {
	        $response = $this->soap(
		        $this->productName(),
		        'HN',
		        'DIGITAL_CAR_RECALCULATE_SCENARIO',
		        $this->parameters->recalculate($flow),
		        'RECALCULATE_PARAMS'
	        );

	        if (in_array($response['code'], [403, 500]))
	        {
		        return $this->redirectIfResponseHasError($response['code'], $this->productName());
	        }
        }

	    return response()->json([
		    'success' => true
	    ]);
    }

	/**
	 * Process fourth step of the form
	 *
	 * @param FourthRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function fourth(FourthRequest $request)
	{
		// Updates customer with form data
		$flow = $this->store->fourth($request);

		$response = '';

		//Metodo(47CL) para devolver la respuesta del motor en la pantalla de evalucacion de motor.
		if(!env('BANK_AUTO_DEMO'))
		{
			$response = $this->soap(
			    $this->productName(),
				'HN',
				'DIGITAL_CAR_EVALUATION_ENGINE',
				$this->parameters->fourth($flow),
				'EVALUATION_PARAMS'
			);

			if (in_array($response['code'], [403, 500]))
			{
				return $this->redirectIfResponseHasError($response['code'], $this->productName());
			}

			//Metodo de envio de mensajes de texto
			$sendAlertMessage = $this->sendAlertMessage($flow);

            if (in_array($sendAlertMessage['code'], [403, 500]))
            {
                return $this->redirectIfResponseHasError($sendAlertMessage['code'], $this->productName());
            }

		} else {
            $response = ['response' => ''];
        }

		$this->response->fourth($response['response'], $flow);

		$answerMotor = get_json($flow->product_information, ['response_engine_evaluation', 'answer_motor_decisions_credit']);

		if($answerMotor === "R")
		{
			$this->genericMessage(
				'¡Lo Sentimos!',
				'<p>Su solicitud ha sido rechazada</p>',
				'success'
			);

			$flow->update(['step' => 'rechazado']);

			session()->forget('bank_auto_id');

			return redirect()->route('bank.auto.index');

		} elseif($answerMotor === "A")
        {
            $this->genericMessage(
                '¡Muy Bien!',
                '<p>La solicitud de crédito ha sido Aprobada. Complete la Proforma para terminar el proceso.</p>',
                'success'
            );
        } else {

            $this->genericMessage(
                '¡Muy Bien!',
                '<p>La solicitud de crédito ha sido pre-calificada, pasara a metodo tradicional, por favor abocarse a la sucursal de Davivienda mas cercana.</p>',
                'success'
            );
        }

        $flow->update(['step' => 'aprobado']);
        
		////dd("enviaría esto como lista: 8" );
        if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'route' => '/solicitudes-en-tramite/',
                'message' => 'success'
            ]);
        }

		return redirect()->route('bank.auto.query');
	}

	/**
	 * @param $id
	 * @param $correlative
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function continue($id, $correlative, $responseMotor)
    {
    	$newFlow = Flow::findOrFail($id);

    	$quotation = Quotations::where('identifier', $newFlow->identifier)->where('correlative', $correlative)->firstOrFail();

    	$flow = $quotation->flow;

    	$this->store->continue($flow, $quotation,'4');

    	$this->persistId($this->productName(), $flow->id);


//      $answerMotor = get_json($flow->product_information, ['response_engine_evaluation', 'answer_motor_decisions_credit']);

        if($responseMotor === 'T')
        {
            $flow->update(['step' => '5']);

			if(!env('BANK_AUTO_DEMO'))
			{
				return redirect()->route('bank.auto.index');
			}
        }
		//dd("enviaría esto como lista: 9" );
        if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

        return redirect()->route('bank.auto.index');
    }


    public function traditional(traditionalRequest $request)
    {

        $flow = $this->store->traditional($request);

        if(!env('BANK_AUTO_DEMO'))
        {
            $traditionalFlow = $this->integrationTraditionalFlow($flow);

            //redireccionamiento segun el codigo de respuesta
            if (in_array($traditionalFlow['code'], [403, 500]))
            {
                return $this->redirectIfResponseHasError($traditionalFlow['code'], $this->productName());
            }
        }

	    $this->storePrintableAutoProduct($flow);

		$this->genericMessage(
		 '¡Muy Bien!',
		 '<p>Felicidades su solicitud esta por terminar. Debe abocarse a una sucursal de Davivienda para terminar el proceso</p>',
		 'success'
		);

		$flow->update(['step' => '6']);

        return redirect()->route('bank.auto.index');
    }


	/**
	 * @param FifthRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function fifth(FifthRequest $request)
    {

        $flow = $this->store->fifth($request);

        if(!env('BANK_AUTO_DEMO'))
        {
            // Call search customer process
            //Metodo(50CL) para almacenar la proforma.
            $response = $this->soap(
                $this->productName(),
                'HN',
                'DIGITAL_CAR_SAVE_PROFORMA_DATA',
                $this->parameters->fifth($flow),
                'PROFORMA_PARAMS'
            );

	        if (in_array($response['code'], [403, 500]))
	        {
		        return $this->redirectIfResponseHasError($response['code'], $this->productName());
	        }

	        $traditionalFlow = $this->integrationTraditionalFlow($flow);

	        if (in_array($traditionalFlow['code'], [403, 500]))
	        {
		        return $this->redirectIfResponseHasError($traditionalFlow['code'], $this->productName());
	        }
        }

	    $this->genericMessage(
            '¡Felicidades!',
            '<p>La solicitud ha sido finalizada de manera exitosa. Favor presentarse a una sucursal Davivienda para la liquidación de su préstamo.</p>',
            'success'
        );

        $this->storePrintableAutoProduct($flow);

        $flow->update(['step' => '6']);
		//dd("enviaría esto como lista: 10" );
        if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

        return redirect()->route('bank.auto.index');
    }


	/**
     * Metodo(51CL) para almacenar la solicitud en el flujo tradicional.
	 *
     * @param $flow
     * @return array|int
     */
	private function integrationTraditionalFlow($flow)
    {

	    $response = $this->soap(
	    	$this->productName(),
		    'HN',
		    'DIGITAL_CAR_INTEGRATE_AND_FINALIZE_FLOW',
		    $this->parameters->integrationTraditional($flow),
		    'INTEGRATION_PARAMS'
	    );

	    return $response;
    }


	/**
     * Metodo(37CL) para filtrado de datos, determina la existencia se una session iniciada para el cliente, entre otras..
	 * @param $flow
	 * @return int
	 */
	private function initialFilterData($flow)
    {
        $response = $this->soap(
        	$this->productName(),
            'HN',
            'DIGITAL_CAR_INITIAL_FILTER_DATA',
            $this->parameters->initialFilterData($flow),
            'FILTER_PARAMS',
	        $flow
        );

        return $response;
    }

    /**
     * Metodo(43CL) para el almacenamiento de datos iniciales del cliente.
     * @param $flow
     * @return array|int
     */
    private function initialCustomerData($flow)
    {
	    $response = $this->soap(
		    $this->productName(),
		    'HN',
		    'DIGITAL_CAR_INITIAL_CUSTOMER_DATA',
		    $this->parameters->second($flow),
		    'INITIAL_DATA_PARAMS',
		    $flow
	    );

	    return $response;
    }

	/**
     * Metodo(48CL) para extraer las cotizaciones realizadas por el cliente.
	 *
	 * @param $flow
	 * @return array|int
	 */
	private function evaluations($flow)
    {
        $response = $this->soap(
            $this->productName(),
            'HN',
            'DIGITAL_CAR_EVALUATIONS_LIST',
            $this->parameters->evaluationListCar($flow),
            'EVALUATION_PARAMS',
            $flow
        );

        return $response;
    }

	/**
     * Metodo(53CL) para el envio de mensajes a los ejecutivos de auto, para informar del avance del flujo.
	 * @param $flow
	 * @return int
	 */
	private function sendAlertMessage($flow)
    {
        $response = $this->soap(
            $this->productName(),
            'HN',
            'DIGITAL_CAR_SEND_ALERT_MESSAGE',
            $this->parameters->sendAlertMessage($flow),
            'ALERT_PARAMS',
            $flow
        );

        return $response;
    }

    private function storePrintableAutoProduct($flow)
    {
	    // Stores customer information in printable table
	    $product = $this->storePrintableProduct($flow, env('BANK_AUTO_PRINTABLES_VERSION'), 'HN');

	    $quotations = $flow->quotations()->get();

	    foreach($quotations as $quotation)
	    {
	    	$quotation->update([
	    		'product_id' => $product->id
		    ]);
	    }

	    // Forget all session keys
	    $this->forgetId($this->productName());

	    return true;
    }

	/**
	 * @return string
	 */
	private function productName()
    {
        return 'bank_auto';
    }

}
