<?php

namespace App\Http\Controllers\Bank\Account;

// Core
use App\Http\Controllers\Controller;
use App\Core\Bank\Account\AccountParameters;
use App\Core\Bank\Account\AccountRegistry;
use App\Core\Bank\Account\GetEntailedAccountDetails;

// Requests
use App\Http\Requests\Bank\Account\FifthRequest;
use App\Http\Requests\Bank\Account\SixthRequest;
use App\Http\Requests\Bank\Account\ThirdRequest;
use App\Http\Requests\Bank\Account\FourthRequest;
use App\Http\Requests\Bank\Account\SecondRequest;
use App\Models\Customer;

/**
 * Class AccountController
 * @package App\Http\Controllers\Account
 */
class AccountController extends Controller
{
    /**
     * @var AccountRegistry
     */
    protected $store;

    /**
     * @var AccountParameters
     */
    protected $parameters;

	/**
	 * @var GetEntailedAccountDetails
	 */
	protected $getAccountDetails;

	/**
	 * AccountController constructor.
	 * @param AccountRegistry $store
	 * @param AccountParameters $parameters
	 * @param GetEntailedAccountDetails $getAccountDetails
	 */
    public function __construct(AccountRegistry $store, AccountParameters $parameters, GetEntailedAccountDetails $getAccountDetails)
    {
        $this->store = $store;
        $this->parameters = $parameters;
        $this->getAccountDetails = $getAccountDetails;
    }

    /**
     * Show index view with prefilled data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
    	$productParameters = $this->productParameters($this->productName());

    	// Checks to see if there is an open session for card process and sends the user back to that view
	    $activeFlow = $this->flowIsActive($productParameters['active_flows']);

	    if ($activeFlow)
	    {
		    $activeFlowParameters = $this->productParameters($activeFlow);

		    return redirect()->to($activeFlowParameters['home_route']);
	    }

	    // Gets prefilled lists
        $lists = $this->getLists($this->productName());

	    // Retrieve recent flow
	    $flow = $this->recentFlow($this->productName());

        return view('bank.account.index', compact('lists', 'flow'));
    }

	/**
	 * Second form step
	 * Evaluate customer
	 *
	 * @param SecondRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function second(SecondRequest $request)
    {
    	$flow = $this->store->second($request);

    	if(!env('BANK_ACCOUNT_DEMO'))
        {
            $response = $this->soap(
                $this->productName(),
                'HN',
                'DIGITAL_ACCOUNT_EVALUATE_CONTROL_LISTS',
                $this->parameters->second($flow),
                'EVALUATE_PARAMS'
            );

            if(in_array($response['code'], [403, 500]))
            {
                return $this->redirectIfResponseHasError($response['code'], $this->productName());
            }
        }

        $this->updateStep($flow, 'next');

    	if(env('ENABLE_VUE_RESPONSES'))
        {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

		//return redirect()->route('bank.account.index');
		return response()->json([
			'status' => 500,
			'flow' => $flow,
			'message' => '2'
		], 200);
    }

    /**
     * Third step
     * Store personal information
     *
     * @param ThirdRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function third(ThirdRequest $request)
    {

    	// Updates customer with form data
        $flow = $this->store->third($request);

	    $this->updateStep($flow, 'next');

        if(env('ENABLE_VUE_RESPONSES'))
        {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

        return redirect()->route('bank.account.index');
    }

    /**
     * Process second step of the form
     *
     * @param FourthRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fourth(FourthRequest $request)
    {
	    // Updates customer with form data
        $flow = $this->store->fourth($request);

	    $this->updateStep($flow, 'next');

        if(env('ENABLE_VUE_RESPONSES'))
        {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

        return redirect()->route('bank.account.index');
    }

    /**
     * Process third step of the form
     *
     * @param FifthRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fifth(FifthRequest $request)
    {
	    // Updates customer with form data
        $flow = $this->store->fifth($request);

	    $this->updateStep($flow, 'next');

	    if(env('ENABLE_VUE_RESPONSES'))
        {
            return response()->json([
                'data' => $flow,
                'status' => 200,
                'message' => 'success'
            ]);
        }

	    return redirect()->route('bank.account.index');
    }

	/**
	 * Process fourth step of the form and send customer information to core app
	 *
	 * @param SixthRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function sixth(SixthRequest $request)
	{
		// Updates customer with form data
		$flow = $this->store->sixth($request);


		$entailmentProcess = ['response' => null];

		if(!env('BANK_ACCOUNT_DEMO')) {
            $entailmentProcess = $this->soap(
                $this->productName(),
                'HN',
                'DIGITAL_ACCOUNT_ENTAILMENT_CREATE_ACCOUNT_PROCESS',
                $this->parameters->sixth($flow)
            );


            if (in_array($entailmentProcess['code'], [403, 500])) {
                return $this->redirectIfResponseHasError($entailmentProcess['code'], $this->productName());
            }
        }

		$this->getAccountDetails->details($entailmentProcess['response'], $flow);

		// Stores customer information in printable table
		$product = $this->storePrintableProduct($flow, env('BANK_ACCOUNT_PRINTABLES_VERSION'), 'HN');

		// Return success flash message
		$this->genericMessage(
			'¡Listo!',
			'<p>Se ha creado de manera exitosa la Cuenta de Ahorro, favor proceder a imprimir el documento de Firma.</p>',
			'success'
		);

		//$this->updateStep($flow, 'next');

		// Forget all session keys
		$this->forgetId($this->productName());


        if(env('ENABLE_VUE_RESPONSES')) {
            return response()->json([
                'data' => $flow,
                'route' => '/otros-productos/',
                'id_product' => $product->id,
                'status' => 200,
                'message' => 'success'
            ]);
        }


		return redirect()->route('cross.sell.index', $product->id);
	/* 	return response()->json([
			'status' => 200,
			'flow' => $flow,
			'message' => '7'
		], 200); */
	}

	private function productName()
	{
		return 'bank_account';
	}
}