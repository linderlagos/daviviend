<?php

namespace App\Http\Controllers\Insurance\SafeFamily;

// Core
use App\Core\Insurance\SafeFamily\SafeFamilyRegistry;
use App\Core\Insurance\SafeFamily\SafeFamilyParameters;

// Helpers
use Jenssegers\Date\Date;
use App\Http\Controllers\Controller;

// Requests
use App\Http\Requests\Insurance\SafeFamily\FifthRequest;
use App\Http\Requests\Insurance\SafeFamily\FirstRequest;
use App\Http\Requests\Insurance\SafeFamily\FourthRequest;
use App\Http\Requests\Insurance\SafeFamily\SecondRequest;
use App\Http\Requests\Insurance\SafeFamily\SixthRequest;
use App\Http\Requests\Insurance\SafeFamily\ThirdRequest;

// Models
use App\Models\Product;

/**
 * Class AccountController
 * @package App\Http\Controllers\Account
 */
class SafeFamilyController extends Controller
{
	/**
	 * @var SafeFamilyRegistry
	 */
	protected $store;

	/**
	 * @var SafeFamilyParameters
	 */
	protected $parameters;


	/**
	 * SafeFamilyController constructor.
	 *
	 * @param SafeFamilyRegistry $store
	 * @param SafeFamilyParameters $parameters
	 */
	public function __construct(SafeFamilyRegistry $store, SafeFamilyParameters $parameters)
	{
		$this->store = $store;
		$this->parameters = $parameters;
	}


	/**
	 * Return index view with select options if available
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function index()
	{

		$flow = $this->recentFlow($this->productName());

		$parent = null;

		if ($flow)
		{
			$parent = $flow->product()->first();
		}

		// Return prefilled lists
		$lists = $this->getLists($this->productName());

		// Default array values for form lists
		$jsonPlans = [];

		$plans = [];

		$products = [];

		$todayDate = Date::today();

		// Check if product has registered plans
		if (isset($flow->product_information['plans'])) {
			// Recover all plans
			$allPlans = $flow->product_information['plans'];

			// Generate select options with recovered plans
			foreach ($allPlans as $plan)
			{

				$plans[$plan['Codigo']] = $plan['Descripcion'];

				if ($plan['Marca'] === 'S')
				{
					$plans[$plan['Codigo']] = $plan['Descripcion'] . ' (Recomendado)';
				}
			}

			// Encode plans in json to show premium based on user selection
			$jsonPlans = json_encode($allPlans);
		}

		// Check if product has the customer actual products to choose as payment form
		if (isset($flow->product_information['products']))
		{
			// Recover all products
			$allProducts = $flow->product_information['products'];

			// Generate select options with recovered products
			foreach ($allProducts as $value)
			{

				$key = $value['code'] . '-' . $value['value'];

				$actual = '1-' . $flow->product()->first()->identifier;

				$products[$key] = $value['value'];

				if ($key === $actual)
				{
					$products[$key] = $value['value'] . ' (Cuenta actual)';
				}

				if ($value['code'] === '8')
				{
					$products[$key] = $value['value'] . ' (Tarjeta de crédito)';
				}
			}
		}

		return view('insurance.safeFamily.index', compact('lists', 'flow', 'parent', 'plans', 'jsonPlans', 'products', 'todayDate'));
	}

	/*
	 * Search customer by identifier
	 *
	 */
	public function search()
	{
		// Show form with identity input only if insurance is requested through full process

		// TODO: Generate first form if now previous product is being associated with safe family insurance
	}


	/**
	 * Send main parameters for first evaluation
	 *
	 * @param FirstRequest $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function first(FirstRequest $request, $id)
	{

		// Get product information with identifier and unique random string
		$parent = Product::findOrFail($id);

        $validateIdentity = ['response' => null];

		if ($parent->type === 'bank_account' && $parent->version === '1')
		{
			$this->errorMessage(
				'<p>Esta cuenta fue creada previo a la implementación del flujo de Familia Segura. Por lo que debe ingresar la solicitud por el flujo tradicional</p>'
			);

			//return redirect()->back();
			return response()->json([
				'status' => 500,
				'parent' => $parent,
				'id' => $id,
			], 200);
		}


		if(!env('INSURANCE_SAFE_FAMILY_DEMO'))
        {
            $validateIdentity = $this->soap(
                $this->productName(),
                'HN-SEGUROS',
                'DIGITAL_INSURACE_VALIDATE_IDENTITY',
                $this->parameters->first($parent),
                'DIGITAL_INSURACE_VALIDATE_IDENTITY_PARAMS'
            );

            if (in_array($validateIdentity['code'], [403, 500])) {
                if ($validateIdentity['response_error_code'] === 100001) {
                    $this->store->customerAlreadyHasPolicy($parent, [
                        'code' => $validateIdentity['response_error_code'],
                        'message' => $validateIdentity['message'],
                    ], $this->productName());
                }

                return $this->redirectIfResponseHasError($validateIdentity['code'], $this->productName());
            }

        }

		// Store first response from soap request
		$flow = $this->store->first($parent, $validateIdentity['response']);

		$this->updateStep($flow, 'next');

		// Store in session newly created resource id with process step
		$this->persistId($this->productName(), $flow->id);


		return redirect()->route('insurance.safe.family.index');
	}

	/**
	 * Send stature, weight and first medical questions
	 *
	 * @param SecondRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function second(SecondRequest $request)
    {

        $flow = $this->store->second($request);

        if(!env('INSURANCE_SAFE_FAMILY_DEMO'))
        {
            $response = $this->soap(
                $this->productName(),
                'HN-SEGUROS',
                'DIGITAL_INSURACE_POLICY_ISSUANCE',
                $this->parameters->second($flow),
                'DIGITAL_INSURACE_VALIDATE_IDENTITY_PARAMS',
                $flow
            );


            if (in_array($response['code'], [403, 500])) {
                return $this->redirectIfResponseHasError($response['code'], $this->productName());
            }
        }

		$this->store->secondResponse($flow);

		$this->updateStep($flow, 'next');

		//return redirect()->route('insurance.safe.family.index');
		return response()->json([
			'status' => 500,
			'flow' => $flow,
			'request' => $request,
		], 200);
	}

	/**
	 * Send second medical questions, with select insurance plan and payment form
	 *
	 * @param ThirdRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function third(ThirdRequest $request)
	{
		$flow = $this->store->third($request);

		$this->updateStep($flow, 'next');

		//return redirect()->route('insurance.safe.family.index');
		return response()->json([
			'status' => 500,
			'flow' => $flow,
			'request' => $request,
		], 200);
	}

	/**
	 * Send selected plan, frequency and payment method
	 *
	 * @param FourthRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function fourth(FourthRequest $request)
	{
		$flow = $this->store->fourth($request);

		if(!env('INSURANCE_SAFE_FAMILY_DEMO')) {
            $response = $this->soap(
                $this->productName(),
                'HN-SEGUROS',
                'DIGITAL_INSURACE_FORM_ONE_ANSWER',
                $this->parameters->fourth($flow),
                null,
                $flow
            );

            if (in_array($response['code'], [403, 500])) {
                return $this->redirectIfResponseHasError($response['code'], $this->productName());
            }
        }

		$this->updateStep($flow, 'next');

		//return redirect()->route('insurance.safe.family.index');
		return response()->json([
			'status' => 500,
			'flow' => $flow,
			'request' => $request,
		], 200);
	}

	/**
	 * Send beneficiaries
	 *
	 * @param FifthRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function fifth(FifthRequest $request)
	{
		$flow = $this->store->fifth($request);

        $response = ['response' => null];

		if(!env('INSURANCE_SAFE_FAMILY_DEMO')) {

            $response = $this->soap(
                $this->productName(),
                'HN-SEGUROS',
                'DIGITAL_INSURACE_INSERT_BENEFICIARY',
                $this->parameters->fifth($flow),
                null,
                $flow
            );

            if (in_array($response['code'], [403, 500])) {
                return $this->redirectIfResponseHasError($response['code'], $this->productName());
            }
        }

		$this->store->fifthResponse($flow, $response['response']);

		$this->updateStep($flow, 'next');

		//return redirect()->route('insurance.safe.family.index');
		return response()->json([
			'status' => 500,
			'flow' => $flow,
			'request' => $request,
		], 200);
	}

	/**
	 * Send acceptance
	 *
	 * @param SixthRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function sixth(SixthRequest $request)
	{
		$flow = $this->store->sixth($request);
		$response = ['response' => null];
		if(!env('INSURANCE_SAFE_FAMILY_DEMO')) {

            $response = $this->soap(
                $this->productName(),
                'HN-SEGUROS',
                'DIGITAL_INSURACE_FINAL_DATA',
                $this->parameters->sixth($flow),
                'DIGITAL_INSURACE_FINAL_DATA_PARAMS',
                $flow
            );

            if (in_array($response['code'], [403, 500])) {
                return $this->redirectIfResponseHasError($response['code'], $this->productName());
            }
        }

		$this->store->sixthResponse($flow, $response['response']);

		$this->updateStep($flow, 'next');

		// Stores customer information in printable table
		$product = $this->storePrintableProduct($flow, env('INSURANCE_SAFE_FAMILY_PRINTABLES_VERSION'), 'HN-SEGUROS');

		// Return success flash message
		$this->genericMessage(
			'¡Listo!',
			'<p>Se ha generado de manera exitosa la póliza de Familia Segura, favor proceder a enviar la póliza vía correo electrónico.</p>',
			'success'
		);
		
		// Forget all session keys
		$this->forgetId($this->productName());


		// Forget all session keys
		$this->forgetId($this->productName());

		//return redirect()->route('show.product', $product->parent_id);
		return response()->json([
			'status' => 500,
			'flow' => $flow,
			'request' => $request,
			'product' => $product,
		], 200);
	}

	/**
	 * Get product name
	 *
	 * @return string
	 */
	private function productName()
	{
		return 'insurance_safe_family';
	}
}