<?php

namespace App\Http\Controllers;

//use App\Core\Bridge;
use App\Core\Data;
use App\Core\GetPreFilledData;
use App\Mail\Account\SendContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Google2FA;
use Jenssegers\Date\Date;

/**
 * Class CoreController
 * @package App\Http\Controllers
 */
class CoreController extends Controller
{
    /**
     * Show the main page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
//	    $lists = $this->soap('lists', 'HN', 'GLOBAL_PREFILLED_DATA', null, 'GET_ALL');
//
//	    dd($lists['response']->getCreditCardProcessResult()->Data->DataList->Data[23]);

//	    $lists = new GetPreFilledData();
//
//	    dd($lists->bankAutoData()['agentOptions']);

//	    $lists = new GetPreFilledData();
//
//	    dd($lists->bankAutoData());

//
//	    dd($lists->getCreditCardProcessResult()->Data->DataList->Data[19]);

//    	session()->forget([
//    		'card-id',
//		    'card-step',
//		    'account-id',
//		    'account-step'
//	    ]);
	    
//	    dd(Google2FA::generateSecretKey());

	    $user = Auth::user();

	    if ($user->hasAnyRole([env('ROL_FIRMAS'), env('ROL_CIF')]))
	    {
		    return redirect()->route('query.index');
	    }

	    if ($user->hasAnyRole([env('ROL_SEGURIDAD')]))
	    {
		    return redirect()->route('security.index');
	    }

	    if ($user->hasAnyRole([env('ROL_CONCESIONARIA')]))
	    {
		    return redirect()->route('bank.auto.index');
	    }

        return view('index');
    }

    /**
     * Show browser restriction to user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function incompatibleBrowser()
    {
        return view('errors.incompatibleBrowser');
    }

	public function dynamicList(Request $request)
	{
		$flow = $request->flow;

		$listKey = $request->list_key;

		$key = $request->key;

		$placeholder = $request->placeholder;

		$lists = $this->getLists($flow);

		$list = $lists[$listKey];

		if ($key)
		{
			$list = $list[$key];
		}

		if (!$placeholder)
		{
			$placeholder = 'Seleccione';
		}

		$options = '<option value="">' . $placeholder . '</option>';

		foreach ($list as $key => $option)
		{
			$options .= '<option value="' . $key . '">' . $option . '</option>';
		}

		return response()->json([
			'success' => true,
			'list' => $options
		]);

//		return response()->json([
//			'true' => false
//		]);
//
//		if (isset($lists[$request->list]))
//		{
//			$list = $lists[$request->list];
//
//			return response()->json([
//				'success' => true,
//				'list' => $list
//			]);
//		}
//
//		return response()->json([
//			'success' => false
//		], 404);
	}

	/**
	 * Test email contract notification module
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function emailTest()
	{
		$printable = Printable::orderBy('created_at', 'asc')->first();

		$email = get_json($printable->fields, 'email');

		// If customer doesn't have email redirect back with error message
//		if ($email === 'notiene@davivienda.com.hn')
//		{
//			$this->message(
//				'¡Oh no!',
//				'<p>No se puede enviar correo al cliente ya que no tiene correo electrónico registrado</p>',
//				'error'
//			);
//
//			return redirect()->route('home');
//		}

//		try {
			Mail::to('dennis@andes.la')->send(new SendContract($printable));
//		} catch (\Exception $e) {
//			$this->message(
//				'¡Oh no!',
//				'<p>No se pudo enviar el correo electrónico por un problema interno.</p>',
//				'error'
//			);
//
//			return redirect()->route('home');
//		}

		$this->message(
			'¡Excelente!',
			'<p>Se ha enviado correctamente el correo con el link del contrato</p>',
			'success'
		);

		return view('index');
	}
}