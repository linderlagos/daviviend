<?php

namespace App\Http\Controllers;

use App\Exports\ProductsExport;
use App\Http\Requests\QueryRequest;
use App\Models\Audit;
use App\Models\Customer;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;
use Maatwebsite\Excel\Facades\Excel;

class QueryController extends Controller
{
	public function index(QueryRequest $request)
	{
		$month = Date::now()->sub('10 day');

		$auditParameters = $this->auditParameters();

		if (count($request->all()) > 0)
		{
			$products = $this->filterProducts($request);
		} else {
			$products =  Product::where('created_at', '>', $month)
				->filterByRole()
				->orderBy('created_at', 'DESC')
				->whereNotIn('type', ['bank_card','bank_auto'])
				->paginate(30);
		}

//		foreach ($products as $product)
//		{
//			if ($product->audits()->count() === 0)
//			{
//				return $product->id;
//			}
//		}

		$productTypes = [
			'bank_account' => 'Cuenta de ahorro',
			'insurance_safe_family' => 'Familia segura',
		];

		return view('query', compact('products', 'auditParameters', 'request', 'productTypes'));
	}

	public function export(QueryRequest $request)
	{
		$products = $this->filterProducts($request);

		if ($products->count() === 0)
		{
			$this->genericMessage(
				'No se encontraron productos',
				'<p>En base a los filtros seleccionados no se encontraron productos para exportar. Favor modifique los filtros y vuelva a intentar</p>',
				'error'
			);

			return redirect()->back();
		}

		$date = Date::now()->format('Y.m.d H:i:s');
		$user = Auth::user()->peopleSoft;

		$filename = $date . ' - ' . $user . ' - Lista de productos.xlsx';

		return Excel::download(new ProductsExport($products), $filename);
	}
}
