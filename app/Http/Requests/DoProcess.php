<?php 

namespace App\Http\Requests;

class DoProcess
{
    /**
     * @var object
     */
    protected $DataTransferObject;

    /**
     * Constructor
     *
     * @param object $DataTransferObject
     */
    public function __construct($DataTransferObject)
    {
        $this->DataTransferObject = $DataTransferObject;
    }

    /**
     * @return object
     */
    public function getDataTransferObject()
    {
        return $this->DataTransferObject;
    }
}