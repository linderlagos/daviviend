<?php

namespace App\Http\Requests\Bank\Account;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\NumDash;
use App\Rules\hn\RealId;

class SearchRequest extends FormRequest
{
    protected $redirect = '#form';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//	        'id_type' => [
//		        'required'
//	        ],
            'identity' => [
                'required',
                new NumDash(),
                new RealId()
            ]
        ];
    }
}
