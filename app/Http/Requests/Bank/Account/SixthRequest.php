<?php

namespace App\Http\Requests\Bank\Account;

// Core
use App\Rules\Amount;
use App\Rules\hn\RequiredIfAndWithoutAll;
use App\Rules\MinIncome;
use Illuminate\Foundation\Http\FormRequest;

// Rules
use App\Rules\AlphaSpace;
use App\Rules\FirstNumber;
use App\Rules\NumDash;
use App\Rules\PhoneLength;
use App\Rules\RepeatedCharacters;
use App\Rules\SameDigitInput;

class SixthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'foreign_currency' => [
        		'sometimes',
		        'required'
	        ],
	        'typical_monthly_income' => [
		        'sometimes',
		        'required_if:foreign_currency,S',
		        'max:12',
		        new MinIncome(),
		        new Amount(),
	        ],
	        'typical_monthly_currency' => [
		        'required_if:foreign_currency,S',
	        ],
	        'probable_monthly_income' => [
		        'required_if:foreign_currency,S',
		        'max:12',
		        new MinIncome(),
		        new Amount(),
	        ],
	        'wire_transfers' => [
		        'sometimes',
		        new RequiredIfAndWithoutAll('foreign_currency', 'S', ['wire_transfers', 'currency_purchase', 'checks_in_dollars', 'remittances'])
	        ],
	        'probable_monthly_currency' => [
		        'required_if:wire_transfers,S',
		        'required_if:remittances,S'
	        ],
	        'received_transfer_country' => [
//		        'required_if:wire_transfers,S',
		        new RequiredIfAndWithoutAll('wire_transfers', 'S', ['sent_transfer_sender', 'received_transfer_country'])
	        ],
	        'received_transfer_sender' => [
//		        'required_if:wire_transfers,S',
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaSpace(),
		        new RequiredIfAndWithoutAll('wire_transfers', 'S', ['sent_transfer_sender', 'received_transfer_sender'])
	        ],
	        'received_transfer_reason' => [
		        'max:40',
//		        'required_if:wire_transfers,S',
		        new RequiredIfAndWithoutAll('wire_transfers', 'S', ['sent_transfer_sender', 'received_transfer_reason'])
	        ],
	        'sent_transfer_country' => [
//		        'required_if:wire_transfers,S',
		        new RequiredIfAndWithoutAll('wire_transfers', 'S', ['received_transfer_sender', 'sent_transfer_country'])
	        ],
	        'sent_transfer_sender' => [
//		        'required_if:wire_transfers,S',
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaSpace(),
		        new RequiredIfAndWithoutAll('wire_transfers', 'S', ['received_transfer_sender', 'sent_transfer_sender'])
	        ],
	        'sent_transfer_reason' => [
		        'max:40',
//		        'required_if:wire_transfers,S',
		        new RequiredIfAndWithoutAll('wire_transfers', 'S', ['received_transfer_sender', 'sent_transfer_reason'])
	        ],
	        'remittances' => [
		        'sometimes',
		        new RequiredIfAndWithoutAll('foreign_currency', 'S', ['wire_transfers', 'checks_in_dollars', 'currency_purchase', 'remittances'])
	        ],
	        'remittance_country' => [
		        'required_if:remittances,S'
	        ],
	        'remittance_sender' => [
		        'required_if:remittances,S',
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaSpace()
	        ],
	        'remittance_sender_relationship' => [
		        'required_if:remittances,S'
	        ],
	        'remittance_reason' => [
		        'max:40',
		        'required_if:remittances,S'
	        ],
	        'currency_purchase' => [
		        'sometimes',
		        new RequiredIfAndWithoutAll('foreign_currency', 'S', ['wire_transfers', 'currency_purchase', 'checks_in_dollars', 'remittances'])
	        ],
	        'checks_in_dollars' => [
		        'sometimes',
		        new RequiredIfAndWithoutAll('foreign_currency', 'S', ['wire_transfers', 'currency_purchase', 'checks_in_dollars', 'remittances'])
	        ],
        ];
    }
}
