<?php

namespace App\Http\Requests\Bank\Account;

// Core
use App\Core\Traits\RecentFlow;
use App\Models\Flow;
use App\Rules\hn\CheckIdentificationByType;
use App\Rules\hn\RealId;
use App\Rules\IfUserHasNoEmailCheckDefaultEmail;
use App\Rules\RequiredIfResource;
use App\Rules\RequiredUnlessResource;
use Illuminate\Foundation\Http\FormRequest;

// Rules
use App\Rules\Amount;
use App\Rules\NumDash;
use App\Rules\MinIncome;
use App\Rules\AlphaSpace;
use App\Rules\PhoneLength;
use App\Rules\FirstNumber;
use App\Rules\AlphaNumSpace;
use App\Rules\SameDigitInput;
use App\Rules\CheckIfFutureDate;
use App\Rules\RepeatedCharacters;
use App\Rules\RequiredIfOtherCity;
use App\Rules\CheckPublicJobToDate;
use App\Rules\CheckPublicJobFromDate;
use App\Rules\RequiredIfResourceNull;
use App\Rules\CheckIfDayBelongsToMonth;
use App\Rules\CheckPublicJobToDateToday;

// Models
use App\Models\Account\Customer;

class FourthRequest extends FormRequest
{
	use RecentFlow;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
    	$flow = $this->recentFlow('bank_account');

    	$jobType = get_json($flow->customer_information, ['employer', 'job', 'type', 'code']);

    	$jobTypeDependants = ['10','11','44'];

        return [
//            'spouse' => [
//                'nullable',
//                'required_if:marital_status,C',
//                new RepeatedCharacters('2'),
//                new AlphaSpace()
//            ],
	        'professional_dependant_name' => [
	        	'sometimes',
		        new RequiredIfResource($jobType, $jobTypeDependants),
//		        'required_if:job_type,10,11,44',
		        new RepeatedCharacters('2'),
		        new AlphaSpace(),
	        ],
	        'professional_dependant_relationship' => [
		        'sometimes',
		        new RequiredIfResource($jobType, $jobTypeDependants),
	        ],
	        'professional_dependant_id_type' => [
		        'sometimes',
		        new RequiredIfResource($jobType, $jobTypeDependants),
	        ],
	        'professional_dependant_identity' => [
		        'sometimes',
		        new RequiredIfResource($jobType, $jobTypeDependants),
		        new RealId()
	        ],
	        'professional_dependant_job_type' => [
	        	'sometimes',
		        new RequiredIfResource($jobType, $jobTypeDependants),
            ],
	        'income' => [
		        'sometimes',
		        'required',
		        'max:12',
		        new MinIncome(),
		        new Amount(),
                new RequiredIfResource($jobType, $jobTypeDependants),
//		        new RequiredIfResourceNull($flow->customer_information)
	        ],
	        'profession' => [
		        'sometimes',
	        	'required'
	        ],
	        'employer_name' => [
	        	'sometimes',
		        new RequiredUnlessResource($jobType, $jobTypeDependants),
		        'max:40'
	        ],
	        'employer_type' => [
	        	'sometimes',
		        new RequiredUnlessResource($jobType, $jobTypeDependants),
		        'max:1'
	        ],
	        'employer_day' => [
		        'sometimes',
		        new RequiredUnlessResource($jobType, $jobTypeDependants),
		        new CheckIfDayBelongsToMonth('employer_day', 'employer_month')
	        ],
	        'employer_month' => [
		        'sometimes',
		        new RequiredUnlessResource($jobType, $jobTypeDependants),
		        new CheckIfFutureDate('employer_day', 'employer_month', 'employer_year')
	        ],
	        'employer_year' => [
		        'sometimes',
		        new RequiredUnlessResource($jobType, $jobTypeDependants),
//		        'gt:year'
	        ],
	        'employer_phone' => [
		        'sometimes',
		        new RequiredUnlessResource($jobType, $jobTypeDependants),
		        new NumDash(),
		        new FirstNumber([2,3,7,8,9]),
		        new SameDigitInput(),
		        new PhoneLength(8)
	        ],
	        'job' => [
	        	'sometimes',
		        new RequiredUnlessResource($jobType, $jobTypeDependants),
		        'max:1'
	        ],
	        'job_name' => [
		        'sometimes',
		        new RequiredUnlessResource($jobType, $jobTypeDependants),
		        'max:40'
	        ],
	        'job_email' => [
		        'email',
		        'max:40',
		        new IfUserHasNoEmailCheckDefaultEmail('has_job_email'),
                new RequiredUnlessResource($jobType, $jobTypeDependants),
	        ],
//			'job_status' => [
//			    'sometimes',
//                'max:1',
//                new RequiredUnlessResource($jobType, $jobTypeDependants),
//                ],
	        'employer_address_1' => [
                'sometimes',
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace(),
                new RequiredUnlessResource($jobType, $jobTypeDependants),
	        ],
	        'employer_address_2' => [
                'sometimes',
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace(),
                new RequiredUnlessResource($jobType, $jobTypeDependants),
	        ],
	        'employer_address_3' => [
                'sometimes',
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace(),
//                new RequiredUnlessResource($jobType, $jobTypeDependants),
	        ],
	        'employer_city' => [
		        'sometimes',
		        new RequiredUnlessResource($jobType, $jobTypeDependants),
	        ],
//	        'other_employer_city' => [
//		        'sometimes',
//		        'max:40',
//		        //new RequiredIfOtherCity('employer_city', 'colonia de trabajo'),
//		        new RepeatedCharacters('2'),
//		        new AlphaNumSpace()
//	        ],
            'public_job' => [
                'sometimes',
                'required'
            ],
            'public_job_employer_name' => [
                'required_if:public_job,S',
                new RepeatedCharacters('2'),
                new AlphaSpace(),
//			    'nullable'
            ],
            'public_job_name' => [
                'required_if:public_job,S',
                new RepeatedCharacters('2'),
                new AlphaSpace(),
//			    'nullable'
            ],
            'public_job_active' => [
                'required_if:public_job,S',
//			    'nullable'
            ],
            'public_job_from' => [
                'required_if:public_job,S',
                new CheckPublicJobFromDate($flow->customer_information),
//			    'nullable'
            ],
            'public_job_to' => [
                'required_if:public_job_active,N',
                'nullable',
                new CheckPublicJobToDate(),
                new CheckPublicJobToDateToday(),
            ],
            'assets' => 'required|sometimes',
            'passive' => 'required|sometimes',
            'expenses' => 'required|sometimes',
            'other_income' => 'required|sometimes'
//			'receive_sms' => 'max:1',
//			'receive_emails' => 'max:1',
//			'terms' => 'sometimes|required|max:1'
        ];
    }
}
