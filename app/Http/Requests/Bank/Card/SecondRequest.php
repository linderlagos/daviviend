<?php

namespace App\Http\Requests\Bank\Card;

use App\Core\Traits\RecentFlow;
use App\Models\Card\Customer;
use App\Rules\AlphaNumSpace;
use App\Rules\AlphaSpace;
use App\Rules\Amount;
use App\Rules\CheckIfDayBelongsToMonth;
use App\Rules\CheckIfFutureDate;
use App\Rules\FirstNumber;
use App\Rules\MinIncome;
use App\Rules\hn\NameLength;
use App\Rules\NumDash;
use App\Rules\PhoneLength;
use App\Rules\RepeatedCharacters;
use App\Rules\RequiredIfOtherCity;
use App\Rules\RequiredIfResourceNull;
use App\Rules\SameDigitInput;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ApprovalProcessRequest
 * @package App\Http\Requests
 */
class SecondRequest extends FormRequest
{
	use RecentFlow;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        session()->keep([
//            'card-id',
//            'card-step'
//        ]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
    	$flow = $this->recentFlow('bank_card', true);

        return [
            'day' => [
                'sometimes',
                'required',
                'nullable',
                new CheckIfDayBelongsToMonth('day', 'month')
            ],
            'month' => [
                'sometimes',
                'required',
                new CheckIfFutureDate('day', 'month', 'year')
            ],
            'year' => 'sometimes|required',
            'first_name' => [
                'sometimes',
                'required',
                'alpha',
                'max:15',
                new NameLength(),
                new RepeatedCharacters('2')
            ],
            'middle_name' => [
                'nullable',
                new AlphaSpace(),
                'max:15',
                new RepeatedCharacters('2')
            ],
            'last_name' => [
                'sometimes',
                'required',
                'max:15',
                new AlphaSpace(),
                new RepeatedCharacters('2')
            ],
            'second_last_name' => [
                'nullable',
                'max:15',
                new AlphaSpace(),
                new RepeatedCharacters('2')
            ],
            'phone' => [
                'nullable',
                new NumDash(),
                new FirstNumber([2,3,8,9]),
                new SameDigitInput(),
                new PhoneLength(8)
            ],
            'mobile' => [
                'sometimes',
                'required',
                new NumDash(),
                new FirstNumber([3,7,8,9]),
                new SameDigitInput(),
                new PhoneLength(8)
            ],
            'job_type' => 'sometimes|required|max:4',
            'income' => [
                'sometimes',
                'required',
                'max:12',
                new MinIncome(),
                new Amount(),
                new RequiredIfResourceNull($flow)
            ],
            'city' => [
                'required',
            ],
            'other_city' => [
                'max:40',
                new RequiredIfOtherCity('city', 'colonia de domicilio'),
                new RepeatedCharacters('2'),
                new AlphaNumSpace()
            ],
            'address_1' => [
                'required',
                'max:40',
                new RepeatedCharacters('2'),
                new AlphaNumSpace()
            ],
	        'address_2' => [
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace()
	        ],
	        'address_3' => [
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace()
	        ],
            'nationality' => 'sometimes|required|max:5',
            'promotion' => 'sometimes|required|max:5',
            'employer_name' => 'sometimes|required|max:40',
            'employer_type' => 'sometimes|required|max:1',
            'employer_day' => [
                'sometimes',
                'required',
                new CheckIfDayBelongsToMonth('employer_day', 'employer_month')
            ],
            'employer_month' => [
                'sometimes',
                'required',
                new CheckIfFutureDate('employer_day', 'employer_month', 'employer_year')
            ],
            'employer_year' => [
                'sometimes',
                'required'
            ],
            'employer_phone' => [
                'sometimes',
                'required',
                new NumDash(),
                new FirstNumber([2,3,7,8,9]),
                new SameDigitInput(),
                new PhoneLength(8)
            ],
            'job' => 'sometimes|required|max:3',
            'job_contract' => 'sometimes|required|max:3',
            'employer_address_1' => [
                'required',
                'max:40',
                new RepeatedCharacters('2'),
                new AlphaNumSpace()
            ],
	        'employer_address_2' => [
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace()
	        ],
	        'employer_address_3' => [
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace()
	        ],
            'employer_city' => [
                'required',
            ],
            'other_employer_city' => [
                'max:40',
                new RequiredIfOtherCity('employer_city', 'colonia de trabajo'),
                new RepeatedCharacters('2'),
                new AlphaNumSpace()
            ],
            'public_job' => [
                'sometimes',
                'required'
            ],
            'bureau' => 'required',
//		    'g-recaptcha-response' => 'required|captcha'
        ];
    }
}
