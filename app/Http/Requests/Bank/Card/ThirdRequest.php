<?php

namespace App\Http\Requests\Bank\Card;

use App\Core\Traits\RecentFlow;
use App\Rules\AlphaNumSpace;
use App\Rules\AlphaSpace;
use App\Rules\CardLimit;
use App\Rules\CheckForPublicJob;
use App\Rules\CheckPublicJobFromDate;
use App\Rules\CheckPublicJobToDate;
use App\Rules\CheckPublicJobToDateToday;
use App\Rules\EmptyOption;
use App\Rules\FirstNumber;
use App\Rules\IfUserHasNoEmailCheckDefaultEmail;
use App\Rules\NumDash;
use App\Rules\PhoneLength;
use App\Rules\RepeatedCharacters;
use App\Rules\SameDigitInput;
use App\Rules\SameValue;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\Card\Customer;

class ThirdRequest extends FormRequest
{
	use RecentFlow;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
//        session()->keep([
//            'card-id',
//            'card-step'
//        ]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
    	$flow = $this->recentFlow('bank_card', true);

        return [
            'customer_limit'=> [
                new CardLimit($flow)
            ],
            'email' => [
            	'sometimes',
	            'required',
	            'email',
	            'max:40',
	            new IfUserHasNoEmailCheckDefaultEmail('has_email')
            ],
            'job_email' => [
	            'email',
	            'max:40',
	            new IfUserHasNoEmailCheckDefaultEmail('has_job_email')
            ],
            'gender' => 'sometimes|required|max:1',
            'marital_status' => 'sometimes|required|max:1',
            'spouse' => [
                'nullable',
                'required_if:marital_status,C',
                new RepeatedCharacters('2'),
                new AlphaSpace()
            ],
            'public_job_employer_name' => [
                new CheckForPublicJob($flow),
                new RepeatedCharacters('2'),
                new AlphaSpace(),
//			    'nullable'
            ],
            'public_job_name' => [
                new CheckForPublicJob($flow),
                new RepeatedCharacters('2'),
                new AlphaSpace(),
//			    'nullable'
            ],
            'public_job_active' => [
                new CheckForPublicJob($flow),
//			    'nullable'
            ],
            'public_job_from' => [
                new CheckForPublicJob($flow),
                new CheckPublicJobFromDate($flow),
//			    'nullable'
            ],
            'public_job_to' => [
                'required_if:public_job_active,N',
                'nullable',
//	            new CheckForPublicJob(Customer::recent('card-id')->firstOrFail()),
                new CheckPublicJobToDate(),
                new CheckPublicJobToDateToday(),
            ],
//            'dependants' => [
//                new EmptyOption()
//            ],
//            'dependant_name_1' => [
//                'nullable',
//                'required_if:dependants,S',
//                'sometimes',
//                new RepeatedCharacters('2'),
//                new AlphaSpace()
//            ],
//            'dependant_name_2' => [
//                'nullable',
//                new RepeatedCharacters('2'),
//                new AlphaSpace(),
//                'required_with:dependant_name_3,dependant_name_4,dependant_name_5,dependant_name_6'
//
//            ],
//            'dependant_name_3' => [
//                'nullable',
//                new RepeatedCharacters('2'),
//                new AlphaSpace(),
//                'required_with:dependant_name_4,dependant_name_5,dependant_name_6',
//
//            ],
//            'dependant_name_4' => [
//                'nullable',
//                new RepeatedCharacters('2'),
//                new AlphaSpace(),
//                'required_with:dependant_name_5,dependant_name_6',
//
//            ],
//            'dependant_name_5' => [
//                'nullable',
//                new RepeatedCharacters('2'),
//                new AlphaSpace(),
//                'required_with:dependant_name_6',
//
//            ],
//            'dependant_name_6' => [
//                'nullable',
//                new RepeatedCharacters('2'),
//                new AlphaSpace()
//            ],
            'assets' => 'required|sometimes',
            'passive' => 'required|sometimes',
            'expenses' => 'required|sometimes',
            'other_income' => 'required|sometimes',
            'reference_1_name' => [
                'sometimes',
                'required',
                new RepeatedCharacters('2'),
                new AlphaSpace(),
	            new SameValue('reference_2_name')
            ],
            'reference_1_relationship' => [
                'required',
                'sometimes'
            ],
            'reference_1_mobile' => [
                'nullable',
                new NumDash(),
                new FirstNumber([3,7,8,9]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_1_phone,reference_1_work_phone',
	            new SameValue('reference_2_mobile')
            ],
            'reference_1_phone' => [
                'nullable',
                new NumDash(),
                new FirstNumber([2]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_1_work_phone,reference_2_phone,reference_2_work_phone',
	            new SameValue('reference_2_phone')
            ],
            'reference_1_work_phone' => [
                'nullable',
                new NumDash(),
                new FirstNumber([2]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_1_phone,reference_2_phone,reference_2_work_phone',
	            new SameValue('reference_2_work_phone')
            ],
            'reference_2_name' => [
                'sometimes',
                'required',
                new RepeatedCharacters('2'),
                new AlphaSpace(),
	            new SameValue('reference_1_name')
            ],
            'reference_2_mobile' => [
                'nullable',
                new NumDash(),
                new FirstNumber([3,7,8,9]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_2_phone,reference_2_work_phone',
	            new SameValue('reference_1_mobile')
            ],
            'reference_2_phone' => [
                'nullable',
                new NumDash(),
                new FirstNumber([2]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_2_work_phone,reference_1_phone,reference_1_work_phone',
	            new SameValue('reference_1_phone')
            ],
            'reference_2_work_phone' => [
                'nullable',
                new NumDash(),
                new FirstNumber([2]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_2_phone,reference_1_phone,reference_1_work_phone',
	            new SameValue('reference_1_work_phone')
            ],
	        'notes' => [
		        'sometimes',
		        'max:240'
	        ],
            'receive_sms' => 'max:1',
            'receive_emails' => 'max:1',
            'terms' => 'sometimes|required|max:1'
        ];
    }
}
