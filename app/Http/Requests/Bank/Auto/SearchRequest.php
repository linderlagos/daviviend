<?php

namespace App\Http\Requests\Bank\Auto;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\NumDash;
use App\Rules\hn\RealId;

class SearchRequest extends FormRequest
{
    //protected $redirect = 'home';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        //dd("hola");
        return [
            'identidad' => [
                'required',
            ],
	        /*'bureau' => [
	        	'required'
	        ]*/
        ];
    }



}
