<?php

namespace App\Http\Requests\Bank\Auto;

// Core
use App\Core\Traits\RecentFlow;
use App\Rules\CheckIfAdult;
use App\Rules\EqualFields;
use App\Rules\IfUserHasNoEmailCheckDefaultEmail;
use Illuminate\Foundation\Http\FormRequest;

// Rules
use App\Rules\NumDash;
use App\Rules\Amount;
use App\Rules\CardLimit;
use App\Rules\AlphaSpace;
use App\Rules\PhoneLength;
use App\Rules\FirstNumber;
use App\Rules\hn\NameLength;
use App\Rules\CheckForPublicJob;
use App\Rules\SameValue;
use App\Rules\MinIncome;
use App\Rules\AlphaNumSpace;
use App\Rules\SameDigitInput;
use App\Rules\CheckIfFutureDate;
use App\Rules\RepeatedCharacters;
use App\Rules\CheckPublicJobToDate;
use App\Rules\CheckPublicJobFromDate;
use App\Rules\RequiredIfOtherCity;
use App\Rules\RequiredIfResourceNull;
use App\Rules\CheckIfDayBelongsToMonth;
use App\Rules\CheckPublicJobToDateToday;
use App\Rules\RequiredUnlessResource;

// Models
use App\Models\Account\Customer;


class FourthRequest extends FormRequest
{
    use RecentFlow;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {

        $flow = $this->recentFlow('bank_auto', true);

//        $jobType = get_json($flow->customer_information, ['employer', 'job', 'type', 'code']);

        $mobile = $flow->customer_information['mobile'];
        $phone = $flow->customer_information['phone'];
//        $jobTypeDependants = ['10','11','44'];

        return [
//            'concessionaire' => 'required',
//            'seller' => 'required',
	        'email' => [
		        'sometimes',
		        'required',
		        'email',
		        'max:40',
		        new IfUserHasNoEmailCheckDefaultEmail('has_email')
	        ],
	        'job_email' => [
		        'email',
		        'max:40',
		        new IfUserHasNoEmailCheckDefaultEmail('has_job_email')
	        ],
            'city' => [
                'required',
            ],
            /*'other_city' => [
                'max:40',
                new RequiredIfOtherCity('city', 'colonia de domicilio'),
                new RepeatedCharacters('2'),
                new AlphaNumSpace()
            ],*/
	        'address_1' => [
		        'required',
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace()
	        ],
	        'address_2' => [
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace()
	        ],
	        'address_3' => [
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace()
	        ],
            'employer_name' => [
                'sometimes',
                'required',
                //new RequiredUnlessResource($jobType, $jobTypeDependants),
                'max:40'
            ],
            'job' => [
                'sometimes',
                'required',
                //new RequiredUnlessResource($jobType, $jobTypeDependants),
                'max:1'
            ],
            'employer_phone' => [
                'sometimes',
                'required',
                //new RequiredUnlessResource($jobType, $jobTypeDependants),
                new NumDash(),
                new FirstNumber([2,3,7,8,9]),
                new SameDigitInput(),
                new PhoneLength(8)
            ],
            'employer_type' => [
                'sometimes',
                'required',
                //new RequiredUnlessResource($jobType, $jobTypeDependants),
                'max:1'
            ],
            'employer_city' => [
                'sometimes',
                'required'
                //new RequiredUnlessResource($jobType, $jobTypeDependants),
            ],
            'other_employer_city' => [
                'sometimes',
                'max:40',
                new RequiredIfOtherCity('employer_city', 'colonia de trabajo'),
                new RepeatedCharacters('2'),
                new AlphaNumSpace()
            ],
	        'employer_address_1' => [
		        'required',
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace()
	        ],
	        'employer_address_2' => [
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace()
	        ],
	        'employer_address_3' => [
		        'max:40',
		        new RepeatedCharacters('2'),
		        new AlphaNumSpace()
	        ],
            'employer_day' => [
                'sometimes',
                'required',
                new CheckIfDayBelongsToMonth('employer_day', 'employer_month')
            ],
            'employer_month' => [
                'sometimes',
                'required',
                new CheckIfFutureDate('employer_day', 'employer_month', 'employer_year')
            ],
            'employer_year' => [
                'sometimes',
                'required',
//                'gt:year'
            ],
            'marital_status' => 'sometimes|required|max:1',
            'spouse_identity' => 'required_if:marital_status,C',
            'spouse' => [
                'nullable',
                'required_if:marital_status,C',
                new RepeatedCharacters('2'),
                new AlphaSpace()
            ],
            'nationality' => 'sometimes|required|max:5',
            'fatca' => [
                'sometimes',
                'required'
            ],
            'reference_1_name' => [
                'sometimes',
                'required',
                new RepeatedCharacters('2'),
                new AlphaSpace(),
                new SameValue('reference_2_name')
            ],
            'reference_1_relationship' => [
                'required',
                'sometimes'
            ],
            'reference_1_mobile' => [
                'nullable',
                new NumDash(),
                new FirstNumber([3,7,8,9]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_1_phone,reference_1_work_phone',
                new SameValue('reference_2_mobile'),
                new EqualFields( $mobile, 'mobile', 'reference_1_mobile')
            ],
            'reference_1_phone' => [
                'nullable',
                new NumDash(),
                new FirstNumber([2]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_1_work_phone,reference_2_phone,reference_2_work_phone',
                new SameValue('reference_2_phone'),
                new SameValue('employer_phone'),
                new EqualFields($phone, 'phone', 'reference_1_phone')
            ],
            'reference_1_work_phone' => [
                'nullable',
                new NumDash(),
                new FirstNumber([2]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_1_phone,reference_2_phone,reference_2_work_phone',
                //new SameValue('employer_phone')
                //new SameValue('reference_2_work_phone'),
            ],
            'reference_2_name' => [
                'sometimes',
                'required',
                new RepeatedCharacters('2'),
                new AlphaSpace(),
                new SameValue('reference_1_name')
            ],
            'reference_2_mobile' => [
                'nullable',
                new NumDash(),
                new FirstNumber([3,7,8,9]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_2_phone,reference_2_work_phone',
                new SameValue('reference_1_mobile'),
                new EqualFields( $mobile, 'mobile', 'reference_2_mobile')
            ],
            'reference_2_phone' => [
                'nullable',
                new NumDash(),
                new FirstNumber([2]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_2_work_phone,reference_1_phone,reference_1_work_phone',
                new SameValue('reference_1_phone'),
                new SameValue('employer_phone'),
                new EqualFields($phone, 'phone', 'reference_2_phone')
            ],
            'reference_2_work_phone' => [
                'nullable',
                new NumDash(),
                new FirstNumber([2]),
                new SameDigitInput(),
                new PhoneLength(8),
                'required_without_all:reference_2_phone,reference_1_phone,reference_1_work_phone',
                //new SameValue('employer_phone'),
            ],
            'auto_executive' => [
                'required',
                'sometimes'
            ],
            'profession' => 'required',
            'job_name' => [
                'required',
                'max:20',
                new AlphaSpace()
             ],
            'gender' => 'required'
        ];
    }
}
