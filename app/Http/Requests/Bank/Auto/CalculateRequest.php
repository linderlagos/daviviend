<?php

namespace App\Http\Requests\Bank\Auto;

// Core
use App\Core\Traits\RecentFlow;
use App\Rules\CheckIfAdult;
use Illuminate\Foundation\Http\FormRequest;

// Rules
use App\Rules\NumDash;
use App\Rules\Amount;
use App\Rules\AlphaSpace;
use App\Rules\PhoneLength;
use App\Rules\FirstNumber;
use App\Rules\hn\NameLength;
use App\Rules\MinIncome;
use App\Rules\GreaterThan;
use App\Rules\AlphaNumSpace;
use App\Rules\SameDigitInput;
use App\Rules\CheckIfFutureDate;
use App\Rules\RepeatedCharacters;
use App\Rules\CheckPublicJobToDate;
use App\Rules\CheckPublicJobFromDate;
use App\Rules\RequiredIfOtherCity;
use App\Rules\RequiredIfResourceNull;
use App\Rules\CheckIfDayBelongsToMonth;
use App\Rules\CheckPublicJobToDateToday;
use App\Rules\RequiredIfConcessionaireIsNotConnected;

// Models
use App\Models\Account\Customer;

class CalculateRequest extends FormRequest
{
    use RecentFlow;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        $flow = $this->recentFlow('bank_auto', true);
        $dealership_is_connected = get_json($flow->user_information, ['dealership_is_connected', 'code']);

        return [
            'concessionaire' => [
                new RequiredIfConcessionaireIsNotConnected($dealership_is_connected)
            ],
            'seller' => [
                new RequiredIfConcessionaireIsNotConnected($dealership_is_connected)
             ],
            'type_auto' => 'required',
            'auto_brand' => 'required',
            'auto_model' => [
                'required',
                'max:30'
            ],
            'status_auto' => 'required',
            'year_auto' => 'required',
            'sale_price' => [
                'required',
                'max:13',
                new MinIncome(),
                new Amount(),
            ],
            'appraisal' => [
                'sometimes',
            ],
            'premium' => [
                'sometimes',
                'required',
                'max:13',
            ],
            'financing_term' => 'required|max:2',
            'closing_costs' => 'required|max:2',
            'grace_period' => 'required|max:2',
        ];
    }
}
