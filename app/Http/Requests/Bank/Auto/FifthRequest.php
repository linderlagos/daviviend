<?php

namespace App\Http\Requests\Bank\Auto;

// Core
use App\Rules\AlphaNumSpace;
use App\Rules\hn\CheckIdentificationByType;
use App\Rules\hn\RealId;
use App\Rules\hn\RequiredIfRelationship;
use App\Rules\SameValue;
use Illuminate\Foundation\Http\FormRequest;

// Rules
use App\Rules\AlphaSpace;
use App\Rules\FirstNumber;
use App\Rules\NumDash;
use App\Rules\PhoneLength;
use App\Rules\RepeatedCharacters;
use App\Rules\SameDigitInput;
use App\Rules\DifferentValue;

class FifthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'auto_brand' => 'required',
//            'auto_model' => 'required',
//            'auto_type' => 'required',
            'auto_color' => [
                'required',
                new AlphaSpace()
                ],
//            'auto_year' => 'required',
            'auto_displacement' => [
                'required',
                'numeric'

            ],
            'motor_numbers' => [
                'required',
                //new DifferentValue('vin_numbers'),
                //new DifferentValue('chassis_numbers')
            ],
            'vin_numbers' => [
                'required',
                //new DifferentValue('motor_numbers')
            ],
            'chassis_numbers' => [
                'required',
                //new DifferentValue('motor_numbers')
            ],
	        'notes' => [
		        'sometimes',
		        'max:240'
	        ],
        ];
    }

}
