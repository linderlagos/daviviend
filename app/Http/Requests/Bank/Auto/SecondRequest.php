<?php

namespace App\Http\Requests\Bank\Auto;

// Core
use App\Rules\CheckIfAdult;
use Illuminate\Foundation\Http\FormRequest;
use App\Core\Traits\RecentFlow;
use App\Rules\hn\RealId;
use App\Rules\RequiredIfResource;
use App\Rules\RequiredUnlessResource;

// Rules
use App\Rules\NumDash;
use App\Rules\Amount;
use App\Rules\AlphaSpace;
use App\Rules\PhoneLength;
use App\Rules\FirstNumber;
use App\Rules\hn\NameLength;
use App\Rules\MinIncome;
use App\Rules\AlphaNumSpace;
use App\Rules\SameDigitInput;
use App\Rules\CheckIfFutureDate;
use App\Rules\RepeatedCharacters;
use App\Rules\CheckPublicJobToDate;
use App\Rules\CheckPublicJobFromDate;
use App\Rules\RequiredIfOtherCity;
use App\Rules\RequiredIfResourceNull;
use App\Rules\CheckIfDayBelongsToMonth;
use App\Rules\CheckPublicJobToDateToday;

// Models
use App\Models\Account\Customer;

class SecondRequest extends FormRequest
{
    use RecentFlow;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        $flow = $this->recentFlow('bank_auto', true);

        $jobType = get_json($flow->customer_information, ['employer', 'job', 'type', 'code']);

        $jobTypeDependants = ['10','11','44'];

        return [
            'first_name' => [
                'sometimes',
                'required',
                'alpha',
                'max:15',
                new NameLength(),
                new RepeatedCharacters('2')
            ],
            'middle_name' => [
                'nullable',
                new AlphaSpace(),
                'max:15',
                new RepeatedCharacters('2')
            ],
            'last_name' => [
                'sometimes',
                'required',
                'max:15',
                new AlphaSpace(),
                new RepeatedCharacters('2')
            ],
            'second_last_name' => [
                'nullable',
                'max:15',
                new AlphaSpace(),
                new RepeatedCharacters('2')
            ],
            'phone' => [
                'nullable',
                new NumDash(),
                new FirstNumber([2,3,8,9]),
                new SameDigitInput(),
                new PhoneLength(8)
            ],
            'mobile' => [
                'sometimes',
                'required',
                new NumDash(),
                new FirstNumber([3,7,8,9]),
                new SameDigitInput(),
                new PhoneLength(8)
            ],
            'job' => [
                'sometimes',
                new RequiredUnlessResource($jobType, $jobTypeDependants),
                'max:1'
            ],
            'job_type' => 'sometimes|required|max:4',
            'income' => [
                'sometimes',
                'required',
                'max:12',
                new MinIncome(),
                new Amount(),
                new RequiredIfResourceNull($flow)
            ],
            'isr_deduction' => [
                'sometimes',
                //'required',
                'max: 9',
                //new MinIncome(),
                new Amount()
            ],
            'rap_deduction' => [
                'sometimes',
                //'required',
                'max: 9',
                //new MinIncome(),
                new Amount()
            ],
            'ihss_deduction' => [
                'sometimes',
                //'required',
                'max: 9',
                //new MinIncome(),
                new Amount()
            ],
//            'other_deduction' => [
//                'sometimes',
//                'required',
//                'max: 9',
//                new MinIncome(),
//                new Amount(),
//            ],
            'day' => [
                'sometimes',
                'required',
                'nullable',
                new CheckIfDayBelongsToMonth('day', 'month')
            ],
            'month' => [
                'sometimes',
                'required',
                new CheckIfFutureDate('day', 'month', 'year'),
                new CheckIfAdult('day', 'month', 'year', 18)
            ],
            'year' => 'sometimes|required',
            'receive_sms' => 'max:1'
        ];
    }
}
