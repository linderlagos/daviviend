<?php

namespace App\Http\Middleware;

use App\Core\Traits\Message;
use App\Models\Printable\Printable;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfUserHasUploadsPending
{
	use Message;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

    	$user = Auth::user();

    	if ($user)
	    {
		    $lastAccount = $user->products()->whereIn('type', ['bank_account'])->orderBy('created_at', 'DESC')->first();

	    	if (isset($lastAccount) && $lastAccount->uploads()->get()->count() < 2)
		    {
			    \View::share('pendingUploads', $lastAccount);

			    if (env('BLOCK_USER_WITH_PENDING_UPLOADS') && get_json($lastAccount->product_information, ['product', 'code']) !== '110')
			    {
			    	$this->genericMessage(
					    '¡Documentación Pendiente!',
					    '<p>Por favor cargue los documentos pendientes para terminar con el proceso de creación de la cuenta</p>',
					    'error',
					    'Continuar'
				    );

				    return redirect()->route('show.product', $lastAccount->id);

			    }

			    return $next($request);
		    }
	    }

        return $next($request);
    }
}
