<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class AskForToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$user = Auth::user();

    	if (!in_array(request()->ip(), $this->noTokenIps()))
	    {
		    $authenticator = app(Authenticator::class)->boot($request);

		    if ($authenticator->isAuthenticated())
		    {
			    return $next($request);
		    }

		    $tokenAttempts = session()->get('token_attempt') + 1;

		    session()->flash('token_attempt', $tokenAttempts);

		    if ($tokenAttempts >= 3)
		    {
			    activity('login')
				    ->causedBy($user)
				    ->performedOn($user)
				    ->withProperties([
					    'attributes' => [
						    'hostname' => $request->getHost(),
						    'ip' => $request->ip(),
						    'name' => $user->name,
						    'peoplesoft' => $user->peopleSoft,
					    ]
				    ])
				    ->log('El usuario ' . $user->name . ' ha intentado ingresar el token ' . $tokenAttempts . ' veces de manera incorrecta');
		    }

		    return $authenticator->makeRequestOneTimePasswordResponse();
	    }

        return $next($request);
    }

    private function noTokenIps()
    {
    	return [
    		'140.4.53.69',
		    '181.115.19.69',
            '127.0.0.1'
	    ];
    }
}
