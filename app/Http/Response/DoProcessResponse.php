<?php 

namespace App\Http\Response;

/**
 * Class DoProcessResponse
 * @package App\Http\Response
 */
class DoProcessResponse
{
    /**
     * @var object
     */
    protected $DoProcessResult;

    /**
     * Constructor.
     *
     * @param object
     */
    public function __construct($DoProcessResult)
    {
        $this->DoProcessResult = $DoProcessResult;
    }

    /**
     * @return object
     */
    public function getDoProcessResult()
    {
        return $this->DoProcessResult;
    }

    /**
     * Check if response has error
     *
     * @param $flow
     * @return mixed
     */
    public function checkIfError($flow)
    {
	    $success = $this->successCodes($flow);

    	if (isset($this->getDoProcessResult()->Error))
	    {

	    	$headerError = (int) $this->getDoProcessResult()->Error;

	    	if ($headerError > $success)
		    {
			    session()->forget([$flow . '_id']);

			    return [
			    	'code' => $headerError,
				    'message' => $this->getDoProcessResult()->ExternalError
			    ];
		    }
	    }

    	$error = (int) $this->getDoProcessResult()->Data->Error;


    	$interalValidation = (!in_array($error, [64, 172]) and $error > $success);

    	if ($flow === 'insurance_safe_family')
	    {
		    $interalValidation = $error > $success;

	    }

        if ($interalValidation)
        {
            session()->forget([$flow . '_id']);

	        return [
		        'code' => $error,
		        'message' => $this->getDoProcessResult()->Data->ExternalError
	        ];
        }

        return false;
    }
    
    private function successCodes($flow)
    {
    	$codes = [
    		'bank_account' => 0,
    		'bank_card' => 0,
    		'bank_auto' => 0,
    		'login' => 0,
    		'lists' => 0,
    		'insurance_lists' => 100000,
            'insurance_safe_family' => 100000,
            '' => 00,
	    ];
    	
    	return $codes[$flow];
    }
}