<?php

namespace App\Http\Response;

/**
 * Class CreditCardProcessResponse
 * @package App\Http\Response
 */
class CreditCardProcessResponse
{
    /**
     * @var object
     */
    protected $CreditCardProcessResult;

    /**
     * Constructor.
     *
     * @param object
     */
    public function __construct($CreditCardProcessResult)
    {
        $this->CreditCardProcessResult = $CreditCardProcessResult;
    }

    /**
     * @return object
     */
    public function getCreditCardProcessResult()
    {
        return $this->CreditCardProcessResult;
    }

    /**
     * Check if response has error
     *
     * @param $flow
     * @return mixed
     */
    public function checkIfError($flow)
    {
	    $success = $this->successCodes($flow);

    	if (isset($this->getCreditCardProcessResult()->Error))
	    {

	    	$headerError = (int) $this->getCreditCardProcessResult()->Error;

	    	if ($headerError > $success)
		    {
			    session()->forget([$flow . '_id']);

			    return [
			    	'code' => $headerError,
				    'message' => $this->getCreditCardProcessResult()->ExternalError
			    ];
		    }
	    }

    	$error = (int) $this->getCreditCardProcessResult()->Data->Error;


    	$interalValidation = (!in_array($error, [64, 172]) and $error > $success);

    	if ($flow === 'insurance_safe_family')
	    {
		    $interalValidation = $error > $success;

	    }

        if ($interalValidation)
        {
            session()->forget([$flow . '_id']);

	        return [
		        'code' => $error,
                'message' => $this->getCreditCardProcessResult()->Data->ExternalError
	        ];
        }

        return false;
    }
    
    private function successCodes($flow)
    {
    	$codes = [
    		'bank_account' => 0,
    		'bank_card' => 0,
    		'bank_auto' => 0,
    		'login' => 0,
    		'lists' => 0,
    		'insurance_lists' => 100000,
    		'insurance_safe_family' => 100000,
	    ];
    	
    	return $codes[$flow];
    }
}
