<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPDFLink extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;

    protected $subjectText;

    protected $content;

    protected $url;

    /**
     * Create a new message instance.
     *
     * @param $name
     * @param $subjectText
     * @param $content
     * @param $url
     * @return void
     */
    public function __construct($name, $subjectText, $content, $url)
    {
        $this->name = $name;
        $this->subjectText = $subjectText;
        $this->content = $content;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$name = ucwords(strtolower($this->name));

    	$content = $this->content;

    	$url = $this->url;

        return $this
	        ->subject($this->subjectText)
	        ->view('mail.pdf', compact('name', 'content', 'url'));
    }
}
