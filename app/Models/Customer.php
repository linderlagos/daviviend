<?php

namespace App\Models;

use App\Models\Insurance\SafeFamily\SafeFamily;
use App\Models\Insurance\SafeFamily\SafeFamilyPrintable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 * @package App\Models\Card
 */
class Customer extends Model
{
    /**
     * @var string
     */
    protected $connection = 'customers';

    /**
     * @var string
     */
    protected $table = 'customers';

    /**
     * Protect fields for mass assignment
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

	/**
	 * A customer has many product flows
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function flows()
    {
    	return $this->hasMany(Flow::class);
    }

	/**
	 * A customer has many products
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function products()
	{
		return $this->hasMany(Product::class);
	}

	public function scopeByIdentifier($query, $identifier)
	{
		return $query->where('identifier', $identifier);
	}

	public function scopeByCif($query, $cif)
	{
		return $query->where('cif', $cif);
	}
}
