<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quotations extends Model
{
    /**
     * @var string
     */
    protected $connection = 'customers';

    /**
     * @var string
     */
    protected $table = 'bank_auto_quotations';

    /**
     * Protect fields for mass assignment
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'customer_information' => 'array',
        'product_information' => 'array',
        'user_information' => 'array'
    ];

    /**
     * A product flow belongs to a customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A product flow may belong to a parent product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * A quotation flow may belong to a parent flow
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function flow()
    {
        return $this->belongsTo(flow::class);
    }


    /**
     * Get product flow by id saved in session
     *
     * @param $query
     * @param $productName
     * @return mixed
     */
    public function scopeRecent($query, $productName)
    {
        return $query->where('id', session()->get($productName . '_id'));
    }
}
