<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 */
class Audit extends Model
{
	/**
	 * @var string
	 */
	protected $connection = 'customers';

	/**
	 * @var string
	 */
	protected $table = 'audits';

	/**
	 * Protect fields for mass assignment
	 *
	 * @var array
	 */
	protected $guarded = [
		'id',
//		'created_at',
//		'updated_at'
	];

	/**
	 * Get all of the owning auditable models.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function auditable()
	{
		return $this->morphTo();
	}
}
