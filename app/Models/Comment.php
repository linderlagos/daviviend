<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 */
class Comment extends Model
{
	/**
	 * @var string
	 */
	protected $connection = 'customers';

	/**
	 * @var string
	 */
	protected $table = 'comments';

	/**
	 * Protect fields for mass assignment
	 *
	 * @var array
	 */
	protected $guarded = [
		'id',
		'created_at',
		'updated_at'
	];

	/**
	 * Get all of the owning commentable models.
	 */
	public function commentable()
	{
		return $this->morphTo();
	}

	public function creator()
	{
		return $this->belongsTo(User::class, 'created_by', 'id');
	}

	public function solver()
	{
		return $this->belongsTo(User::class, 'solved_by', 'id');
	}

	public function scopeUnsolved(Builder $query)
	{
		return $query->where('solved', 0);
	}

	public function scopeSolved(Builder $query)
	{
		return $query->where('solved', 1);
	}
}
