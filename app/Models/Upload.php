<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Product
 * @package App\Models
 */
class Upload extends Model
{
	/**
	 * @var string
	 */
	protected $connection = 'customers';

	/**
	 * @var string
	 */
	protected $table = 'uploads';

	/**
	 * Protect fields for mass assignment
	 *
	 * @var array
	 */
	protected $guarded = [
		'id',
//		'created_at',
//		'updated_at'
	];

	/**
	 * A document upload belongs to a product
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	/**
	 * A document upload belongs to a user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * Get all of the upload's audits.
	 */
	public function audits()
	{
		return $this->morphMany(Audit::class, 'auditable');
	}

	public function scopeFilterByRole($query)
	{
		return $query;

//		$user = Auth::user();
//
//		if ($user->hasAnyRole([env('ROL_GESTOR'), env('ROL_CIF')]))
//		{
//			return $query->where('type', 'firma');
//		}
	}
}
