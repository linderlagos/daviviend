<?php

namespace App\Models\Printable;

use App\Models\CustomerScopes;
use App\Models\Insurance\SafeFamily\SafeFamily;
use App\Models\Insurance\SafeFamily\SafeFamilyPrintable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Printable extends Model
{
	use CustomerScopes;

	/**
	 * @var string
	 */
	protected $connection = 'customers';

	/**
	 * @var string
	 */
	protected $table = 'printables';

	/**
	 * Protect fields for mass assignment
	 *
	 * @var array
	 */
	protected $guarded = [
		'id',
		'created_at',
		'updated_at'
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at'
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'fields' => 'array',
		'uploads' => 'array'
	];

	public function scopeAuthorized($query)
	{
		if (cifUsers(Auth::user()->peopleSoft))
		{
			return $query->where('id', '>', 0);
		}

		return $query->where('peoplesoft', Auth::user()->peopleSoft);
	}

	public function safeFamily()
	{
		return $this->morphOne(SafeFamily::class, 'productable');
	}

	public function safeFamilyPrintable()
	{
		return $this->morphOne(SafeFamilyPrintable::class, 'productable');
	}
}
