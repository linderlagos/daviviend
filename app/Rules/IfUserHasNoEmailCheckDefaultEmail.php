<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IfUserHasNoEmailCheckDefaultEmail implements Rule
{
	protected $field;

    /**
     * Create a new rule instance.
     *
     * @param $field
     * @return void
     */
    public function __construct($field)
    {
	    $this->field = $field;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
	    $hasEmail = request()->get($this->field);

	    if($hasEmail !== null)
	    {
	    	if (strtolower($value) !== 'notiene@davivienda.com.hn')
		    {
		    	return false;
		    }

	    	return true;
	    }

	    return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.if_user_has_no_email_check_default_email');
    }
}
