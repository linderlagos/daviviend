<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SameValue implements Rule
{
	protected $parameter;

    /**
     * Create a new rule instance.
     *
     * @param $parameter
     * @return void
     */
    public function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

    	$field = request()->get($this->parameter);

        if (isset($value) && isset($field))
        {
        	if ($value === $field)
	        {
	        	return false;
	        }

	        return true;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.same_value', [
        	'parameter' => $this->translate()[$this->parameter]
        ]);
    }

    private function translate()
	{
		return [
			'reference_1_name'      => 'nombre de la referencia familiar',
			'reference_2_name'      => 'nombre de la referencia personal',
			'reference_1_relationship'=> 'relación con la referencia familiar',
			'reference_2_relationship'=> 'relación con la referencia personal',
			'reference_1_mobile'    => 'celular de la referencia familiar',
			'reference_2_mobile'    => 'celular de la referencia personal',
			'reference_1_phone'     => 'teléfono de la referencia familiar',
			'reference_2_phone'     => 'teléfono de la referencia personal',
			'reference_1_work_phone'=> 'teléfono de trabajo de la referencia familiar',
			'reference_2_work_phone'=> 'teléfono de trabajo de la referencia personal',
			'reference_1_address'   => 'dirección de la referencia familiar',
			'reference_2_address'   => 'dirección de la referencia personal',
            'employer_phone' => 'telefono del trabajo'
		];
	}
}
