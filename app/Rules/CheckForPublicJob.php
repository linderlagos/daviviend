<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckForPublicJob implements Rule
{
    protected $model;

    /**
     * Create a new rule instance.
     *
     * @param $model
     * @return void
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $flow = $this->model;

        if ($flow->customer_information['public_job']['code'] === 'N') {
            return true;
        } elseif ($flow->customer_information['public_job']['code'] === 'S' && !$value) {
            return false;
        } elseif ($flow->customer_information['public_job']['code'] === 'S' && $value) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.check_for_public_job');
    }
}
