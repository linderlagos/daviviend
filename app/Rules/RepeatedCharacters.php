<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RepeatedCharacters implements Rule
{
    protected $parameters;

    /**
     * Create a new rule instance.
     *
     * @param string $parameters
     * @return void
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validation = preg_match('/[^ 0-9](.)\1{' . $this->parameters .',}/', mb_strtoupper($value));

        if ($validation === 0) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Existe un caracter o un espacio entre palabras que se repite más de ' . $this->parameters . ' veces de manera seguida';
    }
}
