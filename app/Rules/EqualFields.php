<?php
/**
 * Created by PhpStorm.
 * User: STI-GLOBALS
 * Date: 10/5/2019
 * Time: 17:05
 */

namespace App\Rules;

use App\Core\Traits\GetLists;
use Illuminate\Contracts\Validation\Rule;

class EqualFields implements Rule
{
    use GetLists;

    protected $fieldValue;

    protected $fieldName;

    /**
     * Parameters to validate against
     *
     * @var
     */
    protected $parameter;


    /**
     * Create a new rule instance.
     *
     * @param $fieldName
     * @param $fieldValue
     * @param $parameter
     * @return void
     */
    public function __construct($fieldValue, $fieldName, $parameter)
    {
        $this->fieldValue = $fieldValue;
        $this->fieldName = $fieldName;
        $this->parameter = $parameter;

    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
       // $fields = request()->get($this->parameter);

        if (isset($value) && isset($this->fieldValue))
        {

            if ($value === $this->fieldValue)
            {
                return false;
            }

            return true;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return trans('validation.equal_fields', [
            'parameter' => $this->translate()[$this->parameter],
            'fieldName' => $this->translate()[$this->fieldName]
        ]);
    }


    private function translate()
    {
        return [
            'phone'      => 'numero de telefono',
            'mobile'      => 'numero de celular',
            'reference_1_mobile'    => 'celular de la referencia familiar',
            'reference_2_mobile'    => 'celular de la referencia personal',
            'reference_1_phone'     => 'teléfono de la referencia familiar',
            'reference_2_phone'     => 'teléfono de la referencia personal',
            'reference_1_work_phone'=> 'teléfono de trabajo de la referencia familiar',
            'reference_2_work_phone'=> 'teléfono de trabajo de la referencia personal',
            'reference_1_address'   => 'dirección de la referencia familiar',
            'reference_2_address'   => 'dirección de la referencia personal',
            'employer_phone' => 'telefono del trabajo'
        ];
    }

}