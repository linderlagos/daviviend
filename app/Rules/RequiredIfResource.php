<?php

namespace App\Rules;

use App\Core\Traits\GetLists;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class RequiredIfResource
 * @package App\Rules
 */
class RequiredIfResource implements Rule
{
	use GetLists;

	/**
	 * Field validated
	 *
	 * @var
	 */
	protected $field;

	/**
	 * Parameters to validate against
	 *
	 * @var
	 */
	protected $parameters;

    /**
     * Create a new rule instance.
     *
     * @param $field
     * @param $parameters
     * @return void
     */
    public function __construct($field, $parameters)
    {
        $this->field = $field;
        $this->parameters = $parameters;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if (in_array($this->field, $this->parameters, true) && $value === NULL) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.required_if_resource', [
	        'parameter' => $this->jobTypeDependants(),
	        'field' => $this->jobType()[$this->field]
        ]);
    }

	/**
	 * Find field value
	 *
	 * @return mixed
	 */
	private function jobType()
    {
    	return $this->getLists('bank_account')['jobTypeOptions'];
    }

	/**
	 * Find all parameter values and return single string
	 *
	 * @return string
	 */
	private function jobTypeDependants()
    {
    	$string = '';

    	foreach ($this->parameters as $parameter)
	    {
	    	$string .= $this->getLists('bank_account')['jobTypeOptions'][$parameter] . ', ';
	    }

    	return rtrim($string, ', ');
    }
}
