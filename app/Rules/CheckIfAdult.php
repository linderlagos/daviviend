<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Jenssegers\Date\Date;

/**
 * Class CheckIfAdult
 * @package App\Rules
 */
class CheckIfAdult implements Rule
{
	/**
	 * @var $day
	 */
	protected $day;

	/**
	 * @var $month
	 */
	protected $month;

	/**
	 * @var $year
	 */
	protected $year;

	/**
	 * @var $parameter
	 */
	protected $parameter;

    /**
     * Create a new rule instance.
     *
     * @param $day
     * @param $month
     * @param $year
     * @param $parameter
     * @return void
     */
    public function __construct($day, $month, $year, $parameter)
    {
	    $this->day = $day;
	    $this->month = $month;
	    $this->year = $year;
        $this->parameter = $parameter;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
	    $day = request()->get($this->day);
	    $month = request()->get($this->month);
	    $year = request()->get($this->year);

	    if ($day && $month && $year)
	    {
		    $date = Date::createFromFormat('dmY', $day . $month . $year);

		    $now = Date::now();

		    $adult = date_diff($date, $now);

		    if ($adult->y < $this->parameter) {
			    return false;
		    }
	    }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.check_if_adult', [
        	'parameter' => $this->parameter
        ]);
    }
}
