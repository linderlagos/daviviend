<?php

namespace App\Rules\hn;

use Illuminate\Contracts\Validation\Rule;

class NameLength implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) : bool
    {
        $firstName = request()->get('first_name');
        $middleName = request()->get('middle_name');
        $lastName = request()->get('last_name');
        $secondLastName = request()->get('second_last_name');

        if (request()->has('first_name')) {
            $name = $this->fullname($firstName, $middleName, $lastName, $secondLastName);

            return \strlen($name) <= 40;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El nombre completo (Nombres y apellidos) no deben sumar más de 40 caracteres.';
    }

    /**
     * Create the fullname with available fields
     *
     * @param $firstname
     * @param $middlename
     * @param $lastname
     * @param $secondlastname
     * @return string
     */
    public function fullname($firstname, $middlename, $lastname, $secondlastname) : string
    {
        $name = $firstname;

        if ($middlename) {
            $name .= ' ' . $middlename;
        }

        $name .= ' ' . $lastname;

        if ($secondlastname) {
            $name .= ' ' . $secondlastname;
        }

        return $name;
    }
}
