<?php

namespace App\Rules\hn;

use Illuminate\Contracts\Validation\Rule;

class RequiredIfRelationship implements Rule
{
	protected $parameter;

    /**
     * Create a new rule instance.
     *
     * @param $parameter
     * @return void
     */
    public function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    	if (!isset($value))
	    {
		    $field = request()->get($this->parameter);

		    if (isset($field))
		    {
			    if (array_key_exists($field, $this->relationships()))
			    {
				    return false;
			    }

			    return true;
		    }
	    }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
	    $field = request()->get($this->parameter);

        return trans('validation.required_if_relationship', [
        	'parameter' => $this->translate()[$this->parameter],
        	'field' => $this->relationships()[$field]
        ]);
    }

    private function relationships()
    {
    	return [
//		    'HERMANA' => 'HERMANA',
//		    'HERMANO' => 'HERMANO',
		    'TIA' => 'TIA',
		    'TIO' => 'TIO',
//		    'ABUELO' => 'ABUELO',
//		    'ABUELA' => 'ABUELA',
//		    'PAPA' => 'PAPA',
//		    'MAMA' => 'MAMA',
//		    'HIJO' => 'HIJO',
//		    'HIJA' => 'HIJA',
		    'SOBRINA' => 'SOBRINA',
		    'SOBRINO' => 'SOBRINO',
		    'OTROS' => 'OTROS',
//		    'ESPOSA' => 'ESPOSA',
//		    'ESPOSO' => 'ESPOSO',
		    'AMIGO' => 'AMIGO',
		    'NOVIO' => 'NOVIO',
		    'NOVIA' => 'NOVIA',
		    'SUEGRA' => 'SUEGRA',
		    'CUÑADO' => 'CUÑADO',
		    'CUÑADA' => 'CUÑADA',
		    'HIJASTRO' => 'HIJASTRO',
		    'HIJASTRA' => 'HIJASTRA',
//		    'NIETO' => 'NIETO',
//		    'NIETA' => 'NIETA',
//		    'COMPAÑERO HOGAR' => 'COMPAÑERO HOGAR',
//		    'COMPAÑERA HOGAR' => 'COMPAÑERA HOGAR',
		    'AMIGA' => 'AMIGA',
		    'SUEGRO' => 'SUEGRO',
		    'YERNO' => 'YERNO',
		    'NUERA' => 'NUERA',
		    'BISABUELO' => 'BISABUELO',
		    'BISABUELA' => 'BISABUELA',
		    'BISNIETO' => 'BISNIETO',
		    'BISNIETA' => 'BISNIETA',
		    'PRIMO' => 'PRIMO',
		    'PRIMA' => 'PRIMA'
	    ];
    }

    private function translate()
    {
    	return [
		    'beneficiary_1_name' => 'nombre del beneficiario',
		    'beneficiary_2_name' => 'nombre del beneficiario',
		    'beneficiary_3_name' => 'nombre del beneficiario',
		    'beneficiary_4_name' => 'nombre del beneficiario',
		    'beneficiary_5_name' => 'nombre del beneficiario',
		    'beneficiary_6_name' => 'nombre del beneficiario',
		    'beneficiary_7_name' => 'nombre del beneficiario',
		    'beneficiary_8_name' => 'nombre del beneficiario',
		    'beneficiary_9_name' => 'nombre del beneficiario',
		    'beneficiary_10_name' => 'nombre del beneficiario',
		    'beneficiary_1_relationship' => 'parentesco del beneficiario',
		    'beneficiary_2_relationship' => 'parentesco del beneficiario',
		    'beneficiary_3_relationship' => 'parentesco del beneficiario',
		    'beneficiary_4_relationship' => 'parentesco del beneficiario',
		    'beneficiary_5_relationship' => 'parentesco del beneficiario',
		    'beneficiary_6_relationship' => 'parentesco del beneficiario',
		    'beneficiary_7_relationship' => 'parentesco del beneficiario',
		    'beneficiary_8_relationship' => 'parentesco del beneficiario',
		    'beneficiary_9_relationship' => 'parentesco del beneficiario',
		    'beneficiary_10_relationship' => 'parentesco del beneficiario',
		    'beneficiary_1_id_type' => 'tipo de identificación del beneficiario',
		    'beneficiary_2_id_type' => 'tipo de identificación del beneficiario',
		    'beneficiary_3_id_type' => 'tipo de identificación del beneficiario',
		    'beneficiary_4_id_type' => 'tipo de identificación del beneficiario',
		    'beneficiary_5_id_type' => 'tipo de identificación del beneficiario',
		    'beneficiary_6_id_type' => 'tipo de identificación del beneficiario',
		    'beneficiary_7_id_type' => 'tipo de identificación del beneficiario',
		    'beneficiary_8_id_type' => 'tipo de identificación del beneficiario',
		    'beneficiary_9_id_type' => 'tipo de identificación del beneficiario',
		    'beneficiary_10_id_type' => 'tipo de identificación del beneficiario',
		    'beneficiary_1_identity' => 'identificación del beneficiario',
		    'beneficiary_2_identity' => 'identificación del beneficiario',
		    'beneficiary_3_identity' => 'identificación del beneficiario',
		    'beneficiary_4_identity' => 'identificación del beneficiario',
		    'beneficiary_5_identity' => 'identificación del beneficiario',
		    'beneficiary_6_identity' => 'identificación del beneficiario',
		    'beneficiary_7_identity' => 'identificación del beneficiario',
		    'beneficiary_8_identity' => 'identificación del beneficiario',
		    'beneficiary_9_identity' => 'identificación del beneficiario',
		    'beneficiary_10_identity' => 'identificación del beneficiario',
	    ];
    }
}
