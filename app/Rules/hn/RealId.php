<?php

namespace App\Rules\hn;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class RealId implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    	if (empty($value))
	    {
	    	return true;
	    }

        $value = str_replace('-', '', $value);

        $state = ltrim(substr($value, 0, 2), '0');

        $city = ltrim(substr($value, 2, 2), 0);

        $year = (int) substr($value, 4, 4);

        if (empty($this->getList()[$state]))
        {
        	return false;
        }

        if (!in_array($city, $this->getList()[$state]))
        {
        	return false;
        }

        if (!((date('Y')-100) <= $year) && !($year <= date('Y')))
        {
        	return false;
        }

        $idLength = \strlen($value) === 13;

        return $idLength;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.real_id');
    }

    private function getList()
    {
	    $getLists = Storage::disk('local')->get('public/bank_card/' . env('BANK_CARD_PRINTABLES_VERSION') .'/lists.txt');

	    $array = json_decode($getLists, true);

	    return $array['idValidation'];

    }
}
