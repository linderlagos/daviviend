<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Jenssegers\Date\Date;

class CheckPublicJobFromDate implements Rule
{
    protected $flow;

    /**
     * Create a new rule instance.
     *
     * @param $flow
     * @return void
     */
    public function __construct($flow)
    {
        $this->flow = $flow;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
	    $flow = $this->flow;

        if (request()->get('public_job') === 'N') {
            return true;
        }

        if(isset($flow['public_job']['code']))
        {
            if( $flow['public_job']['code'] === 'N'){
                return true;
            }
        }

        if (empty($value))
        {
        	return true;
        }

        $date = Date::createFromFormat('d/m/Y', $value);

        $now = Date::now();

        if ($date > $now) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.check_public_job_from_date');
    }
}
