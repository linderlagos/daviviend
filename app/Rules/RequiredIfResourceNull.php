<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RequiredIfResourceNull implements Rule
{
    protected $model;

    /**
     * Create a new rule instance.
     *
     * @param $model
     * @return void
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $customer = $this->model;

        if (!$customer['income'] && !$value) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.required_if_resource_null');
    }
}
