<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Jenssegers\Date\Date;

class CheckPublicJobToDateToday implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    	if ($value)
	    {
		    if (request()->get('public_job_active') === 'S') {
			    return true;
		    }

		    $date = Date::createFromFormat('d/m/Y', $value);

		    $now = Date::now();

		    if ($date > $now) {
			    return false;
		    }
	    }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.check_public_job_to_date_today');
    }
}
