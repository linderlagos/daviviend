<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SameDigitInput implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    	if ($value)
	    {
		    $value = str_replace('-', '', $value);

		    $array = str_split($value);

		    return 1 !== \count(array_unique($array));
	    }

	    return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.same_digit_input');
    }
}
