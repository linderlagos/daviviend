<?php

namespace App\Rules;

use App\Core\CustomerRegistry;
use Illuminate\Contracts\Validation\Rule;

class GreaterThan extends CustomerRegistry implements Rule
{
    protected $field;

    /**
     * Create a new rule instance.
     *
     * @param $field
     * @return void
     */
    public function __construct($field)
    {
        $this->field = $field;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $salePrice = request()->get($this->field);

        $salePrice = $this->cleanAmount($salePrice);
        $value = $this->cleanAmount($value);

        if($value >= $salePrice)
        {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.greater_than', [
            'parameter' => $this->translate()[$this->field]
        ]);
    }


    private function translate()
    {
        return [
            'sale_price' => 'precio de venta',
        ];
    }
}
