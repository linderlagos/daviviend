<?php
/**
 * Created by PhpStorm.
 * User: STI-GLOBALS
 * Date: 25/4/2019
 * Time: 08:54
 */

namespace App\Rules;


use Illuminate\Contracts\Validation\Rule;

class DifferentValue implements Rule
{

    protected $parameter;

    /**
     * Create a new rule instance.
     *
     * @param $parameter
     * @return void
     */
    public function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $field = request()->get($this->parameter);
        //$field[] = request()->get($this->parameter[1]);

        if (isset($value) && isset($field))
        {
            if ($value === $field)
            {
                return true;
            }

            return false;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return trans('validation.different_value', [
            'parameter' => $this->translate()[$this->parameter]
        ]);
    }

    private function translate()
    {
        return [
            'motor_numbers'      => 'numero de motor',
            'vin_numbers'      => 'numero de VIN',
            'chassis_numbers'=> 'numero de chassis',
        ];
    }
}