<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Jenssegers\Date\Date;

class CheckPublicJobToDate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (request()->get('public_job_active') === 'S') {
            return true;
        }

        if (request()->get('public_job_from'))
        {
	        $past = Date::createFromFormat('d/m/Y', request()->get('public_job_from'));

	        $future = Date::createFromFormat('d/m/Y', $value);

	        if ($past > $future) {
		        return false;
	        }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.check_public_job_to_date');
    }
}
