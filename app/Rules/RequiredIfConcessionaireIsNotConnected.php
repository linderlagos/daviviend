<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RequiredIfConcessionaireIsNotConnected implements Rule
{
    protected $parameter;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if($this->parameter === 'S')
        {
            return true;
        }

        if($this->parameter === 'N' && $value === null)
        {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.dealership_is_connected', ['parameter' => $this->parameter]);
    }
}
