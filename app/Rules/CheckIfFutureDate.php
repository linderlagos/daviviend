<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Jenssegers\Date\Date;

class CheckIfFutureDate implements Rule
{
    protected $day;

    protected $month;

    protected $year;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($day, $month, $year)
    {
        $this->day = $day;
        $this->month = $month;
        $this->year = $year;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    	$day = request()->get($this->day);
    	$month = request()->get($this->month);
    	$year = request()->get($this->year);

    	if ($day && $month && $year)
	    {
		    $date = Date::createFromFormat('dmY', $day . $month . $year);

		    $now = Date::now();

		    if ($date > $now) {
			    return false;
		    }
	    }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.check_if_future_date');
    }
}
