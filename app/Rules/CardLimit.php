<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CardLimit implements Rule
{
    protected $model;

    /**
     * Create a new rule instance.
     *
     * @param $model
     * @return void
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value)
        {
            $limit = str_replace(',', '', $value);

	        $flow = $this->model;

            return $limit >= 10000 && $limit <= $flow->product_information['product']['limit'];
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
	    $flow = $this->model;

        return 'El límite de la tarjeta debe ser mayor a ' . number_format(10000, 2) . ' y menor a ' . number_format($flow->product_information['product']['limit'], 2);
    }
}
