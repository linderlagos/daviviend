<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	'accepted'             => 'El campo :attribute debe ser aceptado.',
	'active_url'           => 'El campo :attribute no es una URL válida.',
	'after'                => 'El campo :attribute debe ser una fecha posterior a :date.',
	'after_or_equal'       => 'El campo :attribute debe ser una fecha posterior o igual a :date.',
	'alpha'                => 'El campo :attribute sólo debe contener letras.',
	'alpha_dash'           => 'El campo :attribute sólo debe contener letras, números y guiones.',
	'alpha_num'            => 'El campo :attribute sólo debe contener letras y números.',
	'array'                => 'El campo :attribute debe ser un conjunto.',
	'before'               => 'El campo :attribute debe ser una fecha anterior a :date.',
	'before_or_equal'      => 'El campo :attribute debe ser una fecha anterior o igual a :date.',
	'between'              => [
		'numeric' => 'El campo :attribute tiene que estar entre :min - :max.',
		'file'    => 'El campo :attribute debe pesar entre :min - :max kilobytes.',
		'string'  => 'El campo :attribute tiene que tener entre :min - :max caracteres.',
		'array'   => 'El campo :attribute tiene que tener entre :min - :max ítems.',
	],
	'boolean'              => 'El campo :attribute debe tener un valor verdadero o falso.',
	'confirmed'            => 'La confirmación del campo :attribute no coincide.',
	'date'                 => 'El campo :attribute no es una fecha válida.',
	'date_format'          => 'El campo :attribute no corresponde al formato :format.',
	'different'            => 'El campo :attribute y :other deben ser diferentes.',
	'digits'               => 'El campo :attribute debe tener :digits dígitos.',
	'digits_between'       => 'El campo :attribute debe tener entre :min y :max dígitos.',
	'dimensions'           => 'Las dimensiones de la imagen en el campo :attribute no son válidas.',
	'distinct'             => 'El campo :attribute contiene un valor duplicado.',
	'email'                => 'El campo :attribute no es un correo válido',
	'exists'               => 'El campo :attribute es inválido.',
	'file'                 => 'El campo :attribute debe ser un archivo.',
	'filled'               => 'El campo :attribute es obligatorio.',
	'gt'                   => [
		'numeric' => 'La fecha de inicio de trabajo deber ser mayor a la fecha de nacimiento',
		'file'    => 'El campo :attribute debe ser mayor a :value kb.',
		'string'  => 'El campo :attribute debe ser mayor a :value caracteres.',
		'array'   => 'El campo :attribute debe tener más de :value elementos.',
	],
	'image'                => 'El campo :attribute debe ser una imagen.',
	'in'                   => 'El campo :attribute es inválido.',
	'in_array'             => 'El campo :attribute no existe en :other.',
	'integer'              => 'El campo :attribute debe ser un número entero.',
	'ip'                   => 'El campo :attribute debe ser una dirección IP válida.',
	'ipv4'                 => 'El campo :attribute debe ser un dirección IPv4 válida',
	'ipv6'                 => 'El campo :attribute debe ser un dirección IPv6 válida.',
	'json'                 => 'El campo :attribute debe tener una cadena JSON válida.',
	'max'                  => [
		'numeric' => 'El campo :attribute no debe ser mayor a :max.',
		'file'    => 'El campo :attribute no debe ser mayor que :max kilobytes.',
		'string'  => 'El campo :attribute no debe ser mayor que :max caracteres.',
		'array'   => 'El campo :attribute no debe tener más de :max elementos.',
	],
	'mimes'                => 'El campo :attribute debe ser un archivo con formato: :values.',
	'mimetypes'            => 'El campo :attribute debe ser un archivo con formato: :values.',
	'min'                  => [
		'numeric' => 'El tamaño del campo :attribute debe ser de al menos :min.',
		'file'    => 'El tamaño del campo :attribute debe ser de al menos :min kilobytes.',
		'string'  => 'El campo :attribute debe contener al menos :min caracteres.',
		'array'   => 'El campo :attribute debe tener al menos :min elementos.',
	],
	'not_in'               => 'El campo :attribute es inválido.',
	'numeric'              => 'El campo :attribute debe ser numérico.',
	'present'              => 'El campo :attribute debe estar presente.',
	'regex'                => 'El formato del campo :attribute es inválido.',
	'required'             => 'El campo :attribute es obligatorio.',
	//'required_if'          => 'El campo :attribute es obligatorio cuando :other es :value.',
    'required_if'          => 'El campo :attribute es obligatorio',
	'required_unless'      => 'El campo :attribute es obligatorio a menos que :other esté en :values.',
	'required_with'        => 'El campo :attribute es obligatorio cuando :values está presente.',
	'required_with_all'    => 'El campo :attribute es obligatorio cuando :values está presente.',
	'required_without'     => 'El campo :attribute es obligatorio cuando :values no está presente.',
	'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de :values estén presentes.',
	'same'                 => 'El campo :attribute y :other deben coincidir.',
	'size'                 => [
		'numeric' => 'El tamaño del campo :attribute debe ser :size.',
		'file'    => 'El tamaño del campo :attribute debe ser :size kilobytes.',
		'string'  => 'El campo :attribute debe contener :size caracteres.',
		'array'   => 'El campo :attribute debe contener :size elementos.',
	],
	'string'               => 'El campo :attribute debe ser una cadena de caracteres.',
	'timezone'             => 'El campo :attribute debe ser una zona válida.',
	'unique'               => 'El campo :attribute ya ha sido registrado.',
	'uploaded'             => 'La subida del campo :attribute ha fallado.',
	'url'                  => 'El formato del campo :attribute es inválido.',

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom'               => [
		'password' => [
			'min' => 'La :attribute debe contener más de :min caracteres',
		],
		'email' => [
			'unique' => 'El :attribute ya ha sido registrado.',
		],
	],

	'alpha_space' => 'El campo :attribute solo puede contener letras y espacios.',

	'alpha_num_space' => 'El campo :attribute solo puede contener letras, números, espacios, guiones medios y puntos.',

	'real_id' => 'La identidad no es válida.',

	'num_dash' => 'El campo :attribute solo puede contener números y guiones',

	'min_income' => 'El campo :attribute debe ser mayor a 0.',

	'phone' => 'El campo :attribute solo puede contener números.',

	'first_number' => 'El primer número del campo :attribute debe ser X',

	'same_digit_input' => 'Este número no es válido',

	'amount' => 'El campo :attribute solo puede contener números, comas y punto.',

	'check_for_public_job' => 'El campo :attribute es obligatorio cuando cargos públicos es afirmativo',

	'other_city' => 'Este campo es obligatorio cuando el campo :parameter es otra colonia',

	'check_public_job_from_date' => 'La fecha inicial no debe ser mayor a hoy',

	'check_public_job_to_date' => 'La fecha inicial no debe ser mayor a la fecha final',

	'check_public_job_to_date_today' => 'La fecha final no debe ser mayor a hoy',

	'check_if_future_date' => 'La fecha no debe ser mayor a hoy',

	'empty_option' => 'El campo :attribute es obligatorio.',

	'required_if_resource_null' => 'El campo :attribute es obligatorio.',

	'check_if_day_belongs_to_month' => 'El día no corresponde al mes seleccionado.',

	'check_identification_by_type' => 'La identificación no es válida',

	'required_if_and_without_all' => 'El campo :attribute debe completarse si los demás campos con error están vacíos',

	'same_value' => 'El campo :attribute y :parameter deben ser diferentes',

    'different_value' => 'El campo :attribute y :parameter deben ser iguales',

	'required_if_relationship' => 'El campo :attribute es requerido cuando el campo :parameter es :field',

	'check_if_adult' => 'Pasa solicitar una cuenta el cliente deber ser mayor de :parameter años',

	'required_if_resource' => 'El campo :attribute es requerido cuando el campo :field es :parameters',

	'required_unless_resource' => 'El campo :attribute es requerido cuando el campo :field no es :parameters',

	'auto_status_year_validation' => 'El campo :attribute no está dentro del rango permitido según el estado del vehículo',

    'equal_fields' => 'El campo :attribute no debe ser igual a el campo :fieldName del cliente',

	'if_user_has_no_email_check_default_email' => 'El campo :attribute debe ser igual a notiene@davivienda.com.hn si seleccionó la casilla "No tiene correo"',

    'dealership_is_connected' => 'El campo :attribute es requerido',


	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes'           => [
		'name'                  => 'nombre',
		'id_number'             => 'identidad',
		'id_type'               => 'tipo de identificación',
		'identity'              => 'identidad',
		'username'              => 'usuario',
		'email'                 => 'correo electrónico',
		'job_email'             => 'correo electrónico laboral',
		'first_name'            => 'nombre',
		'last_name'             => 'apellido',
		'password'              => 'contraseña',
		'password_confirmation' => 'confirmación de la contraseña',
		'city'                  => 'ciudad',
		'country'               => 'país',
		'address'               => 'dirección',
		'work_address'          => 'dirección de trabajo',
		'phone'                 => 'teléfono',
		'mobile'                => 'celular',
		'age'                   => 'edad',
		'sex'                   => 'género',
		'gender'                => 'género',
		'year'                  => 'año',
		'month'                 => 'mes',
		'day'                   => 'día',
		'hour'                  => 'hora',
		'minute'                => 'minuto',
		'second'                => 'segundo',
		'title'                 => 'título',
		'content'               => 'contenido',
		'body'                  => 'contenido',
		'description'           => 'descripción',
		'excerpt'               => 'extracto',
		'date'                  => 'fecha',
		'time'                  => 'hora',
		'subject'               => 'asunto',
		'offer'                 => 'oferta',
		'message'               => 'mensaje',
		'employer_name'         => 'nombre de la empresa',
		'employer_phone'        => 'teléfono del trabajo',
		'middle_name'           => 'segundo nombre',
		'second_last_name'      => 'segundo apellido',
		'income'                => 'salario',
		'job'                   => 'tipo de empleo',
		'job_status'            => 'situación laboral',
		'g-recaptcha-response'  => 'recaptcha',
		'marital_status'        => 'estado civil',
		'state'                 => 'departamento',
		'colony'                => 'colonia',
		'receive_sms'           => 'recibis sms',
		'receive_emails'        => 'recibis correos electrónicos',
		'bureau'                => 'autorización de buró',
		'employer_type'         => 'tipo de empresa',
		'employer_day'          => 'día',
		'employer_month'        => 'mes',
		'employer_year'         => 'año',
		'employer_seniority'    => 'antigüedad laboral',
		'employer_address'      => 'dirección de trabajo',
		'employer_city'         => 'ciudad de trabajo',
		'employer_state'        => 'departamento de trabajo',
		'employer_municipality' => 'municipalidad de trabajo',
		'employer_colony'       => 'colonia de trabajo',
		'terms'                 => 'términos y condiciones',
		'status'                => 'situación laboral',
		'spouse'                => 'esposa(o)',
		'profession'            => 'profesión',
		'job_name'              => 'cargo laboral',
		'public_job'            => 'cargo público',
		'public_job_employer_name' => 'nombre de la institución',
		'public_job_name'       => 'nombre del cargo público',
		'public_job_active'     => 'estado actual del cargo público',
		'public_job_from'       => 'fecha de inicio',
		'public_job_to'         => 'fecha final',
		'dependants'            => 'dependientes',
		'dependant_name_1'      => 'nombre del dependiente',
		'dependant_name_2'      => 'nombre del dependiente',
		'dependant_name_3'      => 'nombre del dependiente',
		'dependant_name_4'      => 'nombre del dependiente',
		'dependant_name_5'      => 'nombre del dependiente',
		'dependant_name_6'      => 'nombre del dependiente',
		'assets'                => 'activos',
		'passive'               => 'pasivos',
		'expenses'              => 'gastos',
		'other_income'          => 'otros ingresos',
		'reference_1_name'      => 'nombre de la referencia familiar',
		'reference_2_name'      => 'nombre de la referencia personal',
		'reference_1_relationship'=> 'relación con la referencia familiar',
		'reference_2_relationship'=> 'relación con la referencia personal',
		'reference_1_mobile'    => 'celular de la referencia familiar',
		'reference_2_mobile'    => 'celular de la referencia personal',
		'reference_1_phone'     => 'teléfono de la referencia familiar',
		'reference_2_phone'     => 'teléfono de la referencia personal',
		'reference_1_work_phone'=> 'teléfono de trabajo de la referencia familiar',
		'reference_2_work_phone'=> 'teléfono de trabajo de la referencia personal',
		'reference_1_address'   => 'dirección de la referencia familiar',
		'reference_2_address'   => 'dirección de la referencia personal',
		'other_city'            => 'nombre de otra colonia',
		'other_employer_city'   => 'nombre de otra colonia',
		'reject'                => 'tipo de rechazo',
		'card'                  => 'tipo de tarjeta',
		'nationality'          => 'nacionalidad',
		'job_type'             => 'actividad comercial',
		'job_contract'         => 'situación laboral',
		'promotion'             => 'promoción',
		'professional_dependant_name' => 'nombre del dependiente profesional',
		'professional_dependant_relationship' => 'parentesco del dependiente profesional',
		'professional_dependant_id_type' => 'tipo de identificación del dependiente profesional',
		'professional_dependant_identity' => 'identificación del dependiente profesional',
		'beneficiary_1_name' => 'nombre del beneficiario',
		'beneficiary_2_name' => 'nombre del beneficiario',
		'beneficiary_3_name' => 'nombre del beneficiario',
		'beneficiary_4_name' => 'nombre del beneficiario',
		'beneficiary_5_name' => 'nombre del beneficiario',
		'beneficiary_6_name' => 'nombre del beneficiario',
		'beneficiary_7_name' => 'nombre del beneficiario',
		'beneficiary_8_name' => 'nombre del beneficiario',
		'beneficiary_9_name' => 'nombre del beneficiario',
		'beneficiary_10_name' => 'nombre del beneficiario',
		'beneficiary_1_relationship' => 'parentesco del beneficiario',
		'beneficiary_2_relationship' => 'parentesco del beneficiario',
		'beneficiary_3_relationship' => 'parentesco del beneficiario',
		'beneficiary_4_relationship' => 'parentesco del beneficiario',
		'beneficiary_5_relationship' => 'parentesco del beneficiario',
		'beneficiary_6_relationship' => 'parentesco del beneficiario',
		'beneficiary_7_relationship' => 'parentesco del beneficiario',
		'beneficiary_8_relationship' => 'parentesco del beneficiario',
		'beneficiary_9_relationship' => 'parentesco del beneficiario',
		'beneficiary_10_relationship' => 'parentesco del beneficiario',
		'beneficiary_1_id_type' => 'tipo de identificación del beneficiario',
		'beneficiary_2_id_type' => 'tipo de identificación del beneficiario',
		'beneficiary_3_id_type' => 'tipo de identificación del beneficiario',
		'beneficiary_4_id_type' => 'tipo de identificación del beneficiario',
		'beneficiary_5_id_type' => 'tipo de identificación del beneficiario',
		'beneficiary_6_id_type' => 'tipo de identificación del beneficiario',
		'beneficiary_7_id_type' => 'tipo de identificación del beneficiario',
		'beneficiary_8_id_type' => 'tipo de identificación del beneficiario',
		'beneficiary_9_id_type' => 'tipo de identificación del beneficiario',
		'beneficiary_10_id_type' => 'tipo de identificación del beneficiario',
		'beneficiary_1_identity' => 'identificación del beneficiario',
		'beneficiary_2_identity' => 'identificación del beneficiario',
		'beneficiary_3_identity' => 'identificación del beneficiario',
		'beneficiary_4_identity' => 'identificación del beneficiario',
		'beneficiary_5_identity' => 'identificación del beneficiario',
		'beneficiary_6_identity' => 'identificación del beneficiario',
		'beneficiary_7_identity' => 'identificación del beneficiario',
		'beneficiary_8_identity' => 'identificación del beneficiario',
		'beneficiary_9_identity' => 'identificación del beneficiario',
		'beneficiary_10_identity' => 'identificación del beneficiario',
		'foreign_currency' => 'operaciones en moneda extranjera',
		'wire_transfers' => 'transferencias internacionales',
		'currency_purchase' => 'compra/venta de divisas',
		'checks_in_dollars' => 'emisión de giros dólar (Cheques en dólares)',
		'remittances' => 'remesas familiares',
		'typical_monthly_income' => 'monto mensual típico estimado',
		'typical_monthly_currency' => 'moneda',
		'probable_monthly_income' => 'monto mensual esperado eventual',
		'probable_monthly_currency' => 'moneda',
		'received_transfer_country' => 'transferencia recibidas',
		'received_transfer_sender' => 'remitente',
		'received_transfer_reason' => 'motivo',
		'sent_transfer_country' => 'transferencias enviadas',
		'sent_transfer_sender' => 'beneficiario',
		'sent_transfer_reason' => 'motivo',
		'remittance_country' => 'país de procedencia',
		'remittance_sender' => 'remitente',
		'remittance_sender_relationship' => 'parentesco',
		'remittance_reason' => 'motivo',
        'spouse_identity' => 'identificacion de esposo(a)',
        'address_1' => 'direccion',
        'address_2' => 'direccion',
        'address_3' => 'direccion',
        'employer_address_1' => 'direccion empleado',
        'employer_address_2' => 'direccion empleado',
        'employer_address_3' => 'direccion empleado',


        // Auto
        'motor_numbers' => 'numero de motor',
        'vin_numbers' => 'numero de VIN',
        'chassis_numbers' => 'numero de chasis',
        'auto_displacement' => 'cilindraje de vehículo',
        'auto_executive' => 'ejecutivo de auto',
        'concessionaire' => 'concesionaria',
        'seller' => 'vendedor',
        'type_auto' => 'tipo de vehículo',
        'status_auto' => 'estatus de vehículo',
        'year_auto' => 'año del vehículo',
        'auto_brand' => 'marca de vehículo',
        'auto_model' => 'modelo del vehículo',
        'sale_price' => 'precio de vehículo',
        'premium' => 'prima de vehículo',
        'appraisal' => 'precio de avalúo',
        'financing_term' => 'plazo de financiamiento',
        'closing_costs' => 'gatos de cierre',
        'grace_period' => 'periodo de gracia',

		// Insurance Safe Family
		'height' => 'altura',
		'weight' => 'peso',

		'first_md_1' => 'pregunta',
		'first_md_2' => 'pregunta',
		'first_md_3' => 'pregunta',
		'first_md_4' => 'pregunta',
		'first_md_5' => 'pregunta',

		'second_md_1' => 'pregunta',
		'second_md_1_diagnostic' => 'diagnóstico',
		'second_md_1_date' => 'fecha',
		'second_md_1_doctor' => 'doctor',
		'second_md_1_hospital' => 'hospital',
		'second_md_1_other_hospital' => 'otro hospital',

		'second_md_2' => 'pregunta',
		'second_md_2_diagnostic' => 'diagnóstico',
		'second_md_2_date' => 'fecha',
		'second_md_2_doctor' => 'doctor',
		'second_md_2_hospital' => 'hospital',
		'second_md_2_other_hospital' => 'otro hospital',

		'second_md_3' => 'pregunta',
		'second_md_3_diagnostic' => 'diagnóstico',
		'second_md_3_date' => 'fecha',
		'second_md_3_doctor' => 'doctor',
		'second_md_3_hospital' => 'hospital',
		'second_md_3_other_hospital' => 'otro hospital',

		'second_md_4' => 'pregunta',
		'second_md_4_diagnostic' => 'diagnóstico',
		'second_md_4_date' => 'fecha',
		'second_md_4_doctor' => 'doctor',
		'second_md_4_hospital' => 'hospital',
		'second_md_4_other_hospital' => 'otro hospital',

		'second_md_5' => 'pregunta',
		'second_md_5_diagnostic' => 'diagnóstico',
		'second_md_5_date' => 'fecha',
		'second_md_5_doctor' => 'doctor',
		'second_md_5_hospital' => 'hospital',
		'second_md_5_other_hospital' => 'otro hospital',

		'amount' => 'plan',
		'payment_type' => 'frecuencia del pago',
		'debit_option' => 'forma de pago',

		'beneficiary_1_participation' => 'participación',
		'beneficiary_2_participation' => 'participación',
		'beneficiary_3_participation' => 'participación',
		'beneficiary_4_participation' => 'participación',
		'beneficiary_5_participation' => 'participación',

		'confirmation' => 'confirmación',
		'from' => 'desde',
		'to' => 'hasta',
	],

];