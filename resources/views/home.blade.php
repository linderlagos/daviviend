@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<div class="container">
    <div class="row justify-content-center"  style="opacity: 0.8">
        <div class="col-md-7">
            <div class="card">
                <div  class="card-header" style=" background-image: url('images/tecnologia1.jpg'); color: #000000; opacity: 0.9"><img src="images/motive_right.png" align="left;" style=" margin-right:10px;">{{ __('Generar Serial') }}</div>

                <div class="card-body">
                   
                    
                <form method="POST" action="{{ route('serial') }}"> 
                    @csrf

                        <div class="form-group row" style="color: #000000; opacity: 3.9">
                            <label for="email" style="color: #000000; opacity: 3.9" class="col-md-4 col-form-label text-md-right">{{ __('Numero Identidad') }}</label>

                            <div class="col-md-6" style="color: #000000; opacity: 3.9">
                                <input id="identidad" style="color: #000000; opacity: 3.9" type="text" minlength="13" maxlength="13" required pattern="[0-9]+"  class="form-control" value="{{ old('email') }}" name="identidad" required >


                            </div>
                        </div>

                        @if($codMessage == "")
                        @elseif($codMessage == "0")
                        @else
                            <div class="form-group row">
                           
                                <label for="message" style="color: #000000; opacity: 3.9" class="col-md-4 col-form-label text-md-right">{{ __('') }}</label>
                                <label for="menssge" style="color: #000000; opacity: 3.9" name="message" style="color: red;" class="col-md-4 col-form-label text-md-left">{{ $message }}</label>
                            
                            </div>
                        @endif

                        <div class="form-group row">
                       
                            <label for="password" style="color: #000000; opacity: 3.9" class="col-md-4 col-form-label text-md-right">{{ __('Serial') }}</label>
                            <label for="" name="serial" style= "color: gree; opacity: 3.9" class="col-md-4 col-form-label text-md-left">{{ $serial }}</label>

                        </div>
                        
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-3 offset-md-4">
                                
                            
                            
                               
                                    <button type="submit" class="btn btn-primary" style="color: #FFFFFF; opacity: 3.9">
                                        {{ __('Generar Serial') }}
                                    </button>
                                

                            </div>

                            @if ( $codMessage == "0" )
                            
                                <a href=" {{ route('autenticar', ['identidad' => $identidad]) }}" class="btn btn-success">Verificar Codigos</a>
                               <!-- <form method="GET" onclick="{{ route('autenticar') }}">   
                                    @csrf
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Verificar Codigos') }}
                                    </button>
                                </form>-->
                            
                            @endif
                            
                        </div>
                    
                
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
