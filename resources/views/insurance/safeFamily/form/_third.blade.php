@include('modules._progress', [
    'steps' => 5,
    'step' => 3,
    'title' => 'Selección del plan'
])

<form class="form-horizontal" role="form" method="POST" action="{{ route('insurance.safe.family.fourth') }}" id="form">
    {!! csrf_field() !!}

    <div class="row">
        <div class="col-12 form-group">
            <h2>Planes Familia Segura</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-hand-holding-usd"></i>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'amount',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['amount', 'code']),
                    'placeholder' => 'Plan a seleccionar...',
                    'options' => $plans,
                    'required' => true,
                    'enable' => true
                ])
            </div>
            <div class="input-bottom-tooltip">
                Se recomienda que el cliente tome el plan de <span class="no-break"><b>{{ $flow->product_information['recommended_plan']['value'] }}</b></span> por su capacidad de pago.
            </div>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('payment_type') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'payment_type',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['payment_type', 'code']),
                    'placeholder' => 'Frecuencia de pago...',
                    'options' => $lists['paymentTypes'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
            <div class="input-bottom-tooltip" id="premium">
                Su prima <b><span id="premium_concurrency"></span></b> sería de <b><span id="premium_amount" class="no-break"></span></b>.
            </div>
        </div>
    </div>

    <div class="row align-items-baseline">
        <div class="col-12 form-group">
            <h2>Forma de pago</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-money-bill"></i>
        </div>

        <div class="col-6">
            <div>
                <span class="questions">Seleccione el producto de donde se debitará la prima:</span>
            </div>
        </div>

        <div class="col-4">
            <div class="form-group{{ $errors->has('debit_option') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'debit_option',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['debit', 'default']),
                    'placeholder' => 'Seleccione...',
                    'options' => $products,
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <button type="submit" class="btn btn-davivienda-red btn-loading" id="submit">
                SIGUIENTE
            </button>
        </div>
    </div>
</form>