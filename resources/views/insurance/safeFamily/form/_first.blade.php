@include('modules._progress', [
    'steps' => 5,
    'step' => 1,
    'title' => 'Información general'
])

<form class="form-horizontal" role="form" method="POST" action="{{ route('insurance.safe.family.second') }}" id="form">
    {!! csrf_field() !!}

    <div class="row">
        <div class="col-12 form-group">
            <h2>Información personal</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user"></i>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('height') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'height',
                    'length' => 4,
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, 'height'),
                    'placeholder' => 'Altura en metros',
                    'label' => 'Altura en metros',
                    'classes'  => 'format-amount',
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'weight',
                    'length' => 5,
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, 'weight'),
                    'placeholder' => 'Peso en libras',
                    'label' => 'Peso en libras',
                    'classes'  => 'format-amount',
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline">
        <div class="col-12 form-group">
            <h2>Cuestionario médico</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user-md"></i>
        </div>

        <div class="col-7">
            <div>
                <span class="questions">¿Padece de Diábetes?</span>
            </div>
        </div>
        <div class="col-3">
            <div class="form-group{{ $errors->has('first_md_1') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'first_md_1',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['first_medical_questionary', '1', 'question', 'code']),
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline">
        <div class="col-2">
        </div>

        <div class="col-7">
            <div>
                <span class="questions">¿Padece o ha padecido de Cáncer?</span>
            </div>
        </div>
        <div class="col-3">
            <div class="form-group{{ $errors->has('first_md_2') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'first_md_2',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['first_medical_questionary', '2', 'question', 'code']),
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline">
        <div class="col-2">
        </div>

        <div class="col-7">
            <div>
                <span class="questions">¿Padece de insuficiencia renal?</span>
            </div>
        </div>
        <div class="col-3">
            <div class="form-group{{ $errors->has('first_md_3') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'first_md_3',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['first_medical_questionary', '3', 'question', 'code']),
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline">
        <div class="col-2">
        </div>

        <div class="col-7">
            <div>
                <span class="questions">¿Padece de Cardiopatías?</span>
            </div>
        </div>
        <div class="col-3">
            <div class="form-group{{ $errors->has('first_md_4') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'first_md_4',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['first_medical_questionary', '4', 'question', 'code']),
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row align-items-baseline">
        <div class="col-2">
        </div>

        <div class="col-7">
            <div>
                <span class="questions">¿Padece de SIDA?</span>
            </div>
        </div>
        <div class="col-3">
            <div class="form-group{{ $errors->has('first_md_5') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'first_md_5',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['first_medical_questionary', '5', 'question', 'code']),
                    'placeholder' => 'Seleccione...',
                    'options' => $lists['confirmation'],
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <button type="submit" class="btn btn-davivienda-red btn-loading" id="submit">
                SIGUIENTE
            </button>
        </div>
    </div>
</form>