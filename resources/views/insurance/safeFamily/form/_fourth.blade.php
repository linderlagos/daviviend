@include('modules._progress', [
    'steps' => 5,
    'step' => 4,
    'title' => 'Beneficiarios'
])

<form class="form-horizontal" role="form" method="POST" action="{{ route('insurance.safe.family.fifth') }}" id="form">
    {!! csrf_field() !!}

    <div class="form-group">
        <h2>Se pueden agregar hasta un máximo de 5 beneficiarios</h2>
    </div>

    @for($i = 1; $i <= 5; $i++)
        <div id="{{ 'beneficiary_' . $i }}">
            <div class="row">
                {{--<div class="col-2"></div>--}}
                <div class="col-12 text-right">
                    <h3>Beneficiario {{ $i }}</h3>
                </div>
                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-users"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('beneficiary_' . $i . '_name') ? ' has-error' : '' }}">
                        @if($i === 1)
                            @include('modules._textInput', [
                                'name' => 'beneficiary_' . $i . '_name',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['beneficiaries', $i, 'name']),
                                'placeholder' => 'Nombre del beneficiario',
                                'label' => 'Nombre del beneficiario',
                                'length' => '40',
                                'enable' => true,
                                'required' => true
                            ])
                        @else
                            @include('modules._textInput', [
                                'name' => 'beneficiary_' . $i . '_name',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['beneficiaries', $i, 'name']),
                                'placeholder' => 'Nombre del beneficiario',
                                'label' => 'Nombre del beneficiario',
                                'length' => '40',
                                'enable' => true
                            ])
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('beneficiary_' . $i . '_relationship') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'beneficiary_' . $i . '_relationship',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['beneficiaries', $i, 'relationship']),
                            'classes' => 'davivienda-select',
                            'placeholder' => 'Seleccione el parentesco...',
                            'options' => $lists['beneficiaryOptions'],
                            'enable' => true,
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('beneficiary_' . $i . '_birth') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'beneficiary_' . $i . '_birth',
                            'resource' => $flow,
                            'json' => get_json_date(get_json($flow->customer_information, ['beneficiaries', $i, 'birth']), 'd/m/Y'),
                            'placeholder' => 'Fecha de nacimiento DD/MM/AAAA',
                            'label' => 'Fecha de nacimiento',
                            'length' => '10',
                            'classes' => 'date',
                            'autocomplete' => 'No'
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('beneficiary_' . $i . '_participation') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'type' => 'number',
                            'max' => 100,
                            'min' => '',
                            'name' => 'beneficiary_' . $i . '_participation',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['beneficiaries', $i, 'participation']),
                            'classes' => 'sum-participation',
                            'placeholder' => 'Porcentaje de participación',
                            'label' => 'Porcentaje de participación',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            @if($i !== 5)
                <div class="row justify-content-end">
                    <div class="col-2 text-right" id="add_beneficiary_{{ $i }}_btn">
                        <a onclick="addBeneficiary({{ $i + 1 }})" href="javascript:void(0);" class="btn davivienda-add-field">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                </div>
            @endif
        </div>
    @endfor

    <div class="row text-center">
        <div class="col-12">
            <div class="help-block">
                <strong><span id="total-participation"></span></strong>
            </div>
            <button type="submit" class="btn btn-davivienda-red btn-loading" id="submit">
                SIGUIENTE
            </button>
        </div>
    </div>
</form>