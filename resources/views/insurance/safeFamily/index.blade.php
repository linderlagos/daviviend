@extends('layouts.form')
{{-- 
@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/insurance_safe_family.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Formulario de Solicitud de Familia Segura</h1>
                </div>
                <div class="banner-description">
                    @if(checkStep($flow, ['1', '2', '3', '4', '5']))
                        <p>Continúe completando la información del cliente {{ get_json($flow->customer_information, ['name', 'fullname']) }} con ID No. {{ $flow->customer->identifier }} y póliza No. {{ get_json($flow->product_information, ['policy', 'working']) }}</p>
                    @else
                        <p>Complete la información del cliente para terminar el proceso de solicitud del seguro Familia Segura</p>
                    @endif
                </div>
            </div>
        </div>
        <hr class="cintaGris">
    </div>
@stop 





@section('back')
    @if(!checkStep($flow, ['1', '2']))
        <a href="{{ route('index') }}" class="btn-back">
            Inicio
        </a>
    @endif
@stop --}}

@section('content')
        <?php    
            //echo "deniiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiis ". json_encode($jsonPlans);   
        ?>
    <index-safefamily-component 
        :lists="{{ json_encode($lists) }}" 
        :flow="{{ json_encode($flow) }}" 
        :plans="{{ json_encode($plans) }}" 
        :products="{{ json_encode($products) }}"
        :jsonplans="{{ json_encode($jsonPlans) }}" 
        :parent="{{ json_encode($parent) }}" 
    ></index-safefamily-component>
@endsection 

{{-- @section('scripts')
    @include('partials._scripts')
    @include('insurance.safeFamily.partials._scripts')
@stop --}}
