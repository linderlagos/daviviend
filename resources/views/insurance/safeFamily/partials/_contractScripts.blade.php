<script type="text/javascript" src="{{ asset('plugins/dropzone/dropzone.js') }}"></script>

<script>
    Dropzone.autoDiscover = false;
    $(document).ready(function(){
        $(".user-dropzone").dropzone({
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10,
            maxFiles: 1,
            acceptedFiles: 'application/pdf'
        });
    });

    $(document).ready(function(){
        let myDropzone = new Dropzone("#supportForm", {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10,
            maxFiles: 20,
            // autoQueue: false,
            acceptedFiles: 'application/pdf'
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            // if (formData.description) {
            //     swal({
            //         input: 'textarea',
            //         inputPlaceholder: 'Type your message here...',
            //         showCancelButton: true
            //     }).then((result) => {
            //         if (result.value) {
            //             formData.append('description', result.value);
            //             myDropzone.continue();
            //         }
            //     });
            // }

            let description = prompt("Ingrese una descripción", "Descripción del archivo");

            if (description != null) {
                formData.append('description', description);
            } else {
                myDropzone.cancelUpload(file);
            }

            // const {value: text} = swal({
            //     input: 'textarea',
            //     inputPlaceholder: 'Type your message here...',
            //     showCancelButton: true
            // }).then((result) => {
            //     if (result.value) {
            //         formData.append('description', result.value);
            //
            //         console.log(formData);
            //         myDropzone.enqueue(file);
            //     }
            // });

            // if (text) {
            //     console.log(text);
            //     formData.append('description', text);
            //     myDropzone.enqueue(file);
            // }
        });

    });
</script>

<script type="text/javascript" src="{{ asset('plugins/datatables/datatables.js') }}"></script>
<script>
    $(document).ready( function () {
        $('#support-documents').DataTable({
            // "info":     false,
            responsive: false,
            "lengthChange": false,
            "pageLength": 5,
            "pagingType": "full_numbers",
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    } );
</script>