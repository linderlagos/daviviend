@if(checkStep($flow, '2'))
    @for($i = 1; $i <= 5; $i++)
        <script>
            $(document).ready(function() {
                $('.show_if_second_md_{{ $i }}').hide();
                $('.show_if_second_md_{{ $i }}_other_hospital').hide();

                if ($('#second_md_{{ $i }}_hospital option:selected').val() === "Otro") {
                    $('.show_if_second_md_{{ $i }}_other_hospital').fadeIn();
                }

                $('#second_md_{{ $i }}_hospital').change(function () {
                    if ($('#second_md_{{ $i }}_hospital option:selected').val() === "Otro") {
                        $('.show_if_second_md_{{ $i }}_other_hospital').fadeIn();
                    } else {
                        $('.show_if_second_md_{{ $i }}_other_hospital').fadeOut();
                    }
                });

                if ($('#second_md_{{ $i }} option:selected').val() === "S") {
                    $('.show_if_second_md_{{ $i }}').fadeIn();
                }

                $('#second_md_{{ $i }}').change(function () {

                    if ($('#second_md_{{ $i }} option:selected').val() === "S") {
                        $('.show_if_second_md_{{ $i }}').fadeIn();
                        if ($('#second_md_{{ $i }}_hospital option:selected').val() === "Otro") {
                            $('.show_if_second_md_{{ $i }}_other_hospital').fadeIn();
                        }
                    } else {
                        $('.show_if_second_md_{{ $i }}').fadeOut();
                        $('.show_if_second_md_{{ $i }}_other_hospital').fadeOut();
                    }
                });
            });
        </script>
    @endfor

    <script>
        $(document).ready(function() {
            let other = $('.show_if_second_md_5_other_accident');
            let sequel = $('#second_md_5_sequel_container');
            let date = $('#second_md_5_date_container');

            other.hide();

            if ($('#second_md_5_accident option:selected').val() === "OTROS") {
                other.fadeIn();
                sequel.removeClass('col-4');
                sequel.addClass('col-5');

                date.removeClass('col-2');
                date.addClass('col-5');
            }

            $('#second_md_5_accident').change(function () {
                if ($('#second_md_5_accident option:selected').val() === "OTROS") {
                    other.fadeIn();
                    sequel.removeClass('col-4');
                    sequel.addClass('col-5');

                    date.removeClass('col-2');
                    date.addClass('col-5');
                } else {
                    other.fadeOut();
                    sequel.removeClass('col-5');
                    sequel.addClass('col-4');

                    date.removeClass('col-5');
                    date.addClass('col-2');
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            let disease = $('#second_md_1_disease_container');
            let other = $('.show_if_second_md_1_other_disease');
            // let diagnostic = $('#second_md_1_diagnostic_container');

            other.hide();

            if ($('#second_md_1_disease option:selected').val() === "Otro") {
                other.fadeIn();
                disease.removeClass('col-10');
                disease.addClass('col-4');
            }

            $('#second_md_1_disease').change(function () {
                if ($('#second_md_1_disease option:selected').val() === "Otro") {
                    other.fadeIn();
                    disease.removeClass('col-10');
                    disease.addClass('col-4');
                } else {
                    other.fadeOut();
                    disease.removeClass('col-4');
                    disease.addClass('col-10');
                }
            });
        });
    </script>
@endif

@if(checkStep($flow, '3'))
    <script>
        $('#premium').hide();

        let plans = {!! $jsonPlans !!};

        let amount = $('#amount option:selected').val();

        if ($('#payment_type option:selected').val() === 'M' && amount.length > 0) {
            console.log(plans);

            monthlyPremium(plans[amount]['Mensual'])
        } else if ($('#payment_type option:selected').val() === 'A' && amount.length > 0) {
            annualPremium(plans[amount]['Anual'])
        } else {
            $('#premium').hide();
        }

        $('#payment_type').change(function () {
            let amount = $('#amount option:selected').val();

            if ($('#payment_type option:selected').val() === 'M' && amount.length > 0) {
                monthlyPremium(plans[amount]['Mensual'])
            }  else if ($('#payment_type option:selected').val() === 'A' && amount.length > 0) {
                annualPremium(plans[amount]['Anual'])
            } else {
                $('#premium').hide();
            }
        });

        $('#amount').change(function () {
            let amount = $('#amount option:selected').val();

            let type = $('#payment_type option:selected').val();

            if (type === 'M' && amount.length > 0) {
                monthlyPremium(plans[amount]['Mensual'])
            }  else if (type === 'A' && amount.length > 0) {
                annualPremium(plans[amount]['Anual'])
            } else {
                $('#premium').hide();
            }
        });

        function monthlyPremium(amount)
        {
            $('#premium_concurrency').html('mensual');
            $('#premium_amount').html(amount);

            $('#premium').fadeIn();
        }

        function annualPremium(amount)
        {
            $('#premium_concurrency').html('anual');
            $('#premium_amount').html(amount);

            $('#premium').fadeIn();
        }
    </script>
@endif

@if(checkStep($flow, '4'))
    <script>
        $(document).ready(function() {
            for ($i = 2; $i <= 5; $i++)
            {
                let value = $('#beneficiary_' + $i + '_name').val();

                if (value.length === 0) {
                    if ($('#beneficiary_' + $i).find('.has-error').length !== 0)
                    {
                        $('#beneficiary_' + $i).show();
                    } else {
                        $('#beneficiary_' + $i).hide();
                    }
                }
            }
        });

        function addBeneficiary(i)
        {
            let previous = i - 1;

            $('#beneficiary_' + i).fadeIn();
            $('#add_beneficiary_' + previous + '_btn').html(
                '<a onclick="removeBeneficiary(' + i + ')" href="javascript:void(0);" class="btn davivienda-remove-field">' +
                '<i class="fas fa-minus"></i>' +
                '</a>');
        }

        function removeBeneficiary(i)
        {
            let previous = i - 1;
            let next = i;

            $('#beneficiary_' + i).fadeOut();
            $('#beneficiary_' + i + '_name').val('');
            $('#beneficiary_' + i + '_relationship').val('');
            $('#beneficiary_' + i + '_birth').val('');
            $('#beneficiary_' + i + '_participation').val('');
            $('#add_beneficiary_' + previous + '_btn').html(
                '<a onclick="addBeneficiary(' + i + ')" href="javascript:void(0);" class="btn davivienda-add-field">' +
                '<i class="fas fa-plus"></i>' +
                '</a>');

            sumInputs('.sum-participation');
        }

        for (i = 1; i <= 5; i++) {
            $('#beneficiary_' + i + '_participation').keyup(function () {
                sumInputs('.sum-participation');
            });
        }

        function sumInputs(classes) {
            let sum = 0;

            $(classes).each(function(){
                sum += +$(this).val();
            }); 

            return checkSum(sum);
        }

        function checkSum(sum) {
            let form = $('#form');

            if (sum > 100) {
                $("#total-participation").html('Revise el porcentaje de participación de los beneficiarios ya que excede el 100% por ' + (sum - 100) + '%');

                form.find(".btn-loading").html('<i class="fas fa-exclamation"></i> Participación pendiente...');
                form.find(".btn-loading").prop('disabled',true);
            } else if (sum < 100) {
                $("#total-participation").html('Todavía le falta asignar ' + (100 - sum) + '% de participación');

                form.find(".btn-loading").html('<i class="fas fa-exclamation"></i> Participación pendiente...');
                form.find(".btn-loading").prop('disabled',true);
            } else {
                $("#total-participation").html('');

                form.find(".btn-loading").html('Siguiente');
                form.find(".btn-loading").prop('disabled',false);
            }
        }
    </script>
@endif