<table class="fluid-4-column-table">
    <tr>
        <td>{{ $question }}</td>
        <td>
            {{ get_json($print->product_information, ['second_medical_questionary', $i, 'question']) }}
        </td>
    </tr>

    @if (get_json($print->product_information, ['second_medical_questionary', $i, 'question']) === 'S')
        <tr>

            @if ($i === 5)
                <td class="column-title">Tipo de accidente</td>

                <td>
                    @if (get_json($print->product_information, ['second_medical_questionary', $i, 'accident']) === 'OTROS')
                        {{ get_json($print->product_information, ['second_medical_questionary', $i, 'other_accident']) }}
                    @else
                        {{ get_json($print->product_information, ['second_medical_questionary', $i, 'accident']) }}
                    @endif
                </td>
                <td class="column-title">Secuelas</td>

                <td>
                    {{ get_json($print->product_information, ['second_medical_questionary', $i, 'sequel']) }}
                </td>
            @else
                <td class="column-title">
                    {{ $i === 4 ? 'Descripción' : 'Diagnóstico' }}
                </td>

                <td>
                    @if ($i === 1)
                        @if (get_json($print->product_information, ['second_medical_questionary', $i, 'disease']) === 'Otro')
                            {{ get_json($print->product_information, ['second_medical_questionary', $i, 'diagnostic']) }}
                        @else
                            {{ get_json($print->product_information, ['second_medical_questionary', $i, 'disease']) }}
                        @endif
                    @else
                        {{ get_json($print->product_information, ['second_medical_questionary', $i, 'diagnostic']) }}
                    @endif
                </td>
            @endif
            <td class="column-title">Fecha</td>
            <td>
                @if (get_json($print->product_information, ['second_medical_questionary', $i, 'date']))
                    {{ date('d/m/y', get_json($print->product_information, ['second_medical_questionary', $i, 'date'])) }}
                @endif
            </td>
        </tr>
        <tr>
            <td class="column-title">Médico tratante</td>
            <td>
                {{ get_json($print->product_information, ['second_medical_questionary', $i, 'doctor']) }}
            </td>
            <td class="column-title">Hospital o clínica</td>
            <td>
                @if (get_json($print->product_information, ['second_medical_questionary', $i, 'hospital']) === 'Otro')
                    {{ get_json($print->product_information, ['second_medical_questionary', $i, 'other_hospital']) }}
                @else
                    {{ get_json($print->product_information, ['second_medical_questionary', $i, 'hospital']) }}
                @endif
            </td>
        </tr>
    @endif
</table>