<div class="footer">
    <table>
        <tr>
            <td>Póliza Familia Segura</td>
            <td class="text-right">Página {{ $i }} de 11</td>
        </tr>
        <tr>
            <td>No. {{ $print->identifier }}</td>
            {{--<td class="text-right">{{ date('d/m/Y H:i:s', $data['date']) }}</td>--}}
            <td class="text-right">{{ $print->created_at->format('d/m/Y') }}</td>
        </tr>
    </table>
</div>