<!DOCTYPE html>
<html lang="es">

<head>
    @include('insurance.safeFamily.printable.1.partials._meta', ['title' => 'Póliza de Seguro Familia'])
    <link rel="stylesheet" href="{{ asset('css/print/insurance/2/safeFamily.css') }}">
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="letter">

<!-- Each sheet element should have the class "sheet" -->
<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
<section class="sheet padding-10mm">
    <div class="cover">
        @include('insurance.safeFamily.printable.1.partials._header')

        <p>Contratante: Banco DAVIVIENDA S.A., de ahora en adelante denominado DAVIVIENDA Solicito mi inclusión como Asegurado de la Póliza de Seguro de Vida de Grupo suscrita por Davivienda con los amparos y valores asegurados descritos a continuación:</p>

        <table>
            <tr>
                <td class="column-title">Póliza:</td>
                <td>{{ $print->identifier }}</td>
                <td class="column-title">Categoría:</td>
                <td>{{ get_json($print->product_information, 'category') }}</td>
                <td class="column-title">Fecha:</td>
                <td>{{ $print->created_at->format('d/m/y h:i:s') }}</td>
            </tr>
            <tr>
                <td class="column-title">Ramo:</td>
                <td>{{ get_json($print->product_information, 'product_branch') }} FAMILIA SEGURA</td>
                <td class="column-title">Canal de Venta:</td>
                <td>Asistente Davivienda</td>
                <td class="column-title">Empleado:</td>
                <td>{{ get_json($print->user_information, ['agent', 'number']) }}</td>
            </tr>
            <tr>
                <td class="column-title">C. Costo:</td>
                <td>{{ get_json($print->user_information, ['branch', 'value']) }}</td>
                <td class="column-title">Cliente:</td>
                <td>{{ get_json($print->customer_information, ['cif']) }}</td>
                <td class="column-title">Gerente:</td>
                <td>{{ get_json($print->user_information, ['manager', 'value']) }}</td>
            </tr>
            <tr>
                <td class="column-title">Ocupación:</td>
                <td>{{ get_json($print->customer_information, 'job') }}</td>
                <td class="column-title">Plan:</td>
                <td>{{ get_json($print->product_information, ['amount', 'code']) }} {{ get_json($print->product_information, ['amount', 'value']) }}</td>
            </tr>
            <tr>
                <td class="column-title">Vig. Desde:</td>
                <td>{{ date('d/m/Y', get_json($print->product_information, ['policy', 'since'])) }}</td>
                <td class="column-title">Vig. Hasta:</td>
                <td>{{ date('d/m/Y', get_json($print->product_information, ['policy', 'until'])) }}</td>
                <td class="column-title">F/Emisión:</td>
                <td>{{ date('d/m/Y', get_json($print->product_information, ['policy', 'issue'])) }}</td>
            </tr>
            <tr>
                <td class="column-title">Forma Pago:</td>
                <td>{{ get_json($print->product_information, ['payment_type', 'value']) }}</td>
                <td class="column-title">Cuenta:</td>
                <td>{{ get_json($print->product_information, ['account', 'value']) }}  {{ get_json($print->product_information, ['account', 'type']) }}</td>
                <td class="column-title">Cuota:</td>
                <td>{{ $premium }}</td>
            </tr>
        </table>

        <div class="separator"></div>

        <h2>DATOS DEL ASEGURADO</h2>

        <table>
            <tr>
                <td class="column-title">Asegurado:</td>
                <td>{{ get_json($print->customer_information, ['name']) }}</td>
                <td class="column-title">Identidad:</td>
                <td>{{ $print->customer->identifier }}</td>
            </tr>
            <tr>
                <td class="column-title">Dirección:</td>
                <td>{{ get_json($print->customer_information, ['address']) }}</td>
                <td class="column-title">Estado Civil:</td>
                <td>{{ get_json($print->customer_information, 'marital_status') }}</td>
                <td class="column-title">Sexo:</td>
                <td>{{ get_json($print->customer_information, 'gender') }}</td>
            </tr>
            <tr>
                <td class="column-title"></td>
                <td></td>
                <td class="column-title">F/Nacimiento:</td>
                <td>{{ date('d/m/Y', get_json($print->customer_information, 'birth')) }}</td>
                <td class="column-title">Edad:</td>
                <td>{{ $print->created_at->format('Y') - date('Y', get_json($print->customer_information, 'birth')) }}</td>
            </tr>
            <tr>
                <td class="column-title">Ciudad:</td>
                <td>{{ get_json($print->customer_information, 'city') }}</td>
                <td class="column-title">Teléfono:</td>
                <td>{{ get_json($print->customer_information, 'phone') }}</td>
                <td class="column-title">Fax:</td>
                <td>{{ get_json($print->customer_information, 'Fax', true) }}</td>
            </tr>
        </table>

        <div class="separator"></div>

        <table>
            <tr>
                <th class="text-left">COBERTURAS</th>
                <th class="text-right">VALOR ASEGURADO</th>
                <th class="text-right">PRIMA ANUAL</th>
            </tr>

            <tr>
                <td>Vida</td>
                <td class="text-right">L {{ number_format(get_json($print->product_information, ['coverage', 1, 'value']), 2) }}</td>
                <td class="text-right">{{ get_json($print->product_information, ['plans'])[get_json($print->product_information, ['amount', 'code'])]['Anual'] }}</td>
            </tr>
            <tr>
                <td>Incapacidad total y permanente</td>
                <td class="text-right">L {{ number_format(get_json($print->product_information, ['coverage', 2, 'value']), 2) }}</td>
                <td></td>
            </tr>
            <tr>
                <td>Enfermedades graves</td>
                <td class="text-right">L {{ number_format(get_json($print->product_information, ['coverage', 3, 'value']), 2) }}</td>
                <td></td>
            </tr>
            <tr>
                <td>Gastos Funebres</td>
                <td class="text-right">L {{ number_format(get_json($print->product_information, ['coverage', 4, 'value']), 2) }}</td>
                <td></td>
            </tr>
            <tr>
                <td>Renta diaria por hospitalización</td>
                <td class="text-right">L {{ number_format(get_json($print->product_information, ['coverage', 5, 'value']), 2) }}</td>
                <td></td>
            </tr>
        </table>

        <div class="separator"></div>

        <table>
            <tr>
                <th>BENEFICIARIO</th>
                <th>% PARTICIPACIÓN</th>
                <th>PARENTESCO</th>
            </tr>

            @foreach($beneficiaries as $beneficiary)
                @if ($beneficiary['name'])
                    <tr>
                        <td>{{ $beneficiary['name'] }}</td>
                        <td class="text-center">{{ $beneficiary['participation'] }} %</td>
                        <td class="text-center">{{ $beneficiary['relationship_name'] }}</td>
                    </tr>
                @endif
            @endforeach
        </table>

        <div class="separator"></div>

        <p>Débito a la cuenta: {{ get_json($print->product_information, ['debit', 'value']) }} {{ get_json($print->product_information, ['debit', 'type']) }} por {{ $premium }} en concepto de pago de prima.</p>

        <h2>CUESTIONARIO MÉDICO</h2>

        <table class="fluid-4-column-table">
            <tr>
                <td class="column-title">¿Padece de diábetes?</td>
                <td class="text-left">{{ get_json($print->product_information, ['first_medical_questionary', 1, 'question']) }}</td>
                <td class="column-title">Estatura (Mts)</td>
                <td class="text-left">{{ get_json($print->customer_information, 'height') }}</td>
            </tr>
            <tr>
                <td class="column-title">¿Padece o ha padecido de Cáncer?</td>
                <td class="text-left">{{ get_json($print->product_information, ['first_medical_questionary', 2, 'question']) }}</td>
                <td class="column-title">Peso en libras</td>
                <td class="text-left">{{ get_json($print->customer_information, 'weight') }}</td>
            </tr>
            <tr>
                <td class="column-title">¿Padece de insuficiencia renal?</td>
                <td class="text-left">{{ get_json($print->product_information, ['first_medical_questionary', 3, 'question']) }}</td>
                <td class="column-title">Peso en kilos</td>
                <td class="text-left">{{ number_format(get_json($print->customer_information, 'weight') / 2.205, 2) }}</td>
            </tr>
            <tr>
                <td class="column-title">¿Padece de Cardiopatías?</td>
                <td class="text-left">{{ get_json($print->product_information, ['first_medical_questionary', 4, 'question']) }}</td>
                <td class="column-title">Masa corporal</td>
                <td class="text-left">{{ get_json($print->customer_information, 'imc') }}</td>
            </tr>
            <tr>
                <td class="column-title">¿Padece de SIDA?</td>
                <td class="text-left">{{ get_json($print->product_information, ['first_medical_questionary', 5, 'question']) }}</td>
                <td class="column-title"></td>
                <td class="text-left"></td>
            </tr>
        </table>

        <h2>INFORMACION MÉDICA</h2>


        @include('insurance.safeFamily.printable.1.partials._secondMd', [
            'question' => '¿Ha padecido de otra enfermedad no mencionada anteriormente?',
            'i' => 1
        ])

        @include('insurance.safeFamily.printable.1.partials._secondMd', [
            'question' => '¿Ha sido Intervenido quirúrgicamente?',
            'i' => 2
        ])

    </div>

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 1])
</section>

<section class="sheet padding-10mm">
    <div class="cover">
        @include('insurance.safeFamily.printable.1.partials._header')

        @include('insurance.safeFamily.printable.1.partials._secondMd', [
            'question' => '¿Se encuentra en tratamiento médico actualmente?',
            'i' => 3
        ])

        @include('insurance.safeFamily.printable.1.partials._secondMd', [
            'question' => '¿Usted tiene algún impedimento físico?',
            'i' => 4
        ])

        @include('insurance.safeFamily.printable.1.partials._secondMd', [
            'question' => '¿Usted ha sufrido algún tipo de accidente?',
            'i' => 5
        ])

        <div class="separator" style="margin: 1rem 0"></div>

        <h2>AUTORIZACIÓN DEL ASEGURADO</h2>

        <p>Asimismo,  por este medio AUTORIZO para que de acuerdo con el valor indicado anteriormente, se haga cargo a mi cuenta de ahorro,  tarjeta de crédito  o  cuenta  de  cheques de la suma que haya lugar, según la peridiocidad seleccionada de acuerdo a mi edad, el plan escogido y la tabla de primas. A los Médicos, Hospitales, Instituto Hondureño de Seguridad Social, Clínicas y Laboratorios que me hayan asistido o que me asistan en el futuro con respecto a mi salud,  para que suministren a Davivienda Seguros, la información que esta requiera relación  con  el  seguro  de  vida que solicito, relevándolos de cualquier prohibición legal que exista sobre revelación de los datos de sus registros con respecto a mi persona.  Queda entendido y convenido que una copia fotostática  de  esta autorización  deberá  ser  considerada  tan  efectiva  y  válida  como  la  original.</p>

        <h2>DECLARACIÓN DEL ASEGURADO</h2>

        <p>Declaro que todas las respuestas anteriores son exactas,  completas y verídicas y acepto que sean consideradas como base para la emisión del seguro de vida que solicito.  Que no ejerzo actividades peligrosas,  ni fuera de la ley.  Davivienda Seguros, se reserva todos los derechos que le puedan asistir en caso de que antes o despues de mi fallecimiento se compruebe que esta declaración no corresponde a mi verdadero estado de  salud en el momento de aceptarse el seguro, asi mismo estoy recibiendo de la Compañia de Seguros las condiciones generales de la póliza.</p>

        <div class="separator"></div>

        <p>Lugar y Fecha: {{ ucwords(mb_strtolower(get_json($print->user_information, ['branch', 'value']))) }}, {{ $date }}</p>

        <h2>CERTIFICACIÓN</h2>

        <ol>
            <li>Davivienda certifica que tiene contratada con Davivienda Seguros, una póliza de Seguro de Vida de grupo con las Condiciones Generales adjuntas con cobertura de Vida, Invalidez Total  y  Permanente,  Enfermedades Graves, Gastos Fúnebres y Renta Diaria por Hospitalización por Enfermedad o Accidente.</li>
            <li>Que Davivienda Seguros, ha aceptado la inclusión en ella de la persona a quien como Asegurado se expide esta Solicitud-Certificado.Los amparos entrarán en vigencia a las 12:00 horas del día siguiente a la fecha de expedición de este documento, con término de un año contado a partir de esa fecha y con renovación automática por el solo hecho del pago de la prima.</li>
            <li>Que Davivienda Seguros, al recibo de la presente Solicitud-Certificado acompañado de las pruebas fehacientes de la muerte, invalidez total y permanente o enfermedades graves, ocurridos durante la vigencia del seguro, pagará a los beneficiarios designados en la proporción que se anota  o  al Asegurado el valor o proporcion del seguro, como se describe en las Condiciones Generales.</li>
        </ol>

        <p>Esta póliza cancela y reemplaza automáticamente una anterior tomada por el mismo Asegurado. El NO PAGO de la prima o de sus fracciones dentro del mes siguiente a la fecha de cada vencimiento produce la terminación de este contrato de seguros. Respaldado por Davivienda Seguros.</p>
    </div>

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 2])
</section>

<section class="sheet padding-10mm">
    <div class="cover" style="font-size: 1.5rem">
        @include('insurance.safeFamily.printable.1.partials._header')
        <h2 style="font-size: 1.8rem; margin: 6rem 0" class="text-center">PÓLIZA SEGURO DE VIDA FAMILIA SEGURA<br>
            BANCO DAVIVIENDA HONDURAS, S.A.</h2>

        <p>“SEGUROS BOLÍVAR HONDURAS, S.A;” (DAVIVIENDA SEGUROS) con domicilio en Tegucigalpa, M.D.C., República de Honduras, C.A., en adelante denominada, “LA COMPAÑIA”, pagará a través de BANCO DAVIVIENDA HONDURAS, S.A., quien en adelante se denominará “El Contratante”, después de recibidas las pruebas fehacientes de fallecimiento del Cliente Asegurado, ocurrido mientras este contrato y la cobertura individual estén en pleno vigor, a favor de los Beneficiarios que él haya designado, el pago de la suma asegurada individual del asegurado fallecido.</p>

        <p>Todo lo anterior queda sujeto a las Condiciones Particulares y Generales de este Contrato, teniendo prelación las primeras sobre las segundas.</p>

        <p>En testimonio de lo cual se firma el presente contrato en la Ciudad de Tegucigalpa, Municipio del Distrito Central, en la misma fecha de vigencia de la Solicitud – Certificado.</p>
    </div>

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 3])
</section>

<section class="sheet padding-10mm">
    <div class="cover">

        <h2>BANCASEGUROS BANCO DAVIVIENDA HONDURAS, S.A.<br>
            CONDICIONES GENERALESSEGURO DE VIDA</h2>

        <p>I. CONTRATO.- La solicitud-Certificado de seguro firmada por el Asegurado, estas Condiciones Generales y los anexos adheridos a la misma, si los hubiere, constituyen el Contrato de Seguro, y para su validez, será necesario que esté suscrito por un funcionario autorizado de Banco DAVIVIENDA Honduras, S.A., en adelante denominado DAVIVIENDA.</p>

        <p>II. COBERTURAS ASEGURADAS.- Sujeto a las Condiciones, de este Contrato, las coberturas son las siguientes:</p>

        <p>VIDA.- Al ocurrir el fallecimiento del Asegurado, Seguros Bolívar Honduras, S.A.,(Davivienda Seguros) en adelante denominado la Compañía de Seguros, a través de DAVIVIENDA, pagará al o a los beneficiario(s) designados la suma asegurada.</p>

        <p>GASTOS FUNEBRES.- Al ocurrir el fallecimiento del  Asegurado, la Compañía de Seguros, a través de DAVIVIENDA, rembolsara los Gastos Fúnebres a quien demuestre haber incurrido en ellos hasta por el 10% de la Suma Asegurada contratada para la cobertura de Vida. En caso de que los gastos incurridos y demostrados objetivamente fueran inferiores a dicho 10%, el excedente se entregara a los beneficiarios designados.</p>

        <h2>COBERTURAS ADICIONALES</h2>

        <ol>
            <li>Cobertura de pago Anticipado del Capital Asegurado por Invalidez Total y Permanente,</li>
            <li>Cobertura de Enfermedades Graves y</li>
            <li>Beneficio de Renta diaria por Hospitalización  por Accidente o Enfermedad.</li>
        </ol>

        <p>Cada cobertura se regirá por sus propias condiciones:</p>

        <p>A. COBERTURA DE PAGO ANTICIPADO DEL CAPITAL ASEGURADO POR INVALIDEZ TOTAL Y PERMANENTE (PASIT)</p>

        <p>En caso de que se declare médicamente el estado de Invalidez Total y Permanente del Asegurado, a causa  de  accidente  o enfermedad, la Compañía de Seguros, a través de DAVIVIENDA, anticipará al Asegurado en forma de renta mensual durante un período de 24 meses, la Suma Asegurada.</p>

        <h2>DEFINICIÓN DE INVALIDEZ TOTAL Y PERMANENTE</h2>

        <p>Se considerará INVALIDEZ TOTAL cuando el Asegurado esté incapacitado físicamente para dedicarse a cualquier actividad, trabajo u ocupación y se considerará como PERMANENTE cuando razonablemente no pueda esperarse la recuperación de la capacidad del Asegurado para el resto de su vida.</p>

        <p>Se considerará específicamente como Invalidez Total y Permanente:</p>

        <ol>
            <li>La pérdida irreparable y absoluta de la vista de ambos ojos;</li>
            <li>La pérdida total de ambas manos;</li>
            <li>La pérdida total de ambos pies;</li>
            <li>La pérdida total de una mano y un pie conjuntamente;</li>
            <li>La pérdida total de una mano o un pie y la vista de un ojo.</li>
        </ol>

        <p>Por pérdida total se entiende la amputación o la inhabilitación funcional total y definitiva del órgano o miembro lesionado. Entendiéndose por amputación de una mano o un pie su separación a nivel de la articulación de la muñeca o del tobillo, respectivamente, o arriba de ella.</p>

        <h2>PERIODO DE ESPERA</h2>

        <p>El derecho que otorga esta Cobertura comenzará a surtir efecto una vez que el Asegurado cumpla seis (6) meses en estado de invalidez total y continua.</p>

        <p>Este  período  no  opera  en  los  casos  de las pérdidas  orgánicas   que   se   mencionan   en  la  definición de invalidez total y permanente.</p>

        <h2>PRUEBAS DE REALIZACIÓN DEL SINIESTRO</h2>

        <p>La indemnización establecida para esta Cobertura se concederá únicamente si se presentan a la Compañía de Seguros pruebas legalmente aceptadas de que el Asegurado ha sufrido el estado de invalidez total y permanente debido a un evento ocurrido durante el período de vigencia de esta cobertura.</p>
    </div>

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 4])
</section>

<section class="sheet padding-10mm">
    <div class="cover">

        <h2>COMPROBACIÓN DE PERMANENCIA  DEL ESTADO DE INVALIDEZ</h2>

        <p>Cuando lo estime necesario, la Compañía de Seguros podrá exigir comprobación de que continúa el estado de invalidez total del Asegurado. Si éste se niega a esa comprobación, o se hace patente que ha desaparecido el estado de invalidez total y permanente, cesará automáticamente el pago de las  rentas sucesivas.</p>

        <p>Si el Asegurado falleciese encontrándose incapacitado antes de percibir el número total de rentas convenidas, las rentas mensuales pendientes serán pagadas a los beneficiarios designados en un solo pago.</p>

        <p>Queda convenido que los efectos de esta póliza cesarán al completarse el pago total de las rentas mensuales convenidas.</p>

        <h2>EXCLUSIONES</h2>

        <p>El derecho establecido en esta cobertura, no se concederá si la invalidez total y permanente del Asegurado se debe  a:</p>

        <ol>
            <li>Enfermedades, padecimientos mentales o lesiones corporales ocurridos antes de la fecha de vigencia de esta Cobertura;</li>
            <li>Tentativa de suicidio, así como lesiones provocadas por el propio asegurado, consciente o inconscientemente, cualesquiera que sean las causas o circunstancias que lo provoque;</li>
            <li>Cualquier enfermedad mental;</li>
            <li>El desempeño de funciones policíacas, el servicio militar o naval de cualquier clase, actos de guerra, rebelión, revoluciones, alborotos populares o insurrecciones;</li>
            <li>Participación directa del asegurado en actos delictivos intencionales;</li>
            <li>Riña, en la que el Asegurado haya sido el provocador;</li>
            <li>Carreras, pruebas o contiendas de seguridad, resistencia o velocidad, en cualquier clase de vehículos;</li>
            <li>Accidentes de navegación aérea, excepto cuando el Asegurado viaje como pasajero, en aviones de Empresas Comerciales, en vuelo regular, destinados al servicio de pasajeros y mientras dichos aviones sean manejados por Pilotos de  Planta, dentro o fuera del horario e itinerario fijo, o vuelo especial o contratado, entre aeropuertos debidamente establecidos y habilitados por la autoridad competente;</li>
            <li>Actividades de buceo, alpinismo,  rodeo o charrería, esquí, tauromaquia, paracaidismo o  cualquier tipo de deporte aéreo;</li>
            <li>Radiaciones Ionizantes;</li>
            <li>La influencia de algún enervante, estimulante o similar; excepto si fueron prescritos por un médico;</li>
            <li>Alcoholismo, influencia de bebidas alcohólicas o drogadicción;</li>
            <li>Si el estado de invalidez es parcial, aún cuando sea permanente; o invalidez total pero no permanente.</li>
        </ol>

        <p>B. COBERTURA DE ENFERMEDADES GRAVES</p>

        <p>En caso de que el asegurado llegare a padecer una o más de las enfermedades que más adelante se definen, la   Compañía   de  Seguros  a  través  de  DAVIVIENDA anticipará hasta el 50% de la suma asegurada.</p>


        <h2>DEFINICIONES DE ENFERMEDADES GRAVES</h2>

        <p>Las enfermedades graves amparadas por esta cobertura son exclusivas y se limitan a las siguientes.</p>

        <p>1. INFARTO AL MIOCARDIO: La muerte de una parte del músculo cardíaco como resultado de una inadecuada irrigación de sangre, por obstrucción de la arteria correspondiente, generalmente a consecuencia de una trombosis o embolia, evidenciado por el típico dolor de pecho, la aparición reciente de modificaciones electrocardiográficas peculiares y elevación de las enzimas cardíacas.</p>

        <p>2. REVASCULARIZACIÓN CORONARIA (BY-PASS): Es la cirugía realizada para corregir la estenosis o la oclusión de las arterias coronarias, con al menos un doble puente coronario evidenciado por el resultado de una angiografía.</p>

        <p>El resultado de la angiografía junto con el informe médico estará a disposición de la Compañía de Seguros.</p>

        <p>Se excluyen la angioplastía, tratamiento por láser y todas las técnicas que no requieran la apertura quirúrgica del tórax, así como operaciones de válvulas, operación por tumoración intracardíaca o alteración congénita.</p>

        <p>La prestación sólo se pagará después de que la operación se haya efectuado.</p>
    </div>

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 5])
</section>

<section class="sheet padding-10mm">
    <div class="cover">
        <p>3. ACCIDENTE CEREBROVASCULAR: Significa cualquier incidente cerebral vascular que dé lugar a secuelas que duren más de (24) horas y que incluyan infarto del tejido cerebral, hemorragia y embolismo de una fuente extracraneana.</p>

        <p>Se excluyen los síntomas cerebrales de migraña, lesión cerebral causada por traumatismo o hipoxia y enfermedad vascular que afecte al ojo o al nervio óptico y las alteraciones isquémicas del sistema vestibular.</p>

        <p>4. CANCER:  La presencia de uno o más tumores  malignos, de crecimiento incontrolado e invasión de  tejidos y con propagación de células anormales malignas por contigüidad o por metástasis, incluyendo los linfomas, la enfermedad de Hodking (linfo granuloma) y la leucemia ( excepto  la  leucemia linfocítica crónica).</p>

        <p>Los siguientes cánceres quedan excluidos de esta Cobertura:</p>

        <ol>
            <li>Los tumores que presentan   los   cambios   malignos característicos del carcinoma in situ (incluyendo la desplacía cervical  CIN-1, CIN-2,  CIN-3)  o aquellos considerados por histología como premalignos;</li>

            <li>Los melanomas con espesor menor de 1.5 mm, determinado por examen histológico, o cuando la invasión sea menor del nivel 3 de Clark;</li>

            <li>Todas las  hiperqueratosis o los carcinomas    basocelulares de la piel;</li>

            <li>Todos los carcinomas de células escamosas de la piel,  excepto  cuando  se trate   de  diseminación  hacia  otros órganos;</li>

            <li>Los cánceres de la próstata que por histología pertenezcan a  la etapa T1 incluyendo T1(a) o T1(b) del sistema TNM, desarrollado por la Unión Internacional  contra  el  Cáncer,  o  de  cualquier  otra  clasificación  equivalente  o menor;</li>

            <li>El Sarcoma de Kaposi y otros tumores relacionados con la infección VIH o SIDA.</li>
        </ol>

        <p>5. INSUFICIENCIA RENAL: Es el fallo total, crónico e irreversible de ambos riñones, a consecuencia de la cual hay que efectuar sistemáticamente diálisis renal o el trasplante renal.</p>

        <h2>PERIODO DE ESPERA</h2>

        <p>El derecho que otorga esta Cobertura sólo surtirá efecto si los primeros síntomas de tales enfermedades son diagnosticadas por primera vez y en forma definitiva por un Médico Especialista legalmente autorizado para el ejercicio de su profesión después de 180 días contados a partir de la fecha de vigencia del Seguro, excepto si la póliza hubiese sido renovada en forma inmediata y consecutiva.</p>

        <h2>PRUEBAS DE REALIZACIÓN DEL SINIESTRO</h2>

        <p>El derecho establecido en esta Cobertura se concederá únicamente si se presentan a la Compañía de Seguros, pruebas fehacientes de que el Asegurado ha sufrido una o más de las enfermedades enunciadas anteriormente. La Compañía de Seguros tendrá derecho de solicitar al Asegurado someterse a exámenes, auscultaciones y demás pruebas que considere necesarias para confirmar la procedencia del diagnóstico correspondiente.</p>

        <p>Si el Asegurado se negara a someterse a dichos exámenes y pruebas, la Compañía de Seguros quedará liberada de la responsabilidad que le deriva la presente Cobertura de Enfermedades Graves.</p>

        <h2>REDUCCIÓN DE SUMA ASEGURADA POR SINIESTRO</h2>

        <p>En caso que la Compañía de Seguros indemnice al Asegurado el  porcentaje  correspondiente por padecer una o más de las enfermedades graves, el importe de dicho pago se deducirá de la suma asegurada establecida para el caso de muerte y en este caso, el Asegurado podrá renovar la póliza básica por el monto del seguro reducido.</p>

        <h2>EXCLUSIONES</h2>

        <p>El derecho establecido en esta Cobertura, no se concederá si las enfermedades a que el mismo se refiere son consecuencia o están en conexión con:</p>

        <ol>
            <li>Actos de guerra, rebelión, alborotos populares o insurrecciones; por hechos delictivos intencionales imputables al Asegurado o por lesiones que éste sufra al participar en carreras, pruebas o contiendas de velocidad en cualquier clase de vehículos;</li>
            <li>La influencia de algún enervante, estimulante o similar; excepto si fueron prescritos por un médico;</li>

            <li>Alcoholismo, influencia de bebidas alcohólicas o drogadicción;</li>

            <li>Tentativa de suicidio, así como lesiones provocadas por el propio asegurado, consciente o inconscientemente, cualesquiera que sean las causas o circunstancias que lo provoquen;</li>
        </ol>

    </div>

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 6])
</section>

<section class="sheet padding-10mm">
    <div class="cover">
        <ol start="5">

            <li>Enfermedades sufridas por riesgos nucleares;</li>

            <li>Enfermedades preexistentes o padecimientos congénitos;</li>

            <li>Enfermedades distintas a las definidas en esta Cobertura.</li>
        </ol>

        <p>C. BENEFICIO DE RENTA DIARIA POR HOSPITALIZACIÓN POR ACCIDENTE O ENFERMEDAD</p>

        <p>Si, como resultado de la lesión o el comienzo de la enfermedad, el Asegurado se viera necesariamente recluido dentro   de un hospital como paciente hospitalizado mientras que este Beneficio estuviera vigente y bajo la atención continua de un Médico, la Compañía pagará el Beneficio Diario indicado en la Solicitud - Certificado de la póliza, por cada día que el Asegurado continuara estándolo, hasta un máximo de sesenta (60) días por evento, comenzando inmediatamente después del Período de Eliminación (si lo hubiere) indicado en la Solicitud - Certificado. El Asegurado deberá permanecer hospitalizado al menos por un período de veinticuatro (24) horas consecutivas.</p>

        <h2>PERIODOS SUCESIVOS DE RECLUSIÓN EN HOSPITAL</h2>

        <p>Si un Asegurado fuera readmitido y recluido como paciente hospitalizado en dos o más ocasiones dentro de un período de doce (12) meses, por la misma causa o causas relacionadas, La Compañía lo considerará dentro del mismo período de reclusión.  La responsabilidad de La Compañía por el período integro estará sujeta a las limitaciones aplicables a la parte o partes de este Beneficio conforme a las cuales el período de reclusión original fuera indemnizado; de forma tal, que el total de Rentas Diarias pagadas no  exceda de sesenta (60) días.</p>

        <p>Si la reclusión  posterior como paciente hospitalizado estuviera separada por más de doce (12) meses, esta  será considerada como un nuevo período de reclusión e indemnizada de acuerdo con las estipulaciones de esta Póliza.</p>

        <h2>EXCLUSIONES</h2>

        <p>Este Beneficio no cubre pérdida alguna o gasto ocasionado por lo que resultara de:</p>

        <ol>
            <li>Maternidad, parto, aborto o intento del mismo,</li>

            <li>Exámenes físicos o de rutina o cualesquier otros, donde no haya indicaciones objetivas de deterioro en la salud normal y diagnósticos de laboratorios o exámenes de rayos x; excepto en el curso de una Incapacidad establecida por atención de un médico;</li>

            <li>Suicidio o tentativa de suicidio esté o no el Asegurado en su sano juicio;</li>

            <li>Cirugía plástica o cosmética excepto como consecuencia de un accidente;</li>

            <li>Anomalías congénitas y las causas que sobrevengan o resulten con relación a las mismas;</li>

            <li>Cualquier desorden mental o nervioso o curas de descanso;</li>

            <li>Alcoholismo, influencia de bebidas alcohólicas, uso de narcóticos o estupefacientes y tratamiento de los mismos;</li>

            <li>Pérdida ocasionada  por guerra, invasión, acto de enemigo extranjero, hostilidades u operaciones bélicas (haya o no declaración de guerra), motín, conmoción civil, guerra civil, rebelión, revolución, insurrección, conspiración, poder militar o usurpado y servicio en las Fuerzas Armadas o en la Policía;</li>

            <li>Procesos médicos relacionados con el virus HIV o SIDA. Para propósitos de esta cláusula el término “Síndrome de Inmunodeficiencia Adquirida HIV o SIDA” tendrá las definiciones asignadas por la Organización Mundial de la Salud;</li>

            <li>Procesos quirúrgicos programados con anterioridad a la vigencia de esta cobertura o reclusión hospitalaria resultante de  Enfermedades o Lesiones corporales preexistentes al momento de suscribir la póliza, si la hospitalización ocurre durante los primeros dos (2) años de vigencia de la cobertura individual del seguro</li>
        </ol>

        <h2>COMPROBACIÓN DE SINIESTRO</h2>

        <p>El reclamante deberá presentar a La Compañía, a través del Contratante, todas las pruebas del hecho que genera la obligación derivada del seguro y del derecho que tiene de solicitar el pago.</p>

        <p>Al presentarse alguna reclamación de seguro, La Compañía tendrá el derecho de verificar todos los hechos relacionados con el siniestro y de solicitar al Asegurado, a través del Contratante, toda clase de información referente al siniestro a fin de determinar sus causas, circunstancias y consecuencias.</p>

        <p>Al efecto, el Contratante  y el asegurado  autorizanexpresamente  a  las  personas  naturales  o jurídicas a proporcionar a La Compañía toda la información y documentos que tengan relación con el evento que se reclame o con el Asegurado.</p>

        <p>La Compañía tendrá derecho, siempre que lo juzgue conveniente, a comprobar a su costa cualquier hecho o situación de la cual derive para ella una obligación. La obstaculización por parte del Contratante y/o del Asegurado, para que se lleve a cabo dicha comprobación, liberará a La Compañía de cualquier obligación.</p>


    </div>

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 7])
</section>

<section class="sheet padding-10mm">
    <div class="cover">

        <h2>DEFINICIONES</h2>

        <p>1). ACCIDENTE: Se entiende por accidente toda lesión corporal, sufrida por el Asegurado independientemente de su voluntad, causada por la acción repentina de un agente externo y que pueda ser determinada de una manera cierta, por médico autorizado legalmente para ejercer su profesión.</p>

        <p>Para los efectos de esta Póliza se considera, además, como accidente:</p>

        <ol>
            <li>Los causados por explosiones, descargas eléctricas o atmosféricas.</li>

            <li>Las quemaduras accidentales causadas por fuego, escapes de vapor o el contacto con ácidos y corrosivos.</li>

            <li>La asfixia o intoxicación  por vapores o gases, o inmersión u obstrucción.</li>

            <li>La intoxicación o envenenamiento por ingestión involuntaria de substancias tóxicas o alimentos en mal estado.</li>

            <li>Las infecciones respecto de las cuales pueda afirmarse que el virus ha penetrado en el cuerpo por una lesión producida por un accidente cubierto por esta Póliza.</li>

            <li>Las mordeduras de animales o picaduras de insectos o sus consecuencias.</li>

            <li>Los casos que sean consecuencia de legítima defensa o de tentativa de salvar a personas o bienes en peligro.</li>

            <li>Los que se produzcan como consecuencia de fenómenos de la naturaleza y que afecten al Asegurado en forma personal y aislada.</li>
        </ol>

        <p>2). LESIÓN: Para los efectos de esta Póliza, significa lesión corporal causada por un accidente que  ocurra  mientras esta Póliza esté en vigor y que resulte, directa e independientemente de toda otra causa, en una pérdida cubierta por esta Póliza.</p>

        <p>3). ENFERMEDAD: El estado anormal del organismo del asegurado, entendido como una desviación de los procesos biológicos en que se materializa la vida, del plano normal en que se desenvuelven.</p>

        <p>4). ASEGURADO: La o las personas que están cubiertas por los beneficios proporcionados por esta Póliza.</p>

        <p>5). BENEFICIO: Es la indemnización a la que tiene derecho el Asegurado, en caso de ser procedente el siniestro de acuerdo a lo estipulado en el presente Contrato.</p>

        <p>6). CONTRATANTE: Es la persona natural o jurídica que suscribe con La Compañía una Póliza de Seguros, y es responsable ante La Compañía de pagar la prima correspondiente y de informar todos los hechos importantes para la apreciación del riesgo objeto del seguro.</p>

        <p>7). PERÍODO DE ELIMINACIÓN: Es el número de días consecutivos, como aparece (si lo hubiere)  en la Solicitud – Certificado del Beneficio, comenzando el primer día en que el Asegurado estuviera hospitalizado, para los cuales este beneficio no procede.</p>

        <p>8). INDEMNIZACIÓN DIARIA: El monto que la Compañía de Seguros pagará al asegurado cuando se den las condiciones de cobertura prescritas en este beneficio considerando el valor diario convenido, el cual se expresa en la Solicitud - Certificado de la póliza, y de acuerdo a la definición de las coberturas que se indican en estas Condiciones Generales.</p>

        <p>9). MÉDICO: Es una persona legalmente autorizada para practicar la medicina y/o cirugía que no sea el Asegurado o su cónyuge, hijo, hija, madre, padre, hermano, hermana, tía o tío.</p>

        <p>10). HOSPITAL: Es un establecimiento que cumpla con todos los siguientes requisitos: (1) tenga licencia de hospital; (2) opere principalmente para la recepción, cuidado y tratamiento de personas enfermas, achacosas, traumatizadas o heridas como pacientes internos; (3) proporcione servicios de enfermería 24 horas al día por enfermeras registradas o graduadas; (4) cuente con un personal de dos o más médicos disponibles en todo momento; (5) tenga instalaciones organizadas para el diagnóstico y las principales intervenciones quirúrgicas; (6) no sea principalmente una clínica, asilo, lugar de descanso o para convalecientes u otro establecimiento similar y no sea, salvo incidentalmente, un lugar para alcohólicos, o drogadictos; (7) mantenga equipo de rayos-X e instalaciones de quirófano.</p>

        <p>11). HOSPITALIZACIÓN: La internación del asegurado en un establecimiento hospitalario con motivo de una enfermedad o accidente de éste, previa prescripción médica.</p>

        <p>12). PACIENTE HOSPITALIZADO: Es una persona que esté recluida en un Hospital como paciente interno y que se le cobre por lo menos un (1) día de cuarto de Hospital.</p>

        <p>13). SUMA ASEGURADA: Dondequiera que se use en este Beneficio, significa la suma indicada en la Solicitud - Certificado en lo que se refiera a cada Asegurado mencionado. </p>

        <p>III. VIGENCIA.- La vigencia de este Contrato será anual, efectiva a partir del día siguiente al de su expedición y se renovará automáticamente al final de cada año de vigencia, salvo que el Asegurado solicite por  escrito  con  por  lo  menos  treinta  (30)  días  de anticipación a su vencimiento, el deseo de no renovarlo.</p>

    </div>

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 8])
</section>

<section class="sheet padding-10mm">
    <div class="cover">

        <p>IV. FORMA DE PAGO DE LAS PRIMAS.- Este Contrato se expide con la condición  de  que  el  pago  de  la  prima  convenida es anual y  anticipada, contra Débitos  Automáticos  a  la cuenta en DAVIVIENDA del Asegurado, o de su cónyuge en su caso. El Asegurado puede solicitar realizar el pago de la prima anual en forma fraccionada por períodos semestrales, trimestrales o mensuales, siempre anticipadamente.</p>

        <p>V. INDISPUTABILIDAD.- Este contrato será indisputable a partir de la fecha en que cumplan dos (2) años desde el inicio de su vigencia, siempre y cuando dicho término transcurra durante la vida del  Asegurado, y al efecto la Compañía de Seguros renuncia a todos los derechos que, conforme a la Ley, son renunciables para anularlo o rescindirlo en los casos de omisión o de inexacta declaración al describir el riesgo que sirvió de base para su celebración.</p>

        <p>Si el Asegurado solicitara incrementos en la suma asegurada de la póliza, éstos también serán disputables durante los dos (2) primeros años, contados a partir de la fecha de inicio de la vigencia de los mismos, en vida del Asegurado.</p>

        <p>La presente cláusula de indisputabilidad aplica únicamente para la Cobertura  Básica  por  Fallecimiento  y  en ningún caso para las coberturas adicionales contratadas, las que serán disputables en cualquier momento durante o después de la vigencia de esta póliza.</p>

        <p>VI. EDAD.- Los límites de admisión fijados por la Compañía de Seguros para este contrato son: 18 años como mínimo y 64 años como máximo. En consecuencia el Seguro será nulo para aquellas personas que lo hubiesen suscrito fuera de los límites mencionados; en este caso, la Compañía de Seguros devolverá al Asegurado o a los beneficiarios el valor de la prima pagada menos los gastos incurridos.<p>

        <p>VII. PRUEBA DE EDAD.- Antes de la liquidación o constitución de cualquier cobertura, el Asegurado o, en su caso el beneficiario, deberá presentar a la Compañía de Seguros cualquier documento oficial que compruebe su fecha de nacimiento. Si la edad declarada no coincidiese con la edad verdadera, pero encontrándose   dentro  de   los  límites  de  admisión fijados, se aplicará lo dispuesto en los artículos 1231 y 1232 del Código de Comercio de la  República de Honduras.</p>

        <p>VIII. BENEFICIARIOS.- El Asegurado tiene derecho a designar o cambiar libremente a los beneficiarios, siempre que este contrato no haya sido cedido y no exista restricción legal en contrario. El Asegurado deberá notificar por escrito el cambio a la Compañía de Seguros a través de DAVIVIENDA, indicando el nombre del nuevo beneficiario. En caso de que la notificación no se reciba oportunamente, la Compañía de Seguros pagará al último beneficiario de que tenga conocimiento y quedará liberada de las obligaciones contraídas en este contrato.</p>

        <p>El Asegurado podrá renunciar en forma irrevocable al derecho de  cambiar  la  designación  de  beneficiario, siempre  que  la notificación de esa renuncia se haga por escrito al beneficiario y la Compañía de Seguros a  través de DAVIVIENDA y que conste en la presente póliza.</p>

        <p>Si habiendo varios beneficiarios falleciere alguno, la parte correspondiente se distribuirá en partes iguales entre los supervivientes, salvo indicación en contrario del Asegurado.</p>

        <p>Cuando no haya beneficiarios designados, el importe del seguro se pagará a la sucesión legal del Asegurado; la misma regla se observará, salvo estipulación en contrario, en caso de que el beneficiario y el Asegurado mueran simultáneamente, o cuando el beneficiario designado muera antes que el Asegurado y no se hubiera hecho nueva designación.</p>

        <p>IX. MODIFICACIONES.- Todo cambio o modificación a este Contrato deberá ser solicitado por escrito por el Asegurado, el cual una vez aceptado se hará constar en anexo debidamente firmado por un funcionario autorizado de DAVIVIENDA.</p>

        <p>X. EXENCIÓN DE RESTRICCIONES.- Este Contrato no se afectará por razones de residencia, ocupación, viajes y lugar en que ocurra el fallecimiento del Asegurado.</p>

        <h2>XI. EXCLUSIONES</h2>

        <p>No se ampara el fallecimiento del Asegurado a consecuencia de:</p>

        <ol>
            <li>Suicidio o lesiones provocadas por el propio Asegurado, consciente o inconscientemente, cualesquiera que sean las causas o Circunstancias que la provoquen, si ocurre durante los primeros dos (2) años de vigencia de la póliza;</li>

            <li>Guerra declarada o no, revolución o participación directa del Asegurado en motines, huelgas o tumultos populares;</li>

            <li>Participación directa del Asegurado en actos delictivos intencionales;</li>

            <li>Carreras, pruebas o contiendas de seguridad, resistencia o velocidad, en cualquier clase de vehículos;</li>

            <li>Actividades de buceo, alpinismo, rodeo o charrería, esquí, tauromaquia, paracaidismo o cualquier tipo de deporte aéreo; y</li>

            <li>Síndrome de Inmunodeficiencia Adquirida (SIDA), las partes convienen y así se estipula, que no obstante leyes especiales y lo que prescribe el articulo 1106 de Código de Comercio de Honduras, esta póliza de seguro de vida, no cubre ni indemnizará las reclamaciones por gastos médicos o muerte,</li>

            <li>ambos acontecimientos relacionados o como consecuencia del SINDROME DE INMUNODEFICIENCIA ADQUIRIDA (SIDA), independientemente que dicha enfermedad haya sido adquirida antes o después de la suscripción del presente contrato de seguro de vida.</li>
        </ol>
    </div>

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 9])
</section>

<section class="sheet padding-10mm">
    <div class="cover">

        <p>XII. ENDOSO DE EXCLUSIÓN LA/FT: El presente Contrato se dará por terminado de manera anticipada en los casos en el que el asegurado, el contratante y/o el beneficiario sea condenado mediante sentencia firme por algún Tribunal nacional o de otra jurisdicción por los delitos de Narcotráfico, Lavado de Dinero, Financiamiento del Terrorismo, Financiación de la Proliferación de Armas de Destrucción Masiva, o cualquier otro delito de crimen o delincuencia organizada conocidos como tales por Tratados o Convenios Internacionales de los cuales Honduras sea suscriptor; o que el beneficiario o contratante del seguro se encuentren incluidos en las listas de entidades u organizaciones que identifiquen a personas como partícipes, colaboradores, facilitadores del crimen organizado como ser la lista OFAC (Office Foreing Assets Control) y la lista de Designados por la ONU, entre otras. Este Endoso se adecuará en lo pertinente a los procedimientos especiales que podrían derivarse de la Ley Especial Contra el Lavado de Activos, Ley sobre el Uso Indebido y Tráfico Ilícito de Drogas y Sustancias Psicotrópicas, Ley Contra el Financiamiento del Terrorismo, Ley sobre Privación Definitiva del Dominio de Bienes de Origen Ilícito y sus respectivos Reglamentos, en lo relativo al manejo, custodia de pago de primas y de siniestros sobre los bienes asegurados de personas involucradas en ese tipo de actos; sin perjuicio de que la Aseguradora deberá informar a las autoridades competentes cuando aplicare esta cláusula para la terminación anticipada del contrato.</p>
        <p>XIII. PRESCRIPCIÓN.- De acuerdo con el Artículo 1156 del Código de Comercio de la República de Honduras, todas las acciones que se deriven de este contrato   prescribirán   en  tres  (3) años,   contados desde la fecha del acontecimiento que les dio origen, salvo los casos de excepción consignados en los Artículos 1157 y 1159 del mismo Código.</p>

        <p>XIV. TERMINACIÓN DEL SEGURO </p>

        <p>La vigencia de este Seguro termina:</p>

        <ol>
            <li>Por solicitud del Asegurado, manifestada por escrito a BANCO DAVIVIENDA HONDURAS, S.A. en  cualquier  vencimiento de primas y devolviendo el presente Contrato de  Seguro   para  su  anulación.</li>

            <li>Por falta de pago de la prima convenida dentro de los treinta (30) días siguientes a la fecha de cada vencimiento.</li>

            <li>Al fin del aniversario de la póliza en que el asegurado cumpla los setenta (70) años de edad.</li>

            <li>Al dejar de ser cliente de DAVIVIENDA.</li>

            <li>Por fallecimiento o Invalidez Total y Permanente del Asegurado.</li>

            <li>Por solicitud de DAVIVIENDA.</li>
        </ol>

        <p>XV. CONVERSIÓN.- Los Asegurados menores de 64 años que se retiren del Grupo Asegurado y que hayan permanecido asegurados ininterrumpidamente por lo menos durante un (1) año, tendrán derecho a solicitar dentro de los treinta (30) días siguientes a la fecha en que dejó de pertenecer al Grupo Asegurado, la Conversión a una Póliza de Seguro de Vida Individual sin necesidad de pruebas de asegurabilidad, en cualquiera de los planes vigentes en la Compañía de Seguros por una suma asegurada menor o igual a la de este Contrato pero sin beneficios adicionales. La prima de la nueva Póliza será la que corresponda a la clase del riesgo asumido, al importe del Seguro, al Plan y a la edad del Asegurado en su cumpleaños más próximo a la fecha de emisión de la Póliza.</p>

        <p>XVI.PROCEDIMIENTO EN CASO DE SINIESTRO</p>

        <p>Aviso de Siniestro:</p>

        <p>Tan pronto como el Asegurado o beneficiario, en su caso, tenga conocimiento de la realización del siniestro y del derecho constituido a su favor por el contrato de seguro, deberá comunicarlo por escrito a la Compañía de Seguros a través de DAVIVIENDA, en un plazo máximo de cinco (5) días contado desde la realización del acto generador del derecho.</p>

        <p>Documentos, Datos e Informes:</p>

        <p>La Compañía de Seguros, a través de DAVIVIENDA, tiene el derecho de exigir del Asegurado o beneficiario toda clase de documentación e informes sobre los hechos relacionados con el siniestro y por los cuales puedan determinarse las circunstancias  de su realización y las consecuencias del mismo. Los documentos originales a presentar para el análisis y pago de la reclamación son:</p>

        <h2>POR FALLECIMIENTO</h2>

        <ol>
            <li>Contrato de Seguro (Solicitud-Certificado)</li>

            <li>Certificación de Acta de Defunción;</li>

            <li>Certificación  Médica,  expedida por el Médico que atendió al Asegurado en su última enfermedad;</li>

            <li>Certificación   o    parte    de    la    Autoridad   competente;</li>

            <li>Cédula de Identidad o Certificación del Acta de  Nacimiento del Asegurado;</li>

            <li>Cédula de identidad o Certificado  del  Acta  de  Nacimiento de los beneficiarios;</li>

            <li>Certificación de  Tutoría  Legal   (en  caso  de  beneficiarios menores de edad);</li>
        </ol>
    </div>

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 10])
</section>

<section class="sheet padding-10mm">
    <div class="cover">
        <ol start="8">
            <li>Formulario de Reclamación de la Compañía de Seguros; y</li>

            <li>Cualquier otro  documento  que  la  Compañía  de  Seguros estime conveniente.</li>
        </ol>

        <h2>POR INVALIDEZ TOTAL Y PERMANENTE</h2>

        <ol>
            <li>Contrato de Seguro (Solicitud -Certificado)</li>

            <li>Cédula de Identidad o Certificado del Acta de Nacimiento;</li>

            <li>Certificación Médica de la Incapacidad Total y Permanente;</li>

            <li>Formulario de Reclamación de la Compañía de  Seguros;</li>

            <li>Pruebas y exámenes médicos que la Compañía de Seguros requiera; y</li>

            <li>Cualquier otro documento que la Compañía de Seguros  estime conveniente.</li>
        </ol>

        <h2>POR ENFERMEDADES GRAVES</h2>

        <ol>
            <li>Contrato de Seguros (Solicitud-Certificado);</li>

            <li>Cédula  de  Identidad  o  Certificación  del Acta de    Nacimiento;</li>

            <li>Certificación Médica del médico tratante con todos los elementos en que se hubiere fundado el diagnóstico de la enfermedad;</li>

            <li>Formulario de reclamación de la Compañía de Seguros;</li>

            <li>Pruebas y exámenes médicos que la Compañía de Seguros requiera; y</li>

            <li>Cualquier otro documento que la Compañía de Seguros estime conveniente.</li>
        </ol>


        <h2>POR RENTA DIARIA HOSPITALARIA POR ACCIDENTE O ENFERMEDAD</h2>

        <p>Al ocurrir algún siniestro que pudiera dar lugar a indemnización conforme a este Beneficio, el asegurado tendrá la obligación de comunicarlo por escrito a La Compañía, a través del Contratante, a más tardar dentro de los 30 (treinta) días calendarios siguientes a la fecha de hospitalización del Asegurado. </p>

        <p>En caso de un siniestro de la Cobertura de Renta Diaria por Hospitalización, el Asegurado deberá presentar de manera inicial a La Compañía, a través del contratante, la siguiente información y documentación:</p>
        <ol>
            <li>Escrito de reclamación que comunique de manera inmediata el estado de paciente hospitalizado del Asegurado.</li>

            <li>Cédula de Identidad o Certificación del Acta de Nacimiento;</li>

            <li>Constancia médica original debidamente sellada y firmada, extendida en papel membretado del Hospital, donde indique el diagnóstico y la cantidad de noches que el Asegurado estuvo hospitalizado.</li>

            <li>Fotocopia de la factura debidamente cancelada al Hospital por los servicios médicos recibidos incluyendo los días de Hospitalización; exceptuando aquellos casos en que los servicios médicos fueron suministrados: d1.- En un Hospital que pertenezca o sea manejado por el Gobierno o por el Instituto Hondureño de Seguridad Social (IHSS); y, d2.- Por cualquier otro Hospital por cuya asistencia médica el Asegurado no tiene que pagar.</li>
        </ol>

        <p>XVII. COMPETENCIA.- . Cualquier controversia o conflicto entre La Compañía y sus contratantes sobre la interpretación, ejecución, cumplimiento, o términos del contrato, podrán ser resueltos, a opción  de las partes, por la vía de conciliación, arbitraje o por vía judicial, en este último caso deben acudir ante los tribunales de Tegucigalpa M.D.C., a cuya jurisdicción quedan expresamente sometidos.</p>

        <p>XVIII.COMUNICACIONES.- Todas las comunicaciones deberán hacerse por escrito a través del Contratante, a las oficinas de La Compañía en su domicilio social. Las que se dirijan al Contratante y/o Asegurado, serán enviados al último domicilio que el mismo haya señalado para tal efecto.</p>

        <p>EN TESTIMONIO DE LO CUAL, la Compañía ha otorgado esta Póliza, la cual no será válida salvo que sea refrendada por un representante debidamente autorizado de DAVIVIENDA.</p>
    </div>

    @include('insurance.safeFamily.printable.1.partials._signature')

    @include('insurance.safeFamily.printable.1.partials._footer', ['i' => 11])
</section>

@include('modules.printable._printButton', [
    'sendEmail' => env('SEND_EMAIL'),
    'route' => route('send.product.document', $print->id)
])

</body>

</html>