<script>
    $(document).ready(function() {
        let input = $( "#customer_limit" );

        let clasicaInit = {{ $lists['cardLimits']['CLASICA']['begin'] }};
        let clasicaEnd = {{ $lists['cardLimits']['CLASICA']['end'] }};

        let oroInit = {{ $lists['cardLimits']['ORO']['begin'] }};
        let oroEnd = {{ $lists['cardLimits']['ORO']['end'] }};

        let platinumInit = {{ $lists['cardLimits']['PLATINUM']['begin'] }};
        let platinumEnd = {{ $lists['cardLimits']['PLATINUM']['end'] }};

        let signatureInit = {{ $lists['cardLimits']['SIGNATURE']['begin'] }};
        let signatureEnd = {{ $lists['cardLimits']['SIGNATURE']['end'] }};

        let infiniteInit = {{ $lists['cardLimits']['INFINITE']['begin'] }};
        let infiniteEnd = {{ $lists['cardLimits']['INFINITE']['end'] }};

        $('.customer-limit-message').hide();

        input.blur(function() {
            let value = parseFloat(input.val().replace(',', ''));

            console.log(value, clasicaInit);
            if(value < clasicaInit)
            {
                $('#customer-limit-message span').text('El límite es muy bajo');
            } else if (value >= clasicaInit && value <= clasicaEnd) {
                $('#customer-limit-message span').text('Su tarjeta es de tipo CLÁSICA');
            } else if (value >= oroInit && value <= oroEnd) {
                $('#customer-limit-message span').text('Su tarjeta es de tipo ORO');
            } else if (value >= platinumInit && value <= platinumEnd) {
                $('#customer-limit-message span').text('Su tarjeta es de tipo PLATINUM');
            } else if (value >= signatureInit && value <= signatureEnd) {
                $('#customer-limit-message span').text('Su tarjeta es de tipo SIGNATURE');
            } else if (value >= infiniteInit && value <= infiniteEnd) {
                $('#customer-limit-message span').text('Su tarjeta es de tipo INFINITE');
            } else {
                $('#customer-limit-message span').text('');
            }

            $('.customer-limit-message').fadeIn();
        });
    });
</script>

@if(checkStep($flow, ['2']))
    <script>
        $('.format-customer-limit').autoNumeric('init', {
            vMax: '{{ get_json($flow->product_information, ['product', 'limit'], 0) }}'
        });
    </script>
@endif