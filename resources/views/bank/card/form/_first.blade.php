@include('modules._progress', [
    'steps' => 2,
    'step' => 1,
    'title' => 'Formulario de aprobación'
])

<div class="row">
    <div class="col-12 identity-form">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.card.second') }}" id="form">
            {!! csrf_field() !!}

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Fecha de nacimiento</h2> 
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-calendar"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('day') ? ' has-error' : '' }}">
                        <div class="row birth-container">
                            <div class="col-4 birth-day">
                                @include('modules._singleSelect', [
                                    'name' => 'day',
                                    'placeholder' => 'Día',
                                    'options' => $lists['dayOptions'], 
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information,'birth'), 'd'),
                                    'required' => true
                                ])
                            </div>
                            <div class="col-4 birth-month">
                                @include('modules._singleSelect', [
                                    'name' => 'month',
                                    'placeholder' => 'Mes',
                                    'options' => $lists['monthOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information,'birth'), 'm'),
                                    'required' => true
                                ])
                            </div>
                            <div class="col-4 birth-year">
                                @include('modules._singleSelect', [
                                    'name' => 'year',
                                    'options' => $lists['yearOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information,'birth'), 'Y'),
                                    'default' => $lists['selectedYear'],
                                    'placeholder' => 'Año',
                                    'required' => true
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Nombres</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-edit"></span>
                </div>

                <div class="col-5 first-name-field">
                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'first_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['name', 'first']),
                            'placeholder' => 'Primer nombre',
                            'label' => 'Primer nombre',
                            'length' => '15',
                            'required' => true,

                        ])
                    </div>
                </div>
                <div class="col-5 middle-name-field">
                    <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'middle_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['name', 'middle']),
                            'placeholder' => 'Segundo nombre',
                            'label' => 'Segundo nombre',
                            'length' => '15',
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Apellidos</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-edit"></span>
                </div>

                <div class="col-5 last-name-field">
                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'last_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['name', 'last']),
                            'placeholder' => 'Primer apellido',
                            'label' => 'Primer apellido',
                            'length' => '15',
                            'required' => true,

                        ])
                    </div>
                </div>
                <div class="col-5 second-last-name-field">
                    <div class="form-group{{ $errors->has('second_last_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'second_last_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['name', 'second_last']),
                            'placeholder' => 'Segundo apellido',
                            'label' => 'Segundo apellido',
                            'length' => '15',

                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Números de contacto</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-phone"></span>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'phone'),
                            'placeholder' => 'Teléfono de domicilio',
                            'label' => 'Teléfono de domicilio',
                            'length' => '9',
                            'classes' => 'validate-num-dash',
                            'enable' => true

                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'mobile',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'mobile'),
                            'placeholder' => 'Celular',
                            'label' => 'Celular',
                            'length' => '9',
                            'classes' => 'validate-num-dash',
                            'required' => true,
                            'enable' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Colonia de domicilio</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        <select class="form-control davivienda-select" name="city" id="city" value="{{ old('city') }}" placeholder="Seleccione la colonia...">
                            @include('modules._singleSelectOptGroup', [
                                'name' => 'city',
                                'options' => $lists['cities'],
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['address', 'code']),

                            ])
                        </select>
                        @if ($errors->has('city'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row hide-other-city">
                <div class="col-2 form-group"></div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('other_city') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'other_city',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['address', 'other_city']),
                            'placeholder' => 'Nombre de la otra colonia',
                            'label' => 'Nombre de la otra colonia',
                            'length' => '40',
                            'enable' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de domicilio</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address_1') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'address_1',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['address', 'first']),
                                    'placeholder' => 'Avenida, Calle, Bloque, Casa',
                                    'label' => 'Avenida, Calle, Bloque, Casa',
                                    'length' => '40',
                                    'required' => true,
                                    'enable' => true
                                ])
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address_2') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'address_2',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['address', 'second']),
                                    'placeholder' => 'Punto de referencia de la dirección',
                                    'label' => 'Punto de referencia de la dirección',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address_3') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'address_3',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['address', 'third']),
                                    'placeholder' => 'Segundo punto de referencia de la dirección',
                                    'label' => 'Segundo punto de referencia de la dirección',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Nacionalidad</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'nationality',
                            'placeholder' => 'Seleccione su nacionalidad...',
                            'options' => $lists['nations'],
                            'classes' => 'davivienda-select',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['nationality', 'code']),
                            'required' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Promoción</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-card"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('promotion') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'promotion',
                            'placeholder' => 'Seleccione la promoción...',
                            'options' => $lists['promotions'],
                            'classes' => 'davivienda-select',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['promotion', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Información de trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('job_type') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'job_type',
                            'placeholder' => 'Seleccione su actividad comercial...',
                            'options' => $lists['jobTypeOptions'],
                            'classes' => 'davivienda-select',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'job', 'type', 'code']),

                        ])
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group{{ $errors->has('income') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'income',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'income'),
                            'placeholder' => 'Ingrese su salario mensual',
                            'label' => 'Salario mensual',
                            'classes'  => 'format-amount',
                            'required' => 'required',

                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Lugar de trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'employer_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'name']),
                            'placeholder' => 'Nombre de la empresa',
                            'label' => 'Nombre de la empresa',
                            'length' => '40',
                            'required' => 'required',
                            'enable' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Tipo de empresa</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_type') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'employer_type',
                            'placeholder' => 'Seleccione el tipo de empresa...',
                            'options' => $lists['employeeTypes'],
                            'classes' => 'davivienda-select',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'type', 'code']),
                            'required' => true,
                            'enable' => true,
                        ])
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 form-group">
                    <h2>Fecha de ingreso al trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-calendar"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_day') ? ' has-error' : '' }}">
                        <div class="row birth-container">
                            <div class="col-4 birth-day">
                                @include('modules._singleSelect', [
                                    'name' => 'employer_day',
                                    'placeholder' => 'Día',
                                    'options' => $lists['dayOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'd'),
                                    'enable' => true,
                                ])
                            </div>
                            <div class="col-4 birth-month">
                                @include('modules._singleSelect', [
                                    'name' => 'employer_month',
                                    'placeholder' => 'Mes',
                                    'options' => $lists['monthOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'm'),
                                    'enable' => true,

                                ])
                            </div>
                            <div class="col-4 birth-year">
                                @include('modules._singleSelect', [
                                    'name' => 'employer_year',
                                    'options' => $lists['employerYearOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'Y'),
                                    'default' => $lists['selectedEmployerYear'],
                                    'placeholder' => 'Año',
                                    'enable' => true,
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Tipo de empleo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('job') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'job',
                            'placeholder' => 'Seleccione el tipo de empleo...',
                            'options' => $lists['jobOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'job', 'status', 'code']),
                            'required' => true,
                            'enable' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Situación Laboral</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('job_contract') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'job_contract',
                            'placeholder' => 'Seleccione su situación laboral...',
                            'options' => $lists['jobContractOptions'],
                            'classes' => 'davivienda-select',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'job', 'contract', 'code']),
                            'required' => true,
                            'enable' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Teléfono del trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-phone"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'employer_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'phone']),
                            'placeholder' => 'Ingrese el teléfono del trabajo',
                            'label' => 'Teléfono del trabajo',
                            'length' => '9',
                            'classes' => 'validate-num-dash',
                            'required' => true,
                            'enable' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Colonia de trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_city') ? ' has-error' : '' }}">
                        <select class="form-control davivienda-select" name="employer_city" id="employer_city" value="{{ old('employer_city') }}" placeholder="Seleccione la colonia...">
                            @include('modules._singleSelectOptGroup', [
                                    'name' => 'employer_city',
                                    'options' => $lists['cities'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['employer', 'address', 'code']),

                                ])
                        </select>
                        @if ($errors->has('employer_city'))
                            <span class="help-block">
                                <strong>{{ $errors->first('employer_city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row hide-other-employer-city">
                <div class="col-2 form-group"></div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('other_employer_city') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'other_employer_city',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'address', 'other_city']),
                            'placeholder' => 'Nombre de la otra colonia',
                            'label' => 'Nombre de la otra colonia',
                            'length' => '40',
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('employer_address_1') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'employer_address_1',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['employer', 'address', 'first']),
                                    'placeholder' => 'Avenida, Calle, Bloque, Casa',
                                    'label' => 'Avenida, Calle, Bloque, Casa',
                                    'length' => '40',
                                    'required' => true,
                                    'enable' => true
                                ])
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('employer_address_2') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'employer_address_2',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['employer', 'address', 'second']),
                                    'placeholder' => 'Punto de referencia de la dirección',
                                    'label' => 'Punto de referencia de la dirección',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('employer_address_3') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'employer_address_3',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['employer', 'address', 'third']),
                                    'placeholder' => 'Segundo punto de referencia de la dirección',
                                    'label' => 'Segundo punto de referencia de la dirección',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>¿Ha desempeñado un cargo público en el gobierno en los últimos 4 años?</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-10 form-group{{ $errors->has('public_job') ? ' has-error' : '' }}">
                    @include('modules._singleSelect', [
                        'name' => 'public_job',
                        'placeholder' => 'Seleccione la respuesta...',
                        'options' => $lists['confirmation'],
                            'classes' => 'davivienda-select',
                        'resource' => $flow,
                        'json' => get_json($flow->customer_information, ['public_job', 'code']),
                        'required' => true,
                    ])
                </div>
            </div>

            @include('modules._checkbox', [
                    'name' => 'bureau',
                    'label' => 'Autorizo a que el banco revise mi buró de crédito'
                ]
            )

            <div class="row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">
                        Solicitar aprobación
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>