<div class="col-12 identity-form">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('search.customer', ['bank_card']) }}" id="form">
        {!! csrf_field() !!}

        <div class="row">
            <div class="col-12 form-group">
                <h2>1. Seleccione la tarjeta</h2>
            </div>
        </div>

        <div class="row form-group">
            @include('modules._radio', [
                'name' => 'card',
                'options' => [
                    'DDP' => 'oro.png',
                    'DPT' => 'plata.png'
                ]
            ])
        </div>

        <div class="row">
            <div class="col-12 form-group">
                <h2>2. Digite el número de identidad</h2>
            </div>

            <div class="col-2 form-group text-center">
                <span class="davivienda-icon icon-id"></span>
            </div>

            <div class="col-10">
                <div class="form-group{{ $errors->has('identity') ? ' has-error' : '' }}">
                    @include('modules._textInput', [
                        'name' => 'identity',
                        'placeholder' => 'Ingrese el número de identificación',
                        'label' => 'Identidad',
                        'id' => 'identity',
                        'length' => '13',
                        'classes' => 'validate-num-dash',
                        'required' => 'required'
                    ])
                </div>
            </div>
        </div>

        <div class="row text-center">
            <div class="col-12">
                <button type="submit" class="btn btn-davivienda-red btn-loading" id="submit">
                    SIGUIENTE
                </button>
            </div>
        </div>
    </form>
</div>