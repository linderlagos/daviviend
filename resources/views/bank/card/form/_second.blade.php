@include('modules._progress', [
    'steps' => 2,
    'step' => 2,
    'title' => 'Formulario de vinculación'
])

<div class="row">
    <div class="col-12 identity-form">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.card.third') }}" id="form" autocomplete="off">
            {!! csrf_field() !!}

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Límite</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-card"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('customer_limit') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'customer_limit',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['product', 'limit']),
                            'placeholder' => 'Puede solicitar límite menor a ' . number_format(get_json($flow->product_information, ['product', 'limit']), 2),
                            'label' => 'Cambio de límite de la tarjeta',
                            'classes'  => 'format-customer-limit',
                            'value' => get_json($flow->product_information, ['product', 'limit']),
                            'required' => true,
                            'enable' => true
                        ])
                        <div class="customer-limit-message" id="customer-limit-message">
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de correo electrónico</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-messages"></span>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'email',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['email', 'account']),
                            'placeholder' => 'Ingrese su correo electrónico',
                            'label' => 'Correo eletrónico',
                            'length' => '40',
                            'required' => 'required',
                            'enable' => true,
                        ])
                    </div>
                </div>
                <div class="col-3 text-right">
                    <label for="has_email" style="font-size: 1.4rem; line-height: 1;">No tiene correo</label>
                </div>
                <div class="col-1 text-center davivienda-checkbox">
                    @include('modules._checkbox', [
                         'name' => 'has_email',
                         'json' => get_json($flow->customer_information, ['email', 'has_email']),
                         'label' => ''
                        ]
                    )
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Información personal</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'gender',
                            'placeholder' => 'Seleccione su género...',
                            'options' => $lists['genderOptions'],
                            'resource' => $flow,
                            'classes' => 'davivienda-select',
                            'json' => get_json($flow->customer_information, ['gender', 'code']),
                            'required' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('marital_status') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'marital_status',
                            'placeholder' => 'Seleccione su estado civil...',
                            'options' => $lists['maritalStatusOptions'],
                            'resource' => $flow,
                            'classes' => 'davivienda-select',
                            'json' => get_json($flow->customer_information, ['marital_status', 'code']),
                            'required' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row hide-marital-status">
                <div class="col-12 form-group">
                    <h2>Cónyuge</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('spouse') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'spouse',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'spouse'),
                            'placeholder' => 'Nombre completo del cónyuge',
                            'label' => 'Nombre completo del cónyuge',
                            'length' => '40'
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Profesión</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('profession') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'profession',
                            'placeholder' => 'Seleccione la profesión...',
                            'options' => $lists['professionOptions'],
                            'resource' => $flow,
                            'classes' => 'davivienda-select',
                            'json' => get_json($flow->customer_information, ['profession', 'code']),
                            'required' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Puesto de Trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('job_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'job_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'job', 'name']),
                            'placeholder' => 'Nombre del puesto de trabajo',
                            'label' => 'Nombre del puesto actual de trabajo',
                            'length' => '30',
                            'required' => true,
                            'enable' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de correo electrónico laboral</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-messages"></span>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('job_email') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'job_email',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['job_email', 'account']),
                            'placeholder' => 'Ingrese su correo laboral',
                            'label' => 'Correo laboral',
                            'length' => '40',
                            'required' => true
                        ])
                    </div>
                </div>
                <div class="col-3 text-right">
                    <label for="has_job_email" style="font-size: 1.4rem; line-height: 1;">No tiene correo</label>
                </div>
                <div class="col-1 text-center davivienda-checkbox">
                    @include('modules._checkbox', [
                         'name' => 'has_job_email',
                         'json' => get_json($flow->customer_information, ['job_email', 'has_job_email']),
                         'label' => ''
                        ]
                    )
                </div>
            </div>


            @if(get_json($flow->customer_information, ['public_job', 'code']) === 'S')
                <div class="row">
                    <div class="col-12 form-group">
                        <h2>Información de cargos públicos</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <span class="davivienda-icon icon-briefcase"></span>
                    </div>

                    <div class="col-5">
                        <div class="form-group{{ $errors->has('public_job_employer_name') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" name="public_job_employer_name"
                               @if(old('public_job_employer_name'))
                                   value="{{ old('public_job_employer_name') }}"
                               @elseif(get_json($flow->customer_information, ['public_job', 'employer_name']) !== 'NULO')
                                   value="{{ get_json($flow->customer_information, ['public_job', 'employer_name']) }}"
                                @elseif(get_json($flow->customer_information, ['employer', 'type']) == '2')
                                    value="{{ get_json($flow->customer_information, ['employer', 'name']) }}"
                               @endif
                                placeholder="Nombre de la Institución" maxlength="40" id="public_job_employer_name"
                               @if(get_json($flow->customer_information, ['public_job', 'employer_name']) !== 'NULO' && $flow->customer_status == 1)
                                    disabled
                                @endif

                            >
                            @if ($errors->has('public_job_employer_name'))
                                <label class="control-label error-label" for="public_job_employer_name">Nombre de la institución</label>
                                <span class="help-block">
                                    <strong>{{ $errors->first('public_job_employer_name') }}</strong>
                                </span>
                            @else
                                <label class="control-label" for="public_job_employer_name">Nombre de la institución</label>
                            @endif
                        </div>
                    </div>

                    <div class="col-5">
                        <div class="form-group{{ $errors->has('public_job_name') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'public_job_name',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['public_job', 'job']),
                                'placeholder' => 'Cargo desempeñado',
                                'label' => 'Cargo desempeñado',
                                'length' => '30'
                            ])
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-2"></div>

                    <div class="col-2">
                        <div class="form-group{{ $errors->has('public_job_active') ? ' has-error' : '' }}">
                            @include('modules._singleSelect', [
                                'name' => 'public_job_active',
                                'placeholder' => '¿Es actual?',
                                'options' => $lists['confirmation'],
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['public_job', 'active']),

                            ])
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group{{ $errors->has('public_job_from') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'public_job_from',
                                'resource' => $flow,
                                'json' => get_json_date(get_json($flow->customer_information, ['public_job', 'from']), 'd/m/Y'),
                                'placeholder' => 'Desde DD/MM/AAAA',
                                'label' => 'Fecha de inicio',
                                'length' => '10',
                                'classes' => 'date',
                                'autocomplete' => 'No'
                            ])
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group{{ $errors->has('public_job_to') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'public_job_to',
                                'resource' => $flow,
                                'json' => get_json_date(get_json($flow->customer_information, ['public_job', 'to']), 'd/m/Y'),
                                'placeholder' => 'Hasta DD/MM/AAAA',
                                'label' => 'Fecha final',
                                'length' => '10',
                                'classes' => 'hide-public-job-active date',
                                'autocomplete' => 'No'
                            ])
                        </div>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Información financiera</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('assets') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'assets',
                            'placeholder' => 'Total de activos',
                            'options' => $lists['assetRangeOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['assets', 'code']),
                            'required' => true,
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('passive') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'passive',
                            'placeholder' => 'Total de pasivos',
                            'options' => $lists['liabilityRangeOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['passive', 'code']),
                            'required' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('expenses') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'expenses',
                            'placeholder' => 'Total de egresos',
                            'options' => $lists['expenseRangeOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['expenses', 'code']),
                            'required' => true,
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('other_income') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'other_income',
                            'placeholder' => '¿Tiene otros ingresos?',
                            'options' => $lists['confirmation'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['other_income', 'code']),
                            'required' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Referencia familiar</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('reference_1_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'name']),
                            'placeholder' => 'Nombre de la referencia familiar',
                            'label' => 'Nombre de la referencia familiar',
                            'length' => '40',
                            'required' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_relationship') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'reference_1_relationship',
                            'placeholder' => 'Seleccione el parentesco...',
                            'options' => $lists['referenceOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'relationship', 'code']),
                            'required' => true,
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_mobile') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_mobile',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'mobile']),
                            'placeholder' => 'Celular',
                            'label' => 'Celular',
                            'length' => '9',
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'phone']),
                            'placeholder' => 'Teléfono',
                            'label' => 'Teléfono de domicilio',
                            'length' => '9',
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_work_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_work_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'work_phone']),
                            'placeholder' => 'Teléfono de trabajo',
                            'label' => 'Teléfono de trabajo',
                            'length' => '9',
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Referencia personal</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('reference_2_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'name']),
                            'placeholder' => 'Nombre de la referencia personal',
                            'label' => 'Nombre de la referencia personal',
                            'length' => '40',
                            'required' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_mobile') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_mobile',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'mobile']),
                            'placeholder' => 'Celular',
                            'label' => 'Celular',
                            'length' => '9',
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'phone']),
                            'placeholder' => 'Teléfono',
                            'label' => 'Teléfono de domicilio',
                            'length' => '9',
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_work_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_work_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'work_phone']),
                            'placeholder' => 'Teléfono de trabajo',
                            'label' => 'Teléfono de trabajo',
                            'length' => '9',
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Comentarios adicionales:</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon far fa-comment"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
                        @include('modules._textArea', [
                            'name' => 'notes',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['notes', 'full']),
                            'placeholder' => 'Ingrese comentarios adicionales que considere necesarios para esta solicitud',
                            'label' => 'Comentarios adicioanles',
                            'length' => '240',
                        ])
                    </div>
                </div>
            </div>

            {{-- TODO: Add extra space after inputs and before checkboxes --}}
            <hr style="border: none;" class="mb-5">

            @include('modules._checkbox', [
                'name' => 'receive_sms',
                'json' => get_json($flow->customer_information, ['receive_sms', 'code']),
                'label' => 'Acepto envíos de mensajes a mi celular',
                'checked' => true
              ]
            )

            <div class="hide-if-no-email">
                @include('modules._checkbox', [
                         'name' => 'receive_emails',
                         'json' => get_json($flow->customer_information, ['receive_emails', 'code']),
                         'label' => 'Acepto envíos de estados de cuenta a mi correo electrónico personal',
                         'checked' => true
                     ]
                )
            </div>

            <div class="hide-if-no-job-email">
                @include('modules._checkbox', [
                        'name' => 'receive_emails_job',
                        'json' => get_json($flow->customer_information, ['receive_emails_job', 'code']),
                        'label' => 'Acepto envíos de estados de cuenta a mi correo electrónico laboral',
                        'checked' => true
                    ]
                )
            </div>


            <div class="row">
                <div class="col-12 inner-form-disclaimer text-center">
                    Declaro que he suministrado de forma voluntaria la información personal ingresada para el uso de este servicio, y autorizo a Davivienda para que utilice los datos que he proporcionado con el propósito de hacer entrega a mi solicitud de Tarjeta de Crédito.
                </div>
            </div>

            <div class="row davivienda-checkbox form-group terms align-items-center">
                <div class="col-2 text-center">
                    <input id="terms" checked="checked" name="terms" type="checkbox" value="S" required>
                </div>
                <div class="col-10">
                    <label class="standard-label" for="terms">Con este clic está autorizando el tratamiento de sus datos según el fin informado</label>
                    @if ($errors->has('terms'))
                        <span class="help-block">
                            <strong>{{ $errors->first('terms') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">
                        VINCULAR CLIENTE
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>