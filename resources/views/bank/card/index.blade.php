@extends('layouts.form')

@section('content')

<?php 

/* $flows = (object) array('flow'=>$flow, 'customer'=>$flow->customer->identifier); */
/* $flows = new stdObject(); */
/* 
[
    {'flow' -> $flow}, 
    {'customer'->$flow->customer->identifier}
];  */
/* $ordenar = json_encode($lists); */
//$ordenado = sort($ordenar)
//echo $ordenar;
//$lista = sort($ordenar);
//echo "Lista: ".json_encode($lists);

?>
 <index-card-component :lists="{{ json_encode($lists) }}" :flow="{{ json_encode($flow) }}"></index-card-component> 
@stop


{{-- @section('meta')
    <link href="{{ asset('css/plugins/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
@stop

@section('banner')
    @if(checkStep($flow, ['1']))
        @include('modules._banner', [
            'image' => asset('design/' . get_json($flow->product_information, ['product', 'code']) . '.png'),
            'title' => 'Formulario de solicitud de Tarjeta de Crédito',
            'description' => '<p>Complete el formulario de solicitud de ' . get_json($flow->product_information, ['product', 'name']) . ' con los datos del cliente<br> ID No. ' . $flow->customer->identifier .'</p>'
        ])
    @elseif(checkStep($flow, ['2']))
        @include('modules._banner', [
            'class' => 'success-banner',
            'bgColor' => '#F7981B',
            'image' => asset('design/success.png'),
            'title' => 'Su solicitud de Tarjeta de Crédito ha sido aprobada',
            'description' => '<p style="font-size: 2.5rem">Se ha aprobado una <b>' . get_json($flow->product_information, ['product', 'name']) . ' ' . get_json($flow->product_information, ['product', 'type']) . '</b> con un límite de <span style="white-space: nowrap">L ' . number_format(get_json($flow->product_information, ['product', 'limit']) !== 'NULO' ? get_json($flow->product_information, ['product', 'limit']) : 0, 2) . '</span> del cliente con ID No. ' . $flow->customer->identifier .'</p>'
        ])
    @else
        @include('modules._banner', [
            'title' => 'Formulario de solicitud de Tarjeta de Crédito',
            'description' => '<p>Para ingresar una nueva solicitud de Tarjeta de Crédito completa la siguiente información</p>'
        ])
    @endif
@stop

@section('back')
    @if(!checkStep($flow, ['1', '2']))
        <a href="{{ route('index') }}" class="btn-back">
            Inicio 
        </a>
    @endif
@stop
 --}}
{{-- 
@section('reject')
    @if(checkStep($flow, ['1']))
        @include('modules._reject', [
            'route' => route('exit.flow', ['bank_card']),
            'options' => [
                'TA01' => 'Asesor'
            ]
        ])
    @elseif (checkStep($flow, ['2']))
        @include('modules._reject', [
            'route' => route('exit.flow', ['bank_card', 'entailment_exit_process']),
            'options' => [
                'TU15' => 'Solicitado por el cliente',
                'TA01' => 'Asesor'
            ]
        ])
    @endif
@stop

@section('content')

    @if(checkStep($flow, '1'))
        @include('bank.card.form._first')
    @elseif(checkStep($flow, '2'))
        @include('bank.card.form._second')
    @else
        @include('bank.card.form._search')
    @endif --}}

{{-- @section('content') --}}

{{-- <index-card-component :lists="{{ json_encode($lists) }}" :flow="{{ json_encode($flow) }}"></index-card-component>  --}}
{{-- @endsection
--}}
{{-- 
@section('scripts')
    @include('partials._scripts')
    @include('bank.card.partials._scripts')
@stop  --}}