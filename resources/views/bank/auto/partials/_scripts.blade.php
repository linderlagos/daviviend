<script type="text/javascript" src="{{ asset('plugins/datatables/datatables.js') }}"></script>
    <script>
        $(document).ready( function () {
            let table = $('#auto-flows-in-transit').DataTable({
                // "info":     false,
                // responsive: true,
                "lengthChange": false,
                "pageLength": 25,
                "order": [[ 4, "desc" ]],
                "dom": 'Bfrtip',
                "buttons": [],
                "pagingType": "full_numbers",
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "<<",
                        "sLast":     ">>",
                        "sNext":     ">",
                        "sPrevious": "<"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        } );
    </script>
<script>
    $(document).ready(function () {

    @if(checkStep($flow, '1'))
        let job = $('#job');
        let jobType = $('#job_type');

        jobType.prop('disabled', 'disabled');

        if (jobType.val().length > 0) {
            jobType.prop('disabled', false);
        }

        job.change(function () {
            return getJobTypeOptions(job);
        });

        function getJobTypeOptions(job)
        {
            if (job.val().length > 0)
            {
                getList('jobTypeOptionsByJob', jobType, job.val(), 'Seleccione la actividad comercial...');

                return jobType.prop('disabled', 'disabled');
            }
        }
    @endif

    @if(checkStep($flow, '2'))
        let type = $('#type_auto');
        let year = $('#year_auto');
        let status = $('#status_auto');
        let term = $('#financing_term');

        term.prop('disabled', 'disabled');
        year.prop('disabled', 'disabled');

        if (term.val().length > 0) {
            term.prop('disabled', false);
        }

        if (year.val().length > 0) {
            year.prop('disabled', false);
        }

        type.change(function () {
            return getOptions(type, year, status);
        });

        year.change(function () {
            return getOptions(type, year, status);
        });

        status.change(function () {
            getYearOptions(status);

            return getOptions(type, year, status);
        });

        function getYearOptions(status)
        {
            if (status.val().length > 0)
            {
                if (status.val() == '016')
                {
                    getList('yearAutoOptions', year, '', 'Seleccione el año');
                } else {
                    getList('usedYearAutoOptions', year, '', 'Seleccione el año');
                }

                return year.prop('disabled', 'disabled');
            }
        }

        function getOptions(type, year, status)
        {
            if (type.val().length > 0 && year.val().length > 0 && status.val().length > 0)
            {
                let d = new Date();
                let n = d.getFullYear();

                if (status.val() === '016')
                {
                    // Si estado nuevo = 72 meses
                    getList('financingTermOptions', term, '', 'Seleccione el plazo');
                } else if (status.val() === '017' && year.val() >= (n - 4)) {
                    // Si estado usado && 5 año recientes = 60 meses
                    getList('financingTermOptionsFive', term, '', 'Seleccione el plazo');
                } else {
                    // Si estado usado && 5 año viejos = 48 meses
                    getList('financingTermOptionsFour', term, '', 'Seleccione el plazo');
                }

                return term.prop('disabled', 'disabled');
            }
        }
    @endif

        function getList(list, element, key = null, placeholder = null) {
            $.ajax({
                url     : 'generar-lista',
                type    : 'GET',
                data    : { flow: 'bank_auto', list_key : list, key : key, placeholder : placeholder },
                element : element,
                dataType: 'json',
                success : onSuccessApprovalProcess,
                error: onErrorApprovalProcess
            });
        }

        function onSuccessApprovalProcess(data, status, request) {

            this.element.html(data.list);

            return this.element.prop('disabled', false);
        }

        function onErrorApprovalProcess(json) {
            let status = json.status;

            let data = json.responseJSON;

            console.log('no hay lista')
        }
    });
</script>

@if(checkStep($flow, '2'))
    <script>
        function postForm(formId, buttonId, buttonText)
        {
            let form = $(formId);

            form.one('submit', function(e) {

                e.preventDefault();

                let button = $(buttonId);

                button.html('<i class="fas fa-spinner fa-spin"></i> ENVIANDO...');
                button.prop('disabled',true);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url     : form.attr('action'),
                    type    : form.attr('method'),
                    data    : form.serialize(),
                    formId: formId,
                    buttonId: buttonId,
                    buttonText: buttonText,
                    dataType: 'json',
                    success : onSuccessApprovalProcess,
                    error: onErrorApprovalProcess
                });
            });
        }

        function onSuccessApprovalProcess(data, status, request) {

            let formContainer = $('#calculator-form-container');

            let container = $('#calculator-result-container');

            if (this.formId == '#recalculate-form')
            {
                container.slideUp();

                container.html('');

                formContainer.slideDown();
            } else {
                formContainer.slideUp();

                container.html(data.view);

                container.slideDown();
            }

            $(this.buttonId).html(this.buttonText);

            $(this.buttonId).prop('disabled',false);

            // container.fadeIn("slow").delay(4000).fadeOut("slow", function () {
            //     replaceView(data);
            // });
        }

        function onErrorApprovalProcess(json) {
            let status = json.status;

            let data = json.responseJSON;

            if (status === 401 || status === 500)
            {
                // replaceView(data)
                swal({
                    title: "¡OH NO!",
                    text: "Se ha presentado el siguiente error: " + data.error,
                    icon: "error",
                    button: "Continuar",
                });
            }  else if (json.status === 403) {
                window.location.replace(data.route);
            } else if (json.status === 422) {
                let form = $(this.formId);

                $(this.buttonId).html(this.buttonText);
                $(this.buttonId).prop('disabled', false);

                let generalErrorMessage = $('.general-message-error');

                generalErrorMessage.html('<i class="fas fa-exclamation-circle"></i> <strong>Se han presentado algunos errores en el formulario. Por favor corregirlos para continuar</strong>');

                generalErrorMessage.fadeIn('slow');

                $.each(data.errors, function (parameter, value) {
                    $.each(value, function (key, error) {
                        if (key === 0)
                        {
                            $('.'+ parameter +'-error').html('<strong>' + error + '</strong><br>');
                            $('.'+ parameter +'-error').fadeIn("slow");
                        }

                        return false;
                    })
                });
            }
        }

        function replaceView(data) {

            let container = $('#calculator-form-container');

            container.fadeOut("slow", function() {
                container.html(data.view);
            });

            container.fadeIn("slow");
        }
    </script>
@endif

<script>
    $(document).ready(function() {
        $('.hide-deduction, .hide-appraisal, .enable').hide();

        $("#enable").prop('disabled',true);

        ///ocultar deducciones
        if ($('#job option:selected').val() == '1') {
            $('.hide-deduction').fadeIn();
        }

        $('#job').change(function () {
            if ($('#job option:selected').val() == '1') {
                $('.hide-deduction').fadeIn();
            } else {
                $('.hide-deduction').fadeOut(); 
                $('.job-deductions').val(0);
            }
        });

        if ($('#status_auto option:selected').val() == '017') {
            $('.hide-appraisal').fadeIn();
        }

        $('#status_auto').change(function () {
            if ($('#status_auto option:selected').val() == '017') {
                $('.hide-appraisal').fadeIn();
            } else {
                $('.hide-appraisal').fadeOut();
                $('#appraisal').val(0);
            }
        });
    });

</script>
