@include('modules._progress', [
    'steps' => 4,
    'step' => 4,
    'title' => 'Observaciones'
])

<div class="row">
    <div class="col-12 identity-form">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.auto.traditional') }}" id="form">
            {!! csrf_field() !!}

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Ingrese Observaciones:</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon far fa-comment"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
                        @include('modules._textArea', [
                            'name' => 'notes',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['notes', 'full']),
                            'placeholder' => 'Ingrese las observaciones que considere necesarios para esta solicitud',
                            'label' => 'Comentarios adicioanles',
                            'length' => '240',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">
                        FINALIZAR SOLICITUD
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>