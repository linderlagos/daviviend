<form class="form-horizontal" role="form" method="POST" action="{{ route('bank.auto.calculate') }}" id="calculator-form">
    {!! csrf_field() !!}

    <div class="row">

        <div class="col-2 form-group text-center">
            <span class="davivienda-icon fas fa-building"></span>
        </div>

        <div class="col-10">
            <div class="row">
                <div class="col-6">
                    <div class="form-group{{ $errors->has('concessionaire') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'concessionaire',
                            'placeholder' => 'Concesionaria...',
                            'options' => $lists['concessionaireOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->user_information, ['concessionaire', 'code']),
                            'required' => true,
                            'classes' => 'davivienda-select',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('seller') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'seller',
                            'placeholder' => 'Vendedor...',
                            'options' => $lists['sellerOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->user_information, ['seller', 'code']),
                            'classes' => 'davivienda-select',
                            'required' => true,
                            'enable' => true

                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2>Vehículo</h2>
        </div>


        <div class="col-2 form-group text-center">
            <span class="davivienda-icon fas fa-car"></span>
        </div>

        <div class="col-10">
            <div class="row">
                <div class="col-4">
                    <div class="form-group{{ $errors->has('type_auto') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'type_auto',
                            'placeholder' => 'Tipo de vehículo...',
                            'options' => $lists['typeAutoOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'type', 'code']),
                            'required' => true,
                            'classes' => 'davivienda-select',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group{{ $errors->has('status_auto') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'status_auto',
                            'placeholder' => 'Estado de vehículo...',
                            'options' => $lists['statusAutoOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'status', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group{{ $errors->has('year_auto') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'year_auto',
                            'placeholder' => 'Año del vehículo...',
                            'options' => $lists['yearAutoOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'year', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-2 form-group text-center">
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('auto_brand') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'auto_brand',
                    'placeholder' => 'Marca del vehículo...',
                    'options' => $lists['autoBrandOptions'],
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['auto', 'brand', 'code']),
                    'required' => true,
                    'classes' => 'davivienda-select',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('auto_model') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'auto_model',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['auto', 'model']),
                    'placeholder' => 'Modelo de vehículo',
                    'label' => 'Modelo de vehículo',
                    'required' => true,
                    'length' => '40',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2>Préstamo</h2>
        </div>

        <div class="col-2 form-group text-center">
            <span class="davivienda-icon fas fa-money-check"></span>
        </div>

        <div class="col-10">
            <div class="row">
                <div class="col-6">
                    <div class="form-group{{ $errors->has('sale_price') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'sale_price',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, 'sale_price'),
                            'placeholder' => 'Digite el precio',
                            'label' => 'Precio de venta',
                            'classes' => 'format-amount',
                            'autocomplete' => 'No',
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('premium') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'premium',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, 'premium'),
                            'placeholder' => 'Digite la prima',
                            'label' => 'Prima a pagar',
                            'classes' => 'format-amount',
                            'length' => '9',
                            'autocomplete' => 'No',
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row hide-appraisal">
                <div class="col-12">
                    <div class="form-group{{ $errors->has('appraisal') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'appraisal',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, 'appraisal'),
                            'placeholder' => 'Digite el precio de avalúo',
                            'label' => 'Precio de avalúo',
                            'classes' => 'format-amount',
                            'autocomplete' => 'No',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    <div class="form-group{{ $errors->has('financing_term') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'financing_term',
                            'placeholder' => 'Plazo de financiamiento...',
                            'options' => $lists['financingTermOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['financing_term', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group{{ $errors->has('closing_costs') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'closing_costs',
                            'placeholder' => 'Gastos de cierre...',
                            'options' => $lists['confirmation'],
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['closing_costs', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group{{ $errors->has('grace_period') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'grace_period',
                            'placeholder' => 'Período de gracia...',
                            'options' => $lists['confirmation'],
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['grace_period', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit-calculator" onclick="postForm('#calculator-form', '#submit-calculator', 'CALCULAR')">
                CALCULAR
            </button>
        </div>
    </div>
</form>