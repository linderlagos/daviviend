<div class="row">
    <div class="col-12">
        <table id="auto-flows-in-transit" class="davivienda-datatables">
            <thead>
                <tr>
{{--                    <th>Cliente</th>--}}
                    <th>Solicitud No.</th>
                    <th>Tipo</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Estado</th>
                    <th>Año</th>
                    <th>Valor a financiar</th>
                    <th>Precio</th>
                    <th>Fecha</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                @foreach($lists as $list)
                    <tr>
{{--                        <td class="text-center">{{ get_json($flow->customer_information, ['name', 'fullname']) }}</td>--}}
                        <td class="text-center">{{ $list['numero_solicitud'] }}</td>
                        <td class="text-center">{{ $list['tipo_auto'] }}</td>
                        <td class="text-center">{{ $list['marca_auto'] }}</td>
                        <td class="text-center">{{ $list['modelo_auto'] }}</td>
                        <td class="text-center">{{ $list['estado_auto'] }}</td>
                        <td class="text-center">{{ $list['anho_auto'] }}</td>
                        <td class="text-center">{{ $list['valor_financiar'] }}</td>
                        <td class="text-center">{{ $list['precio_venta'] }}</td>
                        <td class="text-center">{{ $list['fecha_escenario'] }}</td>
                        {{--<td class="text-center">{{ $list['hora'] }}</td>--}}
                        <td class="text-center" style="padding: 5px 20px;">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.auto.continue', [$flow->id, $list['correlativo_escenario'], $list['respuesta_motor_decisiones_credito']]) }}">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-query">
                                    @if( $list['respuesta_motor_decisiones_credito'] == "T")
                                        Completar Tradicional <i class="fas fa-chevron-right"></i>
                                    @else
                                        Completar Proforma <i class="fas fa-chevron-right"></i>
                                    @endif
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="col-12">
        <form class="form-horizontal" role="form" method="POST" action="{{ checkStep($flow, ['1']) ? route('bank.auto.start') : route('bank.auto.new') }}" id="form">
            {!! csrf_field() !!}

            <div class="row">
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-davivienda-red btn-loading" id="submit">
                        Crear nueva cotización
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>