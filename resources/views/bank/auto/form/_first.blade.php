@include('modules._progress', [
    'steps' => 4,
    'step' => 1,
    'title' => 'Información general'
])

<form class="form-horizontal" role="form" method="POST" action="{{ route('bank.auto.second') }}" id="form" >
    {!! csrf_field() !!}

    <div class="row">
        <div class="col-12 form-group">
            <h2>Fecha de nacimiento de cliente</h2>
        </div>

        <div class="col-2 form-group text-center">
            <span class="davivienda-icon icon-calendar"></span>
        </div>

        <div class="col-10">
            <div class="form-group{{ $errors->has('day') ? ' has-error' : '' }}">
                <div class="row birth-container">
                    <div class="col-4 birth-day">
                        @include('modules._singleSelect', [
                            'name' => 'day',
                            'placeholder' => 'Día',
                            'options' => $lists['dayOptions'],
                            'resource' => $flow,
                            'json' => get_json_date(get_json($flow->customer_information,'birth'), 'd'),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                    <div class="col-4 birth-month">
                        @include('modules._singleSelect', [
                            'name' => 'month',
                            'placeholder' => 'Mes',
                            'options' => $lists['monthOptions'],
                            'resource' => $flow,
                            'json' => get_json_date(get_json($flow->customer_information,'birth'), 'm'),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                    <div class="col-4 birth-year">
                        @include('modules._singleSelect', [
                            'name' => 'year',
                            'options' => $lists['yearOptions'],
                            'resource' => $flow,
                            'json' => get_json_date(get_json($flow->customer_information,'birth'), 'Y'),
                            'default' => $lists['selectedYear'],
                            'placeholder' => 'Año',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2>Nombres</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user"></i>
        </div>

        <div class="col-5 first-name-field">
            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'first_name',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, ['name', 'first']),
                    'placeholder' => 'Primer nombre',
                    'label' => 'Primer nombre',
                    'length' => '15',
                    'required' => true,
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-5 middle-name-field">
            <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'middle_name',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, ['name', 'middle']),
                    'placeholder' => 'Segundo nombre',
                    'label' => 'Segundo nombre',
                    'length' => '15',
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-12 form-group">
            <h2>Apellidos</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user"></i>
        </div>

        <div class="col-5 last-name-field">
            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'last_name',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, ['name', 'last']),
                    'placeholder' => 'Primer apellido',
                    'label' => 'Primer apellido',
                    'length' => '15',
                    'required' => true,
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>
        <div class="col-5 second-last-name-field">
            <div class="form-group{{ $errors->has('second_last_name') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_last_name',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, ['name', 'second_last']),
                    'placeholder' => 'Segundo apellido',
                    'label' => 'Segundo apellido',
                    'length' => '15',
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2>Números de contacto</h2>
        </div>

        <div class="col-2 form-group text-center">
            <span class="davivienda-icon icon-phone"></span>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'phone',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, 'phone'),
                    'placeholder' => 'Teléfono de domicilio',
                    'label' => 'Teléfono de domicilio',
                    'length' => '9',
                    'classes' => 'validate-num-dash',
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>

        <div class="col-5">
            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'mobile',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, 'mobile'),
                    'placeholder' => 'Celular',
                    'label' => 'Celular',
                    'length' => '9',
                    'classes' => 'validate-num-dash',
                    'required' => true,
                    'autocomplete' => 'No',
                    'enable' => true
                ])
            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-12 form-group">
            <h2>Información financiera</h2>
        </div>

        <div class="col-2 form-group text-center">
            <span class="davivienda-icon fas fa-money-bill"></span>
        </div>

        <div class="col-10">
            <div class="row">
                <div class="col-6">
                    <div class="form-group{{ $errors->has('job') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'job',
                            'placeholder' => 'Tipo de empleo...',
                            'options' => $lists['jobOptions'],
                            'classes' => 'davivienda-select',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'job', 'status', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('income') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'income',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'income'),
                            'placeholder' => 'Digite el salario',
                            'label' => 'Salario mensual bruto',
                            'classes' => 'format-amount',
                            'length' => '6',
                            'autocomplete' => 'No',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">

                    <div class="form-group{{ $errors->has('job_type') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'job_type',
                            'placeholder' => 'Actividad comercial...',
                            'options' => $lists['jobTypeOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'job', 'type', 'code']),
                            'required' => true,
                            'classes' => 'davivienda-select',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row hide-deduction">
        <div class="col-12 form-group">
            <h2>Deducciones</h2>
        </div>

        <div class="col-2 form-group text-center">
            <span class="davivienda-icon fas fa-money-bill"></span>
        </div>

        <div class="col-10">
            <div class="row">
                <div class="col-6">
                    <div class="form-group{{ $errors->has('isr_deduction') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'isr_deduction',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'isr_deduction'),
                            'placeholder' => 'Deducción ISR',
                            'label' => 'Impuesto Sobre la Renta',
                            'classes' => 'format-amount job-deductions',
                            'autocomplete' => 'No',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('rap_deduction') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'rap_deduction',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'rap_deduction'),
                            'placeholder' => 'Deducción RAP',
                            'label' => 'Régimen de Aportación Privada',
                            'classes' => 'format-amount job-deductions',
                            'autocomplete' => 'No',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group{{ $errors->has('ihss_deduction') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'ihss_deduction',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'ihss_deduction'),
                            'placeholder' => 'Deducción IHSS',
                            'label' => 'Instituto Hondureño de Seguridad Social',
                            'classes' => 'format-amount job-deductions',
                            'autocomplete' => 'No',
                            'enable' => true
                        ])
                    </div>
                </div>

                {{--<div class="col-6">--}}
                    {{--<div class="form-group{{ $errors->has('other_deduction') ? ' has-error' : '' }}">--}}
                        {{--@include('modules._textInput', [--}}
                            {{--'name' => 'other_deduction',--}}
                            {{--'resource' => $flow,--}}
                            {{--'json' => get_json($flow->customer_information, 'other_deduction'),--}}
                            {{--'placeholder' => 'Otras deducciones',--}}
                            {{--'label' => 'Otras deducciones',--}}
                            {{--'classes' => 'format-amount',--}}
                            {{--'autocomplete' => 'No',--}}
                            {{--'enable' => true--}}
                        {{--])--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>


        @include('modules._checkbox', [
            'name' => 'receive_sms',
            'json' => get_json($flow->customer_information, ['receive_sms', 'code']),
            'label' => 'Acepto envíos de mensajes a mi celular',
            'checked' => true
          ]
        )


    <div class="row text-center">
        <div class="col-12">
            <button type="submit" class="btn btn-davivienda-red btn-loading" id="submit">
                SIGUIENTE
            </button>
        </div>
    </div>
</form>