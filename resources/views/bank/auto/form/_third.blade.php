@include('modules._progress', [
    'steps' => 4,
    'step' => 3,
    'title' => 'Información general'
])

<div class="row">
    <div class="col-12 identity-form">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.auto.fourth') }}" id="form">
            {!! csrf_field() !!}

            <div class="row">
                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon fas fa-building"></span>
                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group{{ $errors->has('concessionaire') ? ' has-error' : '' }}">
                                @include('modules._singleSelect', [
                                    'name' => 'concessionaire',
                                    'placeholder' => 'Concesionaria...',
                                    'options' => $lists['concessionaireOptions'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->user_information, ['concessionaire', 'code']),

                                ])
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group{{ $errors->has('seller') ? ' has-error' : '' }}" >
                                @include('modules._singleSelect', [
                                    'name' => 'seller',
                                    'placeholder' => 'Vendedor...',
                                    'options' => $lists['sellerOptions'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->user_information, ['seller', 'code']),

                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de correo electrónico</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-messages"></span>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'email',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['email', 'account']),
                            'placeholder' => 'Ingrese su correo electrónico',
                            'label' => 'Correo eletrónico',
                            'length' => '40',
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
                <div class="col-3 text-right">
                    <label for="has_email" style="font-size: 1.4rem; line-height: 1;">No tiene correo</label>
                    {{--@if ($errors->has('has_email'))--}}
                        {{--<span class="help-block">--}}
                            {{--<strong>{{ $errors->first('has_email') }}</strong>--}}
                        {{--</span>--}}
                    {{--@endif--}}
                </div>
                <div >
                    {{--<input id="has_email" name="has_email" type="checkbox" value="N">--}}
                    @include('modules._checkbox', [
                         'name' => 'has_email',
                         'json' => get_json($flow->customer_information, ['email', 'has_email']),
                         'label' => ''
                     ]
                )
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Colonia de domicilio</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        <select class="form-control davivienda-select" name="city" id="city" value="{{ old('city') }}" placeholder="Seleccione la colonia..." required>
                            @include('modules._singleSelectOptGroup', [
                                'name' => 'city',
                                'options' => $lists['cities'],
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['address', 'code']),
                                'enable' => true
                            ])
                        </select>
                        @if ($errors->has('city'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row hide-other-city">
                <div class="col-2 form-group"></div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('other_city') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'other_city',
                            'resource' => $flow,
                            'placeholder' => 'Nombre de la otra colonia',
                            'json' => get_json($flow->customer_information, ['address', 'other_city']),
                            'label' => 'Nombre de la otra colonia',
                            'length' => '40',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de domicilio</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address_1') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'address_1',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['address', 'first']),
                                    'placeholder' => 'Avenida, Calle, Bloque, Casa',
                                    'label' => 'Avenida, Calle, Bloque, Casa',
                                    'length' => '40',
                                    'required' => true,
                                    'enable' => true
                                ])
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address_2') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'address_2',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['address', 'second']),
                                    'placeholder' => 'Punto de referencia de la dirección',
                                    'label' => 'Punto de referencia de la dirección',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address_3') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'address_3',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['address', 'third']),
                                    'placeholder' => 'Segundo punto de referencia de la dirección',
                                    'label' => 'Segundo punto de referencia de la dirección',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 form-group">
                    <h2>Información Laboral</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-briefcase"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'employer_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'name']),
                            'placeholder' => 'Nombre de la empresa',
                            'label' => 'Nombre de la empresa',
                            'length' => '40',
                            'autocomplete' => 'No',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-2"></div>

                    <div class="col-5">
                            <div class="form-group{{ $errors->has('profession') ? ' has-error' : '' }}">
                                @include('modules._singleSelect', [
                                    'name' => 'profession',
                                    'placeholder' => 'Seleccione la profesión...',
                                    'options' => $lists['professionOptions'],
                                    'classes' => 'davivienda-select',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['profession', 'code']),
                                    'required' => true,
                                    'enable' => true
                                ])
                            </div>
                    </div>


                    <div class="col-5">
                        <div class="form-group{{ $errors->has('job_name') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'job_name',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['employer', 'job', 'name']),
                                'placeholder' => 'Nombre del puesto de trabajo',
                                'label' => 'Nombre del puesto actual de trabajo',
                                'required' => true,
                                'length' => '20',
                                'enable' => true
                            ])
                        </div>
                    </div>

            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('employer_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'employer_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'phone']),
                            'placeholder' => 'Ingrese el teléfono del trabajo',
                            'label' => 'Teléfono del trabajo',
                            'length' => '9',
                            'classes' => 'validate-num-dash',
                            'enable' => true
                        ])
                    </div>
                </div>

            @if (get_json($flow->customer_information, ['employer', 'job', 'status', 'code']) == '1')
                <div class="col-5">
                    <div class="form-group{{ $errors->has('job_contract') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'job_contract',
                            'placeholder' => 'Seleccione su situación laboral...',
                            'options' => $lists['jobContractOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'job', 'contract', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            @endif
            </div>



            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de correo electrónico laboral</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-messages"></span>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('job_email') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'job_email',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['job_email', 'account']),
                            'placeholder' => 'Ingrese su correo laboral',
                            'label' => 'Correo laboral',
                            'length' => '40',
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
                <div class="col-3 text-right">
                    <label for="has_job_email" style="font-size: 1.4rem; line-height: 1;">No tiene correo</label>
                </div>
                <div class="col-1 text-center davivienda-checkbox">
                    @include('modules._checkbox', [
                         'name' => 'has_job_email',
                         'json' => get_json($flow->customer_information, ['job_email', 'has_job_email']),
                         'label' => ''
                     ]
                )
                </div>
            </div>

            <div class="row show-if-independent">
                <div class="col-12 form-group">
                    <h2>Tipo de empresa</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-building"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_type') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'employer_type',
                            'placeholder' => 'Seleccione el tipo de empresa...',
                            'options' => $lists['employeeTypes'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['employer', 'type', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row show-if-independent">
                    <div class="col-12 form-group">
                        <h2>Colonia de trabajo</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <span class="davivienda-icon icon-gps"></span>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('employer_city') ? ' has-error' : '' }}">
                            <select class="form-control davivienda-select" name="employer_city" id="employer_city" value="{{ old('employer_city') }}" placeholder="Seleccione la colonia...">
                                @include('modules._singleSelectOptGroup', [
                                        'name' => 'employer_city',
                                        'options' => $lists['cities'],
                                        'resource' => $flow,
                                        'json' => get_json($flow->customer_information, ['employer', 'address', 'code']),
                                        'required' => true,
                                        'enable' => true
                                    ])
                            </select>
                            @if ($errors->has('employer_city'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('employer_city') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row hide-other-employer-city">
                    <div class="col-2 form-group"></div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('other_employer_city') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'other_employer_city',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['employer', 'address', 'other_city']),
                                'placeholder' => 'Nombre de la otra colonia',
                                'label' => 'Nombre de la otra colonia',
                                'length' => '40'
                            ])
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de trabajo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('employer_address_1') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'employer_address_1',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['employer', 'address', 'first']),
                                    'placeholder' => 'Avenida, Calle, Bloque, Casa',
                                    'label' => 'Avenida, Calle, Bloque, Casa',
                                    'length' => '40',
                                    'required' => true,
                                    'enable' => true
                                ])
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('employer_address_2') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'employer_address_2',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['employer', 'address', 'second']),
                                    'placeholder' => 'Punto de referencia de la dirección',
                                    'label' => 'Punto de referencia de la dirección',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('employer_address_3') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'employer_address_3',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['employer', 'address', 'third']),
                                    'placeholder' => 'Segundo punto de referencia de la dirección',
                                    'label' => 'Segundo punto de referencia de la dirección',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Antiguedad laboral</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-calendar"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('employer_day') ? ' has-error' : '' }}">
                        <div class="row birth-container">
                            <div class="col-4 birth-day">
                                @include('modules._singleSelect', [
                                    'name' => 'employer_day',
                                    'placeholder' => 'Día',
                                    'options' => $lists['dayOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'd'),
                                    'enable' => true
                                ])
                            </div>
                            <div class="col-4 birth-month">
                                @include('modules._singleSelect', [
                                    'name' => 'employer_month',
                                    'placeholder' => 'Mes',
                                    'options' => $lists['monthOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'm'),
                                    'enable' => true
                                ])
                            </div>
                            <div class="col-4 birth-year">
                                @include('modules._singleSelect', [
                                    'name' => 'employer_year',
                                    'options' => $lists['employerYearOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'Y'),
                                    'default' => $lists['selectedEmployerYear'],
                                    'placeholder' => 'Año',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 form-group">
                    <h2>Informacion Personal</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'gender',
                            'placeholder' => 'Seleccione su género...',
                            'options' => $lists['genderOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['gender', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('marital_status') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'marital_status',
                            'placeholder' => 'Seleccione su estado civil...',
                            'options' => $lists['maritalStatusOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['marital_status', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row hide-marital-status">
                <div class="col-12 form-group">
                    <h2>Cónyuge</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group{{ $errors->has('spouse_id_type') ? ' has-error' : '' }}">
                                @include('modules._singleSelect', [
                                    'name' => 'spouse_id_type',
                                    'placeholder' => 'Seleccione el tipo de identificación...',
                                    'options' => $lists['idOptions'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['spouse_information', 'id_type', 'code']),
                                    'enable' => true
                                ])
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group{{ $errors->has('spouse_identity') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'spouse_identity',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['spouse_information', 'identity']),
                                    'placeholder' => 'No. de identificación',
                                    'label' => 'No. de identificación',
                                    'length' => '13',
                                    'classes' => 'validate-num-dash',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('spouse') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'spouse',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, 'spouse'),
                                    'placeholder' => 'Nombre completo del cónyuge',
                                    'label' => 'Nombre completo del cónyuge',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Nacionalidad</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-globe-americas"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'nationality',
                            'placeholder' => 'Seleccione su nacionalidad...',
                            'options' => $lists['nations'],
                            'classes' => 'davivienda-select',
                            'required' => true,
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['nationality', 'code']),
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>¿Tiene ciudadanía, residencia o pasaporte estadounidense?</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-flag-usa"></i>
                </div>

                <div class="col-10 form-group{{ $errors->has('fatca') ? ' has-error' : '' }}">
                    @include('modules._singleSelect', [
                        'name' => 'fatca',
                        'placeholder' => 'Seleccione la respuesta...',
                        'options' => $lists['confirmation'],
                        'resource' => $flow,
                        'json' => get_json($flow->customer_information, ['fatca', 'code']),
                        'required' => true,
                        'enable' => true
                    ])
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Referencia familiar</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('reference_1_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'name']),
                            'placeholder' => 'Nombre de la referencia familiar',
                            'label' => 'Nombre de la referencia familiar',
                            'length' => '40',
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_relationship') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'reference_1_relationship',
                            'placeholder' => 'Seleccione el parentesco...',
                            'options' => $lists['referenceOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'relationship', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_mobile') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_mobile',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'mobile']),
                            'placeholder' => 'Celular',
                            'label' => 'Celular',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'phone']),
                            'placeholder' => 'Teléfono',
                            'label' => 'Teléfono de domicilio',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_work_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_work_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'work_phone']),
                            'placeholder' => 'Teléfono de trabajo',
                            'label' => 'Teléfono de trabajo',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Referencia personal</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('reference_2_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'name']),
                            'placeholder' => 'Nombre de la referencia personal',
                            'label' => 'Nombre de la referencia personal',
                            'length' => '40',
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_mobile') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_mobile',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'mobile']),
                            'placeholder' => 'Celular',
                            'label' => 'Celular',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'phone']),
                            'placeholder' => 'Teléfono',
                            'label' => 'Teléfono de domicilio',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_work_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_work_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'work_phone']),
                            'placeholder' => 'Teléfono de trabajo',
                            'label' => 'Teléfono de trabajo',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

        @if(  get_json($flow->user_information, ['dealership_is_connected', 'code']) == 'S' )
            <div class="row">
                <div class="col-12 form-group">
                    <h2>Ejecutivo de auto</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-briefcase"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('auto_executive') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'auto_executive',
                            'placeholder' => 'Seleccione el ejecutivo de auto...',
                            'options' => $lists['agentOptions'][get_json($flow->user_information, ['concessionaire_area', 'code'])],
                            'resource' => $flow,
                            'json' => get_json($flow->user_information, ['auto_executive', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>
        @endif

            {{--<hr style="border: none;" class="mb-3">--}}

            <div class="hide-if-no-email">
                @include('modules._checkbox', [
                         'name' => 'receive_emails',
                         'json' => get_json($flow->customer_information, ['receive_emails', 'code']),
                         'label' => 'Acepto envíos de estados de cuenta a mi correo electrónico personal',
                         'checked' => true
                     ]
                )
            </div>

            <div class="hide-if-no-job-email">
                @include('modules._checkbox', [
                        'name' => 'receive_emails_job',
                        'json' => get_json($flow->customer_information, ['receive_emails_job', 'code']),
                        'label' => 'Acepto envíos de estados de cuenta a mi correo electrónico laboral',
                        'checked' => true
                    ]
                )
            </div>


            <div class="row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">
                        EVALUAR
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>