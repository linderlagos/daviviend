@include('modules._progress', [
    'steps' => 4,
    'step' => 4,
    'title' => 'Información auto'
])

<div class="row">
    <div class="col-12 identity-form">
        <form class="form-horizontal" role="form" method="POST" action="{{  route('bank.auto.fifth') }}" id="form">
            {!! csrf_field() !!}

            <div class="row">
                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon fas fa-building"></span>
                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group{{ $errors->has('concessionaire') ? ' has-error' : '' }}">
                                @include('modules._singleSelect', [
                                    'name' => 'concessionaire',
                                    'placeholder' => 'Concesionaria...',
                                    'options' => $lists['concessionaireOptions'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->user_information, ['concessionaire', 'code']),

                                ])
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group{{ $errors->has('seller') ? ' has-error' : '' }}">
                                @include('modules._singleSelect', [
                                    'name' => 'seller',
                                    'placeholder' => 'Vendedor...',
                                    'options' => $lists['sellerOptions'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->user_information, ['seller', 'code']),

                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Vehículo</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-car"></i>
                </div>

                <div class="col-4">
                    <div class="form-group{{ $errors->has('auto_brand') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'auto_brand',
                            'placeholder' => 'Marca del vehículo...',
                            'options' => $lists['autoBrandOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'brand', 'code']),

                        ])
                    </div>
                </div>


                <div class="col-4">
                    <div class="form-group{{ $errors->has('auto_model') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'auto_model',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'model']),
                            'placeholder' => 'Modelo de vehículo',
                            'label' => 'Modelo de vehículo',
                            'length' => '40',
                        ])
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group{{ $errors->has('status_auto') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'status_auto',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'status', 'value']),
                            'placeholder' => 'Estado de vehículo',
                            'label' => 'Estado de vehículo',
                            'length' => '40',
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2 form-group text-center">
                </div>

                <div class="col-4">
                    <div class="form-group{{ $errors->has('auto_type') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'auto_type',
                            'placeholder' => 'Seleccione tipo de vehículo...',
                            'options' => $lists['typeAutoOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'type', 'code']),

                        ])
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group{{ $errors->has('auto_color') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'auto_color',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'color']),
                            'placeholder' => 'Color de vehículo',
                            'label' => 'Color de vehículo',
                            'length' => '40',
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group{{ $errors->has('auto_year') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'auto_year',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'year', 'code']),
                            'placeholder' => 'Año del vehículo...',
                            'label' => 'Año del vehículo',
                            'length' => '40',
                            'required' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2 form-group text-center">
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('auto_displacement') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'auto_displacement',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'displacement']),
                            'placeholder' => 'Cilindraje de vehículo',
                            'label' => 'Cilindraje de vehículo',
                            'length' => '40',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('motor_numbers') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'motor_numbers',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'motor']),
                            'placeholder' => 'Número de serie de motor',
                            'label' => 'Número de serie de motor',
                            'length' => '40',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2 form-group text-center">
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('vin_numbers') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'vin_numbers',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'vin']),
                            'placeholder' => 'Número de serie del VIN',
                            'label' => 'Número de serie del VIN',
                            'length' => '40',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('chassis_numbers') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'chassis_numbers',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['auto', 'chassis']),
                            'placeholder' => 'Número de serie del chasis',
                            'label' => 'Número de serie del chasis',
                            'length' => '40',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Ingrese Observaciones:</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon far fa-comment"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
                        @include('modules._textArea', [
                            'name' => 'notes',
                            'resource' => $flow,
                            'json' => get_json($flow->product_information, ['notes', 'full']),
                            'placeholder' => 'Ingrese las observaciones que considere necesarios para esta solicitud',
                            'label' => 'Comentarios adicioanles',
                            'length' => '240',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">
                        FINALIZAR SOLICITUD
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>