
@extends('layouts.form')
@section('content')
 <index-auto-component :lists="{{ json_encode($lists) }}" :flow="{{ json_encode($flow) }}"></index-auto-component> 
@stop

{{-- 
@section('scripts')
    @include('partials._scripts')
    @include('bank.auto.partials._scripts')
@stop --}}

{{-- @extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/bank_auto.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Formulario de Solicitud de Auto</h1>
                </div>
                <div class="banner-description">
                    @if(checkStep($flow, ['1', '2', '3', '4', '5']))
                        <p>Continúe completando la información del cliente {{ get_json($flow->customer_information, ['name', 'fullname']) !== 'NULO' ? get_json($flow->customer_information, ['name', 'fullname']) : '' }} con ID No. {{ $flow->customer->identifier }}</p>
                    @elseif(checkStep($flow, 'contracts'))
                        <p>Imprima los documentos del cliente con ID No. {{ $flow->customer->identifier }} y cuenta No. {{ $flow->identifier }}</p>
                    @else
                        <p>Complete la información del cliente para terminar el proceso de solicitud de auto asistido</p>
                    @endif
                </div>
            </div>
        </div>
        <hr class="cintaGris">
    </div>
@stop

@section('back')
    @if(checkStep($flow, ['2', '3', '4']))
        @include('modules._return', [
            'id' => $flow->id
        ])
    @elseif(checkStep($flow, ['2', '3']))
        <a href="{{ route('home') }}" class="btn-back">
            Inicio
        </a>
    @endif
@stop

@section('reject')
    @if(checkStep($flow, ['1', '2', '3', '4', '5']))
        @include('modules._rejectSearch', [
             'route' => route('exit.flow', ['bank_auto']),
         ])
    @endif
@stop

@section('content')

    @if(checkStep($flow, '1'))
        @include('bank.auto.form._first')
    @elseif(checkStep($flow, '2'))
        @include('bank.auto.form._second')
    @elseif(checkStep($flow, '3'))
        @include('bank.auto.form._third')
    @elseif(checkStep($flow, '4'))
        @include('bank.auto.form._fourth')
    @elseif(checkStep($flow, '5'))
        @include('bank.auto.form._fifth')
    @elseif(checkStep($flow, 'query'))
        @include('bank.auto.form._query')
    @else
        @include('bank.auto.form._search')
    @endif

@endsection


@section('scripts')
   @include('partials._scripts')
   @include('bank.auto.partials._scripts')
@stop
 --}}
