<div>
    <div class="row">
        <div class="col-12 form-group result-message">
            <h2>{{ $message }}</h2>
        </div>
    </div>
    <br>

    @if (!$approved)

        <div class="row" >

            <div class="col-12 form-group ">
                <ul>
                    @foreach($suggestion as  $data)
                        <h2><li type="circle" class="text-left">{{$data}}</li></h2>
                    @endforeach

                </ul>
            </div>
        </div>
    @endif



     @if (!$approved)
        <br>
         <div class="row justify-content-center" style="margin-bottom: 1.5rem">
             <div class="col-12 col-sm-6">

                 <form class="form-horizontal" role="form" method="POST"  id="form">
                      {!! csrf_field() !!}
                       <button type="submit" class="btn btn-davivienda-gray" id="submit" style="padding: 0.8rem 3rem" onclick="showCalculator()" >
                            <i class="fas fa-redo" style="margin-right: 1rem"></i>VOLVER A COTIZAR
                        </button>
                  </form>

          </div>
     @endif

 </div>