<div>
    <div class="row">
        <div class="col-12 form-group result-message">
            <h2>{{ $message }}</h2>
        </div>
    </div>
    

    @if (get_json($flow->product_information, ['result_stage']) == 'D')
        <div class="row">
            <div class="col-12 form-group">
                <table class="results-table">
                    @foreach(get_json($flow->product_information, ['suggestions']) as $key => $suggestion)
                        <tr>
                            <td class="result-title text-left">Sugerencia {{ $key + 1 }}</td>
                            <td class="result-value text-left">* {{ $suggestion }}</td>
                        </tr>
                    @endforeach
                </table>



                {{--<ul>--}}
                    {{--@foreach(get_json($flow->product_information, ['suggestions']) as $suggestion)--}}
                        {{--<li>{{$suggestion}}</li>--}}
                    {{--@endforeach--}}

                {{--</ul>--}}
            </div>
        </div>
        <div class="row justify-content-center" style="margin-bottom: 1.5rem">
            <div class="col-12 text-center">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.auto.recalculate') }}" id="recalculate-form">
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-davivienda-gray" id="submit-recalculate" style="padding: 0.8rem 3rem" onclick="postForm('#recalculate-form', '#submit-recalculate', 'VOLVER A COTIZAR')" >
                        <i class="fas fa-redo" style="margin-right: 1rem"></i>VOLVER A COTIZAR
                    </button>
                </form>
            </div>
        </div>
    @elseif (get_json($flow->product_information, ['result_stage']) == 'A')
        <div class="row">
            <div class="col-12 form-group">
                <table class="results-table">
                    <tr>
                        
                        <td class="result-title">Precio de venta:</td>
                        <td class="result-value-secondary">L {{ get_json($flow->product_information, ['sale_price'])  }}</td>
                    </tr>
                    <tr>
                        <td class="result-title">Prima</td>
                        <td class="result-value-secondary">L {{ get_json($flow->product_information, ['premium'])  }}</td>
                    </tr>
                    <tr>
                        <td class="result-title">Valor a financiar:</td>
                        <td class="result-value-secondary">L {{ get_json($flow->product_information, ['response_scenario', 'value_finance']) }}</td>
                    </tr>
                    <tr>
                        <td class="result-title">Plazo:</td>
                        <td class="result-value-secondary">{{ get_json($flow->product_information, ['financing_term', 'value']) }}</td>
                    </tr>
                    <tr>
                        <td class="result-title">Tasa de interes:</td>
                        <td class="result-value-secondary">{{ get_json($flow->product_information, ['response_scenario', 'interest_rate'])  }} %</td>
                    </tr>
                    {{--<tr>--}}
                        {{--<td class="result-title">Cuota origonal de seguro de deuda:</td>--}}
                        {{--<td class="result-value-secondary">L {{ number_format(get_json($flow->product_information, ['response_scenario', 'original_debt_insurance']), 2) }}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td class="result-title">Cuota nivelada:</td>
                        <td class="result-value-secondary">L {{ get_json($flow->product_information, ['response_scenario', 'level_fee']) }}</td>
                    </tr>
                    <tr>
                        <td class="result-title">Cuota seguro de deuda:</td>
                        <td class="result-value">L {{ get_json($flow->product_information, ['response_scenario', 'debt_insurance']) }}</td>
                    </tr>
                    <tr>
                        <td class="result-title">Cuota seguro de daño:</td>
                        <td class="result-value">L {{ get_json($flow->product_information, ['response_scenario', 'safe_insurance']) }}</td>
                    </tr>
                    {{--<tr>--}}
                        {{--<td class="result-title">Cuota origonal de seguro de deuda:</td>--}}
                        {{--<td class="result-value-secondary">L {{ number_format(get_json($flow->product_information, ['response_scenario', 'original_debt_insurance']), 2) }}</td>--}}
                    {{--</tr>--}}

                    {{--<tr>--}}
                        {{--<td class="result-title">Periodo de gracia:</td>--}}
                        {{--<td class="result-value">{{ get_json($flow->product_information, ['grace_period', 'value']) }}</td>--}}
                    {{--</tr>--}}

                    <tr>
                        <td class="result-title">Cuota total:</td>
                        <td class="result-value">L {{ get_json($flow->product_information, ['response_scenario', 'total_quota']) }}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row justify-content-center" style="margin-bottom: 1.5rem">
            <div class="col-12 col-sm-6 text-center">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.auto.recalculate') }}" id="recalculate-form">
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-davivienda-gray" id="submit-recalculate" style="padding: 0.8rem 3rem" onclick="postForm('#recalculate-form', '#submit-recalculate', 'VOLVER A COTIZAR')" >
                        <i class="fas fa-redo" style="margin-right: 1rem"></i>VOLVER A COTIZAR
                    </button>
                </form>
            </div>

            <div class="col-12 col-sm-6 text-center">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.auto.third') }}" id="form">
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-davivienda-red" id="submit" style="padding: 0.8rem 3rem">
                        ACEPTAR
                    </button>
                </form>
            </div>
        </div>
    @endif
</div>