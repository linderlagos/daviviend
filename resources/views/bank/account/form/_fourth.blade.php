@include('modules._progress', [
    'steps' => 5,
    'step' => 4,
    'title' => 'Beneficiarios y Referencias'
])

<div class="row">
    <div class="col-12 identity-form">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.account.fifth') }}" id="form">
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-12 form-group">
                    <h2>Beneficiarios</h2>
                </div>
            </div>

            @for($i = 1; $i < 11; $i++)
                <div id="{{ 'beneficiary_' . $i }}">
                    <div class="row">
                        {{--<div class="col-2"></div>--}}
                        <div class="col-12 text-right">
                            <h3>Beneficiario {{ $i }}</h3>
                        </div>
                        <div class="col-2 form-group text-center">
                            <i class="davivienda-icon fas fa-users"></i>
                        </div>

                        <div class="col-10">
                            <div class="form-group{{ $errors->has('beneficiary_' . $i . '_name') ? ' has-error' : '' }}">
                                @if($i === 1)
                                    @include('modules._textInput', [
                                        'name' => 'beneficiary_' . $i . '_name',
                                        'resource' => $flow,
                                        'json' => get_json($flow->customer_information, ['beneficiaries', $i, 'name']),
                                        'placeholder' => 'Nombre del beneficiario',
                                        'label' => 'Nombre del beneficiario',
                                        'length' => '40',
                                        'enable' => true,
                                        'required' => true
                                    ])
                                @else
                                    @include('modules._textInput', [
                                        'name' => 'beneficiary_' . $i . '_name',
                                        'resource' => $flow,
                                        'json' => get_json($flow->customer_information, ['beneficiaries', $i, 'name']),
                                        'placeholder' => 'Nombre del beneficiario',
                                        'label' => 'Nombre del beneficiario',
                                        'length' => '40',
                                        'enable' => true
                                    ])
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-2"></div>

                        <div class="col-5">
                            <div class="form-group{{ $errors->has('beneficiary_' . $i . '_relationship') ? ' has-error' : '' }}">
                                @include('modules._singleSelect', [
                                    'name' => 'beneficiary_' . $i . '_relationship',
                                    'placeholder' => 'Seleccione el parentesco...',
                                    'options' => $lists['beneficiaryOptions'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['beneficiaries', $i, 'relationship', 'code']),
                                    'enable' => true,
                                ])
                            </div>
                        </div>

                        <div class="col-5">
                            <div class="form-group{{ $errors->has('beneficiary_' . $i . '_id_type') ? ' has-error' : '' }}">
                                @include('modules._singleSelect', [
                                    'name' => 'beneficiary_' . $i . '_id_type',
                                    'placeholder' => 'Seleccione el tipo de identificación...',
                                    'options' => $lists['idOptions'],
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['beneficiaries', $i, 'id_type', 'code']),
                                    'enable' => true,
                                ])
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-2"></div>

                        <div class="col-10">
                            <div class="form-group{{ $errors->has('beneficiary_' . $i . '_identity') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'beneficiary_' . $i . '_identity',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['beneficiaries', $i, 'identity']),
                                    'placeholder' => 'No. de identificación',
                                    'label' => 'No. de identificación',
                                    'length' => '13',
                                    'classes' => 'validate-num-dash',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                    </div>

                    @if($i !== 10)
                        <div class="row justify-content-end">
                            <div class="col-2 text-right" id="add_beneficiary_{{ $i }}_btn">
                                <a onclick="addBeneficiary({{ $i + 1 }})" href="javascript:void(0);" class="btn davivienda-add-field">
                                    <i class="fas fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
            @endfor

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Referencia familiar</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-user-friends"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('reference_1_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'name']),
                            'placeholder' => 'Nombre de la referencia familiar',
                            'label' => 'Nombre de la referencia familiar',
                            'length' => '40',
                            'enable' => true,
                            'required' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                {{--<div class="col-5">--}}
                {{--<div class="form-group{{ $errors->has('reference_1_relationship') ? ' has-error' : '' }}">--}}
                {{--@include('modules._singleSelect', [--}}
                {{--'name' => 'reference_1_relationship',--}}
                {{--'placeholder' => 'Seleccione el parentesco...',--}}
                {{--'options' => $lists['referenceOptions'],--}}
                {{--'resource' => $flow,--}}
                {{--'enable' => true,--}}
                {{--'required' => true,--}}
                {{--])--}}
                {{--</div>--}}
                {{--</div>--}}

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_mobile') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_mobile',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'mobile']),
                            'placeholder' => 'Celular',
                            'label' => 'Celular',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_1_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_1_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 1, 'phone']),
                            'placeholder' => 'Teléfono',
                            'label' => 'Teléfono de domicilio',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            {{--<div class="row">--}}
            {{--<div class="col-2"></div>--}}
            {{--<div class="col-5">--}}
            {{--<div class="form-group{{ $errors->has('reference_1_work_phone') ? ' has-error' : '' }}">--}}
            {{--@include('modules._textInput', [--}}
            {{--'name' => 'reference_1_work_phone',--}}
            {{--'resource' => $flow,--}}
            {{--'placeholder' => 'Teléfono de trabajo',--}}
            {{--'label' => 'Teléfono de trabajo',--}}
            {{--'length' => '9',--}}
            {{--'enable' => true--}}
            {{--])--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Referencia personal</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-user-friends"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('reference_2_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'name']),
                            'placeholder' => 'Nombre de la referencia personal',
                            'label' => 'Nombre de la referencia personal',
                            'length' => '40',
                            'enable' => true,
                            'required' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_mobile') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_mobile',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'mobile']),
                            'placeholder' => 'Celular',
                            'label' => 'Celular',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('reference_2_phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'reference_2_phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['references', 2, 'phone']),
                            'placeholder' => 'Teléfono',
                            'label' => 'Teléfono de domicilio',
                            'length' => '9',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">
                        SIGUIENTE
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>