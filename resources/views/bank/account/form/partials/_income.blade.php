<div class="row">
    <div class="col-12 form-group show-if-independent">
        <h2>{{ $incomeTitle }}</h2>
    </div>

    <div class="col-2 form-group text-center">
        <span class="davivienda-icon icon-card"></span>
    </div>

    <div class="col-10">
        <div class="form-group{{ $errors->has('income') ? ' has-error' : '' }}">
            @include('modules._textInput', [
                'name' => 'income',
                'resource' => $flow,
                'json' => get_json($flow->customer_information, 'income'),
                'placeholder' => $incomePlaceholder,
                'label' => 'Salario mensual',
                'classes'  => 'format-amount',
                'autocomplete' => 'No',
                'enable' => true
            ])
        </div>
    </div>
</div>