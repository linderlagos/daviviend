@include('modules._progress', [
    'steps' => 5,
    'step' => 2,
    'title' => 'Información personal'
])

<div class="row">
    <div class="col-12 identity-form">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.account.third') }}" id="form">
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-12 form-group">
                    <h2>Fecha de nacimiento</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-calendar"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('day') ? ' has-error' : '' }}">
                        <div class="row birth-container">
                            <div class="col-4 birth-day">
                                @include('modules._singleSelect', [
                                    'name' => 'day',
                                    'placeholder' => 'Día',
                                    'options' => $lists['dayOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information,'birth'), 'd'),
                                    'required' => true,
                                ])
                            </div>
                            <div class="col-4 birth-month">
                                @include('modules._singleSelect', [
                                    'name' => 'month',
                                    'placeholder' => 'Mes',
                                    'options' => $lists['monthOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information,'birth'), 'm'),
                                    'required' => true,
                                ])
                            </div>
                            <div class="col-4 birth-year">
                                @include('modules._singleSelect', [
                                    'name' => 'year',
                                    'options' => $lists['yearOptions'],
                                    'resource' => $flow,
                                    'json' => get_json_date(get_json($flow->customer_information,'birth'), 'Y'),
                                    'default' => $lists['selectedYear'],
                                    'placeholder' => 'Año',
                                    'required' => true,
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Números de contacto</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-phone"></span>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'phone',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'phone'),
                            'placeholder' => 'Teléfono de domicilio',
                            'label' => 'Teléfono de domicilio',
                            'length' => '9',
                            'classes' => 'validate-num-dash',
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'mobile',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'mobile'),
                            'placeholder' => 'Celular',
                            'label' => 'Celular',
                            'length' => '9',
                            'classes' => 'validate-num-dash',
                            'required' => true,
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Colonia de domicilio</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        <select class="form-control davivienda-select" name="city" id="city" value="{{ old('city') }}" placeholder="Seleccione la colonia..." required>
                            @include('modules._singleSelectOptGroup', [
                                'name' => 'city',
                                'options' => $lists['cities'],
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['address', 'code']),
                            ])
                        </select>
                        @if ($errors->has('city'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row hide-other-city">
                <div class="col-2 form-group"></div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('other_city') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'other_city',
                            'resource' => $flow,
                            'placeholder' => 'Nombre de la otra colonia',
                            'json' => get_json($flow->customer_information, ['address', 'other_city']),
                            'label' => 'Nombre de la otra colonia',
                            'length' => '40',
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de domicilio</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-gps"></span>
                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address_1') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'address_1',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['address', 'first']),
                                    'placeholder' => 'Avenida, Calle, Bloque, Casa',
                                    'label' => 'Avenida, Calle, Bloque, Casa',
                                    'length' => '40',
                                    'required' => true,
                                    'enable' => true
                                ])
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address_2') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'address_2',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['address', 'second']),
                                    'placeholder' => 'Punto de referencia de la dirección',
                                    'label' => 'Punto de referencia de la dirección',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('address_3') ? ' has-error' : '' }}">
                                @include('modules._textInput', [
                                    'name' => 'address_3',
                                    'resource' => $flow,
                                    'json' => get_json($flow->customer_information, ['address', 'third']),
                                    'placeholder' => 'Segundo punto de referencia de la dirección',
                                    'label' => 'Segundo punto de referencia de la dirección',
                                    'length' => '40',
                                    'enable' => true
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Dirección de correo electrónico</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-messages"></span>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'email',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['email', 'account']),
                            'placeholder' => 'Ingrese su correo electrónico',
                            'label' => 'Correo eletrónico',
                            'length' => '40',
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
                <div class="col-3 text-right">
                    <label for="has_email" style="font-size: 1.4rem; line-height: 1;">No tiene correo</label>
                    {{--@if ($errors->has('has_email'))--}}
                        {{--<span class="help-block">--}}
                            {{--<strong>{{ $errors->first('has_email') }}</strong>--}}
                        {{--</span>--}}
                    {{--@endif--}}
                </div>

                <div class="col-1 text-center davivienda-checkbox">
                    @include('modules._checkbox', [
                         'name' => 'has_email',
                         'json' => get_json($flow->customer_information, ['email', 'has_email']),
                         'label' => ''
                        ]
                    )
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Información personal</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'gender',
                            'placeholder' => 'Seleccione su género...',
                            'options' => $lists['genderOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['gender', 'code']),
                            'required' => true,
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('marital_status') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'marital_status',
                            'placeholder' => 'Seleccione su estado civil...',
                            'options' => $lists['maritalStatusOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['marital_status', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row hide-marital-status">
                <div class="col-12 form-group">
                    <h2>Cónyuge</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <span class="davivienda-icon icon-id"></span>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('spouse') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'spouse',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, 'spouse'),
                            'placeholder' => 'Nombre completo del cónyuge',
                            'label' => 'Nombre completo del cónyuge',
                            'length' => '40',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            @include('modules._checkbox', [
                'name' => 'receive_sms',
                'json' => get_json($flow->customer_information, ['receive_sms', 'code']),
                'label' => 'Acepto envíos de mensajes a mi celular',
                'checked' => true
              ]
            )

            <div class="hide-if-no-email">
                @include('modules._checkbox', [
                         'name' => 'receive_emails',
                         'json' => get_json($flow->customer_information, ['receive_emails', 'code']),
                         'label' => 'Acepto envíos de estados de cuenta a mi correo electrónico personal',
                         'checked' => true
                     ]
                )
            </div>

            <div class="row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">
                        SIGUIENTE
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>