@include('modules._progress', [
    'steps' => 5,
    'step' => 3,
    'title' => 'Información financiera'
])

<div class="row">
    <div class="col-12 identity-form">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('bank.account.fourth') }}" id="form">
            {!! csrf_field() !!}

            @if(!in_array(get_json($flow->customer_information, ['employer', 'job', 'type', 'code']), ['10','11','44']))
                <div class="row show-if-independent">
                    <div class="col-12 form-group">
                        <h2>Lugar de trabajo</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <i class="davivienda-icon fas fa-briefcase"></i>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('employer_name') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'employer_name',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['employer', 'name']),
                                'placeholder' => 'Nombre de la empresa',
                                'label' => 'Nombre de la empresa',
                                'length' => '40',
                                'enable' => true
                            ])
                        </div>
                    </div>
                </div>

                <div class="row show-if-independent">
                    <div class="col-12 form-group">
                        <h2>Puesto de Trabajo</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <i class="davivienda-icon fas fa-briefcase"></i>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('job_name') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'job_name',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['employer', 'job', 'name']),
                                'placeholder' => 'Nombre del puesto de trabajo',
                                'label' => 'Nombre del puesto actual de trabajo',
                                'length' => '30',
                                'enable' => true
                            ])
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 form-group">
                        <h2>Dirección de correo electrónico laboral</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <span class="davivienda-icon icon-messages"></span>
                    </div>

                    <div class="col-6">
                        <div class="form-group{{ $errors->has('job_email') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'job_email',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['job_email', 'account']),
                                'placeholder' => 'Ingrese su correo laboral',
                                'label' => 'Correo laboral',
                                'length' => '40',
                                'required' => true
                            ])
                        </div>
                    </div>
                    <div class="col-3 text-right">
                        <label for="has_job_email" style="font-size: 1.4rem; line-height: 1;">No tiene correo</label>
                    </div>
                    <div class="col-1 text-center davivienda-checkbox">
                        @include('modules._checkbox', [
                             'name' => 'has_job_email',
                             'json' => get_json($flow->customer_information, ['job_email', 'has_job_email']),
                             'label' => ''
                            ]
                        )
                    </div>
                </div>

                @include('bank.account.form.partials._income', [
                    'incomeTitle' => 'Salario mensual',
                    'incomePlaceholder' => 'Ingrese su salario mensual',
                ])

                <div class="row show-if-independent">
                    <div class="col-12 form-group">
                        <h2>Tipo de empresa</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <i class="davivienda-icon fas fa-building"></i>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('employer_type') ? ' has-error' : '' }}">
                            @include('modules._singleSelect', [
                                'name' => 'employer_type',
                                'placeholder' => 'Seleccione el tipo de empresa...',
                                'options' => $lists['employeeTypes'],
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['employer', 'type', 'code']),
                                'enable' => true
                            ])
                        </div>
                    </div>
                </div>


                <div class="row show-if-independent">
                    <div class="col-12 form-group">
                        <h2>Fecha de ingreso al trabajo</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <span class="davivienda-icon icon-calendar"></span>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('employer_day') ? ' has-error' : '' }}">
                            <div class="row birth-container">
                                <div class="col-4 birth-day">
                                    @include('modules._singleSelect', [
                                        'name' => 'employer_day',
                                        'placeholder' => 'Día',
                                        'options' => $lists['dayOptions'],
                                        'resource' => $flow,
                                        'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'd'),
                                        'enable' => true
                                    ])
                                </div>
                                <div class="col-4 birth-month">
                                    @include('modules._singleSelect', [
                                        'name' => 'employer_month',
                                        'placeholder' => 'Mes',
                                        'options' => $lists['monthOptions'],
                                        'resource' => $flow,
                                        'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'm'),
                                        'enable' => true
                                    ])
                                </div>
                                <div class="col-4 birth-year">
                                    @include('modules._singleSelect', [
                                        'name' => 'employer_year',
                                        'options' => $lists['employerYearOptions'],
                                        'resource' => $flow,
                                        'json' => get_json_date(get_json($flow->customer_information, ['employer', 'started_at']), 'Y'),
                                        'default' => $lists['selectedEmployerYear'],
                                        'placeholder' => 'Año',
                                        'enable' => true
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row show-if-independent">
                    <div class="col-12 form-group">
                        <h2>Tipo de empleo</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <i class="davivienda-icon fas fa-briefcase"></i>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('job') ? ' has-error' : '' }}">
                            @include('modules._singleSelect', [
                                'name' => 'job',
                                'placeholder' => 'Seleccione el tipo de empleo...',
                                'options' => $lists['jobOptions'],
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['employer', 'job', 'status', 'code']),
                                'enable' => true
                            ])
                        </div>
                    </div>
                </div>

                <div class="row show-if-independent">
                    <div class="col-12 form-group">
                        <h2>Teléfono del trabajo</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <span class="davivienda-icon icon-phone"></span>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('employer_phone') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'employer_phone',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['employer', 'phone']),
                                'placeholder' => 'Ingrese el teléfono del trabajo',
                                'label' => 'Teléfono del trabajo',
                                'length' => '9',
                                'classes' => 'validate-num-dash',
                                'enable' => true
                            ])
                        </div>
                    </div>
                </div>

                <div class="row show-if-independent">
                    <div class="col-12 form-group">
                        <h2>Colonia de trabajo</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <span class="davivienda-icon icon-gps"></span>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('employer_city') ? ' has-error' : '' }}">
                            <select class="form-control davivienda-select" name="employer_city" id="employer_city" value="{{ old('employer_city') }}" placeholder="Seleccione la colonia...">
                                @include('modules._singleSelectOptGroup', [
                                        'name' => 'employer_city',
                                        'options' => $lists['cities'],
                                        'resource' => $flow,
                                        'json' => get_json($flow->customer_information, ['employer', 'address', 'code']),
                                    ])
                            </select>
                            @if ($errors->has('employer_city'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('employer_city') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row hide-other-employer-city">
                    <div class="col-2 form-group"></div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('other_employer_city') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'other_employer_city',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['employer', 'address', 'other_city']),
                                'placeholder' => 'Nombre de la otra colonia',
                                'label' => 'Nombre de la otra colonia',
                                'length' => '40',
                                'enable' => true
                            ])
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 form-group">
                        <h2>Dirección de trabajo</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <span class="davivienda-icon icon-gps"></span>
                    </div>

                    <div class="col-10">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group{{ $errors->has('employer_address_1') ? ' has-error' : '' }}">
                                    @include('modules._textInput', [
                                        'name' => 'employer_address_1',
                                        'resource' => $flow,
                                        'json' => get_json($flow->customer_information, ['employer', 'address', 'first']),
                                        'placeholder' => 'Avenida, Calle, Bloque, Casa',
                                        'label' => 'Avenida, Calle, Bloque, Casa',
                                        'length' => '40',
                                        'required' => true,
                                        'enable' => true
                                    ])
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group{{ $errors->has('employer_address_2') ? ' has-error' : '' }}">
                                    @include('modules._textInput', [
                                        'name' => 'employer_address_2',
                                        'resource' => $flow,
                                        'json' => get_json($flow->customer_information, ['employer', 'address', 'second']),
                                        'placeholder' => 'Punto de referencia de la dirección',
                                        'label' => 'Punto de referencia de la dirección',
                                        'length' => '40',
                                        'enable' => true
                                    ])
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group{{ $errors->has('employer_address_3') ? ' has-error' : '' }}">
                                    @include('modules._textInput', [
                                        'name' => 'employer_address_3',
                                        'resource' => $flow,
                                        'json' => get_json($flow->customer_information, ['employer', 'address', 'third']),
                                        'placeholder' => 'Segundo punto de referencia de la dirección',
                                        'label' => 'Segundo punto de referencia de la dirección',
                                        'length' => '40',
                                        'enable' => true
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Profesión</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-briefcase"></i>
                </div>

                <div class="col-10">
                    <div class="form-group{{ $errors->has('profession') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'profession',
                            'placeholder' => 'Seleccione la profesión...',
                            'options' => $lists['professionOptions'],
                            'classes' => 'davivienda-select',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['profession', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>¿Ha desempeñado un cargo público en el gobierno en los últimos 4 años?</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-landmark"></i>
                </div>

                <div class="col-10 form-group{{ $errors->has('public_job') ? ' has-error' : '' }}">
                    @include('modules._singleSelect', [
                        'name' => 'public_job',
                        'placeholder' => 'Seleccione la respuesta...',
                        'options' => $lists['confirmation'],
                        'resource' => $flow,
                        'json' => get_json($flow->customer_information, ['public_job', 'code']),
                        'enable' => true
                    ])
                </div>
            </div>

            <div class="row hide-public-job">
                <div class="col-12 form-group">
                    <h2>Información de cargos públicos</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-landmark"></i>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('public_job_employer_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'public_job_employer_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['public_job', 'employer_name']),
                            'placeholder' => 'Nombre de la institución',
                            'label' => 'Nombre de la institución',
                            'length' => '40',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('public_job_name') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'public_job_name',
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['public_job', 'job']),
                            'placeholder' => 'Cargo desempeñado',
                            'label' => 'Cargo desempeñado',
                            'length' => '30',
                            'enable' => true
                        ])
                    </div>
                </div>

            </div>

            <div class="row hide-public-job">
                <div class="col-2"></div>

                <div class="col-2">
                    <div class="form-group{{ $errors->has('public_job_active') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'public_job_active',
                            'placeholder' => '¿Es actual?',
                            'options' => $lists['confirmation'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['public_job', 'active']),
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group{{ $errors->has('public_job_from') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'public_job_from',
                            'resource' => $flow,
                            'json' => get_json_date(get_json($flow->customer_information, ['public_job', 'from']), 'd/m/Y'),
                            'placeholder' => 'Desde DD/MM/AAAA',
                            'label' => 'Fecha de inicio',
                            'length' => '10',
                            'classes' => 'date',
                            'autocomplete' => 'No',
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group{{ $errors->has('public_job_to') ? ' has-error' : '' }}">
                        @include('modules._textInput', [
                            'name' => 'public_job_to',
                            'resource' => $flow,
                            'json' => get_json_date(get_json($flow->customer_information, ['public_job', 'to']), 'd/m/Y'),
                            'placeholder' => 'Hasta DD/MM/AAAA',
                            'label' => 'Fecha final',
                            'length' => '10',
                            'classes' => 'hide-public-job-active date',
                            'autocomplete' => 'No',
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 form-group">
                    <h2>Información financiera</h2>
                </div>

                <div class="col-2 form-group text-center">
                    <i class="davivienda-icon fas fa-coins"></i>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('assets') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'assets',
                            'placeholder' => 'Total de activos',
                            'options' => $lists['assetRangeOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['assets', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('passive') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'passive',
                            'placeholder' => 'Total de pasivos',
                            'options' => $lists['liabilityRangeOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['passive', 'code']),
                            'required' => true,
                            'enable' => true

                        ])
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-2"></div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('expenses') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'expenses',
                            'placeholder' => 'Total de egresos',
                            'options' => $lists['expenseRangeOptions'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['expenses', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group{{ $errors->has('other_income') ? ' has-error' : '' }}">
                        @include('modules._singleSelect', [
                            'name' => 'other_income',
                            'placeholder' => '¿Tiene otros ingresos?',
                            'options' => $lists['confirmation'],
                            'resource' => $flow,
                            'json' => get_json($flow->customer_information, ['other_income', 'code']),
                            'required' => true,
                            'enable' => true
                        ])
                    </div>
                </div>
            </div>

            @if(in_array(get_json($flow->customer_information, ['employer', 'job', 'type', 'code']), ['10','11','44']))
                <div class="row">
                    <div class="col-12 form-group">
                        <h2>Dependencia profesional | Datos de quien depende económicamente</h2>
                    </div>

                    <div class="col-2 form-group text-center">
                        <i class="davivienda-icon fas fa-briefcase"></i>
                    </div>

                    <div class="col-10">
                        <div class="form-group{{ $errors->has('professional_dependant_name') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'professional_dependant_name',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['professional_dependant', 'name']),
                                'placeholder' => 'Nombre del dependiente profesional',
                                'label' => 'Nombre del dependiente profesional',
                                'length' => '40',
                                'enable' => true
                            ])
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-2"></div>

                    <div class="col-5">
                        <div class="form-group{{ $errors->has('professional_dependant_relationship') ? ' has-error' : '' }}">
                            @include('modules._singleSelect', [
                                'name' => 'professional_dependant_relationship',
                                'placeholder' => 'Seleccione el parentesco...',
                                'options' => $lists['beneficiaryOptions'],
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['professional_dependant', 'relationship', 'code']),
                                'enable' => true
                            ])
                        </div>
                    </div>

                    <div class="col-5">
                        <div class="form-group{{ $errors->has('professional_dependant_job_type') ? ' has-error' : '' }}">
                            @include('modules._singleSelect', [
                                'name' => 'professional_dependant_job_type',
                                'placeholder' => 'Seleccione la actividad comercial...',
                                'options' => $lists['jobTypeOptions'],
                                'classes' => 'davivienda-select',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['professional_dependant', 'job_type', 'code']),
                                'enable' => true
                            ])
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-2"></div>

                    <div class="col-5">
                        <div class="form-group{{ $errors->has('professional_dependant_id_type') ? ' has-error' : '' }}">
                            @include('modules._singleSelect', [
                                'name' => 'professional_dependant_id_type',
                                'placeholder' => 'Seleccione el tipo de identidad...',
                                'options' => $lists['idOptions'],
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['professional_dependant', 'id_type', 'code']),
                                'enable' => true
                            ])
                        </div>
                    </div>

                    <div class="col-5">
                        <div class="form-group{{ $errors->has('professional_dependant_identity') ? ' has-error' : '' }}">
                            @include('modules._textInput', [
                                'name' => 'professional_dependant_identity',
                                'resource' => $flow,
                                'json' => get_json($flow->customer_information, ['professional_dependant', 'identity']),
                                'placeholder' => 'Identidad del dependiente',
                                'label' => 'Identidad del dependiente',
                                'length' => '15',
                                'enable' => true
                            ])
                        </div>
                    </div>
                </div>

                @include('bank.account.form.partials._income', [
                    'incomeTitle' => 'Salario mensual de la persona de quien depende económicamente',
                    'incomePlaceholder' => 'Ingrese salario del dependiente profesional'
                ])
            @endif

            <div class="hide-if-no-job-email">
                @include('modules._checkbox', [
                        'name' => 'receive_emails_job',
                        'json' => get_json($flow->customer_information, ['receive_emails_job', 'code']),
                        'label' => 'Acepto envíos de estados de cuenta a mi correo electrónico laboral',
                        'checked' => true
                    ]
                )
            </div>

            <div class="row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-davivienda-red btn-long btn-loading" id="submit">
                        SIGUIENTE
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>