@include('modules._progress', [
    'steps' => 5,
    'step' => 1,
    'title' => 'Información general'
])

<form class="form-horizontal" role="form" method="POST" action="{{ route('bank.account.second') }}" id="form">
    {!! csrf_field() !!}

    <div class="row">
        <div class="col-12 form-group">
            <h2>Tipo de cuenta</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-piggy-bank"></i>
        </div>

        <div class="col-10">
            <div class="form-group{{ $errors->has('account_type') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'account_type',
                    'placeholder' => 'Seleccione el tipo...',
                    'resource' => $flow,
                    'json' => get_json($flow->product_information, ['product', 'code']),
                    'options' => $lists['accountOptions'],
                    'required' => true,
                    'enable' => true,
                ])
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2>Nombres</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user"></i>
        </div>

        <div class="col-5 first-name-field">
            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'first_name',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, ['name', 'first']),
                    'placeholder' => 'Primer nombre',
                    'label' => 'Primer nombre',
                    'length' => '15',
                    'required' => true,
                ])
            </div>
        </div>
        <div class="col-5 middle-name-field">
            <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'middle_name',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, ['name', 'middle']),
                    'placeholder' => 'Segundo nombre',
                    'label' => 'Segundo nombre',
                    'length' => '15',
                ])
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2>Apellidos</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-user"></i>
        </div>

        <div class="col-5 last-name-field">
            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'last_name',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, ['name', 'last']),
                    'placeholder' => 'Primer apellido',
                    'label' => 'Primer apellido',
                    'length' => '15',
                    'required' => true,
                ])
            </div>
        </div>
        <div class="col-5 second-last-name-field">
            <div class="form-group{{ $errors->has('second_last_name') ? ' has-error' : '' }}">
                @include('modules._textInput', [
                    'name' => 'second_last_name',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, ['name', 'second_last']),
                    'placeholder' => 'Segundo apellido',
                    'label' => 'Segundo apellido',
                    'length' => '15',
                ])
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2>Nacionalidad</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-globe-americas"></i>
        </div>

        <div class="col-10">
            <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'nationality',
                    'placeholder' => 'Seleccione su nacionalidad...',
                    'options' => $lists['nations'],
                    'classes' => 'davivienda-select',
                    'required' => true,
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, ['nationality', 'code']),
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2>Actividad comercial</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-briefcase"></i>
        </div>

        <div class="col-10">
            <div class="form-group{{ $errors->has('job_type') ? ' has-error' : '' }}">
                @include('modules._singleSelect', [
                    'name' => 'job_type',
                    'placeholder' => 'Seleccione su actividad comercial...',
                    'options' => $lists['jobTypeOptions'],
                    'classes' => 'davivienda-select',
                    'resource' => $flow,
                    'json' => get_json($flow->customer_information, ['employer', 'job', 'type', 'code']),
                    'required' => true,
                    'enable' => true
                ])
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2>¿Tiene ciudadanía, residencia o pasaporte estadounidense?</h2>
        </div>

        <div class="col-2 form-group text-center">
            <i class="davivienda-icon fas fa-flag-usa"></i>
        </div>

        <div class="col-10 form-group{{ $errors->has('fatca') ? ' has-error' : '' }}">
            @include('modules._singleSelect', [
                'name' => 'fatca',
                'placeholder' => 'Seleccione la respuesta...',
                'options' => $lists['confirmation'],
                'resource' => $flow,
                'json' => get_json($flow->customer_information, ['fatca', 'code']),
                'required' => true,
                'enable' => true
            ])
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <button type="submit" class="btn btn-davivienda-red btn-loading" id="submit">
                SIGUIENTE
            </button>
        </div>
    </div>
</form>