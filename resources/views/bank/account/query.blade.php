@extends('layouts.app')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/datatables.css') }}"/>
@stop

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/bank_account.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Consulta la información de clientes</h1>
                </div>
                <div class="banner-description">
                    <p>Puede revisar la información de los clientes que han sido aprobados por el flujo de cuenta de ahorro asistida</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    <a href="{{ route('home') }}" class="btn-back">
        Inicio
    </a>
@stop

@section('content-fluid')

    <div class="row">
        <div class="col-12 form-group">
            <h2>Listado de clientes:</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <table id="account-customers" class="davivienda-datatables">
                <thead>
                    <tr>
                        <th style="width:12%">Identidad</th>
                        <th style="width:12%">Nombre</th>
                        <th style="width:12%">Número de cuenta</th>
                        <th style="width:12%">Asesor</th>
                        <th style="width:12%">Fecha de solicitud</th>
                        <th style="width:12%">Documentación pendiente</th>
                        <th width="150px">Documento para impresión</th>
                        <th width="150px">Documentos escaneados</th>
                        <th width="150px">Firma</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($printables as $printable)
                        <tr>
                            <td class="text-center">
                                <a href="{{ route('account.contracts', [$printable->identifier, $printable->random]) }}" class="btn-table-text">
                                    <i class="fas fa-external-link-alt"></i> {{ $printable->identifier }}
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="{{ route('account.contracts', [$printable->identifier, $printable->random]) }}" class="btn-table-text">
                                    <i class="fas fa-external-link-alt"></i> {{ get_json($printable->fields, 'name') }}
                                </a>
                            </td>
                            <td class="text-center">{{ $printable->product_identifier }}</td>
                            <td>{{ get_json($printable->fields, ['customer_cif_information', 'agent', 'value']) }} - {{ get_json($printable->fields, ['customer_cif_information', 'agent', 'code']) }}</td>
                            <td class="text-center">{{ date('Y/m/d', get_json($printable->fields, 'date')) }}</td>

                            <td class="text-center">
                                @if (count($printable->uploads['identity']) === 0 || count($printable->uploads['signature']) === 0)
                                    <i class="fas fa-exclamation-triangle" style="color: #ED1C27"></i> Si
                                @else
                                    <i class="fas fa-check-square"></i> No
                                @endif
                            </td>
                            <td class="query-files-column">
                                <div class="row">
                                    {{--<div class="col-12">--}}
                                        {{--<a href="{{ route('account.print.signature', [$printable->identifier, $printable->random]) }}" target="_blank" class="btn btn-query">--}}
                                            {{--<i class="fas fa-file-signature"></i> Firma (En blanco)--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    <div class="col-12">
                                        <a href="{{ route('account.print.contract', [$printable->identifier, $printable->random]) }}" target="_blank" class="btn btn-query">
                                            <i class="fas fa-file-alt"></i> Contrato
                                        </a>
                                    </div>
                                    <div class="col-12">
                                        <a href="{{ route('account.print.cover', [$printable->identifier, $printable->random]) }}" target="_blank" class="btn btn-query">
                                            <i class="fas fa-address-book"></i> Solicitud
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <td class="query-files-column">
                                <div class="row">
                                    <div class="col-12">
                                        @if(count(get_json($printable->uploads, 'identity')) > 0)
                                            <a href="{{ asset(get_json($printable->uploads, 'identity')[0]['url']) }}" target="_blank" class="btn btn-query">
                                                <i class="fas fa-id-card"></i> Identidad
                                            </a>
                                        @endif
                                    </div>
                                    <div class="col-12">
                                        @if(count(get_json($printable->uploads, 'signature')) > 0)
                                            <a href="{{ asset(get_json($printable->uploads, 'signature')[0]['url']) }}" target="_blank" class="btn btn-query">
                                                <i class="fas fa-file-pdf"></i> Firma
                                            </a>
                                        @endif
                                    </div>
                                    <div class="col-12">
                                        @if(count(get_json($printable->uploads, 'deed')) > 0)
                                            <a href="{{ asset(get_json($printable->uploads, 'deed')[0]['url']) }}" target="_blank" class="btn btn-query">
                                                <i class="fas fa-file"></i> Escritura
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                            <td>
                                @if(count(get_json($printable->uploads, 'signature')) > 0)
                                    {{ url('/') . get_json($printable->uploads, 'signature')[0]['url'] }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    @include('bank.account.partials._queryScripts')
@stop
