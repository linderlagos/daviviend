<!DOCTYPE html>
<html lang="es">

    <head>
        @include('bank.account.printable.3.partials._meta', ['title' => 'Solicitud Apertura de Cuenta de Depósitos'])
    </head>

    <!-- Set "A5", "A4" or "A3" for class name -->
    <!-- Set also "landscape" if you need -->
    <body class="letter">

        <!-- Each sheet element should have the class "sheet" -->
        <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
        <section class="sheet padding-10mm">
            <div class="cover">
                @include('bank.account.printable.3.partials._coverHeader')

                <table>
                    <tr>
                        <td class="width-30 column-title">Oficial:</td>
                        <td>{{ get_json($print->user_information, ['agent', 'code']) }}</td>
                        <td class="column-title">Fecha de apertura:</td>
                        <td>{{ $print->created_at->format('d/m/Y') }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Número de cuenta:</td>
                        <td>{{ $print->identifier }}</td>
                        <td class="column-title">Moneda:</td>
                        <td>HNL</td>
                    </tr>
                </table>

                <div class="separator"></div>

                <table class="columns-4">
                    <tr>
                        <td class="width-30 column-title">Oficina:</td>
                        <td>{{  get_json($print->user_information, ['branch', 'code']) }}</td>
                        <td class="column-title">CIF:</td>
                        <td colspan="2">{{ get_json($print->customer_information, ['cif', 'value']) }} {{ get_json($print->customer_information, 'customer_risk') }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Nombre de EL CLIENTE:</td>
                        <td colspan="4">{{  get_json($print->customer_information, ['name', 'fullname']) }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Dirección:</td>
                        <td colspan="4">{{ address($print) }}</td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <td class="column-title">Dirección y punto de referencia confirmados:</td>
                        <td>
                            SI
                        </td>
                        <td>
                            <div class="fill-box">X</div>
                        </td>
                        <td>
                            NO
                        </td>
                        <td>
                            <div class="fill-box"></div>
                        </td>
                    </tr>
                </table>

                <div class="separator"></div>

                <table class="columns-4">
                    <tr>
                        <td class="width-30 column-title">CIF</td>
                        <td>{{ get_json($print->customer_information, ['cif', 'value']) }}</td>
                        <td class="column-title">Sexo:</td>
                        <td>{{ get_json($print->customer_information, ['gender', 'value'])  }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Ciudadanía:</td>
                        <td>{{ get_json($print->customer_information, ['nationality', 'value']) }}</td>
                        <td class="column-title">Estado civil:</td>
                        <td>{{ get_json($print->customer_information, ['marital_status', 'value']) }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Nacionalidad:</td>
                        <td colspan="3">{{ get_json($print->customer_information, ['nationality', 'value']) }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Lugar de nacimiento:</td>
                        <td colspan="3">{{ get_json($print->customer_information, 'birthplace') }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">País de nacimiento:</td>
                        <td colspan="3">{{ get_json($print->customer_information, ['nationality', 'value']) }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Fecha de nacimiento:</td>
                        <td colspan="3">{{ get_json_date(get_json($print->customer_information, 'birth'), 'd/m/Y') }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Tipo de Identificación:</td>
                        <td>{{ idTypeValue($print->customer->identifier_type)}}</td>
                        <td class="column-title">RTN:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Identidad/Carnet Residente:</td>
                        <td>{{ $print->customer->identifier }}</td>
                        <td class="column-title">Pasaporte:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Tel. de casa:</td>
                        <td>{{ get_json($print->customer_information, 'phone') !== 'NULO' && get_json($print->customer_information, 'phone') ? '(504) ' . formatPhone(get_json($print->customer_information, 'phone')) : '' }}</td>
                        <td class="column-title">Celular:</td>
                        <td>{{ get_json($print->customer_information, 'mobile') !== 'NULO' && get_json($print->customer_information, 'mobile') ? '(504) ' . formatPhone(get_json($print->customer_information, 'mobile')) : '' }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="column-title">Fax:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Profesión u oficio:</td>
                        <td colspan="3">{{ get_json($print->customer_information, ['profession', 'value']) }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Actividad Económica:</td>
                        <td colspan="3">{{ get_json($print->customer_information, ['economic_activity', 'value']) }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Actividad Comercial:</td>
                        <td colspan="3">{{ get_json($print->customer_information, ['employer', 'job', 'type', 'value']) }}</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Número de dependientes:</td>
                        <td colspan="3">0</td>
                    </tr>
                    <tr>
                        <td class="width-30 column-title">Correo electrónico:</td>
                        <td colspan="3">{{ get_json($print->customer_information, ['email', 'account']) == 'NOTIENE@DAVIVIENDA.COM.HN' ? '' : get_json($print->customer_information, ['email', 'account']) }}</td>
                    </tr>
                </table>

                <div class="separator"></div>

                <h2>Datos del apoderado legal</h2>

                <table>
                    <tr>
                        <td class="column-title">Nombre completo:</td>
                        <td>NO TIENE</td>
                    </tr>
                    <tr>
                        <td class="column-title">Identificación:</td>
                        <td>NO TIENE</td>
                    </tr>
                </table>

                <div class="separator"></div>

                @if(in_array(get_json($print->customer_information, ['employer', 'job', 'type', 'code']), [10,11,44]))
                    <h2>Datos de la persona de la cual depende</h2>

                    <table>
                        <tr>
                            <td class="column-title">Nombre completo:</td>
                            <td>{{ get_json($print->customer_information, ['professional_dependant', 'name']) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Tipo de identificación:</td>
                            <td>{{ idTypeValue(get_json($print->customer_information, ['professional_dependant', 'id_type', 'code'])) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Identificación:</td>
                            <td>{{ get_json($print->customer_information, ['professional_dependant', 'identity']) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Parentesco:</td>
                            <td>{{ get_json($print->customer_information, ['professional_dependant', 'relationship', 'value']) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Actividad comercial:</td>
                            <td>{{ get_json($print->customer_information, ['professional_dependant', 'job_type', 'value']) }}</td>
                        </tr>
                    </table>
                @else
                    <h2>Datos del empleador</h2>

                    <table>
                        <tr>
                            <td class="column-title">Nombre:</td>
                            <td>{{ get_json($print->customer_information, ['employer', 'name']) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Dirección:</td>
                            <td>{{ address($print, 'employer') }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Cargo:</td>
                            <td>{{ get_json($print->customer_information, ['employer', 'job', 'name']) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Correo laboral:</td>
                            <td>{{ get_json($print->customer_information, ['job_email', 'account']) == 'NOTIENE@DAVIVIENDA.COM.HN' ? '' : get_json($print->customer_information, ['job_email', 'account']) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Teléfono:</td>
                            <td>(504) {{ formatPhone(get_json($print->customer_information, ['employer', 'phone'])) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Ingreso mensual:</td>
                            <td>
                                @if (!empty(get_json($print->customer_information, ['income'])) && get_json($print->customer_information, ['income']) != 'NULO')
                                    {{ number_format(get_json($print->customer_information, ['income']), 2) }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="column-title">En empleo desde:</td>
                            <td>{{ get_json_date(get_json($print->customer_information, ['employer', 'started_at']), 'd/m/Y') }}</td>
                        </tr>
                    </table>
                @endif
            </div>

            @include('bank.account.printable.3.partials._coverFooter', ['i' => 1])

        </section>

        <section class="sheet padding-10mm">
            <div class="cover">
                @include('bank.account.printable.3.partials._coverHeader')

                <h2>Dependientes</h2>

                <table>
                    <tr>
                        <td>NO TIENE</td>
                    </tr>
                </table>

                <div class="separator"></div>

                @if(get_json($print->customer_information, ['marital_status', 'code']) === 'C')
                    <h2>Datos del cónyuge</h2>

                    <table>
                        <tr>
                            <td>{{ get_json($print->customer_information, 'spouse') }}</td>
                        </tr>
                    </table>

                    <div class="separator"></div>
                @endif

                <table>
                    <tr>
                        <td>¿Ha desempeñado un Cargo Público en el Gobierno en los últimos 4 años? R/: {{ get_json($print->customer_information, ['public_job', 'code']) }}</td>
                    </tr>
                </table>

                @if(get_json($print->customer_information, ['public_job', 'code']) === 'S')
                    <table>
                        <tr>
                            <td class="column-title">Nombre de la institución:</td>
                            <td>{{ get_json($print->customer_information, ['public_job', 'employer_name']) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Cargo desempeñado:</td>
                            <td>{{ get_json($print->customer_information, ['public_job', 'job']) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">¿Es actual?:</td>
                            <td>{{ get_json($print->customer_information, ['public_job', 'active']) }}</td>
                        </tr>
                        <tr>
                            <td class="column-title">Fecha de inicio:</td>
                            <td>{{ get_json_date(get_json($print->customer_information, ['public_job', 'from']), 'd/m/Y') }}</td>
                        </tr>
                        @if(get_json($print->customer_information, ['public_job', 'active']) === 'N')
                            <tr>
                                <td class="column-title">Fecha final:</td>
                                <td>{{ get_json_date(get_json($print->customer_information, ['public_job', 'to']), 'd/m/Y') }}</td>
                            </tr>
                        @endif
                    </table>
                @endif

                <div class="separator"></div>

                <table>
                    <tr>
                        <td>¿La cuenta es para manejo de fondos de un tercero? R:/ N</td>
                    </tr>
                </table>

                <div class="separator"></div>

                <table>
                    <tr>
                        <th>BENEFICIARIO</th>
                        <th>PARENTESCO</th>
                        <th>IDENTIFICACION</th>
                    </tr>
                    @foreach(get_json($print->customer_information, ['beneficiaries']) as $key => $beneficiary)
                        @if(isset($beneficiary['name']))
                            <tr class="text-center">
                                <td>{{ get_json($beneficiary, ['name']) !== 'NULO' &&  get_json($beneficiary, ['name'])? get_json($beneficiary, ['name']) : ''}}</td>
                                <td>{{ get_json($beneficiary, ['relationship', 'value']) !== 'NULO' && get_json($beneficiary, ['relationship', 'value']) ? get_json($beneficiary, ['relationship', 'value']) : ''}}</td>
                                <td>{{ get_json($beneficiary, ['identity']) !== 'NULO' && get_json($beneficiary, ['identity']) ? get_json($beneficiary, ['identity']) : ''}}</td>
                            </tr>
                        @endif
                    @endforeach
                </table>

                <div class="separator"></div>

                <h2>Referencias</h2>

                <table class="text-center">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th colspan="2">Confirmada</th>
                    </tr>
                    <tr>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                        <th>Celular</th>
                        <th>Si</th>
                        <th>No</th>
                    </tr>
                    @foreach(get_json($print->customer_information, ['references']) as $key => $reference)
                        @if(isset($reference))
                            <tr>
                                <td>{{ get_json($reference, ['name']) }}</td>
                                <td>{{ get_json($reference, ['phone']) ? '(504) ' . $reference['phone'] : '' }}</td>
                                <td>{{ get_json($reference, ['mobile']) ? '(504) ' . formatPhone($reference['mobile']) : '' }}</td>
                                <td>X</td>
                                <td></td>
                            </tr>
                        @endif
                    @endforeach
                </table>

                <div class="separator"></div>

                <h2>Datos financieros del cliente</h2>

                <table>
                    <tr>
                        <th></th>
                        <th>Rango</th>
                    </tr>

                    <tr>
                        <td class="column-title">Total activos:</td>
                        <td class="text-center">{{ convertRangeToNumberFormat(get_json($print->customer_information, ['assets', 'value'])) }}</td>
                    </tr>

                    <tr>
                        <td class="column-title">Total pasivos:</td>
                        <td class="text-center">{{ convertRangeToNumberFormat(get_json($print->customer_information, ['passive', 'value'])) }}</td>
                    </tr>

                    <tr>
                        <td class="column-title">Total Ingresos Mensuales:</td>
                        <td class="text-center">{{ number_format($print->customer_information['income_range']['initial'] === false ? 0 : $print->customer_information['income_range']['initial'], 2) }} - {{ number_format(get_json($print->customer_information, ['income_range', 'end']), 2) }}</td>
                    </tr>

                    <tr>
                        <td class="column-title">Total egresos mensuales:</td>
                        <td class="text-center">{{ convertRangeToNumberFormat(get_json($print->customer_information, ['expenses', 'value'])) }}</td>
                    </tr>
                </table>

                <div class="separator"></div>

                <h2>Firmas autorizadas</h2>

                <table>
                    <tr>
                        <td class="column-title">Tipo de firmas:</td>
                        <td>Indistinta</td>
                    </tr>
                </table>

                <table class="text-center">
                    <tr>
                        <th>Nombre</th>
                        <th>Identidad</th>
                        <th>CIF</th>
                    </tr>
                    <tr>
                        <td>{{ get_json($print->customer_information, ['name', 'fullname']) }}</td>
                        <td>{{ $print->customer->identifier }}</td>
                        <td>{{ get_json($print->customer_information, ['cif', 'value']) }}</td>
                    </tr>
                </table>
            </div>

            @include('bank.account.printable.3.partials._coverFooter', ['i' => 2])

        </section>


        <section class="sheet padding-10mm">
            <div class="cover">
                @include('bank.account.printable.3.partials._coverHeader')
                <h2>Actividad y fuente de ingresos del cliente</h2>

                <table>
                    <!-- este campo no se encuentra en la nueva estructura (purpose) -->
                    <tr>
                        <td class="column-title">Propósito de la cuenta:</td>
                        <td>{{ get_json($print->product_information, ['purpose']) }}</td>
                    </tr>
                    <tr>
                        <td class="column-title">Actividad comercial:</td>
                        <td>{{ get_json($print->customer_information, ['employer', 'job', 'type', 'value']) }}</td>
                    </tr>
                    <tr>
                        <td class="column-title">Ingresos mensuales:</td>
                        <td>
                            @if (!empty(get_json($print->customer_information, ['income'])) && get_json($print->customer_information, ['income']) != 'NULO')
                                {{ number_format(get_json($print->customer_information, ['income']), 2) }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="column-title">Fuente de ingresos:</td>
                        <td>{{ get_json($print->customer_information, 'income_source')  }}</td>
                    </tr>
                </table>

                <div class="separator"></div>

                <h2>Información de operaciones internacionales</h2>

                <table>
                    <tr>
                        <td>¿Realizará Transferencias Internacionales?</td>
                        <td>{{ get_default_or_value(get_json($print->customer_information, ['international_operations_information', 'wire_transfers', 'code']), 'NULO', 'N')  }}</td>
                    </tr>
                    <tr>
                        <td>¿Realizará Compra/Venta de divisas?</td>
                        <td>{{ get_default_or_value(get_json($print->customer_information, ['international_operations_information', 'currency_purchase', 'code']), 'NULO', 'N')  }}</td>
                    </tr>
                    <tr>
                        <td>¿Realizará Emisión de Giros Dólar (Cheques en dólares)?</td>
                        <td>{{ get_default_or_value(get_json($print->customer_information, ['international_operations_information', 'checks_in_dollars', 'code']), 'NULO', 'N')  }}</td>
                    </tr>
                    <tr>
                        <td>¿Recibirá Remesas Familiares?</td>
                        <td>{{ get_default_or_value(get_json($print->customer_information, ['international_operations_information', 'remittances', 'code']), 'NULO', 'N')  }}</td>
                    </tr>
                </table>

                @if(get_json($print->customer_information, ['international_operations_information', 'foreign_currency', 'code']) === 'S')
                    <table>
                        @if(!empty(get_json($print->customer_information, ['international_operations_information', 'typical_monthly_income'])) && get_json($print->customer_information, ['international_operations_information', 'typical_monthly_income']) != 'NULO')
                            <tr>
                                <td>Monto mensual típico estimado:</td>
                                <td>{{ number_format(get_json($print->customer_information, ['international_operations_information', 'typical_monthly_income']), 2) }}</td>
                                <td>Moneda</td>
                                <td>{{ get_json($print->customer_information, ['international_operations_information', 'typical_monthly_currency', 'value']) }}</td>
                            </tr>
                        @endif

                        @if(!empty(get_json($print->customer_information, ['international_operations_information', 'probable_monthly_income'])) && get_json($print->customer_information, ['international_operations_information', 'probable_monthly_income']) != 'NULO')
                            <tr>
                                <td>Monto mensual esperado eventual:</td>
                                <td>{{ number_format(get_json($print->customer_information, ['international_operations_information', 'probable_monthly_income']), 2) }}</td>
                                <td>Moneda</td>
                                <td>{{ get_json($print->customer_information, ['international_operations_information', 'probable_monthly_currency', 'value']) }}</td>
                            </tr>
                        @endif
                    </table>
                @endif


                <table>
                    @if(get_json($print->customer_information, ['international_operations_information', 'received_transfer_sender']) !== 'NULO')
                        <tr>
                            <td colspan="2">Transferencias recibidas</td>
                        </tr>
                        <tr>
                            <td>País de origen:</td>
                            <td>{{ get_json($print->customer_information, ['international_operations_information', 'received_transfer_country', 'value']) }}</td>
                        </tr>
                        <tr>
                            <td>Motivo de la transferencia:</td>
                            <td>{{ get_json($print->customer_information, ['international_operations_information', 'received_transfer_reason']) }}</td>
                        </tr>
                        <tr>
                            <td>Nombre del remitente:</td>
                            <td>{{ get_json($print->customer_information, ['international_operations_information', 'received_transfer_sender']) }}</td>
                        </tr>
                    @endif

                    @if(get_json($print->customer_information, ['international_operations_information', 'send_transfer_sender']) !== 'NULO')
                        <tr>
                            <td colspan="2">Transferencias enviadas</td>
                        </tr>
                        <tr>
                            <td>País de origen:</td>
                            <td>{{  get_json($print->customer_information, ['international_operations_information', 'send_transfer_country', 'value'])  }}</td>
                        </tr>
                        <tr>
                            <td>Motivo de la transferencia:</td>
                            <td>{{  get_json($print->customer_information, ['international_operations_information', 'send_transfer_reason']) }}</td>
                        </tr>
                        <tr>
                            <td>Nombre del beneficiario:</td>
                            <td>{{  get_json($print->customer_information, ['international_operations_information', 'send_transfer_sender']) }}</td>
                        </tr>
                    @endif

                    @if(get_json($print->customer_information, ['international_operations_information', 'remittances', 'code']) == 'S' )
                        <tr>
                            <td colspan="2">Remesas recibidas</td>
                        </tr>
                        <tr>
                            <td>País de origen:</td>
                            <td>{{ get_json($print->customer_information, ['international_operations_information', 'remittance_country', 'value']) }}</td>
                        </tr>
                        <tr>
                            <td>Motivo de la transferencia:</td>
                            <td>{{ get_json($print->customer_information, ['international_operations_information', 'remittance_reason']) }}</td>
                        </tr>
                        <tr>
                            <td>Nombre del beneficiario:</td>
                            <td>{{ get_json($print->customer_information, ['international_operations_information', 'remittance_sender']) }}</td>
                        </tr>
                        <tr>
                            <td>Parentesco:</td>
                            <td>{{ get_json($print->customer_information, ['international_operations_information', 'remittance_sender_relationship', 'value']) }}</td>
                        </tr>
                    @endif
                </table>

                <div class="separator"></div>

                <h2>Perfil de la utilización mensual de la cuenta y servicios</h2>

                <table>
                    <tr>
                        <th colspan="2">Montos de los depósitos esperados</th>
                        <th colspan="2">Montos de los retiros esperados</th>
                    </tr>
                    <tr>

                        <td class="column-title">Total de depósito:</td>
                        <td>{{ number_format(get_json($print->product_information, ['expected_deposits']), 2) }}</td>
                        <td class="column-title">Total de retiro:</td>
                        <td>{{ number_format(get_json($print->product_information, ['expected_withdrawals']), 2) }}</td>
                    </tr>
                </table>

                <div class="separator"></div>

                <h2>Cuentas en otras instituciones nacionales:</h2>

                <table>
                    <tr>
                        <th>Entidad</th>
                    </tr>
                    <tr class="text-center">
                        <td>NO TIENE</td>
                    </tr>
                </table>

                <div class="separator"></div>

                <h2>Cuentas en instituciones extranjeras:</h2>

                <table>
                    <tr>
                        <th>Entidad</th>
                    </tr>
                    <tr class="text-center">
                        <td>NO TIENE</td>
                    </tr>
                </table>

            </div>

            @include('bank.account.printable.3.partials._coverFooter', ['i' => 3])

        </section>

        <section class="sheet padding-10mm">
            <div class="cover">
                @include('bank.account.printable.3.partials._coverHeader')

                <h2>Validación Fatca</h2>

                <table>
                    <tr>
                        <td>¿Tiene usted nacionalidad o pasaporte Americano</td>
                        <td>NO</td>
                    </tr>
                    <tr>
                        <td>¿Tiene usted residencia legal(fiscal) Americana</td>
                        <td>NO</td>
                    </tr>
                    <tr>
                        <td>¿Ha tenido usted presencia sustancial en Estados Unidos?</td>
                        <td>NO</td>
                    </tr>
                </table>

                <div class="separator"></div>

                <h2>Declaración del cliente</h2>

                <p>Declaro que he examinado la información en este formulario y doy fe que la misma es verdadera, correcta y completa. Además, certifico: Que no soy una persona Estadounidense, ni tengo obligaciones fiscales en EE. UU. de acuerdo a su legislación vigente.</p>

                <div class="separator"></div>

                @if ($safeFamily)
                    <h2>Seguros</h2>

                    <table class="text-center">
                        <tr>
                            <th>Ramo</th>
                            <th>Póliza</th>
                            <th>Tipo</th>
                            <th>Identidad</th>
                            <th>Asegurado</th>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>{{ $safeFamily->identifier }}</td>
                            <td>T</td>
                            <td>{{ $safeFamily->customer->identifier }}</td>
                            <td>{{ get_json($safeFamily->customer_information, ['name', 'fullname']) }}</td>
                        </tr>
                    </table>

                    <div class="separator"></div>
                @endif

                <table>
                    <tr>
                        <th colspan="2">Tarjeta(s) DAVIVIENDA</th>
                    </tr>
                    <tr>
                        <td>[NO HAY INFORMACIÓN]</td>
                        <td>{{ get_json($print->customer_information, ['name', 'fullname']) }}</td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <th colspan="2">Acuse recibo Tarjeta Débito</th>
                    </tr>
                    <tr>
                        <td>Si:</td>
                        <td>No:</td>
                    </tr>
                </table>

            </div>

            @include('bank.account.printable.3.partials._coverFooter', ['i' => 4])
        </section>

        @include('modules.printable._printButton')

    </body>

</html>