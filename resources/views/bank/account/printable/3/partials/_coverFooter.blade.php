<div class="footer">
    <table>
        <tr>
            <td>Solicitud de Apertura de Cuenta: {{ $print->identifier }}</td>
            <td class="text-right">Página {{ $i }} de 4</td>
        </tr>
        <tr>
            <td></td>
            <td class="text-right">{{ $print->created_at->format('d/m/Y H:i:s') }}</td>
        </tr>
    </table>
</div>