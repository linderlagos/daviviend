<!DOCTYPE html>
<html lang="es">

<head>
    @include('bank.account.printable.1.partials._meta', ['title' => 'Firma cuenta de ahorro'])
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="letter">

<!-- Each sheet element should have the class "sheet" -->
<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
<section class="sheet padding-10mm">
    <div class="signature">

        <div class="header">
            <div class="logo text-center">
                <img src="{{ asset('design/print/logo.jpg') }}">
            </div>
            <h1 class="text-center uppercase">Solicitud de apertura de cuenta de depósitos</h1>
            <h2 class="text-center uppercase">Cuenta de inversión S/Libreta</h2>

            <table>
                <tr>
                    <td class="strong">No. de cuenta:</td>
                    <td class="column-value">{{ $print->identifier }}</td>
                    <td class="strong">Moneda:</td>
                    <td class="column-value">HNL</td>
                    <td class="strong">Fecha Apertura:</td>
                    <td class="column-value">{{ $print->created_at->format('d/m/Y') }}</td>
                    <td class="strong">Oficina:</td>
                    <td class="column-value">{{ get_json($print->user_information, ['branch', 'code']) }}</td>
                </tr>
            </table>

            <table>
                <tr>
                    <td class="strong">Nombre del CLIENTE:</td>
                    <td class="uppercase column-value">{{ get_json($print->customer_information, ['name', 'fullname']) }}</td>
                    <td class="strong">CIF:</td>
                    <td class="column-value">{{ get_json($print->customer_information, ['cif', 'value']) }}</td>
                    <td>{{ get_json($print->customer_information, 'customer_risk') }}</td>
                </tr>
            </table>
        </div>

        <div class="content">
            <p>El <b>CLIENTE</b> manifiesta estar de acuerdo con todas las cláusulas del contrato, aceptando la totalidad de su contenido y comprometiéndose a su fiel cumplimiento.</p>
            <p>El <b>CLIENTE</b> con la firma del presente, solicita y autoriza la adopción de los siguientes servicios:</p>

            <table class="full-width fillable-table">
                <tr>
                    <th>SI</th>
                    <th>NO</th>
                    <th class="text-left">CONCEPTO</th>
                </tr>
                <tr>
                    <td class="fill-box"></td>
                    <td class="fill-box"></td>
                    <td>Contrato suscrito digital</td>
                </tr>
                <tr>
                    <td class="fill-box"></td>
                    <td class="fill-box"></td>
                    <td>Tarjeta de débito</td>
                </tr>
                <tr>
                    <td class="fill-box"></td>
                    <td class="fill-box"></td>
                    <td>Pin Tarjeta</td>
                </tr>
                <tr>
                    <td class="fill-box"></td>
                    <td class="fill-box"></td>
                    <td>Pin Banca Personal por Internet</td>
                </tr>
                <tr>
                    <td class="fill-box"></td>
                    <td class="fill-box"></td>
                    <td>Condiciones y Cobertura Programa</td>
                </tr>
                <tr>
                    <td class="fill-box"></td>
                    <td class="fill-box"></td>
                    <td>Antifraude Tarjeta de Débito</td>
                </tr>
            </table>

            <p>El <b>CLIENTE</b> declara y garantiza que toda la información expresada y acreditada en el formulario es exacta y verdadera y no se ha omitido dato alguno que se requiera informar para la prestación del servicio, refrendando con su firma toda y cada una de las partes del presente documento, autorizando al banco para la prestación del servicio. Por tanto, el <b>CLIENTE</b>, exime al banco de cualquier tipo de responsabilidades presente, pasada futura por falsedad o inexactitud en la información proporcionada. Asimismo, el <b>CLIENTE</b> declara haber recibido la documentación contractual firmada y aceptada por este acto, la cual confirma haberla recibido a su correo electrónico.</p>

            <table>
                <tr>
                    <td class="strong">Registro de firmas:</td>
                    <td>(1)</td>
                    <td class="strong">Tipo:</td>
                    <td>Indistinta</td>
                </tr>
            </table>

            <table class="signature-table margin-center text-center">
                <tr>
                    <td>1 (Titular)</td>
                </tr>
                <tr>
                    <td class="customer-signature"></td>
                </tr>
                <tr>
                    <td class="strong uppercase" style="padding-top: 1.6rem">{{ get_json($print->customer_information, ['name', 'fullname']) }}</td>
                </tr>
                <tr>
                    <td><b>No. de Identificación:</b> {{ $print->customer->identifier }}</td>
                </tr>
                <tr>
                    <td>
                        <b>Clasificación de Firma:</b> A
                    </td>
                </tr>
            </table>

            <table class="signature-table margin-center text-center">
                <tr>
                    <td class="advisor-signature"></td>
                </tr>
                <tr>
                    <td>Elaborado por</td>
                </tr>
                <tr>
                    <td>{{ get_json($print->user_information, ['agent', 'value']) }}</td>
                </tr>
            </table>
        </div>
    </div>
</section>

@include('modules.printable._printButton')

</body>

</html>