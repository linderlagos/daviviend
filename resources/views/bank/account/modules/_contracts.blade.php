@if ($safeFamilyStatus !== 3 && !cifUsers($peoplesoft))
    <div class="row justify-content-center hide-insurance-safe-family">
        <div class="col-12 col-lg-9 col-xl-6">
            <div class="row text-center">
                <div class="col-12">
                    <div class="btn davivienda-file davivienda-card">
                        <img src="{{ asset('design/index/safe-family.png') }}">
                        <p>¿Desea solicitar Familia Segura?</p>
                        <div class="row">
                            <div class="col-6">
                                <form class="form-horizontal" role="form" method="POST" action="{{ route('insurance.safe.family.first', [$customer->identifier, $customer->random]) }}" id="get-insurance-safe-family">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="btn btn-davivienda-red" id="submit">
                                        Si
                                    </button>
                                </form>
                            </div>
                            <div class="col-6">
                                <form class="form-horizontal" role="form" method="POST" action="{{ route('account.reject.insurance.safe.family', [$customer->identifier, $customer->random, 'insurance_safe_family']) }}" id="reject-insurance-safe-family">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="btn btn-davivienda-red" id="submit">
                                        No
                                    </button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

<div class="hide-documents">

    <div class="row">
        <div class="col-12 form-group">
            <h2><i class="fas fa-print"></i> Seleccione el documento a generar:</h2>
        </div>
    </div>

    <div class="row" style="margin-bottom: 2rem">
        <div class="col-6 col-lg-3">
            <a href="{{ route('account.print.signature', [$customer->identifier, $customer->random]) }}" class="btn davivienda-file" target="_blank">
                <i class="fas fa-file-signature"></i>
                <p>Firma</p>
            </a>
        </div>

        <div class="col-6 col-lg-3">
            <a href="{{ route('account.print.contract', [$customer->identifier, $customer->random]) }}" class="btn davivienda-file" target="_blank">
                <i class="fas fa-file-alt"></i>
                <p>Contrato de cuenta</p>
            </a>
        </div>

        <div class="col-6 col-lg-3">
            <a href="{{ route('account.print.cover', [$customer->identifier, $customer->random]) }}" class="btn davivienda-file" target="_blank">
                <i class="fas fa-address-book"></i>
                <p>Solicitud de cuenta</p>
            </a>
        </div>

        <div class="col-6 col-lg-3">
            <a href="{{ $printDebitCardURL }}" class="btn davivienda-file" target="_blank">
                <i class="fas fa-credit-card"></i>
                <p>Embozar tarjeta</p>
            </a>
        </div>

        @if ($safeFamilyStatus === 2 && $customer->version !== '1')
{{--        @if ($safeFamilyStatus === 3)--}}
            <div class="col-6 col-lg-3">
                <a href="{{ route('insurance.safe.family.print.policy', [$safeFamilyPrintable->customer->identifier, $safeFamilyPrintable->random]) }}" class="btn davivienda-file" target="_blank">
                    <i class="fas fa-user-lock"></i>
                    <p>Póliza</p>
                </a>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-12 form-group">
            <h2><i class="fas fa-cloud-upload-alt"></i> Subir documentos:</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-6 col-lg-4">
            @if(isset($identity[0]['url']))
                {{--@if($canUpdateDocument)--}}
                    {{--@include('account.modules._identityDropzone')--}}
                {{--@else--}}
                    <a href="{{ asset($identity[0]['url']) }}" class="btn davivienda-upload" target="_blank">
                        <i class="fas fa-id-card"></i>
                        <p>Identidad</p>
                    </a>
                {{--@endif--}}
            @else
                @include('account.modules._identityDropzone')
            @endif
        </div>
        <div class="col-6 col-lg-4">
            @if(isset($signature[0]['url']))
                {{--@if($canUpdateDocument)--}}
                    {{--@include('account.modules._signatureDropzone')--}}
                {{--@else--}}
                    <a href="{{ asset($signature[0]['url']) }}" class="btn davivienda-upload" target="_blank">
                        <i class="fas fa-signature"></i>
                        <p>Firma</p>
                    </a>
                {{--@endif--}}
            @else
                @include('account.modules._signatureDropzone')
            @endif
        </div>
        <div class="col-6 col-lg-4">
            @if(isset($deed[0]['url']))
                {{--@if($canUpdateDocument)--}}
                    {{--@include('account.modules._deedDropzone')--}}
                {{--@else--}}
                    <a href="{{ asset($deed[0]['url']) }}" class="btn davivienda-upload" target="_blank">
                        <i class="fas fa-file-contract"></i>
                        <p>Escritura de comerciante</p>
                    </a>
                {{--@endif--}}
            @else
                @include('account.modules._deedDropzone')
            @endif
        </div>
    </div>

    @if(cifUsers($peoplesoft))
        <div class="row" style="margin-top: 2rem">
            <div class="col-12 form-group">
                <h2><i class="fas fa-cloud-upload-alt"></i> Documentos soporte:</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div id="support" class="upload-dropzone">
                    <form class="dropzone" action="{{ route('account.file.upload', [$customer->identifier, $customer->random, 'support']) }}" id="supportForm">
                        {!! csrf_field() !!}
                        <div class="dz-message" data-dz-message>
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <h3><i class="fas fa-hands-helping"></i> Documentos soporte</h3>
                                    <p>Subir todos los documentos soporte que se consideren necesarios para la aplicación</p>
                                    <span>
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <table id="support-documents" class="davivienda-datatables">
                    <thead>
                    <tr>
                        <th>Descripción</th>
                        <th>Peoplesoft</th>
                        <th>Link</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if(is_array($support))
                            @foreach($support as $key => $value)
                                <tr>
                                    <td class="text-center">
                                        {{ isset($value['description']) ? $value['description'] : 'No hay descripción' }}
                                    </td>
                                    <td class="text-center">
                                        {{ $value['peoplesoft'] }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ asset($value['url']) }}" class="btn btn-query" target="_blank">Ver documento</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    @endif
</div>