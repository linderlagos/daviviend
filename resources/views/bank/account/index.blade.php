@extends('layouts.form')

{{-- @section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/bank_account.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Formulario de Solicitud de Cuenta de Ahorro</h1>
                </div>
                <div class="banner-description">
                    @if(checkStep($flow, ['1', '2', '3', '4', '5']))
                        <p>Continúe completando la información del cliente con ID No. {{ $flow->customer->identifier }}</p>
                    @elseif(checkStep($flow, 'contracts'))
                        <p>Imprima los documentos del cliente con ID No. {{ $flow->customer->identifier }} y cuenta No. {{ $flow->identifier }}</p>
                    @else
                        <p>Complete la información del cliente para terminar el proceso de solicitud de cuenta de ahorro asistida</p>
                    @endif
                </div>
            </div>
        </div>
        <hr class="cintaGris">
    </div>
@stop

@section('back')
    @if(!checkStep($flow, ['1', '2']))
        <a href="{{ route('index') }}" class="btn-back">
            Inicio
        </a>
    @endif
@stop --}}

{{-- @section('reject')
    @if(checkStep($flow, ['1', '2', '3', '4', '5']))
        @include('modules._reject', [
            'route' => route('exit.flow', ['bank_account']),
            'options' => [
                'TA01' => 'Asesor'
            ]
        ])
    @endif
@stop --}}

@section('content')
<index-account-component :lists="{{ json_encode($lists) }}" :flow="{{ json_encode($flow) }}"></index-account-component>
@endsection
 @section('scripts')
    @include('partials._scripts')
    @include('bank.account.partials._scripts')
@stop 
