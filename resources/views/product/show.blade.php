@extends('layouts.app')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/dropzone/dropzone.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/datatables.css') }}"/>
@stop

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/' . $product->type . '.png') }}">
                </div>
                <div class="banner-title">
                    <h1>{{ product_name($product->type) }} {{ $product->identifier }}</h1>
                </div>
                <div class="banner-description">
                    <p>Imprima los documentos del cliente {{ $product->customer->name }} con ID No. {{ $product->customer->identifier }} y producto No. {{ $product->identifier }}</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    <a href="{{ route('home') }}" class="btn-back">
        Inicio
    </a>
@stop


@section('reject')
    @role(env('ROL_GESTOR'))
        @if (isset($productParameters['cross_sell']) && ((int) $product->version) > 1)
            <div class="text-right">
                <a href="{{ route('cross.sell.index', $product->id) }}" class="btn-back">
                    Venta cruzada
                </a>
            </div>
        @endif
    @endrole
@stop

@if (isset($productParameters['cross_sell']) && ((int) $product->version > 1))
    @section('reject')
        <div class="text-right">
            <a href="{{ route('cross.sell.index', $product->id) }}" class="btn-back">
                Venta cruzada
            </a>
        </div>
    @stop
@endif


@section('content-fluid')

    <div class="row">
        <div class="col-12 col-sm-9">
            @hasanyrole(env('ROL_GESTOR') . '|' . env('ROL_CIF'))
                @include('modules.product.' . $product->type)
            @endhasanyrole

            @if (count($uploadTypes) > 0)

                @hasanyrole(env('ROL_GESTOR') . '|' . env('ROL_CIF'))
                    @include('modules.product.upload')
                @endhasanyrole

                @include('modules.product.documents')
            @endif

            @hasanyrole(env('ROL_CIF') . '|' . env('ROL_FIRMAS'))
                @include('modules.product.activities')
            @endhasanyrole
        </div>
        <div class="col-12 col-sm-3">
            @include('modules.product.information')

            @role(env('ROL_CIF'))
                @include('modules.product.cifStatus')
            @endrole

            @include('modules.product.comments')
        </div>
    </div>

@endsection

@section('scripts')
    @include('modules.product.partials._scripts')
@stop
