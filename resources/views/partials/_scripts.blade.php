{{-- 
<script
  src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js">
</script> --}}

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js">
</script>
<script src="{{ asset('js/plugins/validateNumDash.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker.es.min.js') }}"></script>
{{-- <script src="{{ asset('plugins/select2/es.js') }}"></script> --}}
<script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
<script src="{{ asset('js/plugins/autoNumeric.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/datatables/datatables.js') }}"></script>



<script>
        $(document).ready( function () {
                    $('#auto-flows-in-transit').DataTable({
                    /*     // "info":     false,
                        // responsive: true,
                        "lengthChange": false,
                        "pageLength": 25,
                        "order": [[ 4, "desc" ]],
                        "dom": 'Bfrtip',
                        "buttons": [],
                        "pagingType": "full_numbers",
                        "language": {
                            "sProcessing":     "Procesando...",
                            "sLengthMenu":     "Mostrar _MENU_ registros",
                            "sZeroRecords":    "No se encontraron resultados",
                            "sEmptyTable":     "Ningún dato disponible en esta tabla",
                            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix":    "",
                            "sSearch":         "Buscar:",
                            "sUrl":            "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst":    "<<",
                                "sLast":     ">>",
                                "sNext":     ">",
                                "sPrevious": "<"
                            },
                            "oAria": {
                                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        } */
                    });
                } );
    </script>

    <script>
        $(document).ready(function(){
            //alert("4");
            /* $(".city" ).change(function() {
                alert( "cambio desde jquery" );
            }); */
        });
         function cambio(name){
            alert("entro")
            var valor=document.getElementById(name).value;
            var posicion = valor.indexOf("OTRA COLONIA");
            if(name == "city"){
                if (posicion != -1){
                    alert("visible: city")
                    document.getElementById("other_cityI").style.display = "flex";
                }else{
                    alert("hidden: city")
                    document.getElementById("other_cityI").style.display = "none";
                }
            }else if(name == "employer_city"){
                if (posicion != -1){
                    alert("visible: employer_city")
                    document.getElementById("other_employer_cityI").style.display = "flex";
                }else{
                    alert("hidden: employer_city")
                    document.getElementById("other_employer_cityI").style.display = "none";
                }
            }
        } 
    </script>

 <script>
    $('.menu-bar').on('click', function(){
        $('.sidebar').toggleClass('abrir');
        $('.imgPadre').toggleClass('ico');
        $('.menu-bar').toggleClass('rotar2');


        if ( $(".desplegar").length > 0 ) {
            //alert('Si');
            $('.hijos').toggleClass('desplegar');
            $('#principal').toggleClass('negrita'); 
        } 
    }); 
        
    $('.padre').on('click', function(){
        $('.hijos').toggleClass('desplegar');
        $('.rotara').toggleClass('rotar'); 
        $('#principal').toggleClass('negrita');  
        //if ( $(".abrir").length > 0 ) {
            //alert('Si');
            //$(".sidebar").toggleClass('abrir');
            //$('.sidebar').toggleClass('abrir');
            //$('.hijos').toggleClass('desplegar');
       //}
    });



  /*   $(".menu-bar").click(function(){
        if ($(this).hasClass('desplegar')){
            alert('Si');
        }else{
            alert('No');
        }
    }); */

</script>



<script>
    $('.in-progress-step').delay(300).queue(function () {
        $(this).css('width', '100%')
    });
</script>

<script> // formato campos de fecha
    $(document).ready(function() {
        $('.date').datepicker({
            format: 'dd/mm/yyyy',
            language: 'es'
        });
    });
</script>

<script> //definir lenguaje en español
    $(document).ready(function() {
        $('.davivienda-select').select2({
            language: "es"
        });
    });
</script>

<script> // sólo números
    $('.format-amount').autoNumeric('init');

    $(document).on("keypress", 'form', function (e) {
        let code = e.keyCode || e.which;
        if (code === 13) {
            e.preventDefault();
            return false;
        }
    });
</script>

<script> // seleccionar si es otra ciudad (muestra el input 'otra ciudad')
    $(document).ready(function() {

        $('.hide-other-city, .hide-other-employer-city').hide();

        if ($('#city option:selected').text().includes('OTRA COLONIA')) {
            $('.hide-other-city').fadeIn();
        }

        $('#city').change(function () {
            let str = $('#city option:selected').text();

            if (str.includes('OTRA COLONIA')) {
                $('.hide-other-city').fadeIn();
            } else {
                $('.hide-other-city').fadeOut();
            }
        });

        if ($('#employer_city option:selected').text().includes('OTRA COLONIA')) {
            $('.hide-other-employer-city').fadeIn();
        }

        $('#employer_city').change(function () {
            let str = $('#employer_city option:selected').text();

            if (str.includes('OTRA COLONIA')) {
                $('.hide-other-employer-city').fadeIn();
            } else {
                $('.hide-other-employer-city').fadeOut();
            }
        });
    });
</script>

<script> // estado marital, esconde los campos que no se ocupan
    $(document).ready(function() {

        $('.hide-marital-status, .hide-public-job-active, .hide-public-job').hide();

        if ($('#marital_status option:selected').text() == "CASADO") {
            $('.hide-marital-status').fadeIn();
        }

        $('#marital_status').change(function () {
            if ($('#marital_status option:selected').text() == "CASADO") {
                $('.hide-marital-status').fadeIn();
            } else {
                $('.hide-marital-status').fadeOut();
            }
        });

        if ($('#public_job_active option:selected').text() == "No") {
            $('.hide-public-job-active').fadeIn();
        }

        $('#public_job_active').change(function () {
            if ($('#public_job_active option:selected').text() == "No") {
                $('.hide-public-job-active').fadeIn();
            } else {
                $('.hide-public-job-active').fadeOut();
            }
        });

        if ($('#public_job option:selected').text() == "Si") {
            $('.hide-public-job').fadeIn();
        }

        $('#public_job').change(function () {
            if ($('#public_job option:selected').text() == "Si") {
                $('.hide-public-job').fadeIn();
            } else {
                $('.hide-public-job').fadeOut();
            }
        }); 

        // if ($('#dependants option:selected').text() == "Si") {
        //     $('.hide-dependants').fadeIn();
        // }
        //
        // $('#dependants').change(function () {
        //     if ($('#dependants option:selected').text() == "Si") {
        //         $('.hide-dependants').fadeIn();
        //     } else {
        //         $('.hide-dependants').fadeOut();
        //     }
        // });
    });
</script>

<script> // chequear si tiene correo
    $(document).ready(function() {
        let checkbok = $('#has_email');

        if (checkbok.prop('checked')) {
            $('#email').attr("placeholder", "Digite: notiene@davivienda.com.hn");
        }

        checkbok.change(function() {
            if(this.checked) {
                $('#email').attr("placeholder", "Digite: notiene@davivienda.com.hn");
            } else {
                $('#email').attr("placeholder", "Ingrese su correo electrónico");
            }
        });
    });
</script>

<script> // chequear si tiene correo de trabajo
    $(document).ready(function() {
        let checkbok = $('#has_job_email');

        if (checkbok.prop('checked')) {
            $('#job_email').attr("placeholder", "Digite: notiene@davivienda.com.hn");
        }

        checkbok.change(function() {
            if(this.checked) {
                $('#job_email').attr("placeholder", "Digite: notiene@davivienda.com.hn");
            } else {
                $('#job_email').attr("placeholder", "Ingrese su correo electrónico");
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('.hide-if-no-email, .hide-if-no-job-email').hide();

        let email = $('#has_email');

        if (!email.prop('checked')) {
            $('.hide-if-no-email').fadeIn();
        }

        email.change(function() {
            if(this.checked) {
                $('.hide-if-no-email').fadeOut();
            } else {
                $('.hide-if-no-email').fadeIn();
                $('#receive_emails').prop( "checked", true );
            }
        });

        let jobEmail = $('#has_job_email');

        if (!jobEmail.prop('checked')) {
            $('.hide-if-no-job-email').fadeIn();
        }

        jobEmail.change(function() {
            if(this.checked) {
                $('.hide-if-no-job-email').fadeOut();
            } else {
                $('.hide-if-no-job-email').fadeIn();
                $('#receive_emails_job').prop( "checked", true );
            }
        });
    });

</script>


