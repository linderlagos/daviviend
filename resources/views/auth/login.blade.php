@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<div class="container"  >
    <div class="row justify-content-center" style="opacity: 0.8">
        <div class="col-md-7">
            <div class="card" >

                <div class="card-header" style=" background-image: url('images/tecnologia1.jpg'); color: #000000; opacity: 0.9"><img src="images/motive_right.png" align="left;" style=" margin-right:10px;">{{ __('Login') }}</div>

                <div class="card-body" >
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row" style="color: #000000; opacity: 3.9">
                            <label for="" style="color: #000000; opacity: 3.9" class="col-md-4 col-form-label text-md-right">{{ __('PeopleSoft') }}</label>

                            <div class="col-md-6" style="color: #000000; opacity: 3.9">
                                <input id="email" style="color: #000000; opacity: 3.9" type="" class="form-control " name="peopleSoft" required autocomplete="peopleSoft" >

                                
                            </div>
                        </div>

                        <div class="form-group row" style="color: #000000; opacity: 2.9">
                            <label for="password" style="color: #000000;  opacity: 2.9" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                            <div class="col-md-6" style="color: #000000;  opacity: 2.9">
                                <input id="password" style="color: #000000;  opacity: 2.9" type="password" class="form-control " name="password" required autocomplete="current-password">

                                
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary" style="color: #FFFFFF; opacity: 2.9">
                                    {{ __('Entrar') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  



@endsection



