@extends('layouts.app')

@section('meta')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/datatables.css') }}"/>
@stop

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/assistant.png') }}">
                </div>
                <div class="banner-title">
                    <h1>Consulta la información de productos</h1>
                </div>
                <div class="banner-description">
                    <p>Puede revisar la información de los productos que han sido aprobados por el flujo de cuenta de ahorro asistida</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('back')
    <a href="{{ route('home') }}" class="btn-back">
        Inicio
    </a>
@stop

@section('content-fluid')

    <div class="row">
        <div class="col-12 form-group">
            <h2>Listado de clientes:</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            Filtros
        </div>
    </div>

    <form class="form-horizontal" role="form" method="GET" id="form-query">
{{--        {!! csrf_field() !!}--}}

        <div class="row">
            <div class="col-2">
                <div class="form-group">
                    @include('modules._textInput', [
                        'name' => 'from',
                        'placeholder' => 'Desde',
                        'value' => $request->from,
                        'label' => 'Desde',
                        'classes' => 'date',
                    ])
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    @include('modules._textInput', [
                        'name' => 'to',
                        'placeholder' => 'Hasta',
                        'value' => $request->to,
                        'label' => 'Hasta',
                        'classes' => 'date',
                    ])
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    @include('modules._textInput', [
                        'name' => 'customer',
                        'placeholder' => 'Identidad',
                        'value' => $request->customer,
                        'label' => 'Identidad'
                    ])
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    @include('modules._textInput', [
                        'name' => 'product',
                        'placeholder' => 'No. del producto',
                        'value' => $request->product,
                        'label' => 'Producto'
                    ])
                </div>
            </div>
            <div class="col-2">
                @role(env('ROL_CIF'))
                    <div class="form-group">
                        @include('modules._singleSelect', [
                            'name' => 'audit',
                            'placeholder' => 'Estado del expediente',
                            'default' => $request->audit,
                            'options' => $auditParameters['cif_types'],
                        ])
                    </div>
                @endrole
            </div>

            <div class="col-2">
                <div class="row">
                    <div class="col-4 form-group">
                        <button class="btn btn-davivienda-red btn-davivienda-update-gray" id="submit" style="padding: 4px 11px 0;">
                            <i class="fas fa-filter"></i>
                        </button>
                    </div>

                    <div class="col-4 form-group">
                        <button class="btn btn-davivienda-green btn-davivienda-update-gray" id="export">
                            <i class="fas fa-file-excel"></i>
                        </button>
                    </div>

                    <div class="col-4 form-group">
                        <a href="{{ route('query.index') }}" class="btn btn-davivienda-gray btn-davivienda-update-gray">
                            <i class="far fa-trash-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-12">
            <table id="account-customers" class="davivienda-datatables">
                <thead>
                    <tr>
                        <th style="width:12%">Identidad</th>
                        <th style="width:12%">Nombre</th>
                        <th style="width:12%">Número del producto</th>
                        <th style="width:12%">Tipo de producto</th>
                        <th style="width:12%">Fecha de aprobación</th>
                        <th style="width:12%">Estado del expediente</th>
                        <th style="width:12%">Link</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td class="text-center">
                                {{ $product->customer->identifier }}
                            </td>
                            <td class="text-center">
                                {{ $product->customer->name }}
                            </td>
                            <td class="text-center">
                                {{ $product->identifier }}
                            </td>
                            <td class="text-center">
                                {{ product_name($product->type) }}
                            </td>
                            <td class="text-center">
                                {{ $product->created_at->format('Y/m/d') }}
                            </td>
                            <td class="text-center">
                                {{ $auditParameters['cif_types'][$product->audits()->where('type', 'cif')->first()->name] }}
                            </td>
                            <td class="text-center" style="padding: 10px">
                                @if(!in_array($product->type, ['bank_auto', 'bank_card']))
                                    <a href="{{ route('show.product', $product->id) }}" class="btn btn-query">
                                        <i class="fas fa-address-book"></i> Ver producto
                                    </a>
                                @endif
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    @include('partials._scripts')
    @include('bank.account.partials._queryScripts')
@stop
