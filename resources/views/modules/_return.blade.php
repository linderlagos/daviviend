<form class="form-horizontal" role="form" method="POST" action="{{ route('return.step.in.flow', [$id]) }}" id="return-form">
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn-back">
                <i class="fas fa-backward"></i>
            </button>
        </div>
    </div>
</form>