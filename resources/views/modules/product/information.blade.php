<div class="row">
    <div class="col-12 form-group">
        <h2><i class="fas fa-info-circle"></i> Información General:</h2>
    </div>
</div>

<div class="row">
    <div class="product-general-information col-12">
        <ul>
            <li><strong>Fecha de creación:</strong> {{ $product->created_at->format('d/m/Y') }}</li>
            <li><strong>Cliente:</strong> {{ $product->customer->name }}</li>
            <li><strong>Identidad:</strong> {{ $product->customer->identifier }}</li>
            <li><strong>No. de producto:</strong> {{ $product->identifier }}</li>
            <li><strong>Gestor:</strong> {{ $product->user->name }}</li>
            <li><strong>Versión:</strong> {{ $product->version }}</li>
            <li><strong>Estado del expediente:</strong> {{ $cifAuditTypes[$product->audits()->where('type', 'cif')->first()->name] }}</li>
        </ul>
    </div>
</div>