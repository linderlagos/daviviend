<div class="row">
    <div class="col-12 form-group">
        <h2>
            <i class="fas fa-user-secret"></i> Bitácora de actividades:
        </h2>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <table id="support-documents" class="davivienda-datatables">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Descripción</th>
                    <th style="width: 25%;">Usuario</th>
                    <th style="width: 25%;">Fecha</th>
                </tr>
            </thead>
            <tbody>
                @foreach($activities as $key => $value)
                    <tr>
                        <td class="text-center">
                            {{ $key + 1 }}
                        </td>
                        <td class="text-center">
                            {{ $value->description }}
                        </td>
                        <td class="text-center">
                            {{ $value->causer->name }}
                        </td>
                        <td class="text-center">
                            {{ $value->created_at->format('Y/m/d H:i') }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>