<script type="text/javascript" src="{{ asset('plugins/dropzone/dropzone.js') }}"></script>
<script>
    Dropzone.autoDiscover = false;

    $(document).ready(function() {
        if ($('#upload').length)
        {
            let type = '';

            let upload = new Dropzone("#upload", {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 10,
                maxFiles: 20,
                autoProcessQueue: false,
                acceptedFiles: 'application/pdf'
            });

            upload.on('addedfile', function(file) {
                swal({
                    title: 'Tipo de documento',
                    text: "Por favor seleccione el tipo de documento que está subiendo",
                    type: 'warning',
                    input: 'select',
                    inputOptions: upload_types,
                    inputPlaceholder: 'Seleccione el tipo...',
                    showCancelButton: true,
                    confirmButtonText: 'Subir',
                    cancelButtonText: '¡Cancelar!',
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (value) {
                                resolve();
                            } else {
                            }
                            resolve('Debe seleccionar un tipo de documento')
                        })
                    }
                }).then((result) => {
                    if (result.value) {
                        type = result.value;
                        upload.processQueue();
                    } else {
                        upload.cancelUpload(file);
                    }
                });
            });

            upload.on('sending', function(file, xhr, formData) {
                formData.append("type", type);
            });

            upload.on('error', function(file, errorMessage) {
                swal({
                    title: '¡OH NO!',
                    text: errorMessage.message,
                    type: 'error',
                    confirmButtonText: 'Continuar'
                })
            });
        }
    });
</script>

<script type="text/javascript" src="{{ asset('plugins/datatables/datatables.js') }}"></script>
<script>
    $(document).ready( function () {
        $('.davivienda-datatables').DataTable({
            // "info":     false,
            responsive: false,
            "lengthChange": false,
            "pageLength": 5,
            "pagingType": "full_numbers",
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    } );
</script>