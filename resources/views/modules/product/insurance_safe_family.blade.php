<div class="hide-documents">
    <div class="row">
        <div class="col-12 form-group">
            <h2><i class="fas fa-print"></i> Seleccione el documento a generar de {{ product_name($product->type) }}:</h2>
        </div>
    </div>

    <div class="row" style="margin-bottom: 2rem">

        <div class="col-6 col-lg-3">
            <a href="{{ route('insurance.safe.family.print.policy', $product->id) }}" class="btn davivienda-file" target="_blank">
                <i class="fas fa-user-lock"></i>
                <p>Póliza</p>
            </a>
        </div>
    </div>
</div>