<form class="" role="form" method="POST" action="{{ $route }}" id="return-form" ALIGN="right">
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn-back">
                <span>Salir</span>
            </button>
        </div>
    </div>
</form>