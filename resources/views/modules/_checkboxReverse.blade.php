<div class="row davivienda-checkbox form-group terms align-items-center">
    <div class="col-2 text-center">
        <input id="{{ $name }}"
               @if ($errors->count() > 0)
                   @if(old($name) !== null)
                     checked
                   @endif
               @elseif(isset($json))
                   @if ($json === 'N')
                    checked
                   @endif
               @endif

               name="{{ $name }}" type="checkbox"

               @if(isset($value))
                    value="{{ $value }}"
               @else
                    value="N"
                @endif
        >
    </div>
    <div class="col-10">
        <label class="standard-label" for="{{ $name }}">
            {{ $label }}
        </label>
    </div>
    @if ($errors->has($name))
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <span class="help-block">
                    <strong>{{ $errors->first($name) }}</strong>
                </span>
            </div>
        </div>
    @endif
</div>