<option value="">Seleccione la colonia...</option>
@foreach($options as $groupKey => $groupValue)
    <optgroup label="{{ $groupKey }}">
        @foreach($groupValue as $key => $value)
            <option value="{{ $key }}"
                @if ($key === old($name))
                    selected="selected"
                @elseif(isset($resource))
                    @if(isset($json))
                        @if ($json !== 'NULO')
                            @if ($key == $json)
                                selected="selected"
                            @endif
                        @endif
                    @elseif (isset($resource->$name))
                        @if ($key == $resource->$name)
                            selected="selected"

                        @endif
                    @elseif (isset($default))
                        @if ($key === $default)
                            selected="selected"
                        @endif
                    @endif
                @else
                    @if (isset($default))
                        @if ($key === $default)
                            selected="selected"
                        @endif
                    @endif
                @endif
            >{{ $value }}</option>
        @endforeach
    </optgroup>
@endforeach