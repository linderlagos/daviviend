<div class="container-fluid imgBanner">
{{-- <div class="container-fluid " style="background-image: url('/design/index/Artboard3_4x.png'); display:block; margin:auto; aling: center;"> --}}
    <div class="container">
        <div class="{{ isset($class) ? $class : 'banner' }}">
            <div class="banner-image">
                <img src="{{ isset($image) ? $image : asset('design/index/bank_card.png') }}">
            </div>
            <div class="banner-title">
                <h1>{{ $title }}</h1>
            </div>
            <div class="banner-description">
                <p>{!! $description !!}</p>
            </div>
        </div> 
    </div>
    <hr class="cintaGris">
</div>