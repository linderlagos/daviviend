<textarea

    @if (isset($rows))
        rows="{{ $rows }}"
    @else
        rows="2"
    @endif

        class="form-control
    @if(isset($classes))
        {{ $classes }}
    @endif

        " name="{{ $name }}"

    placeholder="{{ $placeholder }}"

    @if(isset($length))
        maxlength="{{ $length }}"
    @endif

    @if(isset($id))
       id="{{ $id }}"
    @else
       id="{{ $name }}"
    @endif

    @if(isset($required))
        required
    @endif

    @if(isset($autocomplete))
        autocomplete="off"
    @endif
>
    @if(old($name))
        {{ old($name) }}
    @elseif(isset($value))
        {{ $value }}
    @elseif(isset($resource))
        @if(isset($json))
            @if ($json !== 'NULO')
                {{ $json }}
            @endif
        @elseif(isset($resource->$name))
                {{ $resource->$name }}
        @elseif (isset($default))
            {{ $default }}
        @endif
    @endif
</textarea>

@if ($errors->has($name))
    <label class="control-label-textarea error-label" for="{{ isset($id) ? $id : $name }}">{{ $label }}</label>
    <span class="help-block {{ $name }}-error">
        <strong>{{ $errors->first($name) }}</strong>
    </span>
@else
    <label class="control-label-textarea" for="{{ isset($id) ? $id : $name }}">{{ $label }}</label>
    <span class="help-block {{ $name }}-error">
        <strong></strong>
    </span>
@endif