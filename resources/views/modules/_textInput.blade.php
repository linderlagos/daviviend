<input
    @if(isset($type))
        type="{{ $type }}"
    @else
        type="text"
    @endif
        class="form-control
    @if(isset($classes))
        {{ $classes }}
    @endif

        " name="{{ $name }}"

    autocomplete="off"

    @if(old($name))
        value="{{ old($name) }}"
    @elseif(isset($value))
        value="{{ $value }}"
    @elseif(isset($resource))
        @if(isset($json))
            @if ($json !== 'NULO')
                value="{{ $json }}"

                @if($json != '0' && !empty($json))
                    @include('modules._disabled')
                @endif
            @endif
        @elseif(isset($resource->$name))
            @if(isset($date))
                value="{{date('d/m/Y', strtotime($resource->$name))}}"
            @else
                value="{{ $resource->$name }}"
            @endif

            @if($resource->$name && $resource->$name != '0')
                @include('modules._disabled')
            @endif
        @elseif (isset($default))
            value="{{ $default }}"
        @endif
    @endif

    placeholder="{{ $placeholder }}"

    @if(isset($length))
        maxlength="{{ $length }}"
    @endif

    @if(isset($max))
        max="{{ $max }}"
    @endif

    @if(isset($min))
        min="{{ $min }}"
    @endif


    @if(isset($id))
       id="{{ $id }}"
    @else
       id="{{ $name }}"
    @endif

    @if(isset($required))
        required
    @endif

    @if(isset($autocomplete))
        autocomplete="off"
    @endif
>

@if ($errors->has($name))
    <label class="control-label error-label" for="{{ isset($id) ? $id : $name }}">{{ $label }}</label>
    <span class="help-block {{ $name }}-error">
        <strong>{{ $errors->first($name) }}</strong>
    </span>
@else
    <label class="control-label" for="{{ isset($id) ? $id : $name }}">{{ $label }}</label>
    <span class="help-block {{ $name }}-error">
        <strong></strong>
    </span>
@endif