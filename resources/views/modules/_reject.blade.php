
    <div class="rejection-container">
        <div class="rejection-toggle">
            <button id="rejection-toggle">
                <i id="rejection-icon" class="fas fa-plus-circle"></i>
                <span>Rechazar solicitud</span>
            </button>
        </div>

        <div class="rejection-content">
            <div class="row">
                <div class="col-12">
                    {{--<h3>Si desea rechazar la solicitud, seleccione el motivo y luego presione el botón "Rechazar solicitud"</h3>--}}
                    <form class="form-horizontal" role="form" method="POST" action="{{ $route }}" id="rejection-form">
                        {!! csrf_field() !!}

                        @if(isset($options))
                            <div class="row form-group">
                                <div class="col-12 text-left">
                                    {{--<label>Motivo*:</label>--}}
                                    @include('modules._singleSelect', [
                                        'name' => 'reject',
                                        'placeholder' => 'Seleccione el motivo...',
                                        'options' => $options
                                    ])
                                </div>
                            </div>
                        @endif
                        <div class="row text-center">
                            <div class="col-12">
                                <button type="submit" class="btn btn-davivienda-red btn-loading" id="rejection-submit" style="font-size: 1.2rem; padding: 8px 30px;">
                                    Rechazar solicitud
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
