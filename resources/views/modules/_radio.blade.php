@foreach($options as $key => $option)
    <div class="col-6 radio-image">
        <label>
            <input type="radio" name="{{ $name }}" value="{{ $key }}"
                @if(isset($id))
                   id="{{ $id }}"
                @else
                   id="{{ $name }}"
                @endif

                @if(old($name) === $key)
                    checked
                @endif
            >
            <img src="{{ asset('design/' . $option) }}">
        </label>
    </div>
@endforeach


@if ($errors->has($name))
    <span class="help-block">
        <strong>{{ $errors->first($name) }}</strong>
    </span>
@endif