<div class="row">
    @for($i = 1; $i <= $steps; $i++)
        @if($steps == 3)
            <div class="col-4">
        @elseif($steps == 4)
            <div class="col-3">
        @else
            <div class="col">
        @endif
            <div class="davivienda-progress-bar">
                @if($i == $step)
                    <div class="in-progress-step" style="width: 0;"></div>
                @elseif($step > $i)
                    <div class="filled-step"></div>
                @else
                    <div class="not-filled-step"></div>
                @endif
            </div>
        </div>
    @endfor
</div>

@if(isset($title))
    <div class="row">
        <div class="col-12 text-center">
            <h2 class="form-title">{{ $title }}</h2>
        </div>
    </div>
@endif