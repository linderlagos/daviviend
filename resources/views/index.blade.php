@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="display: inline-block; margin: 0 auto 0 auto; background-image: url('/design/index/Artboard3_4x.png'); height: auto;">
        <div class="container">
            <div class="banner">
                <div class="banner-image">
                    <img src="{{ asset('design/index/assistant.png') }} " class="img_principal">
                </div>
            </div>
        </div>
        <hr class="cintaGris">
    </div>
@stop

@section('back')
    @if(session()->has('bank_card_id'))
        <a href="{{ route('bank.card.index') }}" class="btn-back">
            Solicitud en trámite <i class="fas fa-forward" style="margin-left: 20px"></i>
        </a>
    @elseif(session()->has('bank_account_id'))
        <a href="{{ route('bank.account.index') }}" class="btn-back">
            Solicitud en trámite <i class="fas fa-forward" style="margin-left: 20px"></i>
        </a>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-12 form-group">
            <h2>Seleccione el producto:</h2> 
        </div>
    </div>

    <div class="row justify-content-center" style="margin-bottom: 2rem; margin-top: 2rem">


        <div class="col-6">
            <a href="{{ route('bank.card.index') }}" class="btn davivienda-file margenes">
                <img src="{{ asset('design/index/bank_card.png') }}">
                <div class="col-12 cintaRoja"><p>Tarjeta de Crédito</p></div>
            </a>
        </div>

        <div class="col-6">
            <a href="{{ route('bank.account.index') }}" class="btn davivienda-file margenes">
                <img src="{{ asset('design/index/bank_account.png') }}">
                <div class="col-12 cintaRoja"><p>Cuenta de Ahorro</p></div>
            </a>
        </div>

        @if (env('ENABLE_AUTO_FOR_EVERYONE'))
            <div class="col-6">
                <a href="{{ route('bank.auto.index') }}" class="btn davivienda-file margenes">
                    <img src="{{ asset('design/index/bank_auto.png') }}">
                    <div class="col-12 cintaRoja"><p>Préstamo de Auto</p></div>
                </a>
            </div>
        @endif

        {{-- <div class="col-6">
            <a href="{{ route('insurance.safe.family.index') }}" class="btn davivienda-file margenes">
                <img src="{{ asset('design/index/insurance_safe_family.png') }}">
                <div class="col-12 cintaRoja"><p>Familia Segura</p></div>
            </a>
        </div> --}}
    </div>
@endsection
