@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-error">
                    IE
                </div>
                <div class="banner-title">
                    <h1>Error con su navegador</h1>
                </div>
                <div class="banner-description">
                    <p>Solo puede utilizar la aplicación con su navegador de Google Chrome. Por favor vuelva a intentar ingresar con Google Chrome.</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">

        </div>

        <div class="col-12 text-center">
            Si no tiene instalado Google Chrome, por favor contactar al 911 para solicitarlo.
        </div>
    </div>
@endsection