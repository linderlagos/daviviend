@extends('layouts.app')

@section('banner')
    <div class="container-fluid" style="background-color: #EBEBEB">
        <div class="container">
            <div class="banner">
                <div class="banner-error">

                    <i class="fas fa-cog fa-spin"></i>
                </div>
                <div class="banner-title">
                    <h1>Estamos en mantenimiento</h1>
                </div>
                <div class="banner-description">
                    <p>Le pedimos disculpas, en este momento estamos en mantenimiento.</p>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row justify-content-center">
        {{--<div class="col-12">--}}
            {{----}}
        {{--</div>--}}

        <div class="col-12 text-center">
        </div>
    </div>
@endsection