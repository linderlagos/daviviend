@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
    
            <form method="POST" action="{{ route('autenticarCodigo', ['identidad' => $identidad]) }}">
                                @csrf
                <div class="card-header">{{ __('Autenticar Codigo de Google Autenticador') }} <a href="/home" title="Regresar" align="right"><img src="images/flecha-izquierda2.png" align="right" border="0"></a></div>
                
                <div class="card-body">
                   <!-- <form method="GET" action="{{ route('autenticar') }}">
                        @csrf -->

                        <div class="form-group row">
                            <label for="codigo" class="col-md-4 col-form-label text-md-right">{{ __('Codigo') }}</label>

                            <div class="col-md-6">
                                <input id="codigo" type="text" minlength="6" maxlength="6" pattern="[0-9]+" class="form-control" name="codigo" required>

                            </div>
                        </div>

                        @if($codMessage == "")
                        @elseif($codMessage == "0")
                            <div class="form-group row">
                           
                                <label for="message" class="col-md-4 col-form-label text-md-right">{{ __('') }}</label>
                                <label for="menssge" name="message" style="color: green;" class="col-md-4 col-form-label text-md-left">{{ $message }}</label>
                            
                            </div>
                        @else
                            <div class="form-group row">
                           
                                <label for="message" class="col-md-4 col-form-label text-md-right">{{ __('') }}</label>
                                <label for="menssge" name="message" style="color: red;" class="col-md-4 col-form-label text-md-left">{{ $message }}</label>
                            
                            </div>
                        @endif

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Verificar Codigo') }}
                                    </button>
                            </div>
                        </div>
                    <!--</form> -->
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
