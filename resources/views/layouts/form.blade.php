<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('layouts.partials._meta')
        @include('layouts.partials._noscript')
    </head>
    <body>
        @include('sweetalert::alert')

        <div id="app">
            @include('layouts.partials._header')

            @yield('content')
        </div>

        @include('layouts.partials._footer')

        @include('layouts.partials._scripts')
    </body>
</html>