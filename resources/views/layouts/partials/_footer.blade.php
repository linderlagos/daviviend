<footer>
    <div class="container">
        <div class="return-container">
            <div class="return">
                <a href="#logo-container" class="smooth-scroll">
                    <img src="{{ asset('design/footer/return.png') }}">
                </a>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-12 footer-logo">
                <img src="{{ asset('design/header/logo.png') }}">
            </div>
        </div>
    </div>
</footer>