{{-- Scripts --}}

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js">
</script>


<script src="{{ mix('js/app.js') }}" type="application/javascript"></script>
<script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
<script src="{{ asset('plugins/select2/es.js') }}"></script>
<script src="{{ asset('js/plugins/autoNumeric.js') }}" type="text/javascript"></script>
{{-- <script src="{{ asset('plugins/datatables/datatables.js') }}"></script> --}}

{{-- <script>
        $(document).ready( function () {
            $('#auto-flows-in-transit').DataTable({
                // "info":     false,
                // responsive: true,
                "lengthChange": false,
                "pageLength": 25,
                "order": [[ 4, "desc" ]],
                "dom": 'Bfrtip',
                "buttons": [],
                "pagingType": "full_numbers",
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "<<",
                        "sLast":     ">>",
                        "sNext":     ">",
                        "sPrevious": "<"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        } );
    </script> --}}

<script>
        $('.format-amount').autoNumeric('init');
    
        $(document).on("keypress", 'form', function (e) {
            let code = e.keyCode || e.which;
            if (code === 13) {
                e.preventDefault();
                return false;
            }
        });
    </script>
    
<script>
    $(document).ready(function(){
           // alert("3");
        
    });
        
        /* function cambio(name){
            alert("entro")
            var valor=document.getElementById(name).value;
            var posicion = valor.indexOf("OTRA COLONIA");
            if(name == "city"){
                if (posicion != -1){
                    alert("visible: city")
                    document.getElementById("other_cityI").style.display = "flex";
                }else{
                    alert("hidden: city")
                    document.getElementById("other_cityI").style.display = "none";
                }
            }else if(name == "employer_city"){
                if (posicion != -1){
                    alert("visible: employer_city")
                    document.getElementById("other_employer_cityI").style.display = "flex";
                }else{
                    alert("hidden: employer_city")
                    document.getElementById("other_employer_cityI").style.display = "none";
                }
            }
        } */
    </script>

<script>
    $(document).ready(function() {
        let alert = $('.alert-user-incomplete-process');

       // alert.toggle("slide");

        $('.alert-user-close-btn').click(function () {
            $(".alert-user-incomplete-process").toggle("slide");
        });
    });
</script>
<script> //definir lenguaje en español
    $(document).ready(function() {
        //alert("aqui");
        $('.davivienda-select').select2({
            language: "es"
        });
        /* $("#city").change(function() {
            var valor =$('#city').val();
            var posicion = valor.indexOf("OTRA COLONIA");
            if (posicion != -1){
                   // alert("visible: city")
                    document.getElementById("other_cityI").style.display = "inline";
                }else{
                   // alert("hidden: city")
                    document.getElementById("other_cityI").style.display = "none";
                    return null;
                }
        }); */
        /* $("#employer_city").change(function() {
            var valor =$('#employer_city').val();
            var posicion = valor.indexOf("OTRA COLONIA");
            if (posicion != -1){
                   // alert("visible: employer_city")
                    document.getElementById("other_employer_cityI").style.display = "inline";
                }else{
                    //alert("hidden: employer_city")
                    document.getElementById("other_employer_cityI").style.display = "none";
                }
        }); */
        $("#public_job").change(function() {
            var valor =$('#public_job').val();
            if (valor == "S"){
                   // alert("visible: employer_city")
                    document.getElementById("publib_jon_second_card").style.display = "flex";
                }else{
                    //alert("hidden: employer_city")
                    document.getElementById("publib_jon_second_card").style.display = "none";
                }
        });
    });


/* 
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    }); */
</script>


<script>
    $(document).ready( function () {
        $('.btn-loading').click(function(){
            //alert("debería cambiar")
            $(".btn-loading").html('<i class="fas fa-spinner fa-spin"></i> ENVIANDO...');
            $(".btn-loading").prop('disabled',true);
            let myVar = setTimeout(myFunction, 3000)

            function myFunction() {
                $("i").remove();  
                $(".btn-loading").html('Continuar');
                //$(this).find(".btn-loading").html('Continuar');

                $(".btn-loading").prop('disabled', false);
                /* $(this).find(".btn-loading").html('Continuar');
                $(this).find(".btn-loading").prop('enable',true); */
                /* $("#btn-loading").click(function () {  
                    $("i").remove();  
                    $("#div1").remove();
                });   */
               /*  $(this).find(".btn-loading).html('Continuar');
                $(this).find(".btn-loading").prop('enable',true); */
            }
            //alert("enviara")
           /*  let form = $('#form-query');

            form.attr('action', "{{ route('query.index') }}");

            form.submit("click",function(e) {
                $(this).find("#submit").html('<i class="fas fa-spinner fa-spin"></i>');
                $(this).find("#submit").prop('disabled',true);
            }); */
        });

       /*  $('#export').click(function() {
            let form = $('#form-query');

            form.attr('action', "{{ route('query.export') }}");
        }); */
    });
</script>
<script>
/*     $("#form").on("click",function(e) {
        //alert("entro");
        $(this).find(".btn-loading").html('<i class="fas fa-spinner fa-spin"></i> ENVIANDO...');
        $(this).find(".btn-loading").prop('disabled',true);
        let myVar = setTimeout(myFunction, 3000)

        function myFunction() {
           alert("Hello");
           $(this).find(".btn-loading).html('Continuar');
           $(this).find(".btn-loading").prop('enable',true);
        }
    }); */
</script>

<script>
   /*  $(document).ready(function() {
        @if($errors->has('reject'))
            $(".rejection-content").show();
        @else
            $(".rejection-content").hide();
        @endif

        $( "#rejection-toggle" ).click(function() {
            $( ".rejection-content" ).slideToggle( "slow" );

            let icon = $("#rejection-icon");

            if(icon.hasClass('open-icon')) {
                icon.removeClass('open-icon');
                icon.addClass('close-icon');
            } else {
                icon.removeClass('close-icon');
                icon.addClass('open-icon');
            }
        });
    }); */
</script>

<script>
    $(document).ready(function() {
        $('#rejection-submit').on("click", function(e){
            e.preventDefault();

            swal({
                title: '¿Está seguro?',
                text: "Una vez rechazada la solicitud debe ingresarla de nuevo",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#ED1C27',
                cancelButtonColor: '#959595',
                confirmButtonText: 'Si, rechazarla',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.value) $('#rejection-form').submit();
            });
        });
    });
</script>

{{--<script>--}}
    {{--$(document).ready(function preventLogout() {--}}
        {{--$('#logout').on("click", function(e){--}}
            {{--e.preventDefault();--}}

            {{--swal({--}}
                {{--title: '¿Está seguro?',--}}
                {{--text: "Al salir, no podrá continuar con la solicitud actual. Se recomienda que rechace la solicitud primero antes de salir.",--}}
                {{--type: 'warning',--}}
                {{--showCancelButton: true,--}}
                {{--confirmButtonColor: '#ED1C27',--}}
                {{--cancelButtonColor: '#959595',--}}
                {{--confirmButtonText: 'Si, estoy seguro',--}}
                {{--cancelButtonText: 'No saldré',--}}
            {{--}).then((result) => {--}}
                {{--if (result.value) $('#logout-form').submit();--}}
            {{--});--}}
        {{--});--}}
    {{--});--}}
{{--</script>--}}

{{--<script>--}}
    {{--$(document).ready(function preventReturnHome() {--}}
        {{--$('#home').on("click", function(e){--}}
            {{--e.preventDefault();--}}

            {{--swal({--}}
                {{--title: '¿Está seguro?',--}}
                {{--text: "Al regresar a la página principal, no podrá continuar con la solicitud actual. Se recomienda que rechace la solicitud primero antes de salir.",--}}
                {{--type: 'warning',--}}
                {{--showCancelButton: true,--}}
                {{--confirmButtonColor: '#ED1C27',--}}
                {{--cancelButtonColor: '#959595',--}}
                {{--confirmButtonText: 'Si, estoy seguro',--}}
                {{--cancelButtonText: 'No saldré',--}}
            {{--}).then((result) => {--}}
                {{--if (result.value) window.location.href = "/";--}}
            {{--});--}}
        {{--});--}}
    {{--});--}}
{{--</script>--}}

<script>
    $('.smooth-scroll').click(function(){

        var the_id = $(this).attr("href");

        $('html, body').animate({
            scrollTop:$(the_id).offset().top
        }, 'slow');

        return false;
    });
</script>

@yield('scripts')