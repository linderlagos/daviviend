@if(!Auth::guest())
        @if(session()->has('card-step') || session()->has('account-step'))
        @else
            <div class="sidebar abrir" id="sidebar">
                <h1>Menú</h1> 
                <img src="/design/favicon/ico2.ico" alt="" class="menu-bar">
                <ul>
                    
                    <li class="padre">
                        <hr class="separadorMenu">
                        <img src="/design/nav/Solicitarproductos32x32.ico" class="imgPadre ico">
                        <a id="principal">
                            Solicitar Productos
                        </a>
                    <img src="/design/favicon/motive_right.png" class="rotara"></li>
                        
                        <li class="hijos">
                            <hr class="separadorMenu "> {{-- hrHijos --}}
                            <img src="/design/favicon/motive_right.png">
                            <a href="{{ route('bank.card.index') }}"> 
                                Tarjetas de Crédito
                            </a> 
                        </li>
                        
                        <li class="hijos">
                            <hr class="separadorMenu ">     {{-- hrHijos --}}
                            <img src="/design/favicon/motive_right.png">
                            <a href="{{ route('bank.account.index') }}">
                                Cuentas de Ahorro
                            </a>
                        </li>
                        
                        <li class="hijos">
                            <hr class="separadorMenu "> {{-- hrHijos --}}
                            <img src="/design/favicon/motive_right.png">
                            <a href="{{ route('bank.auto.index') }}">
                                Préstamos
                            </a>
                        </li>
                        
                        <li class="hijos">
                            <hr class="separadorMenu "> {{-- hrHijos --}}
                            <img src="/design/favicon/motive_right.png">
                            <a href="{{ route('insurance.safe.family.index') }}">
                                Familia Segura
                            </a>
                        </li>
                    
                    <li>
                        <hr class="separadorMenu">
                        <img src="/design/nav/Misclientes32x32.ico" class="imgPadre ico">
                        <a>
                            Mis Clientes
                        </a>
                    </li>
                    
                    <li>
                        <hr class="separadorMenu">
                        <img src="/design/nav/Mispendientes32x32.ico" class="imgPadre ico">
                        <a>
                            Mis Pendientes
                        </a>
                    </li> 
                    
                    <li>
                        <hr class="separadorMenu">
                        <img src="/design/nav/Consultas32x32.ico" class="imgPadre ico">
                        <a  href="{{ route('query.index') }}">
                            Consultas
                        </a>
                    </li>
                    
                    <li href="{{ route('logout') }}" >
                        <hr class="separadorMenu">
                        <img src="/design/nav/Consultas32x32.ico" class="imgPadre  ico"><a 
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" title="Cerrar sesión" style="text-decoration: none;">
                            Salir
                        
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div> 

            {{-- <div class="logout-container">
                <div class="btn-group">
                    <button class="btn btn-menu dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menú 
                    </button>
                    <div class="dropdown-menu">
                        @role(env('ROL_GESTOR'))

                            @if(env('ENABLE_BANK_CARD'))
                                <a class="dropdown-item" href="{{ route('bank.card.index') }}">
                                    <i class="fas fa-credit-card"></i> Solicitud de tarjeta
                                </a>
                            @endif

                            <a class="dropdown-item" href="{{ route('bank.account.index') }}">
                                <i class="fas fa-hand-holding-usd"></i> Solicitud de cuenta
                            </a>
                            <a class="dropdown-item" href="{{ route('insurance.safe.family.index') }}">
                                <i class="fas fa-hand-holding-usd"></i> Familia segura
                            </a>
                        @endrole

                        @if (env('ENABLE_AUTO_FOR_EVERYONE'))
                            @role(env('ROL_GESTOR') . '|' . env('ROL_CONCESIONARIA'))

                                <a class="dropdown-item" href="{{ route('bank.auto.index') }}">
                                    <i class="fas fa-car"></i> Préstamo de auto
                                </a>
                            @endrole
                        @endif

                        @unlessrole(env('ROL_CONCESIONARIA') . '|' . env('ROL_SEGURIDAD'))
                            <a class="dropdown-item" href="{{ route('query.index') }}">
                                <i class="fas fa-database"></i> Consultas
                            </a>
                        @endunlessrole

                        @role(env('ROL_SEGURIDAD'))
                            <a class="dropdown-item" href="{{ route('security.index') }}">
                                <i class="fas fa-user-shield"></i> Usuarios
                            </a>
                        @endrole

                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" class="dropdown-item"
                           onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();" title="Cerrar sesión" style="text-decoration: none;">
                            <i class="fas fa-power-off"></i> Salir
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div> --}}

        @endif
    @endif
<div class="navbar-container" id="logo-container">
    
    <div class="navbar-overflow">
        <nav class="navbar navbar-expand-lg" style="z-index: 3;"> {{-- sobrepone a la barra --}}
                <div class="container">
                    <div class="navbar-motive">
                        <img src="{{ asset('design/header/motive.png') }}">
                    </div>
                @if(session()->has('card-step') || session()->has('account-step'))
                    <span class="navbar-brand" style="z-index: 3;">
                        <img src="{{ asset('design/header/logo.png') }}">
                    </span>
                @else
                    <a class="navbar-brand" href="{{ route('home') }}" style="z-index: 3;">
                        <img src="{{ asset('design/header/logo.png') }}">
                    </a>
                @endif
            </div>
        </nav>
    </div>
</div>

@section('scripts')
    @include('partials._scripts')
@stop