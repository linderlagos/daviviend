<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="application-name" content="Asistente Davivienda"/>
<title>Asistente Davivienda</title>
<meta name="description" content="Solicita de manera rápida y fácil los productos Davivienda ideales para tu cliente">

{{-- CSRF Token --}}
<meta name="csrf-token" content="{{ csrf_token() }}">

{{-- Favicons --}}
<link rel="shortcut icon" href="{{asset('design/favicon/favicon.ico')}}" />
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{asset('design/favicon/apple-touch-icon-57x57.png')}}" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('design/favicon/apple-touch-icon-114x114.png')}}" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('design/favicon/apple-touch-icon-72x72.png')}}" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('design/favicon/apple-touch-icon-144x144.png')}}" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{asset('design/favicon/apple-touch-icon-60x60.png')}}" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{asset('design/favicon/apple-touch-icon-120x120.png')}}" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{asset('design/favicon/apple-touch-icon-76x76.png')}}" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{asset('design/favicon/apple-touch-icon-152x152.png')}}" />
<link rel="icon" type="image/png" href="{{asset('design/favicon/favicon-196x196.png')}}" sizes="196x196" />
<link rel="icon" type="image/png" href="{{asset('design/favicon/favicon-96x96.png')}}" sizes="96x96" />
<link rel="icon" type="image/png" href="{{asset('design/favicon/favicon-32x32.png')}}" sizes="32x32" />
<link rel="icon" type="image/png" href="{{asset('design/favicon/favicon-16x16.png')}}" sizes="16x16" />
<link rel="icon" type="image/png" href="{{asset('design/favicon/favicon-128.png')}}" sizes="128x128" />
<meta name="msapplication-TileColor" content="#D7282E" />
<meta name="msapplication-TileImage" content="/design/favicon/mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="/design/favicon/mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="/design/favicon/mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="/design/favicon/mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="/design/favicon/mstile-310x310.png" />

@yield('meta')

{{-- Styles --}}
<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/plugins/sweetalert2.min.css') }}" rel="stylesheet">
{{-- <script src="{{ asset('js/plugins/sweetalert2.min.js') }}"></script> --}}

<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<link href="{{ asset('plugins/datatables/datatables.css') }}" rel="stylesheet" />