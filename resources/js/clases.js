$('.in-progress-step').delay(300).queue(function() {
    $(this).css('width', '100%')
});

// movido
$('.davivienda-select').select2({
    language: "es"
});
// movido
$('.date').datepicker({
    format: 'dd/mm/yyyy',
    language: 'es'
});

// movido

$('.format-amount').autoNumeric('init');

$(document).on("keypress", 'form', function(e) {
    let code = e.keyCode || e.which;
    if (code === 13) {
        e.preventDefault();
        return false;
    }
});


// movido

$('.hide-marital-status, .hide-public-job-active, .hide-public-job').hide();

if ($('#marital_status option:selected').text() == "CASADO") {
    $('.hide-marital-status').fadeIn();
}

$('#marital_status').change(function() {
    if ($('#marital_status option:selected').text() == "CASADO") {
        $('.hide-marital-status').fadeIn();
    } else {
        $('.hide-marital-status').fadeOut();
    }
});

if ($('#public_job_active option:selected').text() == "No") {
    $('.hide-public-job-active').fadeIn();
}

$('#public_job_active').change(function() {
    if ($('#public_job_active option:selected').text() == "No") {
        $('.hide-public-job-active').fadeIn();
    } else {
        $('.hide-public-job-active').fadeOut();
    }
});

if ($('#public_job option:selected').text() == "Si") {
    $('.hide-public-job').fadeIn();
}

$('#public_job').change(function() {
    if ($('#public_job option:selected').text() == "Si") {
        $('.hide-public-job').fadeIn();
    } else {
        $('.hide-public-job').fadeOut();
    }
});

// if ($('#dependants option:selected').text() == "Si") {
//     $('.hide-dependants').fadeIn();
// }
//
// $('#dependants').change(function () {
//     if ($('#dependants option:selected').text() == "Si") {
//         $('.hide-dependants').fadeIn();
//     } else {
//         $('.hide-dependants').fadeOut();
//     }
// });



// movido


let checkbok = $('#has_email');

if (checkbok.prop('checked')) {
    $('#email').attr("placeholder", "Digite: notiene@davivienda.com.hn");
}

checkbok.change(function() {
    if (this.checked) {
        $('#email').attr("placeholder", "Digite: notiene@davivienda.com.hn");
    } else {
        $('#email').attr("placeholder", "Ingrese su correo electrónico");
    }
});