import Errors from './Errors';

export class Form {
    /**
     * Crear una nueva instancia de formularios
     *
     * @param {object} data
     */
    constructor(data) {

        this.originalData = data;

        for (let field in data) {
            this[field] = data[field];
        }

        this.errors = new Errors();
    }

    /**
     * Enviar una solicitud POST a la URL dada.
     * .
     * @param {string} url
     */
    post(url) {
        console.log('2', url);
        return this.submit('post', url);
    }


    /**
     * FGraba todos los datos relevantes de el formulario
     */
    data() {
        let data = {};

        for (let property in this.originalData) {
            if (this[property]) {
                data[property] = this[property];
            }
        }
        console.log('3', data);
        return data;
    }


    /**
     * Reestablece los campos del formulario
     */
    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }
        console.log('4');
        this.errors.clear();
    }


    /**
     * Enviar una solicitud PUT a la URL dada.
     * .
     * @param {string} url
     */
    put(url) {
        return this.submit('put', url);
    }


    /**
     * Enviar una solicitud PATCH a la URL dada. 
     * .
     * @param {string} url
     */
    patch(url) {
        return this.submit('patch', url);
    }


    /**
     * Enviar una solicitud DELETE a la URL dada. 
     * .
     * @param {string} url
     */
    delete(url) {
        return this.submit('delete', url);
    }


    /**
     * Enviar el formulario
     *
     * @param {string} requestType
     * @param {string} url
     */
    submit(requestType, url) {
        console.log("llego al sibmit");
        return new Promise((resolve, reject) => {
            axios[requestType](url, this.data())
                .post(urlLogin, data)
                .then(response => {
                    var status = response.data.status;
                    var mensaje = response.data.searchCustomer.message;
                    if (status == 403) {
                        $('#error').text(mensaje);
                        $('#error').show();
                    } else if (status == 422) {
                        $('#error').text(mensaje);
                        $('#error').show();
                    } else {
                        $('#error').text(mensaje);
                        $('#error').show();
                    }
                })
                .catch(e => {

                    this.errors = e.response.data.errors || {};
                    if (this.errors.identity != undefined) {
                        $('#e_Identidad').text(this.errors.identity);
                    } else if (this.errors.identity == undefined) {
                        $('#e_Identidad').hide()
                    }

                    if (this.errors.card != undefined) {
                        $('#e_Tarjeta').text(this.errors.card);
                    } else if (this.errors.card == undefined) {
                        $('#e_Tarjeta').hide()
                    }
                    this.errors = {}
                });
        });
    }

    /**
     * Handle a successful form submission.
     *
     * @param {object} data
     */
    onSuccess(data) {
        // alert(data.message); // temporary
        swal({
            title: "¡Excelente!",
            text: data.message,
            icon: "success",
            button: false,
            allowOutsideClick: true,
            timer: 5000
        });

        this.reset();
    }

    /**
     * Handle a failed form submission.
     *
     * @param {object} errors
     * @param {number} status
     * @param {string} statusText
     */
    onFail(errors, status, statusText) {
        console.log('5');
        if (status === 422) {
            // swal({
            //     title: "¡Se ha encontrado algunos errores!",
            //     text: "Favor revisar el formulario y corrija los errores en los campos indicados",
            //     type: "warning",
            //     showConfirmButton: false,
            //     allowOutsideClick: true,
            //     timer: 4000
            // });

            this.errors.record(errors);
        } else {
            swal({
                title: "¡Se ha presentado un error!",
                text: "Favor refresque la página e intente de nuevo. Si el problema persiste contáctenos al 2280-1010",
                icon: "error",
                button: false,
                allowOutsideClick: true,
                timer: 6000
            });

            this.errors.record({
                general: {
                    0: status + ' ' + statusText
                }
            });
        }

        return this;
    }
}

export default Form;