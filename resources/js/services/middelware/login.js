
    export default {

        data(){
            return{
                loginDetails:{
                    peopleSoft:'',
                    password:''
                }
            }
        },
        methods:{
            loginPost(){
                let vm = this;
                axios.post('/login', vm.loginDetails)
                .then(function (response) {
                    if(response.status == 200) {
                    this.$router.push({ name: "home" });
                    } else {
                        console.log("Aqui daba indefinido?");
                    }
                })
                .catch(function (error) {
                    var errors = error.response
                    if(errors.statusText === 'Unprocessable Entity'){
                        if(errors.data){
                            if(errors.data.peopleSoft){
                            vm.peopleSoft = _.isArray(errors.data.peopleSoft) ? errors.data.peopleSoft[0]: errors.data.peopleSoft
                            console.log("Error en el usuario")
                            }
                            if(errors.data.password){
                            vm.passwordError = _.isArray(errors.data.password) ? errors.data.password[0] : errors.data.password
                            console.log("Error en la contraseña")
                            }
                        }
                    }

                });
            }
        }
    }