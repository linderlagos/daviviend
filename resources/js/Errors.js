export class Errors {
    /**
     * Crear una nueva instancia de errores.
     */
    constructor() {
        console.log("paso por errors")
        this.errors = {};
    }


    /**
     * Determine si existe un error para el campo dado
     *
     * @param {string} field
     */
    has(field) {
        return this.errors.hasOwnProperty(field);
    }


    /**
     * Determina si tenemos un error
     */
    any() {
        return Object.keys(this.errors).length > 0;
    }


    /**
     * Recuperar el mensaje de error para un campo
     *
     * @param {string} field
     */
    get(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
    }


    /**
     * Registrar los nuevos errores
     *
     * @param {object} errors
     */
    record(errors) {
        this.errors = errors;
    }


    /**
     * Borrar uno o todos los campos de error.
     *
     * @param {string|null} field
     */
    clear(field) {
        if (field) {
            delete this.errors[field];

            return;
        }

        this.errors = {};
    }
}

export default Errors;