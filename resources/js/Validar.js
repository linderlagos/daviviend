import Errors from './Errors';

export class Form {
    /**
     * Crear una nueva instancia de formularios
     *
     * @param {object} data
     */
    constructor(data) {

        console.log('recine esto', data);
        this.originalData = data;

        for (let field in data) {
            this[field] = data[field];
        }

        this.errors = new Errors();
    }


    /**
     * FGraba todos los datos relevantes de el formulario
     */
    data() {
        let data = {};

        for (let property in this.originalData) {
            if (this[property]) {
                data[property] = this[property];
            }
        }

        return data;
    }


    /**
     * Reestablece los campos del formulario
     */
    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }

        this.errors.clear();
    }


    /**
     * Enviar una solicitud POST a la URL dada.
     * .
     * @param {string} url
     */
    post(url) {
        return this.submit('post', url);
    }


    /**
     * Enviar una solicitud PUT a la URL dada.
     * .
     * @param {string} url
     */
    put(url) {
        return this.submit('put', url);
    }


    /**
     * Enviar una solicitud PATCH a la URL dada. 
     * .
     * @param {string} url
     */
    patch(url) {
        return this.submit('patch', url);
    }


    /**
     * Enviar una solicitud DELETE a la URL dada. 
     * .
     * @param {string} url
     */
    delete(url) {
        return this.submit('delete', url);
    }


    /**
     * Enviar el formulario
     *
     * @param {string} requestType
     * @param {string} url
     */
    submit(requestType, url) {
        console.log(url)
    }

    /**
     * Handle a successful form submission.
     *
     * @param {object} data
     */
    onSuccess(data) {
        // alert(data.message); // temporary
        swal({
            title: "¡Excelente!",
            text: data.message,
            icon: "success",
            button: false,
            allowOutsideClick: true,
            timer: 5000
        });

        this.reset();
    }

    /**
     * Handle a failed form submission.
     *
     * @param {object} errors
     * @param {number} status
     * @param {string} statusText
     */
    onFail(errors, status, statusText) {

        if (status === 422) {
            // swal({
            //     title: "¡Se ha encontrado algunos errores!",
            //     text: "Favor revisar el formulario y corrija los errores en los campos indicados",
            //     type: "warning",
            //     showConfirmButton: false,
            //     allowOutsideClick: true,
            //     timer: 4000
            // });

            this.errors.record(errors);
        } else {
            swal({
                title: "¡Se ha presentado un error!",
                text: "Favor refresque la página e intente de nuevo. Si el problema persiste contáctenos al 2280-1010",
                icon: "error",
                button: false,
                allowOutsideClick: true,
                timer: 6000
            });

            this.errors.record({
                general: {
                    0: status + ' ' + statusText
                }
            });
        }

        return this;
    }
}

export default Form;