import Vue from 'vue'


Vue.prototype.$setErrorsFromResponse = function(errorResponse) {

  if(!this.hasOwnProperty('$validator')) {
   
      return;
  }
  
  this.$validator.errors.clear();

  let errorFields = Object.keys(errorResponse);

  errorFields.map(field => {
      let errorString = errorResponse[field].join(', ');
      
      this.$validator.errors.add({
         field: field,
         msg: errorString
      });     
  });
};