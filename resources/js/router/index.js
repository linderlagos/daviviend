import Vue from "vue";
import Router from "vue-router";

// Auth
/* import Login from "../views/auth/Login.vue";
import Dash from "../components/Dash.vue";
import indexHome from "../views/index.vue"; */
import indexCard from "../views/bank/card/index.vue";
/* import indexAccount from "../views/bank/account/index.vue";
import indexAuto from "../views/bank/auto/index.vue";
import indexFamily from "../views/safeFamily/index.vue";
import Query from "../views/query.vue";
import product from "../views/product/show.vue";
import crossSell from "../views/crossSell/index.vue"; */

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [

        {
            path: "card",
            name: "card",
            component: indexCard,
        }

        /* {
            path: '',
            redirect: "/login"
        },
        {
            path: '*',
            redirect: "/login"
        },
        {
            path: "/login",
            name: 'login',
            component: Login,
        },
        {
            path: "/",
            component: Dash,
            children: [{
                    path: "home",
                    name: "home",
                    component: indexHome,
                    meta: { requiresAuth: true }
                    // beforeEnter: (to, from, next) => {
                    //     next(vm => {

                    //       })
                    //   }

                },
                {
                    path: "card",
                    name: "card",
                    component: indexCard,
                    meta: { requiresAuth: true }
                },
                {
                    path: "account",
                    name: "account",
                    component: indexAccount,
                    meta: { requiresAuth: true }
                },
                {
                    path: "auto",
                    name: "auto",
                    component: indexAuto,
                    meta: { requiresAuth: true }
                },
                {
                    path: "insurance_family",
                    name: 'family',
                    component: indexFamily,
                    meta: { requiresAuth: true }
                },
                {
                    path: "query",
                    name: 'query',
                    component: Query,
                    meta: { requiresAuth: true }
                },
                {
                    path: "product",
                    name: 'product',
                    component: product,
                    meta: { requiresAuth: true }
                },
                {
                    path: "crossSell",
                    name: 'crossSell',
                    component: crossSell,
                    meta: { requiresAuth: true }
                }
            ]
        } */

    ]
});

/* router.beforeEach((to, from, next) => {

    if (to.meta.requiresAuth) {
        const authUser = JSON.parse(sessionStorage.getItem('datos'))

        if (authUser != undefined && authUser != null && authUser.length != 0) {
            next()
        } else {
            next({ name: 'login' })
                // location.reload();
        }
    }
    next()
}); */

export default router;